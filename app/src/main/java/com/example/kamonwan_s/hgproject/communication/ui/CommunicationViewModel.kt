package com.example.kamonwan_s.hgproject.communication.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.communication.api.*
import com.example.kamonwan_s.hgproject.communication.repository.CommunicationRepository

class CommunicationViewModel(private val app:Application,private val communicationRepository: CommunicationRepository)
    :AndroidViewModel(app){

    var accountContactInfo = MediatorLiveData<AccountContactInfo>()
    var chatRoomInfo = MediatorLiveData<ChatRoomInfo>()
    var token = MediatorLiveData<Token>()
    var chatRoomStatusInfo = MediatorLiveData<ChatRoomStatusInfo>()
    var chatContentListInfo = MediatorLiveData<ChatContentListInfo>()
    var chatRoomMemberOperationInfo = MediatorLiveData<ChatRoomMemberOperationInfo>()
    var chatRoomPToPInfo = MediatorLiveData<ChatRoomPToPInfo>()
    var chatContentReadInfo = MediatorLiveData<ChatContentReadedInfo>()

    val accoutContactList : LiveData<SingleEvent<SyncAccountContactListResponse>> = Transformations.switchMap(accountContactInfo){
        communicationRepository.syncAccountContactList(it)
    }

    val chatRoomList : LiveData<SingleEvent<SyncChatRoomListResponse>> = Transformations.switchMap(chatRoomInfo){
        communicationRepository.syncChatRoomList(it)
    }

    val chatRoomPending : LiveData<SingleEvent<GetChatRoomPedingListResponse>> = Transformations.switchMap(token){
        communicationRepository.getChatRoomPendingList(it)
    }

    val chatRoomStatus : LiveData<SingleEvent<UpdateChatRoomStatusResponse>> = Transformations.switchMap(chatRoomStatusInfo){
        communicationRepository.updateChatRoomStatus(it)
    }

    val chatContentList : LiveData<SingleEvent<GetChatContentListResponse>> = Transformations.switchMap(chatContentListInfo){
        communicationRepository.getChatContentList(it)
    }

    val chatRoomMemberOperation : LiveData<SingleEvent<GetChatRoomMemberResponse>> = Transformations.switchMap(chatRoomMemberOperationInfo){
        communicationRepository.getChatRoomMember(it)
    }

    val chatRoomPToP : LiveData<SingleEvent<GetChatRoomPToPResponse>>  = Transformations.switchMap(chatRoomPToPInfo){
        communicationRepository.getChatRoomP2P(it)
    }

    val chatContentRead : LiveData<SingleEvent<GetChatContentReadedResponse>> = Transformations.switchMap(chatContentReadInfo){
        communicationRepository.getChatContentRead(it)
    }

    fun syncAccountContact(accountContactInfo: AccountContactInfo){
        this.accountContactInfo.value = accountContactInfo
    }

    fun syncChatRoom(chatRoomInfo: ChatRoomInfo){
        this.chatRoomInfo.value = chatRoomInfo
    }

    fun getChatRoomPending(token: Token){
        this.token.value = token
    }

    fun getChatContentRead(chatContentReadedInfo: ChatContentReadedInfo){
        this.chatContentReadInfo.value = chatContentReadedInfo
    }

    fun updateChatRoomStatus(chatRoomStatusInfo: ChatRoomStatusInfo){
        this.chatRoomStatusInfo.value = chatRoomStatusInfo
    }

    fun getChatContentList(chatContentListInfo: ChatContentListInfo){
        this.chatContentListInfo.value = chatContentListInfo
    }

    fun getChatRoomMember(chatRoomMemberOperationInfo: ChatRoomMemberOperationInfo){
        this.chatRoomMemberOperationInfo.value = chatRoomMemberOperationInfo
    }

    fun getChatRoomPToP(chatRoomPToPInfo: ChatRoomPToPInfo){
        this.chatRoomPToPInfo.value = chatRoomPToPInfo
    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun retrieveOrganizationId(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.organization_id), "")!!
    }

    fun getToken(): String {
        return retrieveToken(app)
    }

    fun getOrganizationId(): String {
        return retrieveOrganizationId(app)
    }
}