package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatContentListInfo
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.api.CreateChatContentPToPResponse
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_chat_message.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChatMessageFragment : Fragment(),ContentChatAdapter.BtnClickListener {


    private var communicationViewModel: CommunicationViewModel? = null
    private var token: String? = null
    private var nameMember: String? = null
    private var accountID: String? = null
    private var chatRoomId: String? = null
    private var content: String? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: ContentChatAdapter? = null
    private var uri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null){
            loadChatContentList()
        }

        loadChatContentList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_chat_message, container, false)


        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]

        token = communicationViewModel?.getToken()


        accountID = arguments?.getString("accountId")
        nameMember = arguments?.getString("nameMember")
        chatRoomId = arguments?.getString("chatRoomId")
        Log.d("chatRoomIdFriednds", chatRoomId.toString())

        getActivity()!!.setTitle(nameMember)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)
        initInstance(view)
        loadChatContentList()
        return view
    }

    private fun initInstance(view: View?) {
        adapter = ContentChatAdapter(context!!,this)
        layoutManager = LinearLayoutManager(view?.context)
        view?.recyclerViewChatLog?.layoutManager= layoutManager
        view?.recyclerViewChatLog?.itemAnimator = DefaultItemAnimator()
        view?.recyclerViewChatLog?.adapter = adapter

        view?.imageSendChat?.setOnClickListener {
            createChatContentOnlyText()

        }


        view?.imageChooseMore?.setOnClickListener {
            FragmentMenuChat(accountID!!,nameMember!!,chatRoomId!!)
        }

        loadChatContentList()
    }



    private fun loadChatContentList() {
        communicationViewModel?.getChatContentList(ChatContentListInfo(token!!, chatRoomId.toString(), "0", "99", "0"))
        communicationViewModel?.chatContentList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatContentListWrapper ->
                chatContentListWrapper.chatContentListWrapper.chatContentListData.let { chatContentListData ->
                    if (chatContentListData != null) {
                        chatContentListData.chatContents.let { chatContents ->
                            adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                        }
                    }

                    if (adapter?.items == null) {
                        adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                    }

                    adapter?.notifyDataSetChanged()
                }
            }
        })
    }


    private fun createChatContentOnlyText() {

        content = view?.editContentChat?.text.toString()
        Log.d("contentText", content)
        CommunicationApi.service.createChatContentP2POnlyText(
                RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                RequestBody.create(MediaType.parse("text/plain"), "1"),
                RequestBody.create(MediaType.parse("text/plain"), content),
                RequestBody.create(MediaType.parse("text/plain"), token!!))
                .enqueue(object : Callback<CreateChatContentPToPResponse> {
                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                        Log.d("onFailure", t.message)
                    }

                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                        if (response.isSuccessful) {
                            loadChatContentList()
                            Toast.makeText(context, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                            view?.editContentChat?.text?.clear()

                        } else
                            Log.d("onResponse", response.errorBody()!!.string())
                    }

                })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.infoChat -> {
                FragmentChatRoomMember(chatRoomId!!)
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBtnClick(content: String) {
        startActivity(Intent(context, VideoActivity::class.java).apply {
            putExtra("contentVideo", Bundle().apply {
                putString("contentVideo", content)
            })
        })
    }


    fun FragmentChatRoomMember(chatRoomId:String) {
        val fragment = ChatRoomMemberFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChat, fragment)
        val bundle = Bundle()
        bundle.putString("chatRoomId",chatRoomId)
        fragment.arguments = bundle
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentMenuChat(accountId:String,name:String,chatRoomId:String) {
        val fragment = MenuChatFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChat, fragment)
        val bundle = Bundle()
        bundle.putString("accountId", accountId)
        bundle.putString("name", name)
        bundle.putString("chatRoomId",chatRoomId)
        fragment.arguments = bundle
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_info, menu)
    }

    companion object {
     val tag :String = ChatMessageFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ChatMessageFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
