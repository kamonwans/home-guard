package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class NewsWrapper(@SerializedName("Data") val newsData :NewsData,
                       @SerializedName("Response") val responseList: ResponseList)