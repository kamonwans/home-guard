package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "news")
data class NewsListEntity(@ColumnInfo(name = "news_content") val content : String,
                          @ColumnInfo(name = "news_header") val header:String,
                          @ColumnInfo(name = "news_modifiedDate") val modifiedDate : String,
                          @PrimaryKey @ColumnInfo(name = "news_id") val newsID :Int,
                          @ColumnInfo(name = "news_type_id") val newsTypeID :Int)