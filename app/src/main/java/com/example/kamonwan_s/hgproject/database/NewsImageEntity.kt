package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "news_image")
data class NewsImageEntity(@PrimaryKey(autoGenerate = true) val id: Int,
                           @ColumnInfo(name = "url") val image: String)