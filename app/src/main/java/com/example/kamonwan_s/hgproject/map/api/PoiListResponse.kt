package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiListResponse(@SerializedName("GetPOIListResult") val poiListWrapper : PoiListWrapper)