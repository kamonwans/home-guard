package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomStatusInfo
import com.example.kamonwan_s.hgproject.communication.api.Token
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_accept.view.*

class AcceptFragment : Fragment(), AcceptAdapter.ClickUpdate {


    private var communicationViewModel: CommunicationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var token: String? = null
    private var adapter: AcceptAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("ตอบรับคำขอ")
        loadAcceptList()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_accept, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_dot)
        setHasOptionsMenu(true)

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {

        token = communicationViewModel?.getToken()

        adapter = AcceptAdapter(context!!, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewAcceptChat?.layoutManager = layoutManager
        view.recyclerViewAcceptChat?.itemAnimator = DefaultItemAnimator()
        view.recyclerViewAcceptChat?.adapter = adapter

        loadAcceptList()

    }

    override fun onUpdateAccept(position: Int, chatRoomId: String, isAccept: Int) {

       communicationViewModel?.updateChatRoomStatus(ChatRoomStatusInfo(token!!,chatRoomId,isAccept))
        communicationViewModel?.chatRoomStatus?.observe(this, Observer { event->
            event?.getContentIfNotHandled()?.let { updateChatRoomStatus ->
                updateChatRoomStatus.chatRoomStatusWrapper.let {

                }
            }
        })
    }

    override fun onUpdateDecline(position: Int, chatRoomId: String, isAccept: Int) {
        communicationViewModel?.updateChatRoomStatus(ChatRoomStatusInfo(token!!, chatRoomId, isAccept))
        communicationViewModel?.chatRoomStatus?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { updateChatRoomStatus ->
                updateChatRoomStatus.chatRoomStatusWrapper.let {

                }
            }
        })
    }

    private fun loadAcceptList() {
        communicationViewModel?.getChatRoomPending(Token(token!!))
        communicationViewModel?.chatRoomPending?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatRoomWrapper ->
                chatRoomWrapper.chatRoomPedingListWrapper.chatRoomPendingData.let { chatRoomList ->

                    if (chatRoomList != null) {
                        chatRoomList.chatRoomPendings.let { chatRoom ->
                            adapter?.items?.chatRoomPendings?.addAll(chatRoom)
                            adapter?.notifyDataSetChanged()
                        }

                        if (adapter?.items == null) {
                            adapter?.items = chatRoomWrapper.chatRoomPedingListWrapper.chatRoomPendingData
                            adapter?.notifyDataSetChanged()
                        }

                    }

                }
            }
        })

    }


    companion object {
        val tag = AcceptFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                AcceptFragment().apply {

                }
    }
}
