package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
@Entity(tableName = "content_read")
data class ChatContentReadedEntity(@PrimaryKey @ColumnInfo(name = "chat_content_Id")val chatContentID:String,
                                   @ColumnInfo(name = "chat_room_Id")val chatRoomID:String,
                                   @ColumnInfo(name = "content_readed")val contentReaded:String,
                                   @ColumnInfo(name = "modified_date")val modifiedDate:String)