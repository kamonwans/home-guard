package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class UpdateChatRoomStatusResponse(@SerializedName("UpdateChatRoomStatusResult")val chatRoomStatusWrapper : ChatRoomStatusWrapper)