package com.example.kamonwan_s.hgproject.login.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity(tableName = "district")
data class SyncDistrictWrapper(@PrimaryKey @NonNull @ColumnInfo(name = "district_id")@SerializedName("DistrictID")val districtID:String,
                               @NonNull @ColumnInfo(name = "district_name")@SerializedName("DistrictName")val districtName:String,
                               @NonNull @ColumnInfo(name = "modified_date") @SerializedName("ModifiedDate")val modifiedDate:String,
                               @NonNull @ColumnInfo(name = "province_id") @SerializedName("ProvinceID")val provinceID:String)