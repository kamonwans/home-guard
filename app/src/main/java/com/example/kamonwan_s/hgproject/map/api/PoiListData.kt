package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiListData(@SerializedName("POIs") val pois:MutableList<POIs>)