package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "report_status")
data class ReportStatusesEntity(@ColumnInfo(name = "report_status_color") val ColorCode : String,
                                @ColumnInfo(name = "report_status_modified") val ModifiedDate:String,
                                @PrimaryKey @ColumnInfo(name = "report_status_id") val ReportStatusID:String,
                                @ColumnInfo(name = "report_status_name") val ReportStatusName:String)