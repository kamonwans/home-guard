package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactListData
import kotlinx.android.synthetic.main.item_list_friends.view.*

class FriendsAdapter(val context: Context, val listener: BtnClickListener) : RecyclerView.Adapter<FriendsAdapter.ViewHolder>() {

    var items: AccountContactListData? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): FriendsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_friends, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items?.accountContacts?.size ?: 0
    }

    override fun onBindViewHolder(holder: FriendsAdapter.ViewHolder, position: Int) {
        var firstName = items?.accountContacts?.get(position)?.firstName
        var lastName = items?.accountContacts?.get(position)?.lastName
        var date = items?.accountContacts?.get(position)?.modifiedDate
        var pic = items?.accountContacts?.get(position)?.profilePic
        var accountId = items?.accountContacts?.get(position)?.accountID
        var chatRoomId = items?.accountContacts?.get(position)?.chatRoomID.toString()


        var dateConvert = convertDate(date!!, "hh:mm")

        holder.itemView.tvFistName.text = firstName
        holder.itemView.tvLastName.text = lastName
        holder.itemView.tvTime.text = dateConvert
        holder.itemView.tvAccountID.text = "[" + accountId + "]"
        holder.itemView.imageUserProfile.setProfile(pic!!)

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listener.onBtnClick(accountId!!)
            }
        })
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    interface BtnClickListener {
        fun onBtnClick(accountId: String)
    }
}