package com.example.kamonwan_s.hgproject.news.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.news.api.GetNewsInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.image.ImageActivity
import kotlinx.android.synthetic.main.fragment_news_detail.*
import kotlinx.android.synthetic.main.fragment_news_detail.view.*


class NewsDetailFragment : Fragment(), PhotoNewsAdapter.ViewPagerClickListener {
    override fun onClickViewPager(position: Int, images: ArrayList<String>) {
        FragmentImageNews(position, images)
    }

    lateinit var titleString: String
    lateinit var dateString: String

    var typeId: Int = 0
    var positions: Int = 0
    var newsId: Int = 0
    private var newsViewModel: NewsViewModel? = null
    var adapter: PagerAdapter? = null
    var token: String? = null
    var timeDate: String? = null
    var newsTypeName:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_news_detail, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)

        newsViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideNewsViewModelFactory(activity!!.application))[NewsViewModel::class.java]

        positions = arguments?.getInt("position") ?: 0
        titleString = arguments?.getString("header") ?: ""
        dateString = arguments?.getString("date") ?: ""
        typeId = arguments?.getInt("typeId") ?: 0
        newsId = arguments?.getInt("newsId") ?: 0
        newsTypeName = arguments?.getString("newsTypeName") ?: ""

        timeDate = convertDate(dateString, "dd/MM/yyyy hh:mm:ss")

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitleNews.text = titleString
        tvDateModified.text = timeDate
        tvTypeNews.text = newsTypeName
        token = newsViewModel?.getToken()

        newsViewModel?.loadNews(GetNewsInfo(token!!, newsId.toString()))
        newsViewModel?.news?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { newsWapper ->
                newsWapper.newsWrapper.newsData.let { news ->
                    news.news.let {
                        adapter = it.let { it1 -> PhotoNewsAdapter(context!!, this, it1.images) }
                        view.pagerPhoto?.adapter = adapter
                        indicator.setViewPager(pagerPhoto)

                        if (it == null) {
                            Toast.makeText(context, "เกิดข้อผิดพลาดบางอย่าง", Toast.LENGTH_LONG).show()
                        } else {
                            tvContentNews.text = it.content
                        }
                    }
                }

            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            FragmentNews()
            //activity?.onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    fun FragmentImageNews(position: Int, images: ArrayList<String>) {
        val intent = Intent(context, ImageActivity::class.java)
        val bundle = Bundle()
        intent.putExtra("name", bundle)
        bundle.putStringArrayList("image", images)
        bundle.putInt("position", position)
        startActivity(intent)
    }

    fun FragmentNews() {
        val fragmentMap = NewsFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragmentMap)
        transaction?.addToBackStack(null)
        var bundle = Bundle()
        fragmentMap.arguments = bundle
        transaction?.commit()
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                NewsDetailFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
