package com.example.kamonwan_s.hgproject.map.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.PoiEntity
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(val onClickPoiItem: (PoiEntity) -> Unit) : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private var poiList = ArrayList<PoiEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SearchAdapter.SearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
        return SearchAdapter.SearchViewHolder(view)
    }

    override fun getItemCount(): Int {
        return poiList.size
    }

    override fun onBindViewHolder(holder: SearchAdapter.SearchViewHolder, position: Int) {
        holder.bind(poiList.get(position).poiName)
        holder.itemView.setOnClickListener { onClickPoiItem(poiList.get(position)) }
    }

    fun updatePoiList(newList: ArrayList<PoiEntity>) {
        poiList.clear()
        poiList.addAll(newList)

        notifyDataSetChanged()
    }

    class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: String) = with(itemView) {
            tvSearch.text = item
        }
    }
}