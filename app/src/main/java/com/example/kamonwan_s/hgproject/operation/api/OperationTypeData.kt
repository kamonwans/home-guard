package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationTypeData(@SerializedName("OperationType")val operationType:MutableList<OperationType>)