package com.example.kamonwan_s.hgproject.map.ui.poi

import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.map.MapViewModel
import com.example.kamonwan_s.hgproject.poi.api.POICategory
import com.example.kamonwan_s.hgproject.poi.api.PoiType
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_filter_map_dialog.view.*
import kotlinx.android.synthetic.main.fragment_poi_dialog.*
import kotlinx.android.synthetic.main.fragment_poi_dialog.view.*

class PoiDialogFragment : BottomSheetDialogFragment(),PoiAdapter.OnItemCheckedChangeListener {

    private var mapViewModel: MapViewModel? = null
    private lateinit var adapter: PoiAdapter
    private lateinit var fragmentView: View
    private var layoutManager: LinearLayoutManager? = null
    private var poiType : ArrayList<PoiType> = ArrayList()
    private var poiCategory : ArrayList<POICategory> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            poiType = it.getParcelableArrayList<PoiType>("poiType")
            poiCategory = it.getParcelableArrayList<POICategory>("poiCategory")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_poi_dialog, container, false)
        fragmentView = view
        mapViewModel = ViewModelProviders.of(parentFragment!!
                , InjectorUtil.provideMapViewModelFactory(activity!!.application)).get(MapViewModel::class.java)

        adapter = PoiAdapter(poiCategory,poiType,this)

        view.recyclerViewPoiFilter.adapter = adapter
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewPoiFilter.layoutManager = layoutManager
        view.recyclerViewPoiFilter.itemAnimator = DefaultItemAnimator()

        view.ivClose.setOnClickListener {
            dismiss()
        }
        return view
    }

    override fun onCategoryCheck(id: Int) {
        fragmentView.recyclerViewPoiFilter.swapAdapter(PoiAdapter(poiCategory, poiType,this), false)
    }

    override fun onTypeCheck(id: Int) {
        fragmentView.recyclerViewPoiFilter?.swapAdapter(PoiAdapter(poiCategory, poiType,this), false)
    }

    private fun getDialogListener(): OnFragmentInteractionListener? {
        val fragment = parentFragment
        return if (fragment != null)
            fragment as? OnFragmentInteractionListener
        else
            activity as? OnFragmentInteractionListener
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        getDialogListener()?.onDialogClose()
    }
    interface OnFragmentInteractionListener {
        fun checkboxCheck(poiId: Int, serverPath: String, layerPath: String, icChecked:Boolean)
        fun checkboxUnCheck(layerID: Int)
        fun onDialogShowLayerFilter()
        fun onDialogHideLayerFilter()
        fun onDialogClose()
    }


    companion object {
        val tag: String = PoiDialogFragment::class.java.simpleName
        @JvmStatic
        fun newInstance(poiTypeList: ArrayList<PoiType>, poiCategoryList: ArrayList<POICategory>): PoiDialogFragment{
            val fragment = PoiDialogFragment()
            val arg = Bundle()
            arg.putParcelableArrayList("poiType", poiTypeList)
            arg.putParcelableArrayList("poiCategory", poiCategoryList)
            fragment.arguments = arg
            return fragment
        }

    }
}
