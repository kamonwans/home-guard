package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomPToPData(@SerializedName("Data")val chatRoomPToPAccountContact:ChatRoomPToPAccountContact,
                            @SerializedName("Response")val responseList: ResponseList)