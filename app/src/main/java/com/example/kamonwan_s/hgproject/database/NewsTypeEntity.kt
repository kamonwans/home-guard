package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "news_type")
data class NewsTypeEntity(@ColumnInfo(name = "news_type_color") val colorCode: String,
                          @ColumnInfo(name = "news_type_modified") val modifiedDate: String,
                          @PrimaryKey @ColumnInfo(name = "news_type_id") val newsTypeID: String,
                          @ColumnInfo(name = "news_type_name") val newsTypeName: String)