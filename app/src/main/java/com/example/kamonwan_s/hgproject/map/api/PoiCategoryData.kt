package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiCategoryData(@SerializedName("POICategory") val poiCategory : MutableList<POICategory>)