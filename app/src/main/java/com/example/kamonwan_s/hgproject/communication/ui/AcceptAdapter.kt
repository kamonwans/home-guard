package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomPendingData
import kotlinx.android.synthetic.main.item_accept_room.view.*

class AcceptAdapter(val context: Context,
                    var updateStatus: AcceptAdapter.ClickUpdate) : RecyclerView.Adapter<AcceptAdapter.ViewHolder>(){

    var items: ChatRoomPendingData? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): AcceptAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_accept_room,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return items?.chatRoomPendings?.size ?: 0
    }

    override fun onBindViewHolder(holder: AcceptAdapter.ViewHolder, position: Int) {

        var chatRoomName = items?.chatRoomPendings?.get(position)?.chatRoomName
        var chatRoomId = items?.chatRoomPendings?.get(position)?.chatRoomId
        holder.itemView.tvChatRoomName.text = chatRoomName

        holder.itemView.btnAccept.setOnClickListener {
            updateStatus.onUpdateAccept(position,chatRoomId!!,1)
        }
        holder.itemView.btnDecline.setOnClickListener {
            updateStatus.onUpdateDecline(position,chatRoomId!!,0)
        }
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    interface ClickUpdate{
        fun onUpdateAccept(position: Int,chatRoomId :String,isAccept:Int)
        fun onUpdateDecline(position: Int,chatRoomId: String,isAccept: Int)
    }

}