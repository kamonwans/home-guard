package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.*

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_choose_frieds_chat_room.view.*


class ChooseFriendsChatRoomFragment : Fragment(),ChooseFriendsAdapter.BtnClickListener {


    private var communicationViewModel: CommunicationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: ChooseFriendsAdapter? = null
    private var token: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("เลือกเพื่อน")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_choose_frieds_chat_room, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = communicationViewModel?.getToken()

        adapter = ChooseFriendsAdapter(context!!,this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewChooseFriends?.layoutManager = layoutManager
        view.recyclerViewChooseFriends?.itemAnimator = DefaultItemAnimator()
        view.recyclerViewChooseFriends?.adapter = adapter

        communicationViewModel?.syncAccountContact(AccountContactInfo(token!!, 1, "0"))
        communicationViewModel?.accoutContactList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { accountContactWrapper ->
                accountContactWrapper.accountContactListWrapper.accountContactListData.let { acountContactList ->
                    acountContactList.accountContacts.let { accountContact ->
                        adapter?.items?.accountContacts?.addAll(accountContact)
                    }

                    if (adapter?.items == null) {
                        adapter?.items = accountContactWrapper.accountContactListWrapper.accountContactListData
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onBtnClick(accountId: String, name: String,checked:Int) {
        FragmentCreateRoom(accountId,name,checked)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_next, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.home) {
            activity?.onBackPressed()
            return true
        } else if (item?.itemId == R.id.nextChat) {

           // FragmentCreateRoom()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    fun FragmentCreateRoom(accountId: String, name: String,checked:Int) {
        val fragment = CreateChatRoomFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                ChooseFriendsChatRoomFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
