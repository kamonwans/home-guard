package com.example.kamonwan_s.hgproject.register.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.example.kamonwan_s.hgproject.*
import com.example.kamonwan_s.hgproject.register.api.*
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean


class RegisterFragment : Fragment() {

    private var registerViewModel: RegisterViewModel? = null
    private var isOpenAddress = AtomicBoolean(true)
    private var fragmentView: View? = null
    private var newToken: String? = null
    val PREFS_PROVINCE = "province"
    val PREFS_DISTRICT = "district"
    val PREFS_SUB_DISTRICT = "sub_district"
    val PREFS_ORGANIZATION = "organization"
    var prefsProvince: SharedPreferences? = null
    var prefsDistrict: SharedPreferences? = null
    var prefsSubDistrict: SharedPreferences? = null
    var prefsOrganization: SharedPreferences? = null
    var editorProvince: SharedPreferences.Editor? = null
    var editorDistrict: SharedPreferences.Editor? = null
    var editorSubDistrict: SharedPreferences.Editor? = null
    var editorOrganization: SharedPreferences.Editor? = null

    val USER = "user"
    var prefsUser: SharedPreferences? = null
    var editorUser: SharedPreferences.Editor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        initInstance(view, savedInstanceState)
        return view
    }



    private fun initInstance(view: View, savedInstanceState: Bundle?) {
        fragmentView = view
        registerViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideRegisterViewModelFactory(activity!!.application))[RegisterViewModel::class.java]
        if (savedInstanceState == null) {
            registerViewModel?.retrieveProvince()
            registerViewModel?.retrieveOrganization()
            registerViewModel?.retrieveDistrict("1")
            registerViewModel?.retrieveSubDistrict("1")
            //localDatabase()
        }

        view.btnSignUp?.setOnClickListener {
            if (view.actFirstName.text.isNotBlank() && view.actLastName.text.isNotBlank() && view.actTelNumber.text.isNotBlank()
                    && view.actEmail.text.isNotBlank() && view.actPassword.text.isNotBlank()) {

//                registerViewModel?.loadRegister(RegisterUserInfo(AccountInfo(view.actFirstName.text.toString(), view.actLastName.text.toString(), view.actTelNumber.text.toString(),
//                        view.actEmail.text.toString(), view.actPassword.text.toString())))
            }
            when {
                view.actFirstName.text.isBlank() -> view.actFirstName.error = getString(R.string.act_error_first_name)
                view.actLastName.text.isBlank() -> view.actLastName.error = getString(R.string.act_error_last_name)
                view.actTelNumber.text.isBlank() -> view.actTelNumber.error = getString(R.string.act_error_tel)

                !Patterns.PHONE.matcher(view.actTelNumber.text.toString()).matches()
                -> view.actTelNumber.error = getString(R.string.act_error_tel_not_match)

                view.actEmail.text.isBlank() -> view.actEmail.error = getString(R.string.act_error_email)
                !Patterns.EMAIL_ADDRESS.matcher(view.actEmail.text.toString()).matches()
                -> view.actEmail.error = getString(R.string.act_error_email_not_match)

                (view.spOrganization.selectedItem as? Organization)?.organizationID == null
                        || (view.spOrganization.selectedItem as? Organization)?.organizationID
                        == OrganizationAdapter.ORGANIZATION_DEFAULT_ID -> {
                    Snackbar.make(btnSignUp, getString(R.string.sp_error_organization), Snackbar.LENGTH_INDEFINITE)
                            .run {
                                setAction(R.string.sb_dismiss, View.OnClickListener { dismiss() })
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                    setActionTextColor(resources.getColor(android.R.color.white))
                                else
                                    setActionTextColor(resources.getColor(android.R.color.white))
                                show()
                            }
                }

                view.actPassword.text.isBlank() -> view.actPassword.error = getString(R.string.act_error_password)
                view.actPassword.text.length < PASSWORD_SIZE -> view.actPassword.error = getString(R.string.act_error_password_size)
                view.actConfirmPassword.text.isBlank() -> view.actConfirmPassword.error = getString(R.string.act_error_confirm_password)
                view.actPassword.text.toString() != view.actConfirmPassword.text.toString()
                -> Snackbar.make(view.btnSignUp, getString(R.string.act_error_password_not_match), Snackbar.LENGTH_INDEFINITE)
                        .run {
                            setAction(R.string.sb_dismiss, View.OnClickListener { dismiss() })
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                                setActionTextColor(resources.getColor(android.R.color.white, null))
                            else
                                setActionTextColor(resources.getColor(android.R.color.white))
                            show()
                        }
                else -> {
                    view.btnSignUp.visibility = View.GONE

                    val provinceID: Int? =
                            if ((view.spProvince.selectedItem as? Province)?.provinceID == null)
                                ProvinceAdapter.PROVINCE_DEFAULT_ID
                            else
                                (view.spProvince.selectedItem as? Province)?.provinceID
                    val districtID: Int? =
                            if (view.spDistrict.selectedItem as? District == null)
                                DistrictAdapter.DISTRICT_DEFAULT_ID
                            else
                                (view.spDistrict.selectedItem as? District)?.districtID
                    val subDistrictId: Int? =
                            if ((spSubDistrict.selectedItem as? SubDistrict)?.subDistrictID == null)
                                SubDistrictAdapter.SUB_DISTRICT_DEFAULT_ID
                            else
                                (view.spSubDistrict.selectedItem as? SubDistrict)?.subDistrictID

                    if (view.actAddress.text.isBlank() && view.actLatitude.text.isBlank() && view.actLongitude.text.isBlank()
                            && provinceID == ProvinceAdapter.PROVINCE_DEFAULT_ID) {
                        registerViewModel?.registerUser(AccountRegisterWrapper(
                                Account(view.actFirstName.text.toString(), view.actLastName.text.toString()
                                        , view.actTelNumber.text.toString(), view.actEmail.text.toString()
                                        , view.actPassword.text.toString())
                                , null
                                , OrganizationAccount((view.spOrganization.selectedItem as Organization).organizationID)
                        ))

                    } else {
                        when {
                            view.actLatitude.text.toString().isBlank() && view.actLongitude.text.toString().isBlank()
                            -> registerViewModel?.registerUser(AccountRegisterWrapper(
                                    Account(view.actFirstName.text.toString(), view.actLastName.text.toString()
                                            , view.actTelNumber.text.toString(), view.actEmail.text.toString()
                                            , view.actPassword.text.toString())
                                    , AddressAccount(
                                    view.actAddress.text.toString(), null
                                    , null, provinceID
                                    , districtID, subDistrictId
                                    , SimpleDateFormat(DATE_FORMAT, Locale("th")).format(Calendar.getInstance().time)
                            )
                                    , OrganizationAccount((view.spOrganization.selectedItem as Organization).organizationID)
                            ))

                            view.actLongitude.text.toString().isBlank() && view.actLastName.text.toString().isNotBlank() -> {
                                registerViewModel?.registerUser(AccountRegisterWrapper(
                                        Account(actFirstName.text.toString(), view.actLastName.text.toString()
                                                , view.actTelNumber.text.toString(), view.actEmail.text.toString()
                                                , view.actPassword.text.toString())
                                        , AddressAccount(
                                        view.actAddress.text.toString(), view.actLatitude.text.toString().toDouble()
                                        , null, provinceID
                                        , districtID, subDistrictId
                                        , SimpleDateFormat(DATE_FORMAT, Locale("th")).format(Calendar.getInstance().time)
                                )
                                        , OrganizationAccount((view.spOrganization.selectedItem as Organization).organizationID)
                                ))
                            }
                            view.actLongitude.text.toString().isNotBlank() && view.actLatitude.text.toString().isBlank() -> {
                                registerViewModel?.registerUser(AccountRegisterWrapper(
                                        Account(view.actFirstName.text.toString(), view.actLastName.text.toString()
                                                , view.actTelNumber.text.toString(), view.actEmail.text.toString()
                                                , view.actPassword.text.toString())
                                        , AddressAccount(
                                        view.actAddress.text.toString(), null
                                        , view.actLongitude.text.toString().toDouble(), provinceID
                                        , districtID, subDistrictId
                                        , SimpleDateFormat(DATE_FORMAT, Locale("th")).format(Calendar.getInstance().time)
                                )
                                        , OrganizationAccount((view.spOrganization.selectedItem as Organization).organizationID)
                                ))
                            }
                            else -> {
                                registerViewModel?.registerUser(AccountRegisterWrapper(
                                        Account(view.actFirstName.text.toString(), view.actLastName.text.toString()
                                                , view.actTelNumber.text.toString(), view.actEmail.text.toString()
                                                , view.actPassword.text.toString())
                                        , AddressAccount(
                                        view.actAddress.text.toString(), view.actLatitude.text.toString().toDouble()
                                        , view.actLongitude.text.toString().toDouble(), provinceID
                                        , districtID, subDistrictId
                                        , SimpleDateFormat(DATE_FORMAT, Locale("th")).format(Calendar.getInstance().time)
                                )
                                        , OrganizationAccount((view.spOrganization.selectedItem as Organization).organizationID)
                                ))
                            }
                        }
                    }
                }
            }
        }

        registerViewModel?.registerState?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let {
                when (it.responseRegister.responseCode) {
                    in OK..Int.MAX_VALUE -> activity!!.finish()
                    SERVER_ERROR ->
                        Snackbar.make(view, "เกิดข้อผิดพลาด", Snackbar.LENGTH_LONG).show()
                    INVALID_EMAIL ->
                        Snackbar.make(view, "อีเมลไม่ถูกต้อง", Snackbar.LENGTH_LONG).show()
                    EXISTED ->
                        Snackbar.make(view, "อีเมลถูกใช้งานแล้ว", Snackbar.LENGTH_LONG).show()
                    WEAK_PASSWORD ->
                        Snackbar.make(view, "รหัสผ่านไม่ปลอดภัย", Snackbar.LENGTH_LONG).show()
                    else -> {
                        if (!it.responseRegister.responseMessage.isNullOrBlank())
                            Snackbar.make(view, it.responseRegister.responseMessage as String, Snackbar.LENGTH_LONG).show()
                        else
                            Snackbar.make(view, "เกิดข้อผิดพลาดบางอย่าง", Snackbar.LENGTH_LONG).show()
                    }
                }
            }
            view.btnSignUp?.visibility = View.VISIBLE
        })
        view.cvExpandAddress?.setOnClickListener {
            if (isOpenAddress.compareAndSet(true, false)) {
                llAddress.visibility = View.VISIBLE
            } else {
                llAddress.visibility = View.GONE
                isOpenAddress.set(true)
            }
        }

        registerViewModel?.provinceData?.observe(this, Observer { wrapper ->
            wrapper?.provinceList?.let {
                val provinceAdapter = ProvinceAdapter(context!!, R.layout.item_spinner_dropdown, it)
                view.spProvince?.adapter = provinceAdapter
                editorProvince = prefsProvince?.edit()
                editorProvince?.putString(PREFS_PROVINCE, wrapper.provinceList.toString())
                editorProvince?.commit()
            }
        })

        registerViewModel?.userAccount?.observe(this, Observer { wrapper ->
            editorUser = prefsUser?.edit()
            editorUser?.putString(USER, wrapper?.AccountList.toString())
            editorUser?.commit()
        })


        view.spProvince?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val provinceID: Int? = (parent!!.getItemAtPosition(position) as? Province)?.provinceID
                provinceID?.let {
                    if (it == ProvinceAdapter.PROVINCE_DEFAULT_ID) {
                        view?.spDistrict?.adapter = null
                        view?.spSubDistrict?.adapter = null
                    } else
                        registerViewModel?.retrieveDistrict(it.toString())
                }
            }
        }

        registerViewModel?.districtData?.observe(this, Observer { wrapper ->
            wrapper?.districtList?.let {
                val districtAdapter = DistrictAdapter(context!!, R.layout.item_spinner_dropdown, it)
                view.spDistrict?.adapter = districtAdapter
                editorDistrict = prefsDistrict?.edit()
                editorDistrict?.putString(PREFS_DISTRICT, wrapper.districtList.toString())
                editorDistrict?.commit()
            }
        })

        view.spDistrict?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val districtID: Int? = (parent!!.getItemAtPosition(position) as? District)?.districtID
                districtID?.let {
                    registerViewModel?.retrieveSubDistrict(it.toString())
                }
            }

        }

        registerViewModel?.subDistrictData?.observe(this, Observer { wrapper ->
            wrapper?.subDistrictList?.let {
                val subDistrictAdapter = SubDistrictAdapter(context!!,
                        R.layout.item_spinner_dropdown, it)
                view.spSubDistrict?.adapter = subDistrictAdapter
                editorSubDistrict = prefsSubDistrict?.edit()
                editorSubDistrict?.putString(PREFS_ORGANIZATION, wrapper.subDistrictList.toString())
                editorSubDistrict?.commit()
            }
        })

        registerViewModel?.organizationData?.observe(this, Observer { wrapper ->
            wrapper?.organizationList?.let {
                val organizationAdapter = OrganizationAdapter(context!!
                        , R.layout.item_spinner_dropdown, wrapper.organizationList)
                view.spOrganization?.adapter = organizationAdapter
                editorOrganization = prefsOrganization?.edit()
                editorOrganization?.putString(PREFS_ORGANIZATION, wrapper.organizationList.toString())
                editorOrganization?.commit()
            }
        })

    }


    companion object {
        private const val DATE_FORMAT = "yy-MM-dd HH:mm:ss"
        val tag: String = RegisterFragment::class.java.simpleName
        const val REQUEST_CODE = 10
        private const val PASSWORD_SIZE = 6
        @JvmStatic
        fun newInstance() =
                RegisterFragment().apply {

                }
    }
}
