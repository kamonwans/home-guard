package com.example.kamonwan_s.hgproject.statution.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.MapDao
import com.example.kamonwan_s.hgproject.database.StationsEntity
import com.example.kamonwan_s.hgproject.statution.api.LoadStationResponse
import com.example.kamonwan_s.hgproject.statution.api.StationApi
import com.example.kamonwan_s.hgproject.statution.api.Token
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StationRepository private constructor(private val executor: AppExecutors,
                                            private val stationApi: StationApi,
                                            private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()


    fun loadStation(token: Token): LiveData<SingleEvent<LoadStationResponse>> {
        val stationData = MutableLiveData<SingleEvent<LoadStationResponse>>()
        stationApi.loadStation(token).enqueue(object : Callback<LoadStationResponse?>{
            override fun onFailure(call: Call<LoadStationResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<LoadStationResponse?>, response: Response<LoadStationResponse?>) {
                if (response.isSuccessful){
                    stationData.value = SingleEvent(response.body()!!)
                    var dbInfoList = response.body()!!.stationWrapper
                    executor.diskIO.execute{
                        try {
                            var stations = dbInfoList.stations.stations
                            var stationsList = ArrayList<StationsEntity>()
                            stations.forEach {
                                val stationsEntity = StationsEntity(
                                        it.districtID,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.organizationID,
                                        it.provinceID,
                                        it.stationID,
                                        it.stationName,
                                        it.subDistrictID,
                                        it.tel
                                )


                                stationsList.add(stationsEntity)
                            }
                            mapDao.insertStation(stationsList)
                            if (dbInfoList.stations.stations.isNotEmpty()) {
                                mapDao.insertStation(stationsList)
                            } else {
                                mapDao.loadAllStationLiveData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }

            }

        })

        return stationData
    }

    companion object {
        @Volatile
        private var instance: StationRepository? = null
        private val tag = StationRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, stationApi: StationApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: StationRepository(executor, stationApi, mapDao).apply {
                                instance = this
                            }
                }
    }

}