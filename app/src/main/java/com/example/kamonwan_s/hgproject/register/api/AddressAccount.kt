package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class AddressAccount(@SerializedName("Address") val address: String?
                          , @SerializedName("Lat") val lat: Double?
                          , @SerializedName("Lng") val lng: Double?
                          , @SerializedName("ProvinceID") val provinceID: Int?
                          , @SerializedName("DistrictID") val districtID: Int?
                          , @SerializedName("SubDistrictID") val subDistrictID: Int?
                          , @SerializedName("ModifiedDate") val modifiedDate: String
                          , @SerializedName("POIID") val poiID: Int = POI_ID
) {
    companion object {
        private const val POI_ID = -1
    }
}