package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ReportReply

class CommentAdapter(val context: Context) : RecyclerView.Adapter<CommentAdapter.ViewHolder>(){
    var items: ArrayList<ReportReply>? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CommentAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_comment,parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }
    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    fun ImageView.setImageComment(url: String) {
        this.visibility = View.GONE
        if(!url.equals("")){
            this.visibility = View.VISIBLE
            Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.default_image).error(R.drawable.default_image)).into(this)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var comment = items!![position].content
        var firstName = items!![position].firstName
        var lastName = items!![position].lastName

        holder.tvNameFirstComment.text = firstName
        holder.tvNameLastComment.text = lastName
        holder.tvComment.text = comment
        holder.imgProfileComment.setProfile(items!![position].profilePic)
        holder.imgComment.setImageComment(items!![position].image)
        Log.d("commentReport",comment)
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var tvNameFirstComment: TextView
        var tvNameLastComment: TextView
        var tvComment: TextView
        var imgProfileComment: ImageView
        var imgComment: ImageView

        init {
            tvNameFirstComment = row.findViewById(R.id.tvNameFirstComment) as TextView
            tvNameLastComment = row.findViewById(R.id.tvNameLastComment) as TextView
            tvComment = row.findViewById(R.id.tvComment) as TextView
            imgProfileComment = row.findViewById(R.id.imgProfileComment) as ImageView
            imgComment = row.findViewById(R.id.imgComment) as ImageView
        }

    }
}