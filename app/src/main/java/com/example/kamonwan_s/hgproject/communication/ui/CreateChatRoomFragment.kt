package com.example.kamonwan_s.hgproject.communication.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.widget.Toast

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactInfo
import com.example.kamonwan_s.hgproject.communication.api.AccountContacts
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.api.CreateChatRoomResponse
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_create_chat_room.*
import kotlinx.android.synthetic.main.fragment_create_chat_room.view.*
import okhttp3.Callback
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File

class CreateChatRoomFragment : Fragment(),ChatRoomAdapter.BtnClickListener {

    private var communicationViewModel: CommunicationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var token: String? = null
    private var organizationId: String? = null
    private var file: File? = null
    private var adapter : ChatRoomAdapter? = null
    private val multipartMember:MutableList<MultipartBody.Part> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("สร้างกลุ่ม")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_chat_room, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)
        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = communicationViewModel?.getToken()
        organizationId = communicationViewModel?.getOrganizationId()
        Log.d("organizationId",organizationId)

        adapter = ChatRoomAdapter(context!!)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewMemberChooseCreateGroup.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL,false)
        view.recyclerViewMemberChooseCreateGroup.itemAnimator = DefaultItemAnimator()
        view.recyclerViewMemberChooseCreateGroup.adapter = adapter

        communicationViewModel?.syncAccountContact(AccountContactInfo(token!!, organizationId!!.toInt(), "10"))
        communicationViewModel?.accoutContactList?.observe(this, Observer {event ->
            event?.getContentIfNotHandled()?.let { accountContactWrapper ->
                accountContactWrapper.accountContactListWrapper.accountContactListData.accountContacts.let {

                }
            }
        })

        view.imageChooseProfile.setOnClickListener {
            val intent = Intent().apply {
                action = Intent.ACTION_PICK
                type = "image/*"
            }
            startActivityForResult(intent, 1)
        }

        view.imageChooseMember.setOnClickListener {
            FragmentChooseFriends()
        }

    }

//    private fun createRoom() {
//        var nameGroup = editTextGroupName.text.toString()
//
//        if (nameGroup == "") {
//            Toast.makeText(context, "กรุณากรอกชื่อกลุ่ม", Toast.LENGTH_LONG).show()
//        } else {
//            if (file != null) {
//                var photoBody = RequestBody.create(MediaType.parse("image/*"), file)
//
//                CommunicationApi.service.createChatRoom(
//                        RequestBody.create(MediaType.parse("text/plain"), nameGroup),
//                        RequestBody.create(MediaType.parse("text/plain"), token),
//                        RequestBody.create(MediaType.parse("text/plain"), "32"),
//                        MultipartBody.Part.createFormData("file", file!!.name, photoBody))
//                        .enqueue(object : retrofit2.Callback<CreateChatRoomResponse> {
//                            override fun onFailure(call: Call<CreateChatRoomResponse>, t: Throwable) {
//                                Log.d("onFailure", t.message)
//                            }
//
//                            override fun onResponse(call: Call<CreateChatRoomResponse>, response: Response<CreateChatRoomResponse>) {
//                                if (response.isSuccessful) {
//                                    Toast.makeText(context, "สร้างห้องสำเร็จ", Toast.LENGTH_SHORT).show()
//                                    FragmentChat()
//                                } else
//                                    Log.d("xxx", response.errorBody()!!.string())
//                            }
//
//                        })
//                editTextGroupName.text.clear()
//            } else {
//                CommunicationApi.service.createChatRoomNoPic(
//                        RequestBody.create(MediaType.parse("text/plain"), nameGroup),
//                        RequestBody.create(MediaType.parse("text/plain"), token),
//                        RequestBody.create(MediaType.parse("text/plain"), "32")
//                ).enqueue(object : retrofit2.Callback<CreateChatRoomResponse> {
//                    override fun onFailure(call: Call<CreateChatRoomResponse>, t: Throwable) {
//                        Log.d("onFailure", t.message)
//                    }
//
//                    override fun onResponse(call: Call<CreateChatRoomResponse>, response: Response<CreateChatRoomResponse>) {
//                        if (response.isSuccessful) {
//                            Toast.makeText(context, "สร้างห้องสำเร็จ", Toast.LENGTH_SHORT).show()
//                            FragmentChat()
//                        } else
//                            Log.d("xxx", response.errorBody()!!.string())
//                    }
//
//                })
//                editTextGroupName.text.clear()
//            }
//        }
//    }


//    private fun addMember(accountId:ArrayList<Int>): ArrayList<Int>{
//
//
//    }
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_done, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.home -> {
                activity?.onBackPressed()
                return true
            }
            R.id.doneChat -> {
            // createRoom()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val uri: Uri = data!!.data
            val filePathColum: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? = context!!.contentResolver.query(uri, filePathColum, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(filePathColum[0])
                path = it.getString(index)
            }
            if (path == null) {
                Log.d("xx", "path is null")
                return
            }
            file = File(path)
            imageChooseProfile!!.setImageURI(uri)


        }
    }

    override fun onBtnClick(accountId: Int, name: String, pic: ArrayList<String>) {
        //createRoom(accountId)
    }

    fun FragmentChat() {
        val fragment = ChatFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentChooseFriends() {
        val fragment = ChooseFriendsChatRoomFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }


    companion object {

        @JvmStatic
        fun newInstance() =
                CreateChatRoomFragment().apply {

                }
    }
}
