package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class SyncReportStatusData(@SerializedName("ReportStatuses") val reportStatuses : MutableList<ReportStatuses>)
