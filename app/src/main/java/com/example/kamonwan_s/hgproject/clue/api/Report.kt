package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class Report(@SerializedName("AccountID") val accountID:Int,
                  @SerializedName("Content") val content:String,
                  @SerializedName("CreatorName") val creatorName:String,
                  @SerializedName("Images") val images : ArrayList<String>,
                  @SerializedName("IsFavorite") val  isFavorite:Int,
                  @SerializedName("Lat") val lat :String,
                  @SerializedName("Lng") val lng:String,
                  @SerializedName("ModifiedDate") val modifiedDate:String,
                  @SerializedName("NumComment") val numComment:Int,
                  @SerializedName("ProfilePic") val profilePic:String,
                  @SerializedName("ReportID") val reportID:Int,
                  @SerializedName("ReportStatusID") val reportStatusID :String,
                  @SerializedName("ReportTypeID") val reportTypeID:String,
                  @SerializedName("Title") val title:String)