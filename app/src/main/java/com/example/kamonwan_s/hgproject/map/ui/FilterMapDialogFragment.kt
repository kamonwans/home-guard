package com.example.kamonwan_s.hgproject.map.ui

import android.arch.lifecycle.ViewModelProviders
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.map.MapViewModel
import com.example.kamonwan_s.hgproject.poi.api.POICategory
import com.example.kamonwan_s.hgproject.poi.api.PoiType
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_filter_map_dialog.view.*


class FilterMapDialogFragment : BottomSheetDialogFragment(), FilterAdapter.OnItemCheckedChangeListener {

    private var mapViewModel: MapViewModel? = null
    private lateinit var adapter: FilterAdapter
    private lateinit var fragmentView: View
    private var layoutManager: LinearLayoutManager? = null
    private var poiType: ArrayList<PoiType> = ArrayList()
    private var poiCategory: ArrayList<POICategory> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            poiType = it.getParcelableArrayList<PoiType>("poiType")
            poiCategory = it.getParcelableArrayList<POICategory>("poiCategory")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_filter_map_dialog, container, false)
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        fragmentView = view
        mapViewModel = ViewModelProviders.of(parentFragment!!
                , InjectorUtil.provideMapViewModelFactory(activity!!.application)).get(MapViewModel::class.java)

        var recyclerViewFilter = view.findViewById(R.id.recyclerViewFilter) as RecyclerView
        adapter = FilterAdapter(poiCategory, poiType, this)

        recyclerViewFilter.adapter = adapter
        layoutManager = LinearLayoutManager(view.context)
        recyclerViewFilter.layoutManager = layoutManager
        recyclerViewFilter.itemAnimator = DefaultItemAnimator()

        view.imageClosed.setOnClickListener {
            dismiss()
        }

    }

    override fun onCategoryCheck(id: Int) {
        fragmentView.recyclerViewFilter.swapAdapter(FilterAdapter(poiCategory, poiType, this), false)

    }

    override fun onTypeCheck(id: Int) {
        fragmentView.recyclerViewFilter?.swapAdapter(FilterAdapter(poiCategory, poiType, this), false)
    }

    private fun getDialogListener(): OnFragmentInteractionListener? {
        val fragment = parentFragment
        return if (fragment != null)
            fragment as? OnFragmentInteractionListener
        else
            activity as? OnFragmentInteractionListener
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        getDialogListener()?.onDialogDismiss()
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onDialogDismiss()
    }

    companion object {
        val tag: String = FilterMapDialogFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(poiTypeList: ArrayList<PoiType>, poiCategoryList: ArrayList<POICategory>): FilterMapDialogFragment {
            val fragment = FilterMapDialogFragment()
            val arg = Bundle()
            arg.putParcelableArrayList("poiType", poiTypeList)
            arg.putParcelableArrayList("poiCategory", poiCategoryList)
            fragment.arguments = arg
            return fragment
        }
    }
}
