package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationStatusWrapper(@SerializedName("Data")val operationStatusData : OperationStatusData,
                                  @SerializedName("Response")val responseList: ResponseList)