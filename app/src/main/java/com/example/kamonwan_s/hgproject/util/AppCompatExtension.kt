package com.example.kamonwan_s.hgproject.util

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply(action).commit()
}
fun AppCompatActivity.addFragmentToActivity(fragment:Fragment, @IdRes containerID:Int, tag:String){
    supportFragmentManager.transact { add(containerID, fragment, tag) }
}
fun AppCompatActivity.replaceFragmentToActivityWithBackStack(fragment: Fragment, @IdRes containerID: Int, tag: String) {
    supportFragmentManager.transact { replace(containerID, fragment, tag).addToBackStack(null) }
}

fun FragmentManager.replaceFragmentToFragmentWithBackStack(fragment: Fragment,@IdRes containerID: Int,tag: String){
    beginTransaction().apply { replace(containerID,fragment,tag) }.addToBackStack(null)
}
fun FragmentManager.onBackStackChangeListener(action:() -> Unit){
    addOnBackStackChangedListener(object: FragmentManager.OnBackStackChangedListener {
        override fun onBackStackChanged() {
        }
    })


}