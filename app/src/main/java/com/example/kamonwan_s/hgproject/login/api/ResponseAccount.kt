package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class ResponseAccount(@SerializedName("ResponseCode") val responseCode : Int,
                           @SerializedName("ResponseMessage") val responseMessage : String)