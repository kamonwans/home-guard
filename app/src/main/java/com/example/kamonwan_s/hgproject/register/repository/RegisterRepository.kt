package com.example.kamonwan_s.hgproject.register.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.MapDao
import com.example.kamonwan_s.hgproject.database.ProvinceEntity
import com.example.kamonwan_s.hgproject.register.api.*
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterRepository constructor(private val executor: AppExecutors,
                                     private val registerApi: RegisterApi
                                     , private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()


    fun loadProvince(): LiveData<ProvinceWrapper> {
        val provinceLiveData = MutableLiveData<ProvinceWrapper>()
        registerApi.loadProvince().enqueue(object : Callback<ProvinceWrapper?> {
            override fun onFailure(call: Call<ProvinceWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<ProvinceWrapper?>, response: Response<ProvinceWrapper?>) {
                if (response.isSuccessful) {
                    provinceLiveData.value = response.body()
                    var dbInfoList = response.body()
                    executor.diskIO.execute {
                        try {
                            val province = dbInfoList?.provinceList
                            var provinceList = ArrayList<ProvinceEntity>()
                            province?.forEach {
                                var provinceEntity = ProvinceEntity(it.provinceID,
                                        it.provinceName,
                                        it.modifiedDate)
                                provinceList.add(provinceEntity)
                            }
                            mapDao.insertProvince(provinceList)
                            if (dbInfoList!!.equals("")) {
                                mapDao.insertProvince(provinceList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }
        })
        return provinceLiveData
    }

    fun loadDistrict(provinceID: String): LiveData<DistrictWrapper> {
        val districtLiveData = MutableLiveData<DistrictWrapper>()
        registerApi.loadDistrict(provinceID).enqueue(object : Callback<DistrictWrapper?> {
            override fun onFailure(call: Call<DistrictWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<DistrictWrapper?>, response: Response<DistrictWrapper?>) {
                if (response.isSuccessful) {
                    districtLiveData.value = response.body()
                    var dbInfoList = response.body()!!
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList.districtList.isNotEmpty()){
//                                mapDao.insertDistrict(dbInfoList.districtList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return districtLiveData
    }

    fun loadOrganization(): LiveData<OrganizationWrapper> {
        val organizationLiveData = MutableLiveData<OrganizationWrapper>()
        registerApi.loadOrganization().enqueue(object : Callback<OrganizationWrapper?> {
            override fun onFailure(call: Call<OrganizationWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<OrganizationWrapper?>, response: Response<OrganizationWrapper?>) {
                if (response.isSuccessful) {
                    organizationLiveData.value = response.body()
                    Log.d("Response", "OK")
                    Log.d("Response1", response.body().toString())

                    var dbInfoList = response.body()!!
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList.organizationList.isNotEmpty()) {
//                                mapDao.insertOrganizaton(dbInfoList.organizationList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }

                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return organizationLiveData
    }

    fun loadSubDistrict(districtID: String): LiveData<SubDistrictWrapper> {
        val subDistrictLiveData = MutableLiveData<SubDistrictWrapper>()
        registerApi.loadSubDistrict(districtID).enqueue(object : Callback<SubDistrictWrapper?> {
            override fun onFailure(call: Call<SubDistrictWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<SubDistrictWrapper?>, response: Response<SubDistrictWrapper?>) {
                if (response.isSuccessful){
                    subDistrictLiveData.value = response.body()
                    var dbInfoList = response.body()!!
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList.subDistrictList.isNotEmpty()) {
//                                mapDao.deleteAllSubDistrict()
//                                mapDao.insertSubDistrict(dbInfoList.subDistrictList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            syncDatabaseError.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }
                }


                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return subDistrictLiveData
    }

    fun registerUser(accountRegisterWrapper: AccountRegisterWrapper): LiveData<SingleEvent<RegisterUserResult>> {
        val networkState = MutableLiveData<SingleEvent<RegisterUserResult>>()
        registerApi.registerUser(accountRegisterWrapper).enqueue(object : Callback<RegisterUserResult?> {
            override fun onFailure(call: Call<RegisterUserResult?>, t: Throwable) {
                Log.d(tag, t.message.toString())
//                if (t.message == null)
//                    networkState.value = SingleEvent(RegisterUserResult(LOCAL_ERROR, t.toString()))
//                else
//                    networkState.value = SingleEvent(RegisterUserResult(LOCAL_ERROR, t.message!!))
            }

            override fun onResponse(call: Call<RegisterUserResult?>, response: Response<RegisterUserResult?>) {
                if (response.isSuccessful) {
                    networkState.value = SingleEvent(response.body()!!)
                    //   Log.d("networkRegis", response.errorBody()!!.string())
                } else
                    syncDatabaseError.value = SingleEvent(response.errorBody()!!.string())

            }

        })
        return networkState
    }

    fun registerUserAccount(registerUserInfo: RegisterUserInfo):LiveData<SingleEvent<RegisterUserResult>>{
        val networkState = MutableLiveData<SingleEvent<RegisterUserResult>>()
        registerApi.registerUserAccount(registerUserInfo).enqueue(object :Callback<RegisterUserResult?>{
            override fun onFailure(call: Call<RegisterUserResult?>, t: Throwable) {
                Log.d(tag, t.message.toString())

            }

            override fun onResponse(call: Call<RegisterUserResult?>, response: Response<RegisterUserResult?>) {
                if (response.isSuccessful) {
                    networkState.value = SingleEvent(response.body()!!)
                    //   Log.d("networkRegis", response.errorBody()!!.string())
                } else
                    syncDatabaseError.value = SingleEvent(response.errorBody()!!.string())


            }

        })

        return networkState
    }

//    fun syncDatabase(modifieDate: Long): LiveData<Boolean> {
//        val completeSync = MutableLiveData<Boolean>()
//        registerApi.syncProvince(modifieDate).enqueue(object : Callback<SyncDBInfoListResponse?> {
//            override fun onFailure(call: Call<SyncDBInfoListResponse?>, t: Throwable) {
//                if (t.message != null)
//                    syncDatabaseError.value = SingleEvent(t.message!!)
//                else
//                    syncDatabaseError.value = SingleEvent(t.toString())
//            }
//
//            override fun onResponse(call: Call<SyncDBInfoListResponse?>, response: Response<SyncDBInfoListResponse?>) {
//               if (response.isSuccessful){
//                   val dbInfoList = response.body()!!.syncProvince
//                   executor.diskIO.execute {
//
//                   }
//               }
//            }
//
//        })
//        return completeSync
//    }

    companion object {
        @Volatile
        private var instance: RegisterRepository? = null
        private val tag = RegisterRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, registerApi: RegisterApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: RegisterRepository(executor, registerApi, mapDao).apply {
                                instance = this
                            }
                }
    }
}