package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class OfficerDetail(@SerializedName("Email") val email : String,
                         @SerializedName("FirstName") val firstName :String,
                         @SerializedName("LastName") val lastName:String,
                         @SerializedName("ModifiedDate") val modifiedDate:String,
                         @SerializedName("ProfilePic") val profilePic : String,
                         @SerializedName("Tel")val tel : String,
                         @SerializedName("AccountID") val accountID :String,
                         @SerializedName("ChatRoomID")val chatRoomID:String)