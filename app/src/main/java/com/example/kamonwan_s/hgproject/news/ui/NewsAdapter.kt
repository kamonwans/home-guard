package com.example.kamonwan_s.hgproject.news.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.news.api.NewsAfterList
import com.example.kamonwan_s.hgproject.news.api.NewsList
import com.example.kamonwan_s.hgproject.news.api.NewsListAfterWrapper
import com.example.kamonwan_s.hgproject.news.api.NewsTypeData
import kotlinx.android.synthetic.main.item_news.view.*

class NewsAdapter(val context: Context, var listener: BtnClickListener) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    var items: NewsList? = null
    var itemsType: NewsTypeData? = null
    val bundle = Bundle()
    var timeDate: String? = null
    var itemAfter: NewsAfterList? = null
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): NewsAdapter.ViewHolder {
        if(position == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_news, parent, false)
            return ViewHolder(view)
        }else{
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_loading, parent, false)
            return ViewHolder(view)
        }

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var title = items?.newss?.get(position)?.header
        var date = items?.newss?.get(position)?.modifiedDate
        var content = items?.newss?.get(position)?.content
        var newsId = items?.newss?.get(position)?.newsID
        var typeId = items?.newss?.get(position)?.newsTypeID

        var newsTypeID = itemsType?.newsType?.get(0)?.newsTypeID
        var newsTypeName = itemsType?.newsType?.get(0)?.newsTypeName
        var newsFontColor = itemsType?.newsType?.get(0)?.fontColorCode
        var image = items?.newss?.get(0)?.images
        var newsTypeColor = itemsType?.newsType?.get(0)?.colorCode


        if (image != null){
            holder.itemView.imgNews?.setImage(image)
        }

        if (typeId.equals(newsTypeID)) {
            holder.itemView.tvTypeNews.text = newsTypeName
            holder.itemView.tvTypeNews.setTextColor(Color.parseColor("#" + newsFontColor))
            holder.itemView.tvTypeNews.setBackgroundColor(Color.parseColor("#" + newsTypeColor))
        }

        timeDate = convertDate(date!!, "dd/MM/yyyy hh:mm:ss")

        holder.itemView.tvTitleNews.text = title
        holder.itemView.tvDetailNews.text = content
        holder.itemView.tvDate.text = timeDate


        holder.itemView.setOnClickListener {
            listener.onBtnClick(position, title!!, date, typeId!!.toInt(), content!!, newsId!!.toInt(), newsTypeName!!)
        }

    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    override fun getItemCount(): Int {
        return items?.newss?.size ?: 0
    }

    fun ImageView.setImage(url: MutableList<String>) {
        if (url.size != 0){
            Glide.with(context).load(url[0]).apply(RequestOptions.placeholderOf(R.drawable.default_image).error(R.drawable.default_image)).into(this)
        }


    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    interface BtnClickListener {
        fun onBtnClick(position: Int, header: String, date: String, typeId: Int, content: String, newsId: Int, newsTypeName: String)

    }
}