package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class Response(@SerializedName("Response") val responseData: ResponseData)