package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "poi")
data class PoiEntity(@PrimaryKey @ColumnInfo(name = "poi_id") val poiId: Int,
                     @ColumnInfo(name = "poi_lat") val lat: String,
                     @ColumnInfo(name = "poi_lng") val lng: String,
                     @ColumnInfo(name = "poi_modified_date") val modifiedDate: String,
                     @ColumnInfo(name = "poi_name") val poiName: String,
                     @ColumnInfo(name = "poi_type_id") val pOITypeID: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(poiId)
        parcel.writeString(lat)
        parcel.writeString(lng)
        parcel.writeString(modifiedDate)
        parcel.writeString(poiName)
        parcel.writeString(pOITypeID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PoiEntity> {
        override fun createFromParcel(parcel: Parcel): PoiEntity {
            return PoiEntity(parcel)
        }

        override fun newArray(size: Int): Array<PoiEntity?> {
            return arrayOfNulls(size)
        }
    }
}