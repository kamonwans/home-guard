package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

@Entity(tableName = "operations")
data class OperationsEntity(@ColumnInfo(name = "operations_chat_id") val chatRoomID: Int,
                            @ColumnInfo(name = "operations_detail") val detail: String,
                            @ColumnInfo(name = "operations_is_operation") val isWorking: Int,
                            @ColumnInfo(name = "operations_modified") val modifiedDate: String,
                            @PrimaryKey @ColumnInfo(name = "operations_id") val operationID: Int,
                            @ColumnInfo(name = "operations_name") val operationName: String,
                            @ColumnInfo(name = "operations_status_id") val operationStatusID: Int,
                            @ColumnInfo(name = "operations_type_id") val operationTypeID: Int,
                            @ColumnInfo(name = "operations_organization_name") val organizationName: String,
                            @ColumnInfo(name = "operations_organization_pic") val organizationPic: String,
                            @ColumnInfo(name = "operations_organization_id") val organizationID: Int)