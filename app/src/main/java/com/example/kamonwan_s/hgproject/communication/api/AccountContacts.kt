package com.example.kamonwan_s.hgproject.communication.api

import android.support.annotation.StringRes
import com.google.gson.annotations.SerializedName

data class AccountContacts(@SerializedName("AccountID") val accountID: String,
                           @SerializedName("ChatRoomID")val chatRoomID:String,
                           @SerializedName("Email") val email: String,
                           @SerializedName("Tel") val tel: String,
                           @SerializedName("FirstName") val firstName: String,
                           @SerializedName("LastName") val lastName: String,
                           @SerializedName("ModifiedDate") val modifiedDate: String,
                           @SerializedName("OrganizationID") val organizationID: Int,
                           @SerializedName("ProfilePic") val profilePic: String) {
    var checkedItem: Boolean = false
}