package com.example.kamonwan_s.hgproject.register.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.example.kamonwan_s.hgproject.Checker
import com.google.gson.annotations.SerializedName

data class SubDistrict(@SerializedName("SubDistrictID") val subDistrictID: Int
                       , @SerializedName("DistrictID") val districtID: Int
                       , @SerializedName("SubDistrictName") val subDistrictName: String
                       , @SerializedName("ModifiedDate") val modifiedDate: Long
                       , override var checked: Boolean = false) : Checker