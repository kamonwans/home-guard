package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class OrganizationWrapper(@SerializedName("LoadOrganizationResult")
                               val organizationList:MutableList<Organization>)