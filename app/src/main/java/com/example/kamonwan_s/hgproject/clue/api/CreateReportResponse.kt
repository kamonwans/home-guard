package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class CreateReportResponse(@SerializedName("CreateReportResult") val responseList: ResponseList)