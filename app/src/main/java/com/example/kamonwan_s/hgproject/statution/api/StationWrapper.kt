package com.example.kamonwan_s.hgproject.statution.api

import com.google.gson.annotations.SerializedName

data class StationWrapper(@SerializedName("Data")val stations : StationsData,
                          @SerializedName("Response") val responseList: ResponseList)