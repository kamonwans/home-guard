package com.example.kamonwan_s.hgproject.operation.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.OperationsEntity
import com.example.kamonwan_s.hgproject.operation.api.*
import com.example.kamonwan_s.hgproject.operation.repository.OperationRepository


class OperationViewModel(private val app: Application, private val operationRepository: OperationRepository)
    : AndroidViewModel(app) {

    var operationInfo = MediatorLiveData<OperationInfo>()
    var operationUpdateInfo = MediatorLiveData<OperationWorkingInfo>()
    var operationDetailInfo = MediatorLiveData<OperationDetailInfo>()
    var accountContactInfo = MediatorLiveData<AccountContactInfo>()
    var token = MediatorLiveData<Token>()
    var updateLocationInfo = MediatorLiveData<UpdateLocationInfo>()

    val operationOwn: LiveData<SingleEvent<SyncOperationListResponse>> = Transformations.switchMap(operationInfo) {
        operationRepository.loadOperationOwn(it)
    }

    val operationUpdateData: LiveData<SingleEvent<UpdateOperationWorkingResponse>> = Transformations.switchMap(operationUpdateInfo) {
        operationRepository.updateOperationWorking(it)
    }

    val operationDetailData: LiveData<SingleEvent<GetOperationDetailResponse>> = Transformations.switchMap(operationDetailInfo) {
        operationRepository.loadOperationDetail(it)
    }

    val accountContacts: LiveData<SingleEvent<SyncAccountContactListResponse>> = Transformations.switchMap(accountContactInfo) {
        operationRepository.syncOperationAccountContactList(it)
    }

    val operationStatus: LiveData<SingleEvent<LoadOperationStatusResponse>> = Transformations.switchMap(token) {
        operationRepository.loadOperationStatus(it)
    }

    val operationType: LiveData<SingleEvent<LoadOperationTypeResponse>> = Transformations.switchMap(token) {
        operationRepository.loadOperationType(it)
    }

    val lastLocationUpdate : LiveData<SingleEvent<UpdateLastLocationResponse>> = Transformations.switchMap(updateLocationInfo){
        operationRepository.updateOperationLastLocation(it)
    }

    fun loadOperationOwn(operationInfo: OperationInfo) {
        this.operationInfo.value = operationInfo
    }

    fun updateWorking(operationWorkingInfo: OperationWorkingInfo) {
        this.operationUpdateInfo.value = operationWorkingInfo
    }

    fun loadOperationDetail(operationDetailInfo: OperationDetailInfo) {
        this.operationDetailInfo.value = operationDetailInfo
    }

    fun syncAccountContact(accountContactInfo: AccountContactInfo) {
        this.accountContactInfo.value = accountContactInfo
    }

    fun loadOperationStatus(token: Token) {
        this.token.value = token
    }

    fun loadOperationType(token: Token) {
        this.token.value = token
    }

    fun updateLastLocation(updateLocationInfo: UpdateLocationInfo){
        this.updateLocationInfo.value = updateLocationInfo
    }

//    fun insertOperation(operation: List<OperationsEntity>){
//        operationRepository.insertOperation(operation)
//    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun getToken(): String {
        return retrieveToken(app)
    }
}