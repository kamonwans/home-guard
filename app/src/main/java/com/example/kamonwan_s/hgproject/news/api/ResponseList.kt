package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class ResponseList(@SerializedName("ResponseCode") val responseCode : Int,
                        @SerializedName("ResponseMessage") val responseMessage : String)