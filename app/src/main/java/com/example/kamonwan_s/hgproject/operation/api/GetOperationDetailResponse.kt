package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class GetOperationDetailResponse(@SerializedName("GetOperationDetailResult") val operationDetailWrapper:OperationDetailWrapper)