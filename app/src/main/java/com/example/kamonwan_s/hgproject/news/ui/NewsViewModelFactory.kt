package com.example.kamonwan_s.hgproject.news.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.news.repository.NewsRepository


class NewsViewModelFactory(private val app: Application, private val newsRepository: NewsRepository)
    : ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsViewModel(app, newsRepository) as T
    }
}