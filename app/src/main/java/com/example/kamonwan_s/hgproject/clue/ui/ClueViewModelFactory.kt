package com.example.kamonwan_s.hgproject.clue.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ClueViewModelFactory(private val app:Application,private val clueRepository :ClueRepository)
    :ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClueViewModel(app,clueRepository) as T
    }
}