package com.example.kamonwan_s.hgproject.register.ui

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.register.api.Organization


class OrganizationAdapter(context: Context, @LayoutRes resource:Int, objects: MutableList<Organization>)
    : ArrayAdapter<Organization>(context,resource,objects){
    init {
        if (objects[0] != Organization(ORGANIZATION_DEFAULT_ID, "กรุณาเลือกสังกัด", 1))
            objects.add(0, Organization(ORGANIZATION_DEFAULT_ID, "กรุณาเลือกสังกัด", 1))
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return if (convertView == null){
            val newView:View = LayoutInflater.from(parent.context).inflate(R.layout.item_spinner_dropdown,
                    parent,false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.name }
        }else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.name }
        }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return if (convertView == null){
            val newView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_spinner_dropdown,
                    parent,false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.name }
        }else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.name }
        }
    }
    companion object {
        const val ORGANIZATION_DEFAULT_ID = -1
    }
}