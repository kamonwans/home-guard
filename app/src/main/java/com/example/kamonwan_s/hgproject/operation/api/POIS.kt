package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

@Entity(tableName = "pois")
data class POIS(@NotNull @ColumnInfo (name = "pois_lat")@SerializedName("Lat")val lat:String,
                @NotNull @ColumnInfo (name = "pois_lng")@SerializedName("Lng")val lng:String,
                @NotNull @ColumnInfo (name = "pois_modified")@SerializedName("ModifiedDate")val modifiedDate:String,
                @PrimaryKey @NotNull @ColumnInfo (name = "pois_id")@SerializedName("POIID") val poiID:Int,
                @NotNull @ColumnInfo (name = "pois_name")@SerializedName("POIName")val poiName:String,
                @NotNull @ColumnInfo (name = "pois_type_id")@SerializedName("POITypeID")val poiTypeID:Int)