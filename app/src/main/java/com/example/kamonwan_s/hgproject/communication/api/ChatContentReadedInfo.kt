package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContentReadedInfo(@SerializedName("Token")val token:String,
                                 @SerializedName("ChatContentID")val chatContentID:String)