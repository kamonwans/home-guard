package com.example.kamonwan_s.hgproject.util

import android.app.Application
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.register.api.Account

object AccountUtil {
    const val DEFAULT__ACCOUNT_ID = -1



    fun retrieveProfilePic(app: Application): String {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        return data.getString(app.getString(R.string.pref_profile_picture_link),"")
    }

    fun saveAccount(app: Application, account: Account) {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        val editor = data.edit()
        editor.run {
            putString(app.getString(R.string.pref_first_name), account.firstName)
            putString(app.getString(R.string.pref_last_name), account.lastName)
            putString(app.getString(R.string.pref_profile_picture_link), account.profilePic)
        }
        editor.apply()
    }

    fun retrieveFirstName(app: Application): String {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        return data.getString(app.getString(R.string.pref_first_name), "")!!
    }

    fun retrieveLastName(app: Application): String {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        return data.getString(app.getString(R.string.pref_last_name), "")!!
    }
}