package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class GetNewsResponse(@SerializedName("GetNewsResult") val newsWrapper : NewsWrapper)