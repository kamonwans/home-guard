package com.example.kamonwan_s.hgproject.map.ui


import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.OK
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.ui.CreateReportClueFragment
import com.example.kamonwan_s.hgproject.database.AppDatabase
import com.example.kamonwan_s.hgproject.database.PoiEntity
import com.example.kamonwan_s.hgproject.map.MapViewModel
import com.example.kamonwan_s.hgproject.map.ui.SearchLocationActivity.Companion.POI_ITEM
import com.example.kamonwan_s.hgproject.map.ui.SearchLocationActivity.Companion.POI_LIST
import com.example.kamonwan_s.hgproject.map.ui.SearchLocationActivity.Companion.requestCode
import com.example.kamonwan_s.hgproject.map.ui.people.PeopleDetailDialogFragment
import com.example.kamonwan_s.hgproject.map.ui.people.PeopleViewModel
import com.example.kamonwan_s.hgproject.map.ui.people.api.OfficerLocation
import com.example.kamonwan_s.hgproject.map.ui.poi.PoiDetailDialogFragment
import com.example.kamonwan_s.hgproject.map.ui.poi.PoiDialogFragment
import com.example.kamonwan_s.hgproject.map.ui.report.ReportDialogFragment
import com.example.kamonwan_s.hgproject.poi.api.POICategory
import com.example.kamonwan_s.hgproject.poi.api.POIs
import com.example.kamonwan_s.hgproject.poi.api.PoiType
import com.example.kamonwan_s.hgproject.statution.api.Token
import com.example.kamonwan_s.hgproject.statution.ui.StationViewModel
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.example.kamonwan_s.hgproject.util.Util
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.maps.android.MarkerManager
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.fragment_maps.view.*
import kotlinx.coroutines.*
import java.util.concurrent.atomic.AtomicBoolean


class MapsFragment : Fragment(),
        OnMapReadyCallback,
        GoogleMap.OnMarkerDragListener,
        PoiDialogFragment.OnFragmentInteractionListener,
        ReportDialogFragment.OnFragmentInteractionListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraIdleListener {


    private lateinit var map: GoogleMap
    private var cameraPosition: CameraPosition? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var latitide: String
    private lateinit var longitude: String
    private lateinit var currentLocation: Location
    private var mapViewModel: MapViewModel? = null
    private var peopleViewModel: PeopleViewModel? = null
    private var stationVewModel : StationViewModel? = null
    private var previousBounds: LatLngBounds? = null
    private var markers: MutableList<Marker> = mutableListOf()
    private var job: Job? = null
    private var poiTypeList: ArrayList<PoiType>? = null
    private var poiCategoryList: ArrayList<POICategory>? = null
    private var poisList: ArrayList<POIs>? = null
    private var token: String? = null
    private val alreadyShowAccountWindowInfo: AtomicBoolean = AtomicBoolean(false)
    private lateinit var clusterManager: ClusterManager<PeopleItem>
    private lateinit var markerManager: MarkerManager
    private var thailandBound: LatLngBounds = LatLngBounds.builder().apply {
        include(TOP)
        include(BOTTOM)
        include(LEFT)
        include(RIGHT)
    }.build()
    private var builder: LatLngBounds.Builder = LatLngBounds.builder()
    private var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appDatabase = AppDatabase.getAppDatabase(context!!)

        if (savedInstanceState != null) {
            cameraPosition = savedInstanceState.getParcelable(CAMERA_POSITION) as CameraPosition
            mapViewModel?.poiCategory?.observe(this, Observer { })
            mapViewModel?.poiAll?.observe(this, Observer { })
            mapViewModel?.poi?.observe(this, Observer { })
            mapViewModel?.poiType?.observe(this, Observer { })
            mapViewModel?.poiTypeData?.observe(this, Observer { })
            mapViewModel?.poiAreaData?.observe(this, Observer { })
            mapViewModel?.syncPoiListDatabase()
            mapViewModel?.syncPoiCategorydatabase()
            mapViewModel?.syncPoiTypedatabase()
        }
    }

    lateinit var poiList: MutableList<PoiEntity>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_maps, container, false)

        activity!!.setTitle(R.string.title_activity_maps)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)

        setHasOptionsMenu(true)

        mapViewModel = ViewModelProviders.of(activity!!, InjectorUtil
                .provideMapViewModelFactory(activity!!.application))[MapViewModel::class.java]

        peopleViewModel = ViewModelProviders.of(activity!!, InjectorUtil
                .providePeopleViewModelFactory(activity!!.application))[PeopleViewModel::class.java]

        stationVewModel = ViewModelProviders.of(activity!!,InjectorUtil
                .provideStationViewModelFactory(activity!!.application))[StationViewModel::class.java]

        initInstance(view)

        poiList = appDatabase!!.mapDao().loadAllPoiLiveData()

        return view
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.searchMap -> {
                val intent = Intent(context, SearchLocationActivity::class.java).apply {
                    putParcelableArrayListExtra(POI_LIST, ArrayList(poiList))
                }
                startActivityForResult(intent, requestCode)
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if(menu != null){
            menu.clear()
        }
        inflater?.inflate(R.menu.menu_search_map, menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == OK) {
            if (requestCode == SearchLocationActivity.requestCode) {
                val poiItem = data?.extras?.getParcelable<PoiEntity>(POI_ITEM)

                if (poiItem != null){
                    val marker = MarkerOptions().apply {
                        position(LatLng(poiItem.lat.toDouble(), poiItem.lng.toDouble()))
                        title(poiItem.poiName)
                    }
                    Log.d("mapTitle", poiItem.poiName)
                    map.addMarker(marker)

                    var isInclude = false
                    if (thailandBound.contains(LatLng(poiItem.lat.toDouble(), poiItem.lng.toDouble()))) {
                        builder.include(LatLng(poiItem.lat.toDouble(), poiItem.lng.toDouble()))
                        isInclude = true
                    }
                    if (isInclude) {
                        map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), Util.dpToPX(context!!, PADDING_CAMERA)))
                    }

                    mapViewModel?.poiAll?.observe(viewLifecycleOwner, Observer { poiAll ->
                        poiAll?.pois?.forEach { poi ->
                            if (poi.poiId == poiItem.poiId) {
                                PoiDetailDialogFragment.newInstance(poiItem.poiId).show(childFragmentManager, PoiDetailDialogFragment.tag)
                            }
                        }
                    })
                }


            }
        }
    }

    private inner class PeopleClusterRenderer(context: Context, map: GoogleMap, clusterManager: ClusterManager<PeopleItem>)
        : DefaultClusterRenderer<PeopleItem>(context, map, clusterManager), GoogleMap.OnCameraIdleListener {
        override fun onCameraIdle() {
            if (peopleViewModel?.officerLocation?.value?.officerLocation?.size != null
                    && peopleViewModel!!.officerLocation.value!!.officerLocation.size > 0
                    && peopleViewModel!!.showLayers)
                addMarkerPeopleIfBound()
            showPeopleWindowInfo()
        }

        override fun onBeforeClusterItemRendered(item: PeopleItem?, markerOptions: MarkerOptions?) {
            super.onBeforeClusterItemRendered(item, markerOptions)
            var icon = createPeopleIcon(item!!.operationName, item.operationTypeId, item.name, item.lastName)
            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
        }

        override fun onClusterItemRendered(clusterItem: PeopleItem?, marker: Marker?) {
            super.onClusterItemRendered(clusterItem, marker)
            if (context != null) {
                val job = GlobalScope.async {
                    try {
                        var icon = createPeopleIcon(clusterItem!!.operationName, clusterItem.operationTypeId, clusterItem.name, clusterItem.lastName)
                        BitmapDescriptorFactory.fromBitmap(Glide.with(context!!).applyDefaultRequestOptions(
                                RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                .asBitmap()
                                .load(icon)
                                .submit(Util.dpToPX(context!!, POI_DP_SIZE), Util.dpToPX(context!!, POI_DP_SIZE)).get())
                    } catch (e: Exception) {
                        Log.d(tag, e.toString())
                        null
                    }
                }
                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        marker?.setIcon(job.await())
                        marker?.tag = MarkerInfo(clusterItem!!.accountID, Companion.MarkerType.PEOPLE)
                        if (peopleViewModel?.people?.value?.peekContent()?.accountID != null
                                && peopleViewModel!!.people.value!!.peekContent().accountID == clusterItem.accountID) {
                            if (alreadyShowAccountWindowInfo.compareAndSet(true, false))
                                marker?.showInfoWindow()
                        }
                    } catch (e: IllegalAccessException) {
                        Log.d(tag, e.toString())

                    }
                }
            }
        }

        override fun onBeforeClusterRendered(cluster: Cluster<PeopleItem>?, markerOptions: MarkerOptions?) {
            super.onBeforeClusterRendered(cluster, markerOptions)
            if (context != null) {
                val clusterIconGenerator = IconGenerator(context!!)
                cluster?.size.let {
                    when (it) {
                        in 0..49 -> {
                            clusterIconGenerator.setBackground(ContextCompat.getDrawable(context!!, R.drawable.icon_blue_cluster))
                            val clusterView = LayoutInflater.from(context!!).inflate(R.layout.item_cluster_text, null, false)
                            clusterIconGenerator.setContentView(clusterView)
                            val icon = clusterIconGenerator.makeIcon(it.toString())
                            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }
                        in 50..99 -> {
                            clusterIconGenerator.setBackground(ContextCompat.getDrawable(context!!, R.drawable.icon_yellow_cluster))
                            val clusterView = LayoutInflater.from(context!!).inflate(R.layout.item_cluster_text, null, false)
                            setMargins(clusterView.findViewById(R.id.amu_text), 0, Util.dpToPX(context!!, 17), 0, 0)
                            clusterIconGenerator.setContentView(clusterView)
                            val icon = clusterIconGenerator.makeIcon(it.toString())
                            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }

                        in 100..499 -> {
                            clusterIconGenerator.setBackground(ContextCompat.getDrawable(context!!, R.drawable.icon_green_cluster))
                            val clusterView = LayoutInflater.from(context!!).inflate(R.layout.item_cluster_text, null, false)
                            clusterIconGenerator.setContentView(clusterView)
                            setMargins(clusterView.findViewById(R.id.amu_text), 0, Util.dpToPX(context!!, 17), 0, 0)
                            //                        clusterIconGenerator.setTextAppearance(context!!, R.style.amu_ClusterIcon_TextAppearance)
                            val icon = clusterIconGenerator.makeIcon(it.toString())
                            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }

                        in 500..999 -> {
                            clusterIconGenerator.setBackground(ContextCompat.getDrawable(context!!, R.drawable.icon_pink_cluster))
                            val clusterView = LayoutInflater.from(context!!).inflate(R.layout.item_cluster_text, null, false)
                            clusterIconGenerator.setContentView(clusterView)
                            setMargins(clusterView.findViewById(R.id.amu_text), Util.dpToPX(context!!, 9), Util.dpToPX(context!!, 27), 0, 0)
                            //                        clusterIconGenerator.setTextAppearance(context!!, R.style.amu_ClusterIcon_TextAppearance)
                            val icon = clusterIconGenerator.makeIcon(it.toString())
                            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }
                        else -> {
                            clusterIconGenerator.setBackground(ContextCompat.getDrawable(context!!, R.drawable.icon_purple_cluster))
                            val clusterView = LayoutInflater.from(context!!).inflate(R.layout.item_cluster_text, null, false)
                            clusterIconGenerator.setContentView(clusterView)
                            setMargins(clusterView.findViewById(R.id.amu_text), Util.dpToPX(context!!, 15), Util.dpToPX(context!!, 35), 0, 0)
                            //                        clusterIconGenerator.setTextAppearance(context!!, R.style.amu_ClusterIcon_TextAppearance)
                            val icon = clusterIconGenerator.makeIcon(it.toString())
                            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }
                    }
                }
            }

        }

        override fun onClusterRendered(cluster: Cluster<PeopleItem>?, marker: Marker?) {
            super.onClusterRendered(cluster, marker)
        }
    }

    private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val mlp = view.layoutParams as ViewGroup.MarginLayoutParams
            mlp.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val markerInfo: MarkerInfo? = marker.tag as? MarkerInfo
        markerInfo?.let {
            marker.showInfoWindow()
        }

        Log.d("markerInfo", markerInfo.toString())
//        if (markerInfo == null) {
//            marker.title = "แจ้งเบาะแส"
//            marker.showInfoWindow()
//        }

        return false
    }

    override fun onCameraIdle() {
        addMarkerPoiIfBound()
    }

    private fun initInstance(view: View?) {
        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        token = mapViewModel?.getToken()

        mapViewModel?.poiType?.observe(this, Observer { event ->
            event?.poiType?.let {
                poiTypeList = ArrayList(it)
            }

        })
        mapViewModel?.poiCategory?.observe(this, Observer { event ->
            event?.poiCategory?.let {
                poiCategoryList = ArrayList(it)
            }

        })

        mapViewModel?.poiAll?.observe(this, Observer { event ->
            event?.pois?.let {
                poisList = ArrayList(it)
            }
        })

        stationVewModel?.loadStation(Token(token!!))
        stationVewModel?.stationData?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { wrapper ->
                wrapper.stationWrapper.stations.let { station ->
                    station.stations.forEach {

                    }
                }
            }
        })

        setUpView(view!!)
    }

    private fun addPeopleItems(people: MutableList<OfficerLocation>) {

        val addPeopleJob = GlobalScope.async {
            val peopleItemList = mutableListOf<PeopleItem>()
            people.forEach {
                peopleItemList.add(PeopleItem(it.lat.toDouble(), it.lng.toDouble(), it.accountID, it.firstName,
                        "https://idc2.klickerlab.com/img/ORGANIZATION/1/1_resized.jpg", it.lastName, it.accountID, it.operationTypeID, it.operationName))
            }
            clusterManager.addItems(peopleItemList)
            if (peopleItemList.size != 0) {

            }
        }

        GlobalScope.launch(Dispatchers.Main) {
            try {
                addPeopleJob.await()
            } catch (e: CancellationException) {
                Log.d(tag, e.toString())
            }
            clusterManager.cluster()
        }
    }


    private fun showPeopleWindowInfo() {
        if (alreadyShowAccountWindowInfo.get()) {
            val peopleMarker = clusterManager.markerCollection.markers.asSequence().filter { marker ->
                val peopleInfo = marker.tag as? MarkerInfo
                peopleInfo != null && peopleViewModel!!.people.value!!.peekContent().operationID == peopleInfo.id
            }.take(1).toList()
            if (peopleMarker.isNotEmpty()) {
                peopleMarker[0].showInfoWindow()
                alreadyShowAccountWindowInfo.set(false)
            }
        }
    }

    private fun addMarkerPoiIfBound() {
        val bound = map.projection.visibleRegion.latLngBounds
        val placePoiJob = GlobalScope.async {
            mapViewModel?.poiAll?.value?.pois?.forEach { poi ->
                val latLng = LatLng(poi.lat.toDouble(), poi.lng.toDouble())
                if ((previousBounds != null && !previousBounds!!.contains(latLng)) || previousBounds == null) {
                    if (checkFilterPoiTypes(poi) && bound.contains(latLng)) {
                        var url: String? = null
                        GlobalScope.run {
                            mapViewModel?.poiType?.value?.poiType?.forEach { poiType ->
                                if (poiType.poiTypeId == poi.pOITypeID) {
                                    url = poiType.icon
                                    return@run
                                }
                            }
                        }

                        if (url != null) {
                            val marker = MarkerOptions().apply {
                                position(latLng)
                                title(poi.poiName)
                                Log.d("mapTitle", poi.poiName)
                            }

                            val bitmapDescriptor = async {
                                BitmapDescriptorFactory.fromBitmap(Glide.with(this@MapsFragment)
                                        .applyDefaultRequestOptions(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
                                        .asBitmap().load(url)
                                        .submit(Util.dpToPX(context!!, POI_DP_SIZE), Util.dpToPX(context!!, POI_DP_SIZE)).get())
                            }

                            launch(Dispatchers.Main) {
                                marker.icon(bitmapDescriptor.await())
                                markers.add(map.addMarker(marker).apply {
                                    tag = MarkerInfo(poi.poiId.toInt(), Companion.MarkerType.MAP)
                                })
                            }
                        }
                    }
                } else {
                    if (!checkFilterPoiTypes(poi)) {
                        launch(Dispatchers.Main) {
                            val iterator = markers.iterator()
                            val latLong = LatLng(poi.lat.toDouble(), poi.lng.toDouble())
                            while (iterator.hasNext()) {
                                val marker = iterator.next()
                                if (marker.position == latLong) {
                                    marker.remove()
                                    iterator.remove()
                                }
                            }
                        }
                    }
                }
            }
        }

        job?.cancel()
        job = GlobalScope.launch {
            placePoiJob.await()
            previousBounds = bound
        }

        val iterator = markers.iterator()
        while (iterator.hasNext()) {
            val marker = iterator.next()
            if (!bound.contains(marker.position)) {
                marker.remove()
                iterator.remove()
            }
        }

    }

    private fun addMarkerPeopleIfBound() {
        val bound = map.projection.visibleRegion.latLngBounds
        val placePeopleJob = GlobalScope.async {
            peopleViewModel?.officerLocation?.value?.officerLocation?.forEach { people ->
                val latLng = LatLng(people.lat.toDouble(), people.lng.toDouble())
                if ((previousBounds != null && !previousBounds!!.contains(latLng) || previousBounds == null)) {

                }
            }
        }
        markerManager = MarkerManager(map)
        clusterManager = ClusterManager(context, map, markerManager)
        clusterManager.setOnClusterClickListener {
            Log.d(tag, "people cluster")
            it.items
            false
        }

        map.setOnCameraMoveListener(this)
        clusterManager.renderer = PeopleClusterRenderer(context!!, map, clusterManager)

        job?.cancel()
        job = GlobalScope.launch {
            placePeopleJob.await()
            previousBounds = bound
        }

        val iterator = markers.iterator()
        while (iterator.hasNext()) {
            val marker = iterator.next()
            if (!bound.contains(marker.position)) {
                marker.remove()
                iterator.remove()
            }
        }
    }


    // draw rec to icon people canvas
    private fun createPeopleIcon(operationName: String,
                                 operationTypeId: Int,
                                 firstName: String,
                                 lastName: String): Bitmap {

        val scaleDensity = getResources().getDisplayMetrics().scaledDensity
        val refDensity = 2.6
        val k = 1 + ((scaleDensity - refDensity) / refDensity)
        val width = 170 * k
        val height = 150 * k
        val recOutHeight = height - 30 * k
        val recInHeight = recOutHeight - 10 * k
        val recInWidth = width - 10 * k


        var statusColor = 0xff808080.toInt()
        var typeColor = 0xff7FFF00.toInt()

        var bitmap: Bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val paint = Paint()

        // สี่เหลี่ยมอันนอก
        val outRec = RectF(0f, 0f, width.toFloat(), recOutHeight.toFloat())
        paint.style = Paint.Style.FILL
        paint.color = statusColor
        canvas.drawRoundRect(outRec, 30 * k.toFloat(), 30 * k.toFloat(), paint)

        paint.strokeWidth = 4f
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.isAntiAlias = true

        val midPoint = width / 2

        // สามเหลี่ยม
        val path = Path()
        path.setFillType(Path.FillType.EVEN_ODD)
        path.moveTo(midPoint.toFloat() - 20 * k.toFloat(), recOutHeight.toFloat())
        path.lineTo(midPoint.toFloat(), height.toFloat())
        path.lineTo(midPoint.toFloat() + 20 * k.toFloat(), recOutHeight.toFloat())
        path.lineTo(midPoint.toFloat() - 20 * k.toFloat(), recOutHeight.toFloat())
        canvas.drawPath(path, paint)

        val operationColor = operationTypeId + 0xff0000F5

        // สี่เหลี่ยมอันใน
        val inRec = RectF(10 * k.toFloat(), 10 * k.toFloat(), recInWidth.toFloat(), recInHeight.toFloat())
        paint.style = Paint.Style.FILL
        paint.color = operationColor.toInt()
        canvas.drawRoundRect(inRec, 20 * k.toFloat(), 20 * k.toFloat(), paint)

        // สี่เเหลี่ยมเล็ก
        val smallRec = RectF(30 * k.toFloat(), 100 * k.toFloat(), (width.toFloat() / 2) * 1.6f, (height.toFloat() / 2) * 1.4f)
        paint.style = Paint.Style.FILL
        paint.color = typeColor
        canvas.drawRoundRect(smallRec, 20 * k.toFloat(), 20 * k.toFloat(), paint)

        val textPaint = Paint()
        textPaint.style = Paint.Style.FILL
        textPaint.color = Color.WHITE
        textPaint.isAntiAlias = true
        textPaint.textSize = 26 * k.toFloat()
        paint.isFakeBoldText = true
        textPaint.textAlign = Paint.Align.CENTER
        canvas.drawText(firstName, width.toFloat() / 2, 35 * k.toFloat(), textPaint)
        canvas.drawText(lastName, width.toFloat() / 2, 65 * k.toFloat(), textPaint)
        canvas.drawText(operationName, width.toFloat() / 2, 95 * k.toFloat(), textPaint)
        return bitmap
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = false
        map.setOnMarkerClickListener(this)
        checkPermissionAndSetUpMap()
        map.setOnCameraIdleListener(this)
        addMarkerPoiIfBound()

        map.setOnInfoWindowClickListener(object : GoogleMap.OnInfoWindowClickListener {
            override fun onInfoWindowClick(marker: Marker?) {
                Log.d("markerTag", marker?.tag.toString())
                marker?.title = "แจ้งเบาะแส"
                val markerInfo: MarkerInfo? = marker?.tag as? MarkerInfo
                marker?.showInfoWindow()
                if (marker?.tag == null) {
                    FragmentCreateReport(currentLocation)
                } else {
                    markerInfo?.let {
                        marker.showInfoWindow()
                        mapViewModel?.poiAll?.observe(viewLifecycleOwner, Observer { poiAll ->
                            poiAll?.pois?.forEach { poi ->
                                if (poi.poiId == it.id && it.type == MarkerType.MAP) {
                                    PoiDetailDialogFragment.newInstance(poi.poiId).show(childFragmentManager, PoiDetailDialogFragment.tag)
                                }
                            }
                        })

                        peopleViewModel?.officerLocation?.observe(viewLifecycleOwner, Observer { peopleData ->
                            peopleData?.officerLocation?.forEach { people ->
                                if (people.accountID == it.id && it.type == MarkerType.PEOPLE) {
                                    PeopleDetailDialogFragment.newInstance(people.accountID).show(childFragmentManager, PeopleDetailDialogFragment.tag)
                                }
                            }
                        })
                    }
                }

            }

        })

        // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE,this)

        markerManager = MarkerManager(map)
        clusterManager = ClusterManager(context, map, markerManager)
//        map.setOnMarkerClickListener(markerManager)
        markerManager.newCollection(PEOPLE).setOnMarkerClickListener(this)
//        clusterManager.setOnClusterClickListener {
//            Log.d(tag, "people cluster")
//            it.items
//            false
//        }
        //      clusterManager.setOnClusterItemClickListener { false }
        //       map.setOnCameraIdleListener(clusterManager)
//        map.setOnCameraMoveListener(this)
        clusterManager.renderer = PeopleClusterRenderer(context!!, map, clusterManager)

        if (peopleViewModel?.officerLocation?.value != null)
            addPeopleItems(peopleViewModel!!.officerLocation.value!!.officerLocation)
    }

    private fun checkFilterPoiTypes(poi: POIs): Boolean {
        mapViewModel?.poiType?.value?.poiType?.forEach {
            if (it.poiTypeId.toInt() == poi.pOITypeID.toInt() && it.checkedItem)
                return true
        }
        return false
    }

    override fun onDialogShowLayerFilter() {
        if (peopleViewModel?.officerLocation?.value != null)
            addPeopleItems(peopleViewModel!!.officerLocation.value!!.officerLocation)
    }

    private class MarkerInfo(val id: Int, val type: MarkerType)

    private fun placeMarkerOnMap(location: LatLng) {
        var markerOptions = MarkerOptions().position(location)
        latitide = "${location.latitude}"
        longitude = location.longitude.toString()

        map.clear()

        map.setOnMapLongClickListener(object : GoogleMap.OnMapLongClickListener {
            override fun onMapLongClick(latLng: LatLng?) {
                latLng?.let { markerOptions.position(it) }
                map.addMarker(markerOptions)
            }
        })

        map.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(p0: LatLng?) {
                map.clear()
            }
        })

        map.addMarker(markerOptions)

    }

    private fun setUpView(view: View) {
        view.ibExpand.setOnClickListener {
            if (view.llGroup.visibility == View.GONE) {
                view.llGroup.visibility = View.VISIBLE
                view.ibExpand.setImageResource(R.drawable.icon_arrow_down_active_map)
            } else {
                view.llGroup.visibility = View.GONE
                view.ibExpand.setImageResource(R.drawable.icon_arrow_down_map)
            }
        }

        view.ibFilterPoi.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_poi_active)
            PoiDialogFragment.newInstance(poiTypeList!!, poiCategoryList!!).run {
                isCancelable = true
                show(this@MapsFragment.childFragmentManager, PoiDialogFragment.tag)
            }
        }
        view.ibFilterPeople.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_people_active)

            peopleViewModel?.officerLocation?.observe(viewLifecycleOwner, Observer { event ->
                event?.officerLocation?.let {
                    if (this::clusterManager.isInitialized && peopleViewModel!!.showLayers) {
                        addPeopleItems(it)
                        var isInclude = false
                        it.forEach { people ->
                            if (thailandBound.contains(LatLng(people.lat.toDouble(), people.lng.toDouble()))) {
                                builder.include(LatLng(people.lat.toDouble(), people.lng.toDouble()))
                                isInclude = true
                            }
                        }
                        if (isInclude)
                            map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), Util.dpToPX(context!!, PADDING_CAMERA)))
                    } else if (!peopleViewModel!!.showLayers)
                        Log.d(tag, "unlock")
                }
            })
        }
        view.ibFilterReport.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_report_active)
            ReportDialogFragment.newInstance().run {
                isCancelable = true
                show(this@MapsFragment.childFragmentManager, ReportDialogFragment.tag)
            }
        }

        view.ibFilterConvertCoordinate.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_map_active)
            map.mapType = GoogleMap.MAP_TYPE_SATELLITE
        }
        view.ibFilterCurrent.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_lat_lng_active)
        }
        view.ibFilterSearchPeople.setOnClickListener {
            setImageButtonSrcInactive(view)
            (it as AppCompatImageButton).setImageResource(R.drawable.icon_search_people_active)
        }
    }

    private fun setImageButtonSrcInactive(view: View) {
        view.ibFilterPoi.setOnClickListener {
            view.ibFilterPoi.setImageResource(R.drawable.icon_poi)
            setUpView(view)
        }

        view.ibFilterPeople.setOnClickListener {
            view.ibFilterPeople.setImageResource(R.drawable.icon_people_non_active)
            setUpView(view)
        }

        view.ibFilterConvertCoordinate.setOnClickListener {
            view.ibFilterConvertCoordinate.setImageResource(R.drawable.icon_map_terrain)
            map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            view.ibFilterConvertCoordinate.setOnClickListener {
                typeMaps(view)
            }

        }

        view.ibFilterReport.setOnClickListener {
            view.ibFilterReport.setImageResource(R.drawable.icon_report_non_active)
            setUpView(view)
        }
        view.ibFilterCurrent.setOnClickListener {
            view.ibFilterCurrent.setImageResource(R.drawable.icon_lat_lng_non_active)
            setUpView(view)
        }

        view.ibFilterSearchPeople.setOnClickListener {
            view.ibFilterSearchPeople.setImageResource(R.drawable.icon_search_people_non_active)
            setUpView(view)
        }

    }

    fun typeMaps(view: View) {
        view.ibFilterConvertCoordinate.setImageResource(R.drawable.icon_map_non_active)
        map.mapType = GoogleMap.MAP_TYPE_NORMAL
        view.ibFilterConvertCoordinate.setOnClickListener {
            setUpView(view)
        }
    }

    private fun checkPermissionAndSetUpMap() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST)
                } else {
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST)
                }
            } else {
                setUpMap()
            }
        } else {
            setUpMap()
        }
    }

    @SuppressLint("MissingPermission")
    private fun setUpMap() {
        map.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            if (location != null) {
                currentLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                if (cameraPosition == null) {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                    // map.addMarker(MarkerOptions().position(currentLatLng).title("แจ้งเบาะแส"))
                } else {
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                }
            }

        }
        map.setOnMapLongClickListener {
            placeMarkerOnMap(it)

        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (this::map.isInitialized)
            outState.putParcelable(CAMERA_POSITION, map.cameraPosition)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST -> {
                if (grantResults.size == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setUpMap()
                } else {

                }
            }

        }
    }

    override fun onMarkerDragEnd(marker: Marker?) {

    }

    override fun onMarkerDragStart(marker: Marker) {

    }

    override fun onMarkerDrag(marker: Marker?) {

    }

    override fun checkboxCheck(poiId: Int, serverPath: String, layerPath: String, icChecked: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun checkboxUnCheck(layerID: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onDialogHideLayerFilter() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDialogClose() {
        if (view != null)
            setImageButtonSrcInactive(view!!)
    }

    override fun onCameraMove() {
        Log.d(tag, "camera")
    }


    fun FragmentCreateReport(currentLocation: Location) {
        val fragment = CreateReportClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        val bundle = Bundle()
        var lat = currentLocation.latitude
        var lng = currentLocation.longitude
        bundle.putDouble("latMap", lat)
        bundle.putDouble("lngMap", lng)
        fragment.arguments = bundle
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    companion object {
        val tag: String = MapFragment::class.java.simpleName
        private const val MARKER_TAG = "red marker"
        private val TOP = LatLng(20.455948, 99.950047)
        private val BOTTOM = LatLng(5.654315, 101.144561)
        private val LEFT = LatLng(18.559516, 97.373000)
        private val RIGHT = LatLng(14.984830, 105.587202)
        private const val PADDING_CAMERA = 40
        private const val INCREASE_HEIGHT = 80
        private const val POI_DP_SIZE = 30
        private const val PERMISSION_REQUEST = 1
        private const val CAMERA_POSITION = "camera_position"
        private const val PEOPLE = "people"
        private const val POI = "poi"
        private const val SINGLE = "single"
        private const val OPERATION = "operation"
        @JvmStatic
        fun newInstance() =
                MapsFragment().apply {

                }


        enum class MarkerType {
            MAP, REPORT, PEOPLE, POI, SINGLE, OPERATION
        }


        private const val PEOPLE_COUNT = "people_count"
    }

}

