package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName


data class Poi(@SerializedName("ContactPerson") val contactPerson : String,
               @SerializedName("Description") val description : String,
               @SerializedName("DistrictName") val districtName : String,
               @SerializedName("Images") val images : MutableList<String>,
               @SerializedName("Lat") val lat : String,
               @SerializedName("Lng") val lng : String,
               @SerializedName("POIID") val poiId : String,
               @SerializedName("POIName") val poiName : String,
               @SerializedName("POITypeID") val poiTypeID : String,
               @SerializedName("ProvinceName") val provinceName:String,
               @SerializedName("SubdistrictName") val subdistrictName:String,
               @SerializedName("Tel") val tel : String){}