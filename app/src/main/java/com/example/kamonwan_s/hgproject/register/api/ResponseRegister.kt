package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class ResponseRegister(@SerializedName("ResponseCode") val responseCode : Int,
                            @SerializedName("ResponseMessage") val responseMessage:String)