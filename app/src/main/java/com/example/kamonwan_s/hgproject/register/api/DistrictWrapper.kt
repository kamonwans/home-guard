package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class DistrictWrapper(@SerializedName("LoadDistrictResult") val districtList: MutableList<District>) {
}