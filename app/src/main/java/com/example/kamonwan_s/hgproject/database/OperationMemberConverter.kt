package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.TypeConverter
import com.example.kamonwan_s.hgproject.operation.api.OperationMembers
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType

class OperationMemberConverter : BaseListConverters<OperationMembers>() {
    @TypeConverter
    override fun fromString(value: String): ArrayList<OperationMembers>? {
        val typeToken = object : TypeToken<ArrayList<OperationMembers>>() {}
        val type = typeToken.type as ParameterizedType
        val list = GsonBuilder().create().fromJson<ArrayList<OperationMembers>>(value, type)
        return list ?: ArrayList()
    }


}