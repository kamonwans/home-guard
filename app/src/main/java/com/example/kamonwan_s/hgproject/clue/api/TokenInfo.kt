package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class TokenInfo(@SerializedName("Token") val token : String)