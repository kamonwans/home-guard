package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class ResponseList(@SerializedName("ResponseCode") val responseCode : Int,
                        @SerializedName("ResponseMessage") val responseMessage : String)