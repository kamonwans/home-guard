package com.example.kamonwan_s.hgproject.database

import com.example.kamonwan_s.hgproject.poi.api.POICategory
import com.example.kamonwan_s.hgproject.poi.api.POIs
import com.example.kamonwan_s.hgproject.poi.api.PoiListData
import com.example.kamonwan_s.hgproject.poi.api.PoiType

data class PoiAreaData(val poiType : MutableList<PoiType>,
                       val poiCategory: MutableList<POICategory>,
                       val poiList : MutableList<POIs>)