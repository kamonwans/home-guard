package com.example.kamonwan_s.hgproject.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface MapDao {
    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertProvince(province : List<ProvinceEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertDistrict(district : List<DistrictEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertSubDistrict(subDistrict : List<SubDistrictEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOrganizaton(organization : List<OrganizationEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertPoiType(poiType : List<PoiTypeEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertPoiList(poiList : List<PoiEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertPoiCategory(poiCategory : List<PoiCategoryEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertAccountContactList(accountContact : List<AccountContactEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertAccount(account : AccountEntity)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperation(operations: List<OperationsEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperationStatus(operationStatus : List<OperationStatusEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperationType(operationType : List<OperationTypeEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperationPois(operationPois : List<OperationPOIsEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperationMember(operationMember : List<OperationMembersEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportListOwn(reportOwn: List<ReportOwnListEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportListOrganization(reportOwn: List<ReportOrganizationListEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportListFavorite(reportOwn: List<ReportFavoriteListEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportImage(reportImage : List<ReportImageEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportType(reportType: List<ReportTypeEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertReportStatus(reportStatus: List<ReportStatusesEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertNewsType(newsType : List<NewsTypeEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertNewsList(newsList : List<NewsListEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertNewsImage(newsImage : List<NewsImageEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertOperationImage(operationImage : List<OperationImageEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertChatContent(chatContent : List<ContentChatListEntity>)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertChatContentRead(chatContentRead : ChatContentReadedEntity)

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertStation(stations: List<StationsEntity>)

    @Query("DELETE FROM province")
    fun deleteAllProvince()

    @Query("DELETE FROM district")
    fun deleteAllDistrict()

    @Query("DELETE FROM sub_district")
    fun deleteAllSubDistrict()

    @Query("DELETE FROM poi_type")
    fun deleteAllPoiType()

    @Query("DELETE FROM organization")
    fun deleteAllOrganization()

    @Query("DELETE FROM poi")
    fun deleteAllPoiList()

    @Query("DELETE FROM poi_category")
    fun deleteAllPoiCategory()

    @Query("DELETE FROM report_type")
    fun deleteAllReportType()

    @Query("DELETE FROM account")
    fun deleteAllAccount()

//    @Query("DELETE FROM operations")
//    fun deleteAllOperations()

    @Query("DELETE FROM account_contacts")
    fun deleteAllAccountContact()

    @Query("DELETE FROM operation_status")
    fun deleteAllOperationStatus()

    @Query("DELETE FROM operation_type")
    fun deleteAllOperationType()

    @Query("DELETE FROM report_status")
    fun deleteAllReportStatus()

//    @Query("DELETE FROM operation_pois")
//    fun deleteAllOperationsPoi()
//
//    @Query("DELETE FROM operation_member")
//    fun deleteAllOperationsMember()

    @Query("DELETE FROM content_read")
    fun deleteAllContentRead()

    @Query("DELETE FROM station")
    fun deleteAllStation()

    @Update
    fun updateProvince(province: MutableList<ProvinceEntity>)

    @Update
    fun updateDistrict(district: MutableList<DistrictEntity>)

    @Update
    fun updateSubDistrict(subDistrict: MutableList<SubDistrictEntity>)

    @Update
    fun updateOrganization(organization: MutableList<OrganizationEntity>)

    @Update
    fun updatePoiLis(poiList: List<PoiEntity>)

    @Update
    fun updatePoiCategory(poiCategory: List<PoiCategoryEntity>)

    @Update
    fun updatePoiType(poiType: List<PoiTypeEntity>)

    @Update
    fun updateAccountContact(accountContact: List<AccountContactEntity>)

    @Update
    fun updateOperationType(operationType: List<OperationTypeEntity>)

    @Update
    fun updateOperationStatus(operationStatus: List<OperationStatusEntity>)

    @Update
    fun updateReportType(reportType: List<ReportTypeEntity>)

    @Update
    fun updateReportStatus(reportStatus: List<ReportStatusesEntity>)

    @Update
    fun updateAccount(account: List<AccountEntity>)


    @Update
    fun updateContentRead(contentRead: List<ChatContentReadedEntity>)

    @Update
    fun updateStation(stations: List<StationsEntity>)

//    @Update
//    fun updateOperationPois(operationPois: List<OperationPOIs>)
//
//    @Update
//    fun updateOperationMember(operationMember: List<OperationMembers>)
//
//    @Update
//    fun updateOperations(operationList: List<Operations>)

    @Update
    fun updateNewsType(newsType: List<NewsTypeEntity>)

    @Query("SELECT * FROM province ORDER BY province_id")
    fun loadAllProvinceLiveData(): MutableList<ProvinceEntity>

    @Query("SELECT * FROM district ORDER BY district_name")
    fun loadAllDistrictLiveData(): MutableList<DistrictEntity>

    @Query("SELECT * FROM sub_district ORDER BY sub_district_name")
    fun loadAllSubDistrictLiveData(): MutableList<SubDistrictEntity>

    @Query("SELECT * FROM poi_type ORDER BY poi_type_short_name")
    fun loadAllPoiTypeLiveData(): MutableList<PoiTypeEntity>

    @Query("SELECT * FROM organization ORDER BY organization_name")
    fun loadAllOrganizationLiveData(): MutableList<OrganizationEntity>

    @Query("SELECT * FROM poi ORDER BY poi_name")
    fun loadAllPoiLiveData(): MutableList<PoiEntity>

    @Query("SELECT * FROM account_contacts ORDER BY account_contacts_first_name")
    fun loadAllAccountContactLiveData() : MutableList<AccountContactEntity>


    @Query("SELECT * FROM poi_category ORDER BY poi_category_id")
    fun loadAllPoiCategoryLiveData(): MutableList<PoiCategoryEntity>

    @Query("SELECT * FROM operations ORDER BY operations_id")
    fun loadAllOperations() : MutableList<OperationsEntity>

    @Query("SELECT * FROM operation_type ORDER BY operation_type_id")
    fun loadAllOperationType() : MutableList<OperationTypeEntity>

    @Query("SELECT * FROM operation_status ORDER BY operation_status_id")
    fun loadAllOperationStatus() : MutableList<OperationStatusEntity>

    @Query("SELECT * FROM operation_pois ORDER BY operation_pois_poi_id")
    fun loadAllOperationPois() : MutableList<OperationPOIsEntity>

    @Query("SELECT * FROM operation_member ORDER BY operation_member_operation_id")
    fun loadAllOperationMember() : MutableList<OperationMembersEntity>

    @Query("SELECT * FROM news_type ORDER BY news_type_id")
    fun loadAllNewsType() : MutableList<NewsTypeEntity>

    @Query("SELECT * FROM report_type ORDER BY report_type_id")
    fun loadAllReportType() : MutableList<ReportTypeEntity>

    @Query("SELECT * FROM report_status ORDER BY report_status_id")
    fun loadAllReportStatus() : MutableList<ReportStatusesEntity>

    @Query("SELECT * FROM account ORDER BY account_account_id")
    fun loadAllAccountData(): AccountEntity

    @Query("SELECT * FROM poi WHERE poi_name LIKE :query")
    fun loadPoisList(query:String) : LiveData<MutableList<PoiEntity>>

    @Query("SELECT * FROM account_contacts WHERE account_contacts_first_name LIKE :query")
    fun loadAccountContactsList(query:String) : LiveData<MutableList<AccountContactEntity>>

    @Query("SELECT * FROM province ORDER BY province_id")
    fun syncProvince() : LiveData<MutableList<ProvinceEntity>>

    @Query("SELECT * FROM content_read ORDER BY chat_content_Id")
    fun loadAllContentRead() : ChatContentReadedEntity

    @Query("SELECT * FROM station ORDER BY station_id")
    fun loadAllStationLiveData(): MutableList<StationsEntity>


    @Query("SELECT COUNT(*) FROM poi_type")
    fun getPoiTypeCount(): Int
}