package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationListOwnWrapper(@SerializedName("Data") val operationList : OperationList,
                                   @SerializedName("Response") val responseList :ResponseList)