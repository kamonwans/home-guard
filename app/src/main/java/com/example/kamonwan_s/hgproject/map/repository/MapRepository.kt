package com.example.kamonwan_s.hgproject.map.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.*
import com.example.kamonwan_s.hgproject.map.api.PoiApi
import com.example.kamonwan_s.hgproject.poi.api.*
import com.example.kamonwan_s.hgproject.util.AppExecutors
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapRepository private constructor(private val executor: AppExecutors,
                                        private val poiApi: PoiApi,
                                        private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    fun loadPoi(app: Application, poi: GetPOI): LiveData<PoiResponse> {
        val poiData = MutableLiveData<PoiResponse>()
        poiApi.loadPoi(poi).enqueue(object : Callback<PoiResponse?> {
            override fun onFailure(call: Call<PoiResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<PoiResponse?>, response: Response<PoiResponse?>) {
                if (response.isSuccessful)
                    poiData.value = response.body()!!
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return poiData
    }

    fun loadPoiList(token: Token): LiveData<PoiListResponse> {
        val poiListData = MutableLiveData<PoiListResponse>()
        poiApi.loadPoiList(token).enqueue(object : Callback<PoiListResponse?> {
            override fun onFailure(call: Call<PoiListResponse?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<PoiListResponse?>, response: Response<PoiListResponse?>) {
                if (response.isSuccessful) {
                    poiListData.value = response.body()!!
                    var dbInfoList = response.body()!!.poiListWrapper

                    executor.diskIO.execute {
                        try {
                            var poi = dbInfoList.poiListData.pois
                            var poiList = ArrayList<PoiEntity>()

                            poi?.forEach {
                                var poiEntity = PoiEntity(it.poiId,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.poiName,
                                        it.pOITypeID)
                                poiList.add(poiEntity)
                            }
                            if (dbInfoList.poiListData.pois.isNotEmpty()) {
                                mapDao.insertPoiList(poiList)
                            } else {
                                mapDao.loadAllPoiLiveData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return poiListData
    }

    fun loadPoiType(token: Token): LiveData<PoiTypeResponse> {
        val poiTypeData = MutableLiveData<PoiTypeResponse>()
        poiApi.loadPoiType(token).enqueue(object : Callback<PoiTypeResponse?> {
            override fun onFailure(call: Call<PoiTypeResponse?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<PoiTypeResponse?>, response: Response<PoiTypeResponse?>) {
                if (response.isSuccessful) {
                    poiTypeData.value = response.body()
                    Log.d(tag, response.body().toString())
                    var dbInfoList = response.body()!!.poiTypeWrapper

                    executor.diskIO.execute {
                        try {
                            var poiType = dbInfoList.poiTypeData.poiType
                            var poiTypeList = ArrayList<PoiTypeEntity>()
                            poiType?.forEach {
                                var poiTypeEntity = PoiTypeEntity(it.poiTypeId,
                                        it.icon,
                                        it.modifiedDate,
                                        it.poiCategoryId,
                                        it.shortName)
                                poiTypeList.add(poiTypeEntity)
                            }
                            if (dbInfoList.poiTypeData.poiType.isNotEmpty()) {
                                mapDao.insertPoiType(poiTypeList)
                            } else {
                                mapDao.loadAllPoiTypeLiveData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    syncDatabaseError.value = SingleEvent(response.errorBody()!!.string())

            }

        })
        return poiTypeData
    }

    fun loadPoiCategory(token: Token): LiveData<PoiCategoryResponse> {
        val poiCategoryData = MutableLiveData<PoiCategoryResponse>()
        poiApi.loadPoiCategory(token).enqueue(object : Callback<PoiCategoryResponse> {
            override fun onFailure(call: Call<PoiCategoryResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<PoiCategoryResponse>, response: Response<PoiCategoryResponse>) {
                if (response.isSuccessful) {
                    poiCategoryData.value = response.body()
                    Log.d(tag, response.body().toString())
                    var dbInfoList = response.body()!!.categoryWrapper
                    executor.diskIO.execute {
                        try {
                            var poiCategory = dbInfoList.poiCategoryData.poiCategory
                            var poiCategoryList = ArrayList<PoiCategoryEntity>()
                            poiCategory?.forEach {
                                val poiCategoryEntity = PoiCategoryEntity(
                                        it.poiCategoryID,
                                        it.modifiedDate,
                                        it.shortName
                                )

                                poiCategoryList.add(poiCategoryEntity)
                            }
                            if (dbInfoList.poiCategoryData.poiCategory.isNotEmpty()) {
                                mapDao.insertPoiCategory(poiCategoryList)
                            }else{
                                mapDao.loadAllPoiCategoryLiveData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else {
                    Log.d(tag, response.errorBody()!!.string())
                }
            }

        })
        return poiCategoryData
    }

    fun syncPoiTypeDatabase(token: Token): LiveData<Boolean> {
        val completeSync = MutableLiveData<Boolean>()
        poiApi.syncPOIType(token).enqueue(object : Callback<SyncPOITypeResponse> {
            override fun onFailure(call: Call<SyncPOITypeResponse>, t: Throwable) {
                if (t.message != null)
                    syncDatabaseError.value = SingleEvent(t.message!!)
                else
                    syncDatabaseError.value = SingleEvent(t.toString())
            }

            override fun onResponse(call: Call<SyncPOITypeResponse>, response: Response<SyncPOITypeResponse>) {
                if (response.isSuccessful) {
                    val dbInfoList = response.body()!!.syncPOITypeWrapper.poiTypeData
                  executor.diskIO.execute {
                      try {
                          var poiType = dbInfoList.poiType
                          var poiTypeList = ArrayList<PoiTypeEntity>()
                          poiType.forEach {
                              var poiType = PoiTypeEntity(
                                      it.poiTypeId,
                                      it.icon,
                                      it.modifiedDate,
                                      it.poiCategoryId,it.shortName
                              )
                              poiTypeList.add(poiType)
                          }
                          if (dbInfoList.poiType.isNotEmpty()){
                              mapDao.insertPoiType(poiTypeList)
                          }
                          completeSync.postValue(true)
                      }catch (e: SQLiteConstraintException){
                          syncDatabaseError.postValue(SingleEvent(e.toString()))
                          Log.d(tag, e.toString())
                      }catch (e: Exception){
                          syncDatabaseError.postValue(SingleEvent(e.toString()))
                          Log.d(tag, e.toString())
                      }
                  }
                }

            }
        })
        return completeSync
    }

    fun syncPoiCategoryDatabase(syncPoiInfo: SyncPoiInfo): LiveData<Boolean> {
        val completeSync = MutableLiveData<Boolean>()
        poiApi.syncPOICategorys(syncPoiInfo).enqueue(object : Callback<SyncPOICategorysResponse> {
            override fun onFailure(call: Call<SyncPOICategorysResponse>, t: Throwable) {
                if (t.message != null)
                    syncDatabaseError.value = SingleEvent(t.message!!)
                else
                    syncDatabaseError.value = SingleEvent(t.toString())
            }

            override fun onResponse(call: Call<SyncPOICategorysResponse>, response: Response<SyncPOICategorysResponse>) {
                if (response.isSuccessful) {
                    val dbInfoList = response.body()!!.syncPOICategorysWrapper.poiCategoryData
                    executor.diskIO.execute {
                        try {
                            var poiCategory = dbInfoList.poiCategory
                            var poiCategoryList = ArrayList<PoiCategoryEntity>()
                            poiCategory.forEach {
                                var poiCategoryEntity = PoiCategoryEntity(
                                        it.poiCategoryID,
                                        it.modifiedDate,
                                        it.shortName
                                )
                                poiCategoryList.add(poiCategoryEntity)
                            }
                            if (dbInfoList.poiCategory.isNotEmpty()){
                                mapDao.insertPoiCategory(poiCategoryList)
                            }
                            completeSync.postValue(true)
                        }catch (e: SQLiteConstraintException){
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }catch (e: Exception){
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }

            }
        })
        return completeSync
    }

    fun syncPoiListDatabase(syncPoiInfo: SyncPoiInfo): LiveData<Boolean> {
        val completeSync = MutableLiveData<Boolean>()
        poiApi.syncPOIList(syncPoiInfo).enqueue(object : Callback<SyncPOIListResponse> {
            override fun onFailure(call: Call<SyncPOIListResponse>, t: Throwable) {
                if (t.message != null)
                    syncDatabaseError.value = SingleEvent(t.message!!)
                else
                    syncDatabaseError.value = SingleEvent(t.toString())
            }

            override fun onResponse(call: Call<SyncPOIListResponse>, response: Response<SyncPOIListResponse>) {
                if (response.isSuccessful) {
                    val dbInfoList = response.body()!!.syncPOIListWrapper.poiListData
                    executor.diskIO.execute {
                        try {
                            var poi = dbInfoList.pois
                            var poiList = ArrayList<PoiEntity>()
                            poi.forEach {
                                var poiEntity = PoiEntity(
                                        it.poiId,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.poiName,
                                        it.pOITypeID
                                )
                                poiList.add(poiEntity)
                            }
                            if (dbInfoList.pois.isNotEmpty()){
                                mapDao.insertPoiList(poiList)
                            }
                            completeSync.postValue(true)
                        }catch (e: SQLiteConstraintException){
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }catch (e: Exception){
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }

            }
        })
        return completeSync
    }

    fun loadAllPoi(): LiveData<PoiAreaData> {
        val poiData = MutableLiveData<PoiAreaData>()
//        executor.diskIO.execute {
//            val poiType = mapDao.loadAllPoiTypeLiveData()
//            val poi = mapDao.loadAllPoiLiveData()
//            val poiCategory = mapDao.loadAllPoiCategoryLiveData()
//            poiData.postValue(PoiAreaData(poiType, poiCategory, poi))
//        }
        return poiData
    }

    companion object {
        @Volatile
        private var instance: MapRepository? = null
        private val tag = MapRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, poiApi: PoiApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: MapRepository(executor, poiApi, mapDao).apply {
                                instance = this
                            }
                }
    }

}