package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRooms(
        @SerializedName("AccountID")val accountID:String,
        @SerializedName("ChatContentTypeID")val chatContentTypeID :String,
        @SerializedName("ChatRoomID") val chatRoomID:String,
        @SerializedName("ChatRoomName") val chatRoomName:String,
        @SerializedName("ChatRoomPic")val chatRoomPic:String,
        @SerializedName("Content")val content:String,
        @SerializedName("IsGroup")val isGroup:String,
        @SerializedName("ModifiedDate")val modifiedDate:String,
        @SerializedName("OperationID")val operationID:String,
        @SerializedName("SenderID")val senderID:String)