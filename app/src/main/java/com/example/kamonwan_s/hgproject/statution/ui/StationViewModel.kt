package com.example.kamonwan_s.hgproject.statution.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.statution.api.LoadStationResponse
import com.example.kamonwan_s.hgproject.statution.api.Token
import com.example.kamonwan_s.hgproject.statution.repository.StationRepository

class StationViewModel(private val app : Application,private val stationRepository: StationRepository)
    :AndroidViewModel(app){

    var token = MediatorLiveData<Token>()

    val stationData : LiveData<SingleEvent<LoadStationResponse>> = Transformations.switchMap(token){
        stationRepository.loadStation(it)
    }

    fun loadStation(token: Token){
        this.token.value = token
    }
}