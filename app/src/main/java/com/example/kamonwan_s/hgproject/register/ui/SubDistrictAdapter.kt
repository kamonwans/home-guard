package com.example.kamonwan_s.hgproject.register.ui

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.register.api.SubDistrict

class SubDistrictAdapter(context: Context, @LayoutRes resource: Int, objects: MutableList<SubDistrict>)
    : ArrayAdapter<SubDistrict>(context, resource, objects) {

    init {
        objects.add(0, SubDistrict(SUB_DISTRICT_DEFAULT_ID,1, "กรุณาเลือกตำบล",0))
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return if (convertView == null) {
            val newView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_spinner_dropdown
                    , parent, false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.subDistrictName }
        }
        else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.subDistrictName }
        }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return if (convertView == null) {
            val newView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_spinner_dropdown
                    , parent, false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.subDistrictName }
        }
        else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.subDistrictName }
        }
    }
    companion object {
        const val SUB_DISTRICT_DEFAULT_ID = -1
    }
}