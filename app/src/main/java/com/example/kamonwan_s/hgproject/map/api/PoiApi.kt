package com.example.kamonwan_s.hgproject.map.api

import com.example.kamonwan_s.hgproject.poi.api.*
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface PoiApi {

    @POST("GetPOIList")
    fun loadPoiList(@Body token: Token): Call<PoiListResponse>

    @POST("GetPoi")
    fun loadPoi(@Body getPoi :GetPOI): Call<PoiResponse>

    @POST("LoadPOICategorys")
    fun loadPoiCategory(@Body token: Token): Call<PoiCategoryResponse>

    @POST("LoadPOIType")
    fun loadPoiType(@Body token: Token): Call<PoiTypeResponse>

    @POST("SyncPOIType")
    fun syncPOIType(@Body token: Token) : Call<SyncPOITypeResponse>

    @POST("SyncPOICategorys")
    fun syncPOICategorys(@Body syncPoiInfo: SyncPoiInfo) : Call<SyncPOICategorysResponse>

    @POST("SyncPOIList")
    fun syncPOIList(@Body syncPoiInfo: SyncPoiInfo) : Call<SyncPOIListResponse>

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/PoiService.svc/"
        private const val TIME_OUT: Long = 20
        val service: PoiApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(PoiApi::class.java)
        }
    }
}