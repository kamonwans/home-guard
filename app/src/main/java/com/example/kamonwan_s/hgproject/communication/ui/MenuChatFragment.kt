package com.example.kamonwan_s.hgproject.communication.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatContentListInfo
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.api.CreateChatContentPToPResponse
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.latlngmap.LocationMapsActivity
import kotlinx.android.synthetic.main.fragment_menu_chat.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class MenuChatFragment : BottomSheetDialogFragment() {

    private var communicationViewModel: CommunicationViewModel? = null
    private var token: String? = null
    private var accountID: String? = null
    private var chatRoomId: String? = null
    private var photoBody: RequestBody? = null
    private val multipartPhotoBodies: MutableList<MultipartBody.Part> = mutableListOf()
    private var adapter: ContentChatAdapter? = null
    private var name: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        name = arguments?.getString("name")
        accountID = arguments?.getString("accountId")
        chatRoomId = arguments?.getString("chatRoomId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_menu_chat, container, false)
        initInstance(view)

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]

        token = communicationViewModel?.getToken()
        return view
    }

    private fun initInstance(view: View) {

        view.imageSentLocation.setOnClickListener {
            val intent = Intent(context, LocationMapsActivity::class.java)
            startActivityForResult(intent, 2)
        }

        view.imageSelectPic.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent().apply {
                    action = Intent.ACTION_PICK
                    type = "image/*"
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)

                }
                startActivityForResult(intent
                        , 1)

            }

        })

        view.imageSelectVideo.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent().apply {
                    action = Intent.ACTION_PICK
                    type = "video/*"
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)

                }
                startActivityForResult(Intent.createChooser(intent,"select video")
                        , 3)
            }

        })

        view.imageSelectVoice.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent().apply {
                    action = Intent.ACTION_PICK
                    type = "video/mp3"
                    putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)

                }
                startActivityForResult(Intent.createChooser(intent,"select audio")
                        , 4)
            }

        })
    }

    private fun loadChatContentList() {
        communicationViewModel?.getChatContentList(ChatContentListInfo(token!!, chatRoomId.toString(), "0", "99", "0"))
        communicationViewModel?.chatContentList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatContentListWrapper ->
                chatContentListWrapper.chatContentListWrapper.chatContentListData.let { chatContentListData ->
                    if (chatContentListData != null) {
                        chatContentListData.chatContents.let { chatContents ->
                            adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                        }
                    }

                    if (adapter?.items == null) {
                        adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                    }

                    adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun createChatContentOnlyPic() {
        Log.d("dAccount", multipartPhotoBodies.size.toString())
        if (multipartPhotoBodies.size >= 1) {
            multipartPhotoBodies.forEach {
                CommunicationApi.service.createChatContentP2POnlyPic(
                        RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                        RequestBody.create(MediaType.parse("text/plain"), "2"),
                        RequestBody.create(MediaType.parse("text/plain"), token!!),
                        it).enqueue(object : Callback<CreateChatContentPToPResponse> {
                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                        Log.d("onFailure", t.message)
                    }

                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                        if (response.isSuccessful) {
                            dismiss()
                            loadChatContentList()
                            Toast.makeText(context, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                        } else
                            Log.d("onResponse", response.errorBody()!!.string())
                    }

                })
            }
            Toast.makeText(context, "กำลังอัพโหลดรูปภาพ...", Toast.LENGTH_SHORT).show()
        }
    }


    private fun createChatContentOnlyVideo() {
        Log.d("dAccount", multipartPhotoBodies.size.toString())
        if (multipartPhotoBodies.size >= 1) {
            multipartPhotoBodies.forEach {
                CommunicationApi.service.createChatContentP2POnlyPic(
                        RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                        RequestBody.create(MediaType.parse("text/plain"), "4"),
                        RequestBody.create(MediaType.parse("text/plain"), token!!),
                        it).enqueue(object : Callback<CreateChatContentPToPResponse> {
                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                        Log.d("onFailure", t.message)
                    }

                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                        if (response.isSuccessful) {
                            dismiss()
                            Toast.makeText(context, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                        } else
                            Log.d("onResponse", response.errorBody()!!.string())
                    }

                })
            }

            Toast.makeText(context, "กำลังอัพโหลดรูปภาพ...", Toast.LENGTH_SHORT).show()
        }
    }

    private fun createChatContentOnlyAudio() {
        Log.d("dAccount", multipartPhotoBodies.size.toString())
        if (multipartPhotoBodies.size >= 1) {
            multipartPhotoBodies.forEach {
                CommunicationApi.service.createChatContentP2POnlyPic(
                        RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                        RequestBody.create(MediaType.parse("text/plain"), "3"),
                        RequestBody.create(MediaType.parse("text/plain"), token!!),
                        it).enqueue(object : Callback<CreateChatContentPToPResponse> {
                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                        Log.d("onFailure", t.message)
                    }

                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                        if (response.isSuccessful) {
                            dismiss()
                            Toast.makeText(context, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                        } else
                            Log.d("onResponse", response.errorBody()!!.string())
                    }

                })
            }

            Toast.makeText(context, "กำลังอัพโหลดรูปภาพ...", Toast.LENGTH_SHORT).show()
        }
    }

    private fun selectMultiPhoto(uris: ArrayList<Uri>): ArrayList<File> {
        val arrayListFile = arrayListOf<File>()
        val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
        for (uri in uris) {
            val cursor: Cursor? = context?.contentResolver?.query(uri, filePathColumn, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(filePathColumn[0])
                path = it.getString(index)
            }
            if (path == null) {
                Log.d("xx", "path is null")
                continue
            }
            val file = File(path)
            arrayListFile.add(file)
        }
        return arrayListFile
    }

    private fun selectMultiVideo(uris: ArrayList<Uri>): ArrayList<File> {
        val videoListFile = arrayListOf<File>()
        val videoPathColumn: Array<String> = arrayOf(MediaStore.Video.Media.DATA)
        for (uri in uris) {
            val cursor: Cursor? = context?.contentResolver?.query(uri, videoPathColumn, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(videoPathColumn[0])
                path = it.getString(index)
            }
            if (path == null)
                continue
            val video = File(path)
            videoListFile.add(video)
        }
        return videoListFile
    }

    private fun selectMultiAudio(uris: ArrayList<Uri>): ArrayList<File> {
        val audioListFile = arrayListOf<File>()
        val audioPathColumn: Array<String> = arrayOf(MediaStore.Video.Media.DATA)
        for (uri in uris) {
            val cursor: Cursor? = context?.contentResolver?.query(uri, audioPathColumn, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(audioPathColumn[0])
                path = it.getString(index)
            }
            if (path == null)
                continue
            val audio = File(path)
            audioListFile.add(audio)
            Log.d("audioListFile", multipartPhotoBodies.size.toString())
        }
        return audioListFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // pic
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data?.clipData != null) {
            val uris = ArrayList<Uri>()
            for (index in 0 until data.clipData.itemCount) {
                uris.add(data.clipData.getItemAt(index).uri)
            }
            val files = selectMultiPhoto(uris)
            photoBody = RequestBody.create(null, "")
//            if (file != null) {
//                photoBody = RequestBody.create(MediaType.parse("image/*"), file?.name)
//            }
            var num = 1
            for (file in files) {
                val photoBody = RequestBody.create(MediaType.parse("image/*"), file)
                val photoForm = MultipartBody.Part.createFormData("file$num", file.name, photoBody)
                num++
                multipartPhotoBodies.add(photoForm)
            }
            createChatContentOnlyPic()

        }
        // location
        else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            val lat = data?.getStringExtra("result_lat")
            val lng = data?.getStringExtra("result_lng")
            Log.d("latLng", lat.toString())

            CommunicationApi.service.createChatContentP2POnlyLatLng(
                    RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                    RequestBody.create(MediaType.parse("text/plain"), "5"),
                    RequestBody.create(MediaType.parse("text/plain"), lat.toString()),
                    RequestBody.create(MediaType.parse("text/plain"), lng.toString()),
                    RequestBody.create(MediaType.parse("text/plain"), token)
            ).enqueue(object : Callback<CreateChatContentPToPResponse> {
                override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                    Log.d("onFailure", t.message)
                }

                override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                    if (response.isSuccessful) {
                        dismiss()
                        Toast.makeText(context, "ส่งข้อความสำเร็จแล้ว", Toast.LENGTH_SHORT).show()
                    } else
                        Log.d("onResponse", response.errorBody()!!.string())
                }

            })
        }
        //video
        else if (requestCode == 3 && resultCode == Activity.RESULT_OK && data?.clipData != null) {
            val uris = ArrayList<Uri>()
            for (index in 0 until data.clipData.itemCount) {
                uris.add(data.clipData.getItemAt(index).uri)
            }
            val files = selectMultiVideo(uris)
            photoBody = RequestBody.create(null, "")
            var num = 1
            for (file in files) {
                val photoBody = RequestBody.create(MediaType.parse("video/*"), file)
                val photoForm = MultipartBody.Part.createFormData("file$num", file.name, photoBody)
                num++
                multipartPhotoBodies.add(photoForm)
            }
            createChatContentOnlyVideo()
        }
        //audio
        else if (requestCode == 4 && resultCode == Activity.RESULT_OK && data?.clipData != null) {
            val uris = ArrayList<Uri>()
            for (index in 0 until data.clipData.itemCount) {
                uris.add(data.clipData.getItemAt(index).uri)
            }
            val files = selectMultiAudio(uris)
            photoBody = RequestBody.create(null, "")
            var num = 1
            for (file in files) {
                val photoBody = RequestBody.create(MediaType.parse("audio/*"), file)
                val photoForm = MultipartBody.Part.createFormData("file$num", file.name, photoBody)
                num++
                multipartPhotoBodies.add(photoForm)
            }
            createChatContentOnlyAudio()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }



    companion object {
        @JvmStatic
        fun newInstance() =
                MenuChatFragment().apply {

                }
    }
}
