package com.example.kamonwan_s.hgproject.operation.ui

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.work.*
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.map.ui.MapsFragment
import com.example.kamonwan_s.hgproject.operation.api.OperationInfo
import com.example.kamonwan_s.hgproject.operation.api.OperationWorkingInfo
import com.example.kamonwan_s.hgproject.operation.api.Token
import com.example.kamonwan_s.hgproject.operation.api.UpdateLocationInfo
import com.example.kamonwan_s.hgproject.operation.ui.SendLocationWorker.Companion.LATTITUDE_DATA
import com.example.kamonwan_s.hgproject.operation.ui.SendLocationWorker.Companion.LONGITUDE_DATA
import com.example.kamonwan_s.hgproject.operation.ui.SendLocationWorker.Companion.OPERATION_ID_DATA
import com.example.kamonwan_s.hgproject.operation.ui.SendLocationWorker.Companion.TOKEN_DATA
import com.example.kamonwan_s.hgproject.util.AppExecutors
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.fragment_operation.view.*
import java.util.concurrent.TimeUnit
import kotlin.math.ln


class OperationFragment : Fragment(), OperationAdapter.BtnClickListener,
        OperationAdapter.ClickUpdate,
        OperationAdapter.ImageMemberClickListener,
        OperationAdapter.ClickLocation,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private var adapter: OperationAdapter? = null
    private var adapterMember: MemberAdapter? = null
    private var operationViewModel: OperationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var token: String? = null
    private var offset = 0
    private var limit = 4

    private var googleApiClient: GoogleApiClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationProvider: FusedLocationProviderApi? = null
    private var location: Location? = null
    private var locationManager: LocationManager? = null
    private var isLocationEnable: Boolean? = null
    private var latitude: String? = null
    private var longitude: String? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private val executor: AppExecutors? = null
    private val mainHandler: Handler? = null

    @Volatile
    private var stopRunThread: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("ภารกิจ")

        createLocationRequest()

        googleApiClient = GoogleApiClient.Builder(context!!)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()

        requestLocationStart()
        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_operation, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_more_vert)

        setHasOptionsMenu(true)

        operationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideOperationViewModelFactory(activity!!.application))[OperationViewModel::class.java]
        initInstance(view)

        return view
    }

    private fun initInstance(view: View) {
        token = operationViewModel?.getToken()
        adapter = OperationAdapter(context!!, this, this, this, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewOperation.layoutManager = layoutManager
        view.recyclerViewOperation.itemAnimator = DefaultItemAnimator()
        view.recyclerViewOperation.adapter = adapter

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        operationViewModel?.loadOperationOwn(OperationInfo(token!!, 1, 0))
        operationViewModel?.operationOwn?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { operationWrapper ->
                operationWrapper.operationListOwnWrapper.operationList.let {
                    if (it != null) {

                        it.operations.let {
                            adapter?.items?.operations?.addAll(it)

                        }

                    }
                    if (adapter?.items == null) {
                        adapter?.items = operationWrapper.operationListOwnWrapper.operationList
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })

        operationViewModel?.loadOperationStatus(Token(token!!))
        operationViewModel?.operationStatus?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { operationStatusWrapper ->
                operationStatusWrapper.operationStatusWrapper.operationStatusData.let {
                    if (it != null) {
                        it.operationStatus.let {

                        }
                    }

                }
            }
        })
        operationViewModel?.loadOperationType(Token(token!!))
        operationViewModel?.operationType?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { operationTypeWrapper ->
                operationTypeWrapper.operationTypeWrapper.operationTypeData.let {
                    if (it != null) {
                        it.operationType.let {

                        }
                    }
                }
            }
        })
    }


    protected fun createLocationRequest() {
        locationProvider = LocationServices.FusedLocationApi
        locationRequest = LocationRequest()
        locationRequest?.setInterval(INTERVAL.toLong())
        locationRequest?.setFastestInterval(FASTEST_INTERVAL.toLong())
        locationRequest?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)

//        fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
//            var lat = location?.latitude
//            var lng = location?.longitude
//            Log.d("currentLoc", "currentLat : $lat currentLong: $lng")
//        }


        if (location == null) {
            requestLocationStart()
        } else if (location != null) {

        } else {
            Toast.makeText(context, "Location not Detected", Toast.LENGTH_SHORT).show()
        }

        if (googleApiClient!!.isConnected) {
            requestLocationStart()
        }

    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(location: Location?) {

    }

    override fun onBottomReached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    private fun requestLocationStart() {
        if (googleApiClient != null) {
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }

            locationRequest = LocationRequest.create()
            locationRequest?.run {
                setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                setInterval(INTERVAL.toLong())
                setFastestInterval(FASTEST_INTERVAL.toLong())

                //  Toast.makeText(context, "location: $INTERVAL fast $FASTEST_INTERVAL", Toast.LENGTH_SHORT).show()

                val builder = LocationSettingsRequest.Builder()
                builder.addLocationRequest(locationRequest!!)
                val locationSettingsRequest = builder.build()

                val settingClient = LocationServices.getSettingsClient(context!!)
                settingClient!!.checkLocationSettings(locationSettingsRequest)

                registerLocationListner()

            }
//            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
//            Toast.makeText(context, "Location update started ..............: ", Toast.LENGTH_SHORT).show()
            Log.v(tag, "Location update started ..............: ")
        }
    }

    private fun registerLocationListner() {
        // initialize location callback object
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                onLocationChanged(locationResult!!.getLastLocation())
            }
        }
        // add permission if android version is greater then 23
        if (Build.VERSION.SDK_INT >= 23 && checkPermission()) {
            LocationServices.getFusedLocationProviderClient(context!!).requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
        }


    }

    private fun checkPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true
        } else {
            requestPermissions()
            return false
        }
    }


    private fun requestPermissions() {
        ActivityCompat.requestPermissions(activity!!, arrayOf("Manifest.permission.ACCESS_FINE_LOCATION"), 1)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION) {
                registerLocationListner()
            }
        }
    }

    private fun requestLocationStop() {
        if (googleApiClient != null && googleApiClient?.isConnected!! && LocationServices.FusedLocationApi != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
            //    Toast.makeText(context, "Location update stopped .......................", Toast.LENGTH_SHORT).show()
            Log.v(tag, "Location update stopped .......................")
            googleApiClient?.disconnect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        requestLocationStop()
        Toast.makeText(context, "Service Stopped!", Toast.LENGTH_SHORT).show()
        Log.v(tag, "Service Stopped!")
    }

    override fun onBtnClick(position: Int, date: String, typeId: Int, content: String,
                            operationId: Int, operationTypeName: String, working: Int, organizationId: Int) {
        FragmentDetailOperation(position, date, typeId, content, operationId, operationTypeName, working, organizationId)

    }

    override fun onImageMemberClick() {
        FragmentMemberOperation()
    }

    override fun onLocation(lat: Double, lng: Double, poiTypeId: String, name: String) {
        FragmentMap(lat, lng, poiTypeId, name)
    }

    @SuppressLint("MissingPermission")
    override fun onUpdateWorking(position: Int, operationId: String, isWorking: String) {
        //   Toast.makeText(context, "isWorking = $isWorking", Toast.LENGTH_SHORT).show()
        requestLocationStart()

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->

            val data = Data.Builder().apply {
                putString(LATTITUDE_DATA, location?.latitude.toString())
                putString(LONGITUDE_DATA, location?.longitude.toString())
                putString(OPERATION_ID_DATA, operationId)
                putString(TOKEN_DATA, token)
            }.build()

            val request = PeriodicWorkRequest.Builder(SendLocationWorker()::class.java, 1, TimeUnit.MINUTES).apply {
                setInputData(data)
            }.build()

            WorkManager.getInstance().enqueue(request)

//            WorkManager.getInstance().getStatusById(request.id).observe(this, Observer {
//                if (it != null && it.state == State.SUCCEEDED) {
//                    val lat = it.outputData.getString("lat", "")
//                    val lng = it.outputData.getString("lng", "")
//                    val operationId = it.outputData.getString("operationId", "")
//                    operationViewModel?.updateLastLocation(UpdateLocationInfo(token!!, operationId, lat, lng))
//                }
//            })
        }

        if (isWorking.equals("1")) {
            operationViewModel?.updateWorking(OperationWorkingInfo(token!!, operationId, isWorking))
            operationViewModel?.operationUpdateData?.observe(this, Observer { event ->
                event?.getContentIfNotHandled()?.let { updateWorking ->
                    updateWorking.operationWorkingWrapper.let {

                        fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
                            if  (location != null) {
                                val lat = location.latitude
                                val lng = location.longitude

                                Log.d("periodicRequest", "lat : $lat, lng : $lng")

                                startThread(token!!, operationId, location.latitude.toString(), location.longitude.toString())
                            } else {
                                Toast.makeText(context, "Location not Detected", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            })
        } else if (isWorking.equals("0")) {
            operationViewModel?.updateWorking(OperationWorkingInfo(token!!, operationId, isWorking))
            operationViewModel?.operationUpdateData?.observe(this, Observer { event ->
                event?.getContentIfNotHandled()?.let { updateWorking ->
                    updateWorking.operationWorkingWrapper.let { lastLocation ->
                        stopThread()
                    }
                }
            })
        }

    }

    fun startThread(token: String, operationId: String, lat: String, lng: String) {
        stopRunThread = false
        val runnable = OperationRunnable(10, token, operationId, lat, lng)
        val round = 5

        Thread(runnable).apply {

            for (i in 0..round){
                if (i <= round) {
                    operationViewModel?.updateLastLocation(UpdateLocationInfo(token, operationId, lat, lng))
                    operationViewModel?.lastLocationUpdate?.observe(this@OperationFragment, Observer { event ->
                        event?.getContentIfNotHandled()?.let { lastLocationWrapper ->
                            lastLocationWrapper.updateLastLocationWrapper.let {
                                //Toast.makeText(context, "lastLocationUpdate", Toast.LENGTH_SHORT).show()
                            }
                        }
                    })
                }
            }

        }.start()
    }

    fun stopThread() {
        stopRunThread = true

    }

    inner class OperationRunnable(val secounds: Int, val token: String, val operationId: String, val lat: String, val lng: String) : Runnable {
        override fun run() {
            for (i in 0..secounds) {

                if (stopRunThread) {
                    return
                }
                Log.d(tag, "startThread: $i")
                try {
                    Thread.sleep(1000)
                    if (i <= secounds) {
                        Log.d(tag, "startThread: $i and secounds : $secounds")
                    }
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun sendLastLocation(operationId: String) {
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->

            if (location == null) {
                requestLocationStart()
            }

            if (location != null) {
                var lat = location.latitude
                var lng = location.longitude
                operationViewModel?.updateLastLocation(UpdateLocationInfo(token!!, operationId, lat.toString(), lng.toString()))


                val data = Data.Builder().apply {
                    putString("lat", lat.toString())
                    putString("lng", lng.toString())
                    putString("operationId", operationId)
                }.build()

                val constaint = Constraints.Builder().apply {
                    setRequiredNetworkType(NetworkType.CONNECTED)
                    setRequiredNetworkType(NetworkType.UNMETERED)
                    setRequiresCharging(true)
                    setRequiresBatteryNotLow(true)
                    //   sendLastLocation(operationId)
                }.build()

                val periodicRequest = PeriodicWorkRequest
                        .Builder(SendLocationWorker::class.java, 1,
                                TimeUnit.MINUTES).apply {
                            setConstraints(constaint)
                            setInputData(data)
                            sendLastLocation(operationId)
                            //    Toast.makeText(context, "periodicRequest = ${TimeUnit.MILLISECONDS}", Toast.LENGTH_SHORT).show()
                        }.build()

                WorkManager.getInstance().enqueue(periodicRequest)
                Toast.makeText(context, "WorkManagerIt = $periodicRequest", Toast.LENGTH_SHORT).show()

            } else {
                Toast.makeText(context, "Location not Detected", Toast.LENGTH_SHORT).show()
            }


        }

    }

    fun FragmentMap(lat: Double, long: Double, poiTypeId: String, name: String) {
        val fragment = MapsFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        var bundle = Bundle()
        bundle.putDouble("result_lat", lat)
        bundle.putDouble("result_lng", long)
        bundle.putString("poiTypeId", poiTypeId)
        bundle.putString("name", name)
        fragment.arguments = bundle
        transaction?.commit()
    }

    fun FragmentDetailOperation(position: Int, date: String, typeId: Int, content: String, operationId: Int?, operationName: String, working: Int, organizationId: Int) {
        val fragment = OperationDetailFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        val bundle = Bundle()
        bundle.putInt("position", position)
        bundle.putString("date", date)
        bundle.putInt("typeId", typeId)
        bundle.putString("content", content)
        bundle.putInt("operationId", operationId!!)
        bundle.putString("operationName", operationName)
        bundle.putInt("working", working)
        bundle.putInt("organizationId", organizationId)
        transaction?.addToBackStack(null)
        fragment.arguments = bundle
        transaction?.commit()
    }

    fun FragmentMemberOperation() {
        val fragment = MemberFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    companion object {
        val tag: String = OperationFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                OperationFragment().apply {

                }

        private const val MEDIUM_INTERVAL = 1000 * 30 * 60
        private const val FASTEST_INTERVAL = 2000.toLong() /* 2 sec */
        private const val INTERVAL = (2 * 1000).toLong()  /* 10 secs */
    }
}
