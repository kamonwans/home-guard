package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomPendings(@SerializedName("ChatRoomID")val chatRoomId : String,
                            @SerializedName("ChatRoomName")val chatRoomName:String)