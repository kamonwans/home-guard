package com.example.kamonwan_s.hgproject.map.ui.people

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.map.ui.people.repository.PeopleRepository


class PeopleViewModelFactory(private val app:Application,private val peopleRepository: PeopleRepository)
    :ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PeopleViewModel(app, peopleRepository) as T
    }
}