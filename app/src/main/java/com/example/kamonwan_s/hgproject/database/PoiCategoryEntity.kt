package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "poi_category")
data class PoiCategoryEntity(@PrimaryKey @ColumnInfo(name = "poi_category_id")val poiCategoryID:String,
                             @ColumnInfo(name = "poi_category_modified_date")val modifiedDate :String,
                             @ColumnInfo(name = "poi_category_short_name")val shortName:String)