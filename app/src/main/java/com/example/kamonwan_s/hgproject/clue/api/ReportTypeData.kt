package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportTypeData(@SerializedName("Data") val reportTypeWrapper : ReportTypeWrapper,
                          @SerializedName("Response") val responseList: ResponseList)