package com.example.kamonwan_s.hgproject.statution.api

import com.google.gson.annotations.SerializedName

data class StationsData(@SerializedName("Stations") val stations:MutableList<Stations>)