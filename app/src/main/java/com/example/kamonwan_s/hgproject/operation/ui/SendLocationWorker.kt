package com.example.kamonwan_s.hgproject.operation.ui

import androidx.work.Worker
import com.example.kamonwan_s.hgproject.operation.api.UpdateLocationInfo
import java.lang.Exception

class SendLocationWorker : Worker() {

    private var operationViewModel: OperationViewModel? = null

    companion object {
        const val LATTITUDE_DATA = "lattitude"
        const val LONGITUDE_DATA = "longitude"
        const val OPERATION_ID_DATA = "operationId"
        const val TOKEN_DATA = "token"
    }

    override fun doWork(): WorkerResult {

        try {

            val inputData = getInputData()
            val lattitude = inputData.getString(LATTITUDE_DATA, "")
            val longitude = inputData.getString(LONGITUDE_DATA, "")
            val operationId = inputData.getString(OPERATION_ID_DATA, "")
            val token = inputData.getString(TOKEN_DATA, "")


            return WorkerResult.SUCCESS
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return WorkerResult.FAILURE
    }
}