package com.example.kamonwan_s.hgproject.poi.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

data class POIs(@SerializedName("Lat") val lat: String,
                @SerializedName("Lng") val lng: String,
                @SerializedName("ModifiedDate") val modifiedDate: String,
                @SerializedName("POIID") val poiId: Int,
                @SerializedName("POIName") val poiName: String,
                @SerializedName("POITypeID") val pOITypeID: String) {

//    constructor(lat: String,lng: String,modifiedDate: String,poiId: String,poiName: String,pOITypeID: String)
//    :this(lat,lng,modifiedDate,poiId,poiName,pOITypeID)
}