package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationDetail(@SerializedName("ChatRoomID") val chatRoomID : Int,
                           @SerializedName("Detail") val detail : String,
                           @SerializedName("Images") val images : ArrayList<String>,
                           @SerializedName("IsOperate") val isWorking:Int,
                           @SerializedName("ModifiedDate") val modifiedDate:String,
                           @SerializedName("OperationID") val operationID:Int,
                           @SerializedName("OperationName") val operationName:String,
                           @SerializedName("OperationStatusID") val operationStatusID:Int,
                           @SerializedName("OperationTypeID") val operationTypeID:Int,
                           @SerializedName("OrganizationName") val organizationName:String,
                           @SerializedName("OrganizationPic") val organizationPic:String)