package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatContentListInfo
import com.example.kamonwan_s.hgproject.communication.api.ChatContentReadedInfo
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.api.CreateChatContentPToPResponse
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.activity_chat_message.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChatMessageActivity : AppCompatActivity(),ContentChatAdapter.BtnClickListener {

    private var communicationViewModel: CommunicationViewModel? = null
    private var token: String? = null
    private var nameMember: String? = null
    private var accountID: String? = null
    private var chatRoomId: String? = null
    private var content: String? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: ContentChatAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_message)
        if (savedInstanceState != null){
            loadChatContentList()
        }

        loadChatContentList()
        accountID = intent?.getStringExtra("accountId")
        nameMember = intent?.getStringExtra("nameMember")
        chatRoomId = intent?.getStringExtra("chatRoomId")
        Log.d("chatRoomIdFriednds", accountID.toString())
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.icon_arrow_left)
            setTitle(nameMember)
        }

        initInstance()
    }

    private fun initInstance() {
        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(application))[CommunicationViewModel::class.java]

        token = communicationViewModel?.getToken()


        adapter = ContentChatAdapter(this!!,this)
        layoutManager = LinearLayoutManager(applicationContext)
        recyclerViewChatLog?.layoutManager= layoutManager
        recyclerViewChatLog?.itemAnimator = DefaultItemAnimator()
        recyclerViewChatLog?.adapter = adapter



        imageSendChat?.setOnClickListener {
            createChatContentOnlyText()
        }


       imageChooseMore?.setOnClickListener {
            FragmentMenuChat(accountID!!,nameMember!!,chatRoomId!!)
        }

        loadChatContentList()

        communicationViewModel?.getChatContentRead(ChatContentReadedInfo(token!!,"3"))
        communicationViewModel?.chatContentRead?.observe(this, Observer {event ->
            event?.getContentIfNotHandled()?.let {contentReadWrapper ->
                contentReadWrapper.chatContentReadedWrapper.chatContentReaded.let {contentRead ->
             //       Toast.makeText(this,contentRead.chatRoomID,Toast.LENGTH_SHORT).show()
                    //                    if (contentRead != null){
//                        contentRead.let {
//                            adapter?.itemRead = contentRead
//                        }
//                    }
                }
//                if (adapter?.itemRead == null){
//                    adapter?.itemRead = contentReadWrapper.chatContentReadedWrapper.chatContentReaded
//                }
//                adapter?.notifyDataSetChanged()

            }

        })
    }


    private fun loadChatContentList() {
        communicationViewModel?.getChatContentList(ChatContentListInfo(token!!, chatRoomId.toString(), "0", "99", "0"))
        communicationViewModel?.chatContentList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatContentListWrapper ->
                chatContentListWrapper.chatContentListWrapper.chatContentListData.let { chatContentListData ->
                    if (chatContentListData != null) {
                        chatContentListData.chatContents.let { chatContents ->
                            adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                        }
                    }

                    if (adapter?.items == null) {
                        adapter?.items = chatContentListWrapper.chatContentListWrapper.chatContentListData
                    }

                    adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun createChatContentOnlyText() {

        content = editContentChat?.text.toString()
        Log.d("contentText", content)
        CommunicationApi.service.createChatContentP2POnlyText(
                RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
                RequestBody.create(MediaType.parse("text/plain"), "1"),
                RequestBody.create(MediaType.parse("text/plain"), content),
                RequestBody.create(MediaType.parse("text/plain"), token!!))
                .enqueue(object : Callback<CreateChatContentPToPResponse> {
                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
                        Log.d("onFailure", t.message)
                    }

                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
                        if (response.isSuccessful) {
                            loadChatContentList()
                            Toast.makeText(this@ChatMessageActivity, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                            editContentChat?.text?.clear()

                        } else
                            Log.d("onResponse", response.errorBody()!!.string())
                    }
                })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.infoChat -> {
                FragmentChatRoomMember(chatRoomId!!)
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
    override fun onBtnClick(content: String) {
        startActivity(Intent(this, VideoActivity::class.java).apply {
            putExtra("contentVideo", Bundle().apply {
                putString("contentVideo", content)
            })
        })
    }


    fun FragmentChatRoomMember(chatRoomId:String) {
        val fragment = ChatRoomMemberFragment()
        val transaction = supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChat, fragment)
        val bundle = Bundle()
        bundle.putString("chatRoomId",chatRoomId)
        fragment.arguments = bundle
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentMenuChat(accountId:String,name:String,chatRoomId:String) {
        val fragment = MenuChatFragment()
        val transaction = supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChat, fragment)
        val bundle = Bundle()
        bundle.putString("accountId", accountId)
        bundle.putString("name", name)
        bundle.putString("chatRoomId",chatRoomId)
        fragment.arguments = bundle
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_info, menu)
        return super.onCreateOptionsMenu(menu)
    }


}
