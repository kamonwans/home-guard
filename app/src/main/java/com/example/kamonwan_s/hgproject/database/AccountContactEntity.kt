package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "account_contacts")
data class AccountContactEntity(@PrimaryKey @ColumnInfo(name = "account_contacts_id")val accountID:Int,
                                @ColumnInfo(name = "account_contacts_email")val email :String,
                                @ColumnInfo(name = "account_contacts_first_name")val firstName:String,
                                @ColumnInfo(name = "account_contacts_last_name")val lastName:String,
                                @ColumnInfo(name = "account_contacts_modified")val modifiedDate:String,
                                @ColumnInfo(name = "account_contacts_organization_id")val organizationID:Int,
                                @ColumnInfo(name = "account_contacts_profile")val profilePic:String) :Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(accountID)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(modifiedDate)
        parcel.writeInt(organizationID)
        parcel.writeString(profilePic)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountContactEntity> {
        override fun createFromParcel(parcel: Parcel): AccountContactEntity {
            return AccountContactEntity(parcel)
        }

        override fun newArray(size: Int): Array<AccountContactEntity?> {
            return arrayOfNulls(size)
        }
    }

}