package com.example.kamonwan_s.hgproject.map.ui.people

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomPToPInfo
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.api.CreateChatRoomResponse
import com.example.kamonwan_s.hgproject.communication.api.Token
import com.example.kamonwan_s.hgproject.communication.ui.ChatMessageActivity
import com.example.kamonwan_s.hgproject.communication.ui.ChatMessageFragment
import com.example.kamonwan_s.hgproject.communication.ui.CommunicationViewModel
import com.example.kamonwan_s.hgproject.map.ui.people.api.PeopleDetailInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_people_detail_dialog.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response


class PeopleDetailDialogFragment : DialogFragment() {
    private var accountID: Int? = null
    private var peopleViewModel: PeopleViewModel? = null
    private var communicationViewModel: CommunicationViewModel? = null
    private var token: String? = null
    private var phoneNumber: String? = null
    private var firstName: String? = null
    private var chatRoomId: String? = null
    private var accountId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            accountID = it.getInt("accountID")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_people_detail_dialog, container, false)
        initInstance(view, savedInstanceState)
        return view
    }

    private fun initInstance(view: View?, savedInstanceState: Bundle?) {
        peopleViewModel = ViewModelProviders.of(parentFragment!!
                , InjectorUtil.providePeopleViewModelFactory(activity!!.application))[PeopleViewModel::class.java]

        communicationViewModel = ViewModelProviders.of(parentFragment!!,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]

        Log.d("accountId", accountID.toString())
        token = peopleViewModel?.getToken()

        if (accountID != 0) {
            peopleViewModel?.loadOfficerDetail(PeopleDetailInfo(token!!, accountID!!))
            peopleViewModel?.peopleDetail?.observe(this, Observer { event ->
                event?.getContentIfNotHandled()?.let { people ->
                    people.officerDetailWrapper.officerDetailData.let {
                        if (it != null) {
                            view?.tvPeopleFirstName?.text = it.officerDetail.firstName
                            view?.tvPeopleLastName?.text = it.officerDetail.lastName
                            view?.tvPeopleEmail?.text = it.officerDetail.email
                            view?.tvPeopleTel?.text = it.officerDetail.tel
                            phoneNumber = it.officerDetail.tel
                            firstName = it.officerDetail.firstName

                            view?.llCallPhoneNumber?.setOnClickListener {
                                val phone = phoneNumber
                                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                                startActivity(intent)
                            }

                            communicationViewModel?.getChatRoomPToP(ChatRoomPToPInfo(token!!, accountID.toString()))
                            communicationViewModel?.chatRoomPToP?.observe(this, Observer { event ->
                                event?.getContentIfNotHandled()?.let { chatRoom ->
                                    chatRoom.chatRoomPToPData.chatRoomPToPAccountContact.let {
                                        if (it != null) {
                                            chatRoomId = it.accountContacts.chatRoomID
                                            firstName = it.accountContacts.firstName
                                            accountId = it.accountContacts.accountID
                                        }

                                        view?.llChat?.setOnClickListener {
                                            val intent = Intent(context, ChatMessageActivity::class.java)
                                            intent.putExtra("nameMember", firstName)
                                            intent.putExtra("accountId", accountID.toString())
                                            intent.putExtra("chatRoomId", chatRoomId)
                                            startActivity(intent)
                                        }
                                    }
                                }


                            })
                        }
                    }

                }
            })
        } else {
            Toast.makeText(context, "account is 0", Toast.LENGTH_SHORT).show()
        }

        view?.imageClosed?.setOnClickListener {
            dismiss()
        }


    }

    fun FragmentChatMessage() {
        val fragment = ChatMessageFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    companion object {
        val tag: String = PeopleDetailDialogFragment::class.java.simpleName
        @JvmStatic
        fun newInstance(accountID: Int) =
                PeopleDetailDialogFragment().apply {
                    arguments = Bundle().apply {
                        putInt("accountID", accountID)
                    }
                }
    }
}
