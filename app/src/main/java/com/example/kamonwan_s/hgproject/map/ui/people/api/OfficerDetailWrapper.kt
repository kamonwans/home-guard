package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class OfficerDetailWrapper(@SerializedName("Data") val officerDetailData : OfficerDetailData,
                                @SerializedName("Response") val responseList : ResponseList)