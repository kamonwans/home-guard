package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class GetOperationListOwnResponse(@SerializedName("GetOperationListOwnResult") val operationListOwnWrapper : OperationListOwnWrapper)