package com.example.kamonwan_s.hgproject.map.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

interface TcsdApi {

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/CoreService.svc/"
        private const val TIME_OUT: Long = 20
        val service: PoiApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(PoiApi::class.java)
        }
    }
}