package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface NewsApi {

    @POST("GetNewsList")
    fun loadNewsList(@Body getNewsListInfo: GetNewsListInfo): Call<GetNewsListResponse>

    @POST("GetNews")
    fun loadNews(@Body getNewsInfo: GetNewsInfo):Call<GetNewsResponse>

    @POST("LoadNewsType")
    fun loadNewsType(@Body token: Token) : Call<LoadNewsTypeResponse>

    @POST("GetNewsListAfter")
    fun loadNewsListAfter(@Body getNewsListInfo: GetNewsListInfo) : Call<GetNewsListAfterResponse>

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/NewsService.svc/"
        private const val TIME_OUT: Long = 20
        val service:NewsApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(NewsApi::class.java)
        }
    }
}