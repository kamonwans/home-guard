package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class Login(@SerializedName("DataType") val dataToken: MutableList<DataToken>,
                 @SerializedName("Response") val responseData: MutableList<ResponseData>)