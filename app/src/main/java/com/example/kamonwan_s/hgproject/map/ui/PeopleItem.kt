package com.example.kamonwan_s.hgproject.map.ui

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class PeopleItem(lat: Double, lng: Double, val accountID: Int,
                 val name: String, val icon: String,val lastName:String,val networkStatus:Int,
                 val operationTypeId:Int,val operationName:String) : ClusterItem {

    private val peoplePosition: LatLng = LatLng(lat, lng)

    override fun getSnippet(): String? {
        return null
    }

    override fun getTitle(): String {
        return name
    }

    override fun getPosition(): LatLng {
        return peoplePosition
    }

}