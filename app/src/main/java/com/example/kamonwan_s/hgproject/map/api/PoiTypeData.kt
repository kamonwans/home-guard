package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiTypeData(@SerializedName("POITypes") val poiType: MutableList<PoiType>)