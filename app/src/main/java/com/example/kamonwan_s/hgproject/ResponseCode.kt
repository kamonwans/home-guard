package com.example.kamonwan_s.hgproject

const val OK = 0
const val NOT_FOUND = -404
const val SERVER_ERROR = -500
const val LOCAL_ERROR = -501
const val INVALID_EMAIL = -600
const val EXISTED = -601
const val WEAK_PASSWORD = -602
const val NOT_FOUND_TOKEN = -101
