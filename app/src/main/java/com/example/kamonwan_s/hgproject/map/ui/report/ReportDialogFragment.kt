package com.example.kamonwan_s.hgproject.map.ui.report

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.kamonwan_s.hgproject.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class ReportDialogFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_people_dialog, container, false)
        return view
    }


    interface OnFragmentInteractionListener {
        fun checkboxCheck(poiId: Int, serverPath: String, layerPath: String, icChecked:Boolean)
        fun checkboxUnCheck(layerID: Int)
        fun onDialogShowLayerFilter()
        fun onDialogHideLayerFilter()
        fun onDialogClose()
    }
    companion object {
        val tag = ReportDialogFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ReportDialogFragment().apply {

                }
    }
}
