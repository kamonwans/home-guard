package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName


data class OperationList(@SerializedName("Operations") val operations : MutableList<Operations>,
                         @SerializedName("AccountContacts") val accountContacts:MutableList<AccountContacts>,
                         @SerializedName("POIs") val pois:MutableList<POIS>)