package com.example.kamonwan_s.hgproject.login.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.util.TableInfo
import android.database.sqlite.SQLiteConstraintException
import android.preference.PreferenceManager
import android.util.Log
import com.example.kamonwan_s.hgproject.LOCAL_ERROR
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.*
import com.example.kamonwan_s.hgproject.login.AccountApi
import com.example.kamonwan_s.hgproject.login.api.*
import com.example.kamonwan_s.hgproject.login.api.Account
import com.example.kamonwan_s.hgproject.login.ui.AccountUtil
import com.example.kamonwan_s.hgproject.login.ui.LoginViewModelFactory
import com.example.kamonwan_s.hgproject.map.repository.MapRepository
import com.example.kamonwan_s.hgproject.register.api.*
import com.example.kamonwan_s.hgproject.synceDatabase.SyncDBInfoListResponse
import com.example.kamonwan_s.hgproject.util.AppExecutors
import com.example.kamonwan_s.hgproject.util.Util
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountRepository private constructor(private val executor: AppExecutors
                                            , private val accountApi: AccountApi
                                            , private val mapDao: MapDao) {

    val errorString: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    fun loginUser(userAccount: UserAccount): LiveData<SingleEvent<LoginUserResult>> {
        val networkState = MutableLiveData<SingleEvent<LoginUserResult>>()
        accountApi.loginUser(userAccount).enqueue(object : Callback<LoginUserResult?> {
            override fun onFailure(call: Call<LoginUserResult?>?, t: Throwable) {
                Log.d(AccountRepository.tag, t.toString())
            }

            override fun onResponse(call: Call<LoginUserResult?>?, response: Response<LoginUserResult?>) {
                if (response.isSuccessful) {
                    networkState.value = SingleEvent(response.body()!!)
                    Log.d("networkState", response.body().toString())
                } else {
                    Log.d(tag, response.errorBody()!!.string())

                }
            }

        })

        return networkState
    }

    fun retrieveUser(app: Application): LiveData<SingleEvent<UserAccount>?> {
        val userLiveData = MutableLiveData<SingleEvent<UserAccount>>()
        executor.diskIO.execute {
            val userData = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
            val encryptedString: String = userData.getString("", "")!!
            if (encryptedString.isNotBlank()) {
                val jsonString = Util.decryptString(app, encryptedString)
                val user = Gson().fromJson<UserAccount>(jsonString, UserAccount::class.java)
                userLiveData.postValue(SingleEvent(user))
            } else {
                userLiveData.postValue(null)
            }
        }

        return userLiveData
    }

    fun loadAccount(token: Token): LiveData<SingleEvent<AccountWrapper>> {
        val account = MutableLiveData<SingleEvent<AccountWrapper>>()
        // val token = AccountUtil.retrieveToken(app)
        //if (token != AccountUtil.DEFAULT__TOKEN)
        accountApi.loadAccount(token).enqueue(object : Callback<AccountWrapper?> {
            override fun onFailure(call: Call<AccountWrapper?>, t: Throwable) {
                errorString.value = SingleEvent(t.toString())
            }

            override fun onResponse(call: Call<AccountWrapper?>, response: Response<AccountWrapper?>) {
                if (response.isSuccessful){
                    account.value = SingleEvent(response.body()!!)
                    var dbInfoList = response.body()!!
                    executor.diskIO.execute {
                        try {
                            val account = dbInfoList.AccountList.dataAccount.account
                            val accountEntity = AccountEntity(
                                    account.firstName,
                                    account.lastName,
                                    account.tel,
                                    account.email,
                                    account.password,
                                    account.accountTypeID,
                                    account.profilePic,
                                    account.OrganizationID
                            )
                            mapDao.insertAccount(accountEntity)

                            if (dbInfoList.equals("")) {
                                mapDao.deleteAllAccount()
                                mapDao.insertAccount(accountEntity)
                            }else{
                                mapDao.loadAllAccountData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            errorString.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            errorString.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else {
                    errorString.value = SingleEvent(response.errorBody()!!.toString())
                }
            }

        })
        return account
    }

    fun checkToken(token: Token): LiveData<CheckTokenResult> {
        val checkToken = MutableLiveData<CheckTokenResult>()
        accountApi.checkToken(token).enqueue(object : Callback<CheckTokenResult> {
            override fun onFailure(call: Call<CheckTokenResult>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<CheckTokenResult>, response: Response<CheckTokenResult>) {
                if (response.isSuccessful) {
                    checkToken.value = response.body()
                    Log.d(tag, response.message())
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return checkToken
    }

    fun insertProvince(province: List<ProvinceEntity>){
        executor.diskIO.execute {
            try {
                mapDao.insertProvince(province)
            }catch (e: SQLiteConstraintException) {
                errorString.postValue(SingleEvent(province.toString()))
            } catch (e: Exception) {
                errorString.postValue(SingleEvent(province.toString()))
                Log.d(tag, e.toString())
            }
        }
    }

    fun insertDistrict(district: List<DistrictEntity>){
        executor.diskIO.execute {
            try {
                mapDao.insertDistrict(district)
            }catch (e: SQLiteConstraintException) {
                errorString.postValue(SingleEvent(district.toString()))
            } catch (e: Exception) {
                errorString.postValue(SingleEvent(district.toString()))
                Log.d(tag, e.toString())
            }
        }
    }

    fun insertSubDistrict(subDistrict: List<SubDistrictEntity>){
        executor.diskIO.execute {
            try {
                mapDao.insertSubDistrict(subDistrict)
            }catch (e: SQLiteConstraintException) {
                errorString.postValue(SingleEvent(subDistrict.toString()))
            } catch (e: Exception) {
                errorString.postValue(SingleEvent(subDistrict.toString()))
                Log.d(tag, e.toString())
            }
        }
    }

    fun updateSubDistrict(subDistrict: List<SubDistrict>){
//        executor.diskIO.execute {
//            try {
//                mapDao.updateSubDistrict(subDistrict.toMutableList())
//            }catch (e: SQLiteConstraintException) {
//                errorString.postValue(SingleEvent(subDistrict.toString()))
//            } catch (e: Exception) {
//                errorString.postValue(SingleEvent(subDistrict.toString()))
//                Log.d(tag, e.toString())
//            }
//        }
    }

    fun updateDistrict(district: List<District>){
//        executor.diskIO.execute {
//            try {
//                mapDao.updateDistrict(district.toMutableList())
//            }catch (e: SQLiteConstraintException) {
//                errorString.postValue(SingleEvent(district.toString()))
//            } catch (e: Exception) {
//                errorString.postValue(SingleEvent(district.toString()))
//                Log.d(tag, e.toString())
//            }
//        }
    }

    fun updateProvince(province: List<Province>){
//        executor.diskIO.execute {
//            try {
//                mapDao.updateProvince(province.toMutableList())
//            }catch (e: SQLiteConstraintException) {
//                errorString.postValue(SingleEvent(province.toString()))
//            } catch (e: Exception) {
//                errorString.postValue(SingleEvent(province.toString()))
//                Log.d(tag, e.toString())
//            }
//        }
    }

    fun loadProvince(): LiveData<ProvinceWrapper> {
        val provinceLiveData = MutableLiveData<ProvinceWrapper>()
        accountApi.loadProvince().enqueue(object : Callback<ProvinceWrapper?> {
            override fun onFailure(call: Call<ProvinceWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<ProvinceWrapper?>, response: Response<ProvinceWrapper?>) {
                if (response.isSuccessful) {
                    provinceLiveData.value = response.body()
                    var dbInfoList = response.body()
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList!!.provinceList.isNotEmpty()) {
//                                mapDao.insertProvince(dbInfoList.provinceList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }
        })
        return provinceLiveData
    }

    fun loadDistrict(provinceID: String): LiveData<DistrictWrapper> {
        val districtLiveData = MutableLiveData<DistrictWrapper>()
        accountApi.loadDistrict(provinceID).enqueue(object : Callback<DistrictWrapper?> {
            override fun onFailure(call: Call<DistrictWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<DistrictWrapper?>, response: Response<DistrictWrapper?>) {
                if (response.isSuccessful) {
                    districtLiveData.value = response.body()
                    var dbInfoList = response.body()!!
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList.districtList.isNotEmpty()){
//                                mapDao.insertDistrict(dbInfoList.districtList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return districtLiveData
    }

    fun loadSubDistrict(districtID: String): LiveData<SubDistrictWrapper> {
        val subDistrictLiveData = MutableLiveData<SubDistrictWrapper>()
        accountApi.loadSubDistrict(districtID).enqueue(object : Callback<SubDistrictWrapper?> {
            override fun onFailure(call: Call<SubDistrictWrapper?>, t: Throwable) {
                Log.d(tag, t.toString())
            }

            override fun onResponse(call: Call<SubDistrictWrapper?>, response: Response<SubDistrictWrapper?>) {
                if (response.isSuccessful){
                    subDistrictLiveData.value = response.body()
                    var dbInfoList = response.body()!!
//                    executor.diskIO.execute {
//                        try {
//                            if (dbInfoList.subDistrictList.isNotEmpty()) {
//                                mapDao.deleteAllSubDistrict()
//                                mapDao.insertSubDistrict(dbInfoList.subDistrictList)
//                            }
//                        } catch (e: SQLiteConstraintException) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                        } catch (e: Exception) {
//                            errorString.postValue(SingleEvent(e.toString()))
//                            Log.d(tag, e.toString())
//                        }
//                    }
                }


                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return subDistrictLiveData
    }


    fun syncProvince(modifiedDate : String): LiveData<SyncProvinceResponse> {
        val syncDB = MutableLiveData<SyncProvinceResponse>()
        accountApi.syncProvince(modifiedDate).enqueue(object : Callback<SyncProvinceResponse> {
            override fun onFailure(call: Call<SyncProvinceResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncProvinceResponse>, response: Response<SyncProvinceResponse>) {
                if (response.isSuccessful) {
                    syncDB.value = response.body()
                    val dbInfoList = response.body()!!.syncProvinceList
                    executor.diskIO.execute {
                        try {
                            var province = dbInfoList
                            var provinceList = ArrayList<ProvinceEntity>()
                            province.forEach {
                                var provinceEntity = ProvinceEntity(
                                        it.provinceID.toInt(),
                                        it.provinceName,
                                        it.modifiedDate.toLong()
                                )
                                provinceList.add(provinceEntity)
                            }
                            mapDao.insertProvince(provinceList)
                            if (dbInfoList.isNotEmpty()) {
                                mapDao.updateProvince(provinceList)
                            }
                        }catch (e: SQLiteConstraintException) {
                            errorString.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            errorString.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())

            }
        })
        return syncDB
    }

    fun syncDistrict(modifiedDate : String): LiveData<SyncDistrictResponse> {
        val syncDB = MutableLiveData<SyncDistrictResponse>()
        accountApi.syncDistrict(modifiedDate).enqueue(object : Callback<SyncDistrictResponse> {
            override fun onFailure(call: Call<SyncDistrictResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncDistrictResponse>, response: Response<SyncDistrictResponse>) {
                if (response.isSuccessful) {
                    syncDB.value = response.body()
                    var dbAddressList = response.body()!!
                    executor.diskIO.execute {
                        try {
                            var district = dbAddressList.syncDistrictWrapper
                            var districtList = ArrayList<DistrictEntity>()
                            district.forEach {
                                var districtEntity = DistrictEntity(
                                        it.districtID.toInt(),
                                        it.districtName,
                                        it.provinceID.toInt(),
                                        it.modifiedDate.toLong()
                                )

                                districtList.add(districtEntity)
                            }
                            mapDao.insertDistrict(districtList)
                            if (dbAddressList.syncDistrictWrapper.isNotEmpty()) {
                                mapDao.updateDistrict(districtList)
                            }
                        }catch (e: SQLiteConstraintException) {
                            errorString.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            errorString.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())

            }
        })
        return syncDB
    }

    fun syncSubDistrict(modifiedDate : String): LiveData<SyncSubDistrictResponse> {
        val syncDB = MutableLiveData<SyncSubDistrictResponse>()
        accountApi.syncSubDistrict(modifiedDate).enqueue(object : Callback<SyncSubDistrictResponse> {
            override fun onFailure(call: Call<SyncSubDistrictResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncSubDistrictResponse>, response: Response<SyncSubDistrictResponse>) {
                if (response.isSuccessful) {
                    syncDB.value = response.body()
                    var dbAddressList = response.body()!!
                    executor.diskIO.execute {
                        try {

                            var subDistrict = dbAddressList.syncSubDistrictWrapper
                            var subDistrictList = ArrayList<SubDistrictEntity>()

                            subDistrict.forEach {
                                var subDistrictEntity = SubDistrictEntity(
                                        it.subDistrictID.toInt(),
                                        it.districtID.toInt(),
                                        it.subDistrictName,
                                        it.modifiedDate.toLong()
                                )
                                subDistrictList.add(subDistrictEntity)
                            }
                            mapDao.insertSubDistrict(subDistrictList)
                            if (dbAddressList.syncSubDistrictWrapper.isNotEmpty()) {
                                mapDao.updateSubDistrict(subDistrictList)
                            }
                        }catch (e: SQLiteConstraintException) {
                            errorString.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            errorString.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())

            }
        })
        return syncDB
    }

    fun syncOrganization(modifiedDate : String): LiveData<SyncOrganizationResponse> {
        val syncDB = MutableLiveData<SyncOrganizationResponse>()
        accountApi.syncOrganization(modifiedDate).enqueue(object : Callback<SyncOrganizationResponse> {
            override fun onFailure(call: Call<SyncOrganizationResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncOrganizationResponse>, response: Response<SyncOrganizationResponse>) {
                if (response.isSuccessful) {
                    syncDB.value = response.body()
                    var dbAddressList = response.body()!!
                    executor.diskIO.execute {
                        try {
                            var organization = dbAddressList.syncOrganizationWrapper
                            var organizationList = ArrayList<OrganizationEntity>()
                            organization.forEach {
                                var organizationEntity = OrganizationEntity(
                                        it.organizationID.toInt(),
                                        it.name,
                                        it.modifiedDate.toLong()
                                )
                                organizationList.add(organizationEntity)
                            }

                            mapDao.insertOrganizaton(organizationList)
                            if (dbAddressList.syncOrganizationWrapper.isNotEmpty()) {
                                mapDao.updateOrganization(organizationList)
                            }
                        }catch (e: SQLiteConstraintException) {
                            errorString.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            errorString.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())

            }
        })
        return syncDB
    }

    fun saveAccount(app: Application, accountList: Account) {
        executor.diskIO.execute {
            AccountUtil.saveAccount(app, accountList)
        }
    }

    fun saveAccountOrganiation(app: Application,accountList: Account) {
        executor.diskIO.execute {
            AccountUtil.saveOrganizationID(app,accountList)
        }
    }

    fun saveToken(app: Application, token: String) {
        executor.diskIO.execute {
            AccountUtil.saveToken(app, token)
        }
    }

    fun saveAccountID(app: Application, id: Int) {
        executor.diskIO.execute {
            AccountUtil.saveAccountID(app, id)
        }
    }



    fun saveUser(app: Application, userAccount: UserAccount) {
        executor.diskIO.execute {
            val userData = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
            val editor = userData.edit()
            editor.run {
                val jsonData = Gson().toJson(userAccount)
//                putString(app.getString(R.string.pref_user),Util.decryptString(app,jsonData))
            }
            editor.apply()
        }
    }


    companion object {
        private val tag = AccountRepository::class.java.simpleName
        @Volatile
        private var instance: AccountRepository? = null

        fun getInstance(executor: AppExecutors, accountApi: AccountApi,mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance ?: AccountRepository(executor, accountApi,mapDao).also { instance = it }
                }
    }
}