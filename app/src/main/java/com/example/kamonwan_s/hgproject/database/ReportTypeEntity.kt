package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "report_type")
data class ReportTypeEntity(@ColumnInfo(name = "report_type_icon") val icon:String,
                            @ColumnInfo(name = "report_type_modified")val modifiedDate :Long,
                            @PrimaryKey @ColumnInfo(name = "report_type_id")val reportTypeID:Int,
                            @ColumnInfo(name = "report_type_name")val reportTypeName:String)