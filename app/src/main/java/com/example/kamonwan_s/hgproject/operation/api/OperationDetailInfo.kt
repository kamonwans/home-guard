package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationDetailInfo(@SerializedName("Token")val token :String,
                               @SerializedName("OperationID") val operationID:Int)