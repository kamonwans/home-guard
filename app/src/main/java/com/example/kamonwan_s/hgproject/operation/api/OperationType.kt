package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

data class OperationType(@SerializedName("Color")val color:String,
                         @SerializedName("ModifiedDate")val modifiedDate:String,
                         @SerializedName("OperationTypeID")val operationTypeID:String,
                        @SerializedName("OperationTypeName")val operationTypeName:String)