package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class GetChatRoomPedingListResponse(@SerializedName("GetChatRoomPedingListResult") val chatRoomPedingListWrapper : ChatRoomPedingListWrapper)