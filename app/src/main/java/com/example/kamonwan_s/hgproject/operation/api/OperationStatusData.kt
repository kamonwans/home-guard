package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationStatusData(@SerializedName("OperationStatus")val operationStatus : MutableList<OperationStatus>)