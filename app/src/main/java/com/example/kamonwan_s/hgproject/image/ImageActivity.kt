package com.klickerlab.tcsd.ui.image

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.kamonwan_s.hgproject.R
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    var image: ArrayList<String>? = null
    var position: Int = 0
    lateinit var scaleGestureDetector: ScaleGestureDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        val intent = intent
        image = intent.extras.getBundle("name").getStringArrayList("image")
        position = intent.extras.getBundle("name").getInt("position")

        imageShow.setImage(this!!.image!!)
        //imageShow.setOnTouchListener(this)
        var scaleListener = ScaleListener(imageShow)
        scaleGestureDetector = ScaleGestureDetector(this, scaleListener)
    }

    fun ImageView.setImage(url: ArrayList<String>) {
        Glide.with(context).load(url[position]).into(this)
    }

    class ScaleListener(var imageshow: ImageView) : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        var scale = 1f
        override fun onScale(detector: ScaleGestureDetector?): Boolean {
            scale = scale * detector!!.scaleFactor
            scale = Math.max(0.1f, Math.min(scale, 10f))
            imageshow.scaleX = scale
            imageshow.scaleY = scale
            return true
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        scaleGestureDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }



}
