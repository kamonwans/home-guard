package com.example.kamonwan_s.hgproject.map

import android.app.Application
import android.arch.lifecycle.*
import android.preference.PreferenceManager
import android.support.annotation.MainThread
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.PoiAreaData
import com.example.kamonwan_s.hgproject.map.repository.MapRepository
import com.example.kamonwan_s.hgproject.poi.api.*
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.firebase.internal.FirebaseAppHelper.getToken
import java.util.concurrent.atomic.AtomicBoolean

class MapViewModel(private val app: Application, private val mapRepository: MapRepository)
    : AndroidViewModel(app) {

    var getPoi = MediatorLiveData<GetPOI>()
    var token = MediatorLiveData<Token>()
    var showPois: Boolean = true
    //private val poiList:LiveData<PoiResponse> = mapRepository.loadPoi(app,GetPOI(getToken(),1))

    private val alreadySyncDatabase: AtomicBoolean = AtomicBoolean(false)
    val poiTypeBitmapDescriptors: LinkedHashMap<Int, BitmapDescriptor> = linkedMapOf()


//    val pois : LiveData<Poi> = Transformations.map(poiList){
//        it.poiWrapper.poiData.poi
//    }

//     val poiTypesList: LiveData<SingleEvent<PoiTypeResponse>> = Transformations.switchMap(token){
//        mapRepository.loadPoiType(it)
//    }
//
//     val poiCategoryList : LiveData<SingleEvent<PoiCategoryResponse>> = Transformations.switchMap(token){
//        mapRepository.loadPoiCategory(it)
//    }
//
//     val poiList : LiveData<SingleEvent<PoiListResponse>> = Transformations.switchMap(token){
//      mapRepository.loadPoiList(it)
//    }

//     val poi  : LiveData<PoiResponse>> = Transformations.switchMap(getPoi){
//        mapRepository.loadPoi(it)
//    }


    fun loadPoiType(token: Token) {
        this.token.value = token
    }

    fun loadPoiCategory(token: Token) {
        this.token.value = token
    }

    fun loadPoiList(token: Token) {
        this.token.value = token
    }

    fun loadPoi(getPOI: GetPOI) {
        this.getPoi.value = getPOI
    }


    private val poiTypesList: LiveData<PoiTypeResponse> = mapRepository.loadPoiType(Token(getToken()))
    var poiType: LiveData<PoiTypeData>? = Transformations.map(poiTypesList) {
        it.poiTypeWrapper.poiTypeData.poiType.forEach { poiType ->
            poiType.checkedItem = CHECKED_DEFAULT
        }
        it.poiTypeWrapper.poiTypeData
    }

    private val poiCategoryList: LiveData<PoiCategoryResponse> = mapRepository.loadPoiCategory(Token(getToken()))
    var poiCategory: LiveData<PoiCategoryData>? = Transformations.map(poiCategoryList) {
        it.categoryWrapper.poiCategoryData.poiCategory.forEach { poiCategory ->
            poiCategory.checkedItem = CHECKED_DEFAULT
        }
        it.categoryWrapper.poiCategoryData

    }

    // private val poiList:LiveData<PoiResponse> = mapRepository.loadPoi(GetPOI(getToken(),1))
    var poi: LiveData<PoiResponse>? = Transformations.switchMap(getPoi) {
        mapRepository.loadPoi(app,it)
    }

    private val poiListAll: LiveData<PoiListResponse> = mapRepository.loadPoiList(Token(getToken()))
    val poiAll: LiveData<PoiListData>? = Transformations.map(poiListAll) {
        it.poiListWrapper.poiListData
    }


    private val completeSyncDatabase = MediatorLiveData<Boolean>()
    val poiTypeData: LiveData<PoiAreaData> = Transformations.switchMap(completeSyncDatabase) {
        mapRepository.loadAllPoi()
    }

    @MainThread
    fun syncPoiTypedatabase() {
        if (alreadySyncDatabase.compareAndSet(false, true))
            completeSyncDatabase.addSource(mapRepository.syncPoiTypeDatabase(Token(getToken()))) {
                if (it != null)
                    completeSyncDatabase.value = it
            }
    }

    @MainThread
    fun syncPoiCategorydatabase() {
        if (alreadySyncDatabase.compareAndSet(false, true))
            completeSyncDatabase.addSource(mapRepository.syncPoiCategoryDatabase(SyncPoiInfo(getToken(), "0"))) {
                if (it != null)
                    completeSyncDatabase.value = it
            }
    }

    @MainThread
    fun syncPoiListDatabase() {
        if (alreadySyncDatabase.compareAndSet(false, true))
            completeSyncDatabase.addSource(mapRepository.syncPoiListDatabase(SyncPoiInfo(getToken(), "0"))) {
                if (it != null)
                    completeSyncDatabase.value = it
            }
    }

    val poiAreaData: LiveData<PoiAreaData> = Transformations.switchMap(completeSyncDatabase) {
        mapRepository.loadAllPoi()
    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun getToken(): String {
        return retrieveToken(app)
    }


    companion object {
        private val tag: String = MapViewModel::class.java.simpleName
        private const val CHECKED_DEFAULT = false
    }
}