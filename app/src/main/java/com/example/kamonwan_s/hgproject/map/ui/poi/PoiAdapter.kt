package com.example.kamonwan_s.hgproject.map.ui.poi

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.poi.api.POICategory
import com.example.kamonwan_s.hgproject.poi.api.PoiType
import kotlinx.android.synthetic.main.item_filter_map.view.*

class PoiAdapter(private val poiCategories: ArrayList<POICategory> = ArrayList()
                 , private val poiTypes: ArrayList<PoiType> = ArrayList()
                 , private val onItemClickListener: OnItemCheckedChangeListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val indexes: ArrayList<Int> = arrayListOf()

    init {
        poiCategories.forEach {
            indexes.add(-it.poiCategoryID.toInt())
            poiTypes.forEachIndexed { index, poiType ->
                if (poiType.poiCategoryId.toInt() == it.poiCategoryID.toInt())
                    indexes.add(index)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        return FilterViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return poiTypes.size + poiCategories.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FilterViewHolder) {
            if (indexes[position] >= 0) {
                holder.bindToType(poiTypes[indexes[position]])
                holder.itemView.cbFilter.isChecked = poiTypes[indexes[position]].checkedItem
            } else {
                poiCategories.filter { poiCategory ->
                    -poiCategory.poiCategoryID.toInt() == indexes[position]
                }.forEach {
                    holder.bindToCategory(it)
                    holder.itemView.cbFilter.isChecked = it.checkedItem

                }
            }
        }
    }

    private fun checkAllTypeWithCategoryId(poiCategory: POICategory) {
        poiTypes.filter {
            it.poiCategoryId == poiCategory.poiCategoryID
        }.forEach { poiType ->
            poiType.checkedItem = true
        }
    }

    private fun unCheckAllTypeWithCategoryId(poiCategory: POICategory) {
        poiTypes.filter {
            it.poiCategoryId == poiCategory.poiCategoryID
        }.forEach { poiType ->
            poiType.checkedItem = false
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (indexes[position] >= 0) 0
        else 1
    }

    inner class FilterViewHolder(private val parent: ViewGroup)
        : RecyclerView.ViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_filter_map, parent, false)) {

        fun bindToCategory(poiCategory: POICategory) {
            itemView.cbFilter.text = poiCategory.shortName
            itemView.cbFilter.typeface = ResourcesCompat.getFont(parent.context, R.font.kanit_regular)
            val marginLayoutParams = itemView.cbFilter.layoutParams as ViewGroup.MarginLayoutParams
            marginLayoutParams.run {
                marginEnd = 0
                marginStart = 0
            }
            itemView.layoutParams = marginLayoutParams
            itemView.cbFilter.setOnClickListener {
                poiCategory.checkedItem = itemView.cbFilter.isChecked
                if (!itemView.cbFilter.isChecked) {
                    unCheckAllTypeWithCategoryId(poiCategory)
                } else {
                    checkAllTypeWithCategoryId(poiCategory)
                }
                onItemClickListener.onCategoryCheck(poiCategory.poiCategoryID.toInt())
            }
        }

        fun bindToType(poiType: PoiType) {
            itemView.cbFilter.text = poiType.shortName
            itemView.cbFilter.typeface = ResourcesCompat.getFont(parent.context, R.font.kanit_regular)
            itemView.cbFilter.setOnCheckedChangeListener { _, isChecked ->
                poiType.checkedItem = isChecked
                if (!isChecked) {
                    poiCategories.forEach {
                        if (it.poiCategoryID == poiType.poiCategoryId && it.checkedItem) {
                            it.checkedItem = false
                        }
                        onItemClickListener.onTypeCheck(poiType.poiTypeId.toInt())
                    }
                }
            }
        }
    }

    interface OnItemCheckedChangeListener {
        fun onCategoryCheck(id: Int)
        fun onTypeCheck(id: Int)
    }
}