package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiWrapper(@SerializedName("Data") val poiData: PoiData,
                      @SerializedName("Response") val responseList: ResponseList)