package com.example.kamonwan_s.hgproject.register.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.util.addFragmentToActivity


class RegisterActivity : AppCompatActivity() {
     val DATABASE_NAME : String = "home_guard-db"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        if (savedInstanceState == null)
            addFragmentToActivity(RegisterFragment.newInstance(),R.id.contentContainer,RegisterFragment.tag)
    initInstance()
    }

    private fun initInstance(){

//        val db = Room.databaseBuilder(
//                this.getApplicationContext(),
//                AppDatabase::class.java,
//                DATABASE_NAME)
//                .build()

        supportActionBar?.run {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home -> {
                finish()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
    companion object {
        private val tag: String = RegisterFragment::class.java.simpleName
    }
}
