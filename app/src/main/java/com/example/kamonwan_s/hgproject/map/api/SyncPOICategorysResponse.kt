package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class SyncPOICategorysResponse(@SerializedName("SyncPOICategorysResult") val syncPOICategorysWrapper : SyncPOICategorysWrapper)