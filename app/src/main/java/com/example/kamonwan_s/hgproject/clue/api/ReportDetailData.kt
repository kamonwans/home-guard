package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportDetailData(@SerializedName("Data") val reportDetailAll:ReportDetailAll,
                            @SerializedName("Response") val responseList: ResponseList)