package com.example.kamonwan_s.hgproject.news.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName


data class NewsType(@SerializedName("ColorCode") val colorCode: String,
                    @SerializedName("FontColorCode") val fontColorCode:String,
                    @SerializedName("ModifiedDate") val modifiedDate: String,
                    @SerializedName("NewsTypeID") val newsTypeID: String,
                    @SerializedName("NewsTypeName") val newsTypeName: String)