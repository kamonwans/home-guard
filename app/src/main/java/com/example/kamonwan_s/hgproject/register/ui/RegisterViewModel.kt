package com.example.kamonwan_s.hgproject.register.ui

import android.app.Application
import android.arch.lifecycle.*
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.login.api.AccountWrapper
import com.example.kamonwan_s.hgproject.register.api.*
import com.example.kamonwan_s.hgproject.register.repository.RegisterRepository

class RegisterViewModel(private val app: Application, private var registerRepository: RegisterRepository)
    : AndroidViewModel(app) {

    private val accountInfo = MutableLiveData<AccountRegisterWrapper>()
    private val districtID = MutableLiveData<String>()
    private val provinceID = MutableLiveData<String>()
    val userAccount = MutableLiveData<AccountWrapper>()
    val registerUserInfo = MutableLiveData<RegisterUserInfo>()
    var allProvince: LiveData<List<Province>>? = null

    val provinceData: MediatorLiveData<ProvinceWrapper> = MediatorLiveData()
    val districtData: LiveData<DistrictWrapper> = Transformations.switchMap(provinceID) {
        registerRepository.loadDistrict(it)
    }

    val registerState: LiveData<SingleEvent<RegisterUserResult>> = Transformations.switchMap(accountInfo) {
        registerRepository.registerUser(it)
    }

    val register : LiveData<SingleEvent<RegisterUserResult>> = Transformations.switchMap(registerUserInfo){
        registerRepository.registerUserAccount(it)
    }
    fun retrieveProvince() {
        provinceData.addSource(registerRepository.loadProvince(), Observer {
            provinceData.value = it
        })
    }

    val subDistrictData: LiveData<SubDistrictWrapper> = Transformations.switchMap(districtID) {
        registerRepository.loadSubDistrict(it)
    }

    fun retrieveDistrict(provinceID: String) {
        this.provinceID.value = provinceID
    }

    fun retrieveSubDistrict(districtID: String) {
        this.districtID.value = districtID
    }

    val organizationData = MediatorLiveData<OrganizationWrapper>()
    fun retrieveOrganization() {
        organizationData.addSource(registerRepository.loadOrganization(), Observer {
            organizationData.value = it
        })
    }

    fun registerUser(accountRegisterWrapper: AccountRegisterWrapper) {
        accountInfo.value = accountRegisterWrapper
    }

    fun loadRegister(registerUserInfo: RegisterUserInfo){
        this.registerUserInfo.value = registerUserInfo
    }

    companion object {
        private val tag: String = RegisterViewModel::class.java.simpleName
    }
}