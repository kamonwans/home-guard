package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationInfo(@SerializedName("Token") val token : String,
                         @SerializedName("OrganizationID") val organizationID :Int,
                         @SerializedName("DateTimeStr") val dateTimeStr :Int)