package com.example.kamonwan_s.hgproject.operation.ui

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.example.kamonwan_s.hgproject.R

class PhotoOperationAdapter : PagerAdapter{

    var context: Context
    var listener: ViewPagerClickListener
    lateinit var infator: LayoutInflater
    var images: ArrayList<String>? = null

    constructor(context: Context,listener: ViewPagerClickListener, images: ArrayList<String>?) : super(){
        this.context = context
        this.listener = listener
        this.images = images
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        if (images != null){
            return images!!.size
        }
        return 0
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var imageShow: ImageView
        infator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view: View = infator.inflate(R.layout.item_photo, container, false)
        imageShow = view.findViewById(R.id.imgSlider) as ImageView
        Glide.with(context).load(images?.get(position)).into(imageShow)

        container.addView(view)

        imageShow.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (listener != null)
                    listener.onClickViewPager(position,images!!)
                Log.d("imagePuc", images!![position])
            }

        })

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    interface ViewPagerClickListener {
        fun onClickViewPager(position: Int,images:ArrayList<String>)

    }

}