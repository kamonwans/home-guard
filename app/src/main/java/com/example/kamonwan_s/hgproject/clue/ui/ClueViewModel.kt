package com.example.kamonwan_s.hgproject.clue.ui

import android.app.Application
import android.arch.lifecycle.*
import android.preference.PreferenceManager
import android.util.Log
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.clue.api.*
import com.example.kamonwan_s.hgproject.database.ProvinceEntity
import com.example.kamonwan_s.hgproject.database.StationsEntity
import com.example.kamonwan_s.hgproject.login.api.AccountWrapper
import com.example.kamonwan_s.hgproject.register.api.AccountRegisterWrapper
import com.example.kamonwan_s.hgproject.register.api.Province
import com.example.kamonwan_s.hgproject.register.api.ProvinceWrapper
import com.example.kamonwan_s.hgproject.util.AccountUtil

class ClueViewModel(private val app: Application, private val clueRepository: ClueRepository)
    :AndroidViewModel(app){

    var clueOwnInfo =MutableLiveData<ClueOwn>()
    var clueFavoriteUpdateInfo = MutableLiveData<UpdateFavoriteInfo>()
    var clueOrganizationInfo = MutableLiveData<ReportOrganizationInfo>()
    var reportTypeInfo = MutableLiveData<TokenInfo>()
    var clueDetailReport = MutableLiveData<ReportDetail>()
    var clueReportStatus = MutableLiveData<SyncReportInfo>()

    val ownData : MediatorLiveData<ReportListData> = MediatorLiveData()

    val reportOwn:LiveData<SingleEvent<GetReportListOwnResponse>> = Transformations.switchMap(clueOwnInfo){
        clueRepository.loadReportOwn(it)
    }

    val reportFavorite:LiveData<SingleEvent<GetReportListOwnFavoriteResponse>> = Transformations.switchMap(clueOwnInfo){
        clueRepository.loadReportFavorite(it)
    }

    val reportOrganization:LiveData<SingleEvent<GetReportListOrganizationResponse>> = Transformations.switchMap(clueOrganizationInfo){
        clueRepository.loadOrganization(it)
    }
    val favorite:LiveData<SingleEvent<UpdateReportFavoriteResponse>> = Transformations.switchMap(clueFavoriteUpdateInfo){
        clueRepository.updateFavorite(it)
    }

    val reportClueType : LiveData<SingleEvent<LoadReportTypeResponse>> = Transformations.switchMap(reportTypeInfo){
        clueRepository.loadReportType(it)
    }

    val organizationOwn : LiveData<SingleEvent<AccountWrapper>> = Transformations.switchMap(reportTypeInfo){
        clueRepository.loadOrganizationReport(it)
    }

    val reportDetail : LiveData<SingleEvent<GetReportDetailResponse>> = Transformations.switchMap(clueDetailReport){
        clueRepository.loadDetailReport(it)
    }

    val reportStatus : LiveData<SingleEvent<LoadReportStatusResponse>> = Transformations.switchMap(clueReportStatus){
        clueRepository.loadSatusReport(it)
    }

    val ownAfter : LiveData<SingleEvent<GetReportListOwnAfterResponse>> = Transformations.switchMap(clueOwnInfo){
        clueRepository.loadClueOwnAfter(it)
    }

    val favoriteAfter :LiveData<SingleEvent<GetReportListOwnFavoriteAfterResponse>> = Transformations.switchMap(clueOwnInfo){
        clueRepository.loadClueFavoriteAfter(it)
    }

    val organizationAfter : LiveData<SingleEvent<GetReportListOrganizationAfterResponse>> = Transformations.switchMap(clueOrganizationInfo){
        clueRepository.loadClueOrganizationAfter(it)
    }

    val provinceList: MediatorLiveData<ArrayList<ProvinceEntity>> = MediatorLiveData()
    fun loadProvinceList() : LiveData<ArrayList<ProvinceEntity>>{
        provinceList.addSource(clueRepository.loadProvince(), Observer {
            provinceList.value = it

        })
        return provinceList

    }

    val stationList : MediatorLiveData<ArrayList<StationsEntity>> = MediatorLiveData()
    fun loadStation() : LiveData<ArrayList<StationsEntity>>{
        stationList.addSource(clueRepository.loadStation(), Observer {
            stationList.value = it
        })
        return stationList
    }

//    private val completeSyncDatabase = MediatorLiveData<Boolean>()
//    val provinceData : LiveData<ProvinceWrapper> = Transformations.switchMap(completeSyncDatabase){
//        clueRepository.loadProvince()
//    }

    val commentData:MediatorLiveData<GetReportReplyResponse> = MediatorLiveData()
    fun loadComment(commentInfo: CommentInfo) : LiveData<GetReportReplyResponse>{
        commentData.addSource(clueRepository.loadReportReply(commentInfo), Observer {
            if (commentData.value == null){
                commentData.value = it
            }else{
                it?.reportReplyWrapper?.reportReplyList?.reportReply.let { comment ->
                    comment?.forEach {
                        commentData.value?.reportReplyWrapper?.reportReplyList?.reportReply?.add(it) }
                    }

            }
        })
        return commentData
    }


    fun loadOwnReport(clueOwn: ClueOwn){
        this.clueOwnInfo.value = clueOwn
    }

    fun loadOrganizationReport(reportOrganizationInfo: ReportOrganizationInfo){
        this.clueOrganizationInfo.value = reportOrganizationInfo
    }

    fun loadFavoriteReport(clueOwn: ClueOwn){
        this.clueOwnInfo.value = clueOwn
    }

    fun updateFavorite(updateFavoriteInfo: UpdateFavoriteInfo){
        this.clueFavoriteUpdateInfo.value = updateFavoriteInfo
    }

    fun loadReportType(tokenInfo: TokenInfo){
        this.reportTypeInfo.value = tokenInfo
    }

    fun loadOrganizationOwn(tokenInfo: TokenInfo){
        this.reportTypeInfo.value = tokenInfo
    }

    fun loadDetailReport(reportDetail: ReportDetail){
        this.clueDetailReport.value = reportDetail
    }

    fun loadStatusReport(syncReportInfo: SyncReportInfo){
        this.clueReportStatus.value = syncReportInfo
    }

    fun loadOwnAfter(clueOwn: ClueOwn){
        this.clueOwnInfo.value = clueOwn
    }

    fun loadFavoriteAfter(clueOwn: ClueOwn){
        this.clueOwnInfo.value = clueOwn
    }

    fun loadOrganizationAfter(reportOrganizationInfo: ReportOrganizationInfo){
        this.clueOrganizationInfo.value = reportOrganizationInfo
    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun retrieveFirstName(): String{
        return AccountUtil.retrieveFirstName(app)
    }

    fun retrieveLastName() :String{
        return AccountUtil.retrieveLastName(app)
    }

    fun retrieveProfilePic() :String{
        return AccountUtil.retrieveProfilePic(app)
    }

    fun insertReportType(reportType: List<ReportType>){
       // clueRepository.insertReportType(reportType)
    }

    fun insertReportStatus(reportStatus: List<ReportStatuses>){
       // clueRepository.insertReportStatus(reportStatus)
    }

    fun getToken(): String {
        return retrieveToken(app)
    }
//
//    fun getFirtName():String{
//        return retrieveFirstName(app)
//    }
//
//    fun getLastName():String{
//        return retrieveLastName(app)
//    }
//
//    fun getProfile():String{
//        return retrieveProfilePic(app)
//    }

    companion object {
        private val tag: String = ClueViewModel::class.java.simpleName
    }
}