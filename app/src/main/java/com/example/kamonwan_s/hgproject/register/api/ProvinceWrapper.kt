package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class ProvinceWrapper(@SerializedName("LoadProvinceResult") val provinceList: MutableList<Province>) {
}