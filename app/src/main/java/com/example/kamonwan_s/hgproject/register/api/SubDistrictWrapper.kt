package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class SubDistrictWrapper(@SerializedName("LoadSubDistrictResult") val subDistrictList: MutableList<SubDistrict>) {
}