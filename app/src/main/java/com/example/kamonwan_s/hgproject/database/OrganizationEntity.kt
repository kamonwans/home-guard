package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "organization")
data class OrganizationEntity(@PrimaryKey @ColumnInfo(name = "organization_id")val organizationID: Int,
                              @ColumnInfo(name = "organization_name")val name: String,
                              @ColumnInfo(name = "organization_modified_date")val modifiedDate: Long)