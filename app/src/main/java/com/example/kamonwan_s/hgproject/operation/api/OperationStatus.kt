package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull


data class OperationStatus(@SerializedName("Color") val color: String,
                           @SerializedName("ModifiedDate") val modifiedDate: String,
                           @SerializedName("OperationStatusID") val operationStatusID: String,
                           @SerializedName("OperationStatusName") val operationStatusName: String)