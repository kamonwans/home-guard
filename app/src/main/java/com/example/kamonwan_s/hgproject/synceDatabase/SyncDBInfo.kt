package com.example.kamonwan_s.hgproject.synceDatabase

import com.example.kamonwan_s.hgproject.register.api.Province
import com.google.gson.annotations.SerializedName

data class TableInfo(@SerializedName("ModifiedDate") val modifiedDate: Long)

data class SyncDBInfoList(@SerializedName("SyncProvince") val provinceTableInfo: TableInfo,
                        @SerializedName("SyncDistrict") val districtTableInfo: TableInfo,
                        @SerializedName("SyncSubDistrict") val SubDistrictTableInfo: TableInfo,
                          @SerializedName("SyncOrganization") val organizationTableInfo : TableInfo)

data class SyncDBInfoListResponse(@SerializedName("SyncTableResult") val syncTable: SyncDBInfoList)