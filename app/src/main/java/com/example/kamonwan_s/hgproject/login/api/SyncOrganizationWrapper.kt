package com.example.kamonwan_s.hgproject.login.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity(tableName = "organization")
data class SyncOrganizationWrapper(@NonNull @ColumnInfo(name = "organization_modified_date") @SerializedName("ModifiedDate")val modifiedDate:String,
                                   @NonNull @ColumnInfo(name = "organization_name") @SerializedName("Name")val name:String,
                                   @PrimaryKey @NonNull @ColumnInfo(name = "organization_id")@SerializedName("OrganizationID")val organizationID:String)