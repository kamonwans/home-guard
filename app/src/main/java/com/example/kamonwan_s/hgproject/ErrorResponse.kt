package com.example.kamonwan_s.hgproject

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.annotation.MainThread
import android.util.Log
import java.util.concurrent.atomic.AtomicBoolean

class ErrorResponse private constructor() : MutableLiveData<Int>() {
    private val pending = AtomicBoolean(false)
    private var observer: Observer<Int>? = null
    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<Int>) {
        if (hasActiveObservers())
            Log.w(tag, "Multiple observers registered but only one will be notified of changes.")
        super.observe(owner, Observer {
            if (pending.compareAndSet(true, false))
                observer.onChanged(it)
        })
    }

    @MainThread
    override fun setValue(value: Int?) {
        pending.set(true)
        super.setValue(value)
    }

    override fun observeForever(observer: Observer<Int>) {
        this.observer = Observer {
            if (pending.compareAndSet(true, false))
                observer.onChanged(it)
        }
        super.observeForever(this.observer!!)
    }

    fun removeObserverForever() {
        if (observer != null)
            super.removeObserver(observer!!)
        Log.d(tag, "${hasObservers()}")
    }

    companion object {
        private val tag: String = ErrorResponse::class.java.simpleName
        @Volatile
        private var instance: ErrorResponse? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: ErrorResponse().also {
                instance = it
            }
        }

        @Synchronized
        fun clearData() {
            instance = null
        }
    }
}