package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.StationsEntity

class DropDownStationAdapter(val context: Context) : BaseAdapter() {
    val inflater: LayoutInflater = LayoutInflater.from(context)
    var station: ArrayList<StationsEntity>? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ItemRowHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_drop_down_station, parent, false)
            viewHolder = ItemRowHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ItemRowHolder
        }

        Log.d("stationSize",station!!.size.toString())
        view.layoutParams.height = 80
        val stationId = station?.get(position)?.stationID
        var stationName = station?.get(position)?.stationName

        viewHolder.tvStation.text = stationName
        return view
    }

    override fun getItem(position: Int): Any? {
        return station?.get(position)?.stationName
    }

    override fun getItemId(position: Int): Long {
       return 0
    }

    override fun getCount(): Int {
        return station?.size ?: 0
    }

    private class ItemRowHolder(row: View) {
        val tvStation: TextView

        init {
            this.tvStation = row.findViewById(R.id.txtStation) as TextView
        }
    }
}


