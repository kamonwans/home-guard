package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ReportType

class DropDownTypeAdapter(val context: Context):BaseAdapter(){

    val inflater:LayoutInflater = LayoutInflater.from(context)
    var reportType :MutableList<ReportType>? = null
    var reportTypeId:Int? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view:View
        val viewHolder:ItemRowHolder
        if (convertView == null){
            view = inflater.inflate(R.layout.item_drop_down,parent,false)
            viewHolder = ItemRowHolder(view)
            view?.tag = viewHolder
        }else{
            view = convertView
            viewHolder = view.tag as ItemRowHolder
        }

        view.layoutParams.height = 80
        reportTypeId = reportType?.get(position)?.reportTypeID
        viewHolder.typeName.text = reportType?.get(position)?.reportTypeName
        return view
    }

    override fun getItem(position: Int): Any? {
        return reportType?.get(position)?.reportTypeName
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return reportType?.size ?: 0
    }


    private class ItemRowHolder(row:View){
        val typeName: TextView
        init {
            this.typeName = row.findViewById(R.id.txtDropDownLabel)
        }
    }
}