package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomPedingListWrapper(@SerializedName("Data") val chatRoomPendingData: ChatRoomPendingData,
                                     @SerializedName("Response") val responseList: ResponseList)