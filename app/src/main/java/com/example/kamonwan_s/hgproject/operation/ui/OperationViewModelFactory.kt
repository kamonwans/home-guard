package com.example.kamonwan_s.hgproject.operation.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.operation.repository.OperationRepository

class OperationViewModelFactory(private val app: Application, private val operationRepository: OperationRepository)
    :ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OperationViewModel(app, operationRepository) as T
    }
}