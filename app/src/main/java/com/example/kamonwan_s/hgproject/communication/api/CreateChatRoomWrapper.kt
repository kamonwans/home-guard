package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class CreateChatRoomWrapper(@SerializedName("Response") val responseList: ResponseList)