package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomStatusWrapper(@SerializedName("Response")val responseList: ResponseList)