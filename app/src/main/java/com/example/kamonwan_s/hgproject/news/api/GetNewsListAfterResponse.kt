package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class GetNewsListAfterResponse(@SerializedName("GetNewsListAfterResult") val newsListAfterWrapper : NewsListAfterWrapper)