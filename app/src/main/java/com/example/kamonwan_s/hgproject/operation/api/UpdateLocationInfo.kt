package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class UpdateLocationInfo(@SerializedName("Token")val token:String,
                              @SerializedName("OperationID")val operationID:String,
                              @SerializedName("Lat")val lat : String,
                              @SerializedName("Lng")val lng:String)