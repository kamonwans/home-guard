package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class GetChatRoomPToPResponse(@SerializedName("GetChatRoomP2PResult")val chatRoomPToPData : ChatRoomPToPData)