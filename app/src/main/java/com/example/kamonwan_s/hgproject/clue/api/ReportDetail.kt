package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportDetail(@SerializedName("Token") val token:String,
                        @SerializedName("ReportID") val reportID:Int)