package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomInfo(@SerializedName("Token") val token :String,
                        @SerializedName("DateTimeStr")val dateTimeStr:String)