package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class OrganizationAccount(@SerializedName("OrganizationID") val organizationID:Int
                               , @SerializedName("IsReceiveAlert") val isReceiveAlert:Int = IS_RECEIVE_ALERT) {
    companion object {
        private const val IS_RECEIVE_ALERT = 0
    }
}