package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class GetOfficerLocationResponse(@SerializedName("GetOfficerLocationResult") val officerLocationWrapper : OfficerLocationWrapper)