package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContents(@SerializedName("ChatContentID") val chatContentID: String,
                        @SerializedName("ChatContentTypeID") val chatContentTypeID: String,
                        @SerializedName("ChatRoomID") val chatRoomID: String,
                        @SerializedName("ContentDescription") val contentDescription: String,
                        @SerializedName("Content") val content: String,
                        @SerializedName("FirstName") val firstName: String,
                        @SerializedName("IsGroup") val isGroup: Int,
                        @SerializedName("LastName") val lastName: String,
                        @SerializedName("ModifiedDate") val modifiedDate: String,
                        @SerializedName("ProfilePic") val profilePic: String,
                        @SerializedName("SenderID") val senderID: String)