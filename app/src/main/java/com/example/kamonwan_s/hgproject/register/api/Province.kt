package com.example.kamonwan_s.hgproject.register.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.example.kamonwan_s.hgproject.Checker
import com.google.gson.annotations.SerializedName

data class Province(@SerializedName("ProvinceID") val provinceID: Int
                    , @SerializedName("ProvinceName") val provinceName: String
                    , @SerializedName("ModifiedDate") val modifiedDate: Long
                    , override var checked: Boolean = false) : Checker