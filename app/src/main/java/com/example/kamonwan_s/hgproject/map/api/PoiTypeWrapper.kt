package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiTypeWrapper(@SerializedName("Data") val poiTypeData : PoiTypeData,
                          @SerializedName("Response") val responseList: ResponseList)