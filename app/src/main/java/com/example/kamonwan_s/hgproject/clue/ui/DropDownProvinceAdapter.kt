package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.ProvinceEntity
import com.example.kamonwan_s.hgproject.register.api.ProvinceWrapper

class DropDownProvinceAdapter(val context: Context):BaseAdapter(){

    var province: ArrayList<ProvinceEntity>? = null
    val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val viewHolder: ItemRowHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_drop_down_province, parent, false)
            viewHolder = ItemRowHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ItemRowHolder
        }

        Log.d("provinceSize",province!!.size.toString())
        view.layoutParams.height = 80

        val provinceId = province?.get(position)?.provinceName
        val provinceName = province?.get(position)?.provinceName
        viewHolder.tvProvince.text = provinceName
        return view
    }

    override fun getItem(position: Int): Any? {
        return province?.get(position)?.provinceName
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
      return province?.size ?: 0
    }


    private class ItemRowHolder(row: View) {
        val tvProvince: TextView

        init {
            this.tvProvince= row.findViewById(R.id.txtDropDownLabelProvince) as TextView
        }
    }

}