package com.example.kamonwan_s.hgproject.news.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.MapDao
import com.example.kamonwan_s.hgproject.database.NewsImageEntity
import com.example.kamonwan_s.hgproject.database.NewsListEntity
import com.example.kamonwan_s.hgproject.database.NewsTypeEntity
import com.example.kamonwan_s.hgproject.news.api.*
import com.example.kamonwan_s.hgproject.operation.repository.OperationRepository
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepository constructor(private val executor: AppExecutors,
                                 private val newsApi: NewsApi,
                                 private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    fun loadNewsList(getNewsListInfo: GetNewsListInfo): LiveData<SingleEvent<GetNewsListResponse>> {
        val newsListData = MutableLiveData<SingleEvent<GetNewsListResponse>>()
        newsApi.loadNewsList(getNewsListInfo).enqueue(object : Callback<GetNewsListResponse> {
            override fun onFailure(call: Call<GetNewsListResponse>, t: Throwable) {
                Log.d(NewsRepository.tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetNewsListResponse>, response: Response<GetNewsListResponse>) {
                if (response.isSuccessful) {
                    newsListData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.newsListWrapper
                    executor.diskIO.execute {
                        try {
                            var news = dbInfoList.newsList.newss
                            var newsList = ArrayList<NewsListEntity>()
                            var newsImageList = ArrayList<NewsImageEntity>()
                            news.forEach {
                                val newsListEntity = NewsListEntity(
                                        it.content,
                                        it.header,
                                        it.modifiedDate,
                                        it.newsID.toInt(),
                                        it.newsTypeID.toInt()
                                )
                                newsList.add(newsListEntity)

                            }

                            news.forEach {
                                var newsImage = NewsImageEntity(
                                        it.newsID.toInt(),
                                        it.images.toString()
                                )
                                newsImageList.add(newsImage)
                            }

                            mapDao.insertNewsList(newsList)
                            mapDao.insertNewsImage(newsImageList)
                            if (dbInfoList.newsList.newss.isNotEmpty()) {
                                mapDao.insertNewsList(newsList)
                                mapDao.insertNewsImage(newsImageList)
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else
                    Log.d(NewsRepository.tag, response.errorBody()!!.string())
            }

        })
        return newsListData
    }

    fun loadNews(getNewsInfo: GetNewsInfo): LiveData<SingleEvent<GetNewsResponse>> {
        val newsData = MutableLiveData<SingleEvent<GetNewsResponse>>()
        newsApi.loadNews(getNewsInfo).enqueue(object : Callback<GetNewsResponse> {
            override fun onFailure(call: Call<GetNewsResponse>, t: Throwable) {
                Log.d(NewsRepository.tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetNewsResponse>, response: Response<GetNewsResponse>) {
                if (response.isSuccessful) {
                    newsData.value = SingleEvent(response.body()!!)


                } else
                    Log.d(NewsRepository.tag, response.errorBody()!!.string())
            }

        })
        return newsData
    }

    fun loadNewsType(token: Token): LiveData<SingleEvent<LoadNewsTypeResponse>> {
        val newsTypeData = MutableLiveData<SingleEvent<LoadNewsTypeResponse>>()
        newsApi.loadNewsType(token).enqueue(object : Callback<LoadNewsTypeResponse> {
            override fun onFailure(call: Call<LoadNewsTypeResponse>, t: Throwable) {
                Log.d(NewsRepository.tag, t.message.toString())
            }

            override fun onResponse(call: Call<LoadNewsTypeResponse>, response: Response<LoadNewsTypeResponse>) {
                if (response.isSuccessful) {
                    newsTypeData.value = SingleEvent(response.body()!!)

                    val dbInfoList = response.body()!!.newsTypeWrapper
                    executor.diskIO.execute {
                        try {
                            var newsType = dbInfoList.newsTypeData.newsType
                            var newsTypeList = ArrayList<NewsTypeEntity>()
                            newsType?.forEach {
                                val newsTypeListEntity = NewsTypeEntity(
                                        it.colorCode,
                                        it.modifiedDate,
                                        it.newsTypeID,
                                        it.newsTypeName
                                )

                                newsTypeList.add(newsTypeListEntity)
                            }

                            mapDao.insertNewsType(newsTypeList)
                            if (dbInfoList.newsTypeData.newsType.isNotEmpty()) {
                                mapDao.insertNewsType(newsTypeList)
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(NewsRepository.tag, response.errorBody()!!.string())
            }

        })
        return newsTypeData
    }

    fun loadNewsAfter(getNewsListInfo: GetNewsListInfo): LiveData<SingleEvent<GetNewsListAfterResponse>> {
        val newsAfter = MutableLiveData<SingleEvent<GetNewsListAfterResponse>>()
        newsApi.loadNewsListAfter(getNewsListInfo).enqueue(object : Callback<GetNewsListAfterResponse> {
            override fun onFailure(call: Call<GetNewsListAfterResponse>, t: Throwable) {
                Log.d(NewsRepository.tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetNewsListAfterResponse>, response: Response<GetNewsListAfterResponse>) {
                if (response.isSuccessful)
                    newsAfter.value = SingleEvent(response.body()!!)
                else
                    Log.d(NewsRepository.tag, response.errorBody()!!.string())
            }

        })
        return newsAfter
    }

    fun insertNewsType(newsType: List<NewsType>) {
//        executor.diskIO.execute {
//            try {
//                mapDao.insertNewsType(newsType)
//            }catch (e: SQLiteConstraintException) {
//                syncDatabaseError.postValue(SingleEvent(e.toString()))
//            } catch (e: Exception) {
//                syncDatabaseError.postValue(SingleEvent(e.toString()))
//                Log.d(tag, e.toString())
//            }
//        }
    }

    companion object {
        @Volatile
        private var instance: NewsRepository? = null
        private val tag = NewsRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, newsApi: NewsApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: NewsRepository(executor, newsApi, mapDao).apply {
                                instance = this
                            }
                }
    }

}