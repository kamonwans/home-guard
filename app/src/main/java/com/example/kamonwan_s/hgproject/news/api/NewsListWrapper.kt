package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class NewsListWrapper(@SerializedName("Data") val newsList :NewsList,
                           @SerializedName("Response") val responseList: ResponseList)