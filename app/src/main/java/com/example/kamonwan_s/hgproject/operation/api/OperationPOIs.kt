package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName


data class OperationPOIs(@SerializedName("ActionDetail")val actionDetail:String,
                         @SerializedName("OperationID")val operationID:Int,
                         @SerializedName("POIID")val poiID:Int)