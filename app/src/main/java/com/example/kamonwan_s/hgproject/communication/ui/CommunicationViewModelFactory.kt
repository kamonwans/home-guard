package com.example.kamonwan_s.hgproject.communication.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.communication.repository.CommunicationRepository

class CommunicationViewModelFactory(private val app :Application,private val communicationRepository: CommunicationRepository)
    :ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CommunicationViewModel(app,communicationRepository) as T
    }
}