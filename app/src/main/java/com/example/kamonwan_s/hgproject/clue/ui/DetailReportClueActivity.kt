package com.example.kamonwan_s.hgproject.clue.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.*
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.image.ImageActivity
import com.klickerlab.tcsd.ui.latlngmap.LocationMapsActivity
import kotlinx.android.synthetic.main.activity_detail_report_clue.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.util.ArrayList

class DetailReportClueActivity : AppCompatActivity() ,PhotoReportAdapter.ViewPagerClickListener{

    private var clueViewModel: ClueViewModel? = null
    var layoutManager: LinearLayoutManager? = null
    var token: String? = null
    var dateConvert: String? = null

    var title: String? = null
    var date: String? = null
    var statusID: String? = null
    var picProfile: String? = null
    var lat: Double? = null
    var long: Double? = null
    var favorite = 0
    var reportId = 0
    var accountId: Int = 0
    var commentNum: Int = 0
    var name: String? = null
    var adapter: PagerAdapter? = null
    var contentDetail: String? = null
    var adapterComment: CommentAdapter? = null
    var file: File? = null
    var offset: Int = 0
    var limit: Int = 4
    var sizeItem = 0
    var typeId: Int? = null
    var typeName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_report_clue)

        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.icon_arrow_left)
        }

        clueViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideClueReportViewModelFactory(application))[ClueViewModel::class.java]


        token = clueViewModel?.getToken()

        title = intent?.getStringExtra("title")
        date = intent?.getStringExtra("date")
        statusID = intent?.getStringExtra("status")
        picProfile = intent?.getStringExtra("picProfile")
        lat = intent?.getDoubleExtra("lat",0.0)
        long = intent?.getDoubleExtra("long",0.0)
        favorite = intent!!.getIntExtra("favorite",0)
        reportId = intent!!.getIntExtra("reportId",0)
        commentNum = intent!!.getIntExtra("commentNum",0)
        name = intent?.getStringExtra("name")
        typeId = intent?.getIntExtra("reportTypeId",0)

        imgProfileReport.setProfile(picProfile!!)
        tvNameUser.text = name
        tvTitle.text = title


        Log.d("commentNum", commentNum.toString())
        initInstance()
    }

    private fun initInstance() {
        adapterComment = CommentAdapter(this)
        layoutManager = LinearLayoutManager(this)
        recyclerViewComment.layoutManager = layoutManager
        recyclerViewComment.itemAnimator = DefaultItemAnimator()
       recyclerViewComment.adapter = adapterComment

        recyclerViewComment.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    //       Toast.makeText(context, "Last", Toast.LENGTH_SHORT).show()

                }
            }
        })

        clueViewModel?.loadDetailReport(ReportDetail(token!!, reportId))
        clueViewModel?.reportDetail?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportWrapper ->
                reportWrapper.reportDetailData.let {
                    adapter = it.let { PhotoReportAdapter(this, this, it.reportDetailAll.reportDetailList.report.images) }

                    if (it.reportDetailAll.reportDetailList.report.images.size == 0) {
                        pagerPhotoReportDetail.visibility = View.GONE
                    } else {
                       pagerPhotoReportDetail.visibility = View.VISIBLE
                    }

                    pagerPhotoReportDetail?.adapter = adapter
                    indicatorReport.setViewPager(pagerPhotoReportDetail)
                    tvContentReport.text = it.reportDetailAll.reportDetailList.report.content
                    // var statusId = it.reportDetailAll.reportDetailList.report.reportStatusID.toInt()
                    var dateMo = it.reportDetailAll.reportDetailList.report.modifiedDate
                    var timeDate = convertDate(dateMo, "dd/MM/yyyy hh:mm:ss")
                    tvDate.text = timeDate

                }
            }
        })

        clueViewModel?.loadReportType(TokenInfo(token!!))
        clueViewModel?.reportClueType?.observe(this, Observer {event ->
            event?.getContentIfNotHandled()?.let { wrapper ->
                wrapper.reportTypeData.reportTypeWrapper.reportTypeList.let { reportType ->
                    reportType.forEach {
                        val reportType = it.reportTypeID
                        val reportTypeName = it.reportTypeName
                        if (reportType == typeId){
                            tvType.text = reportTypeName
                        }
                    }
                }
            }

        })

        // load comment report
        clueViewModel?.loadComment(CommentInfo(token!!, reportId, offset, 100))
        clueViewModel?.commentData?.observe(this, Observer { event ->
            event?.reportReplyWrapper?.reportReplyList?.reportReply.let { comment ->
                comment?.forEach {
                    CommentAdapter(this)
                    adapterComment?.items = comment

                }
                adapterComment?.notifyDataSetChanged()
            }
        })

        clueViewModel?.loadStatusReport(SyncReportInfo(token!!, "1544893200000"))
        clueViewModel?.reportStatus?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportStatus ->
                reportStatus.reportStatusWrapper.reportStatusData.let { statusData ->
                    statusData.reportStatuses.let { status ->
                        if (statusID.equals("1")) {
                            tvStatus.text = status.get(0).ReportStatusName
                           tvStatus.setTextColor(Color.parseColor("#" + status.get(0).ColorCode))
                        } else if (statusID.equals("2")) {
                            tvStatus.text = status.get(1).ReportStatusName
                            tvStatus.setTextColor(Color.parseColor("#" + status.get(1).ColorCode))
                        }

                    }

                }
            }
        })

        if (lat == null && long == null) {
           imgLocationNonActive!!.visibility = View.VISIBLE
            imgLocationActive!!.visibility = View.INVISIBLE
        } else {
            imgLocationActive!!.visibility = View.VISIBLE
            imgLocationNonActive!!.visibility = View.INVISIBLE

           imgLocationActive!!.setOnClickListener {
                val intent = Intent(this, LocationMapsActivity::class.java)
                startActivity(intent)
            }
        }

        imgFavActive?.setOnClickListener {
            imgFavNonActive!!.visibility = View.VISIBLE
            imgFavActive!!.visibility = View.INVISIBLE

            clueViewModel?.updateFavorite(UpdateFavoriteInfo(token!!, reportId, 0))
            clueViewModel?.favorite?.observe(this, Observer { event ->
                event?.getContentIfNotHandled()?.let { update ->
                    update.reportList.let {

                    }
                }
            })
        }

        imgFavNonActive?.setOnClickListener {
            imgFavActive!!.visibility = View.VISIBLE
            imgFavNonActive!!.visibility = View.INVISIBLE

            clueViewModel?.updateFavorite(UpdateFavoriteInfo(token!!, reportId, 1))
            clueViewModel?.favorite?.observe(this, Observer { event ->
                event?.getContentIfNotHandled()?.let { update ->
                    update.reportList.let {

                    }
                }
            })
        }

        if (favorite == 0) {
            imgFavNonActive!!.visibility = View.VISIBLE
            imgFavActive!!.visibility = View.INVISIBLE
        } else {
            imgFavActive!!.visibility = View.VISIBLE
            imgFavNonActive!!.visibility = View.INVISIBLE
        }

        imgChoosePhoto!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent().apply {
                    action = Intent.ACTION_PICK
                    type = "image/*"
                }
                startActivityForResult(intent
                        , 1)
            }
        })

        imgSendMessage.setOnClickListener {
            var comment = editComment!!.text.toString()
            var commentCount = adapterComment?.items?.size

            Log.d("commentCount", commentCount.toString())

            if (file == null && comment == "") {
                Toast.makeText(this, "กรุณาพิมพ์ข้อความหรือเลือกรูปภาพ", Toast.LENGTH_LONG).show()
            } else {
                if (file != null) {
                    var photoBody = RequestBody.create(MediaType.parse("image/*"), file)

                    ClueApi.service.createReportReply(RequestBody.create(MediaType.parse("text/plain"), reportId.toString())
                            , RequestBody.create(MediaType.parse("text/plain"), comment)
                            , MultipartBody.Part.createFormData("file", file!!.name, photoBody)
                            , RequestBody.create(MediaType.parse("text/plain"), token))
                            .enqueue(object : retrofit2.Callback<CreateReportReplyResponse> {
                                override fun onFailure(call: Call<CreateReportReplyResponse>, t: Throwable) {
                                    Log.d("onFailure", t.message)
                                }

                                override fun onResponse(call: Call<CreateReportReplyResponse>, response: Response<CreateReportReplyResponse>) {
                                    if (response.isSuccessful) {
                                        loadComment(commentNum)
                                        Toast.makeText(this@DetailReportClueActivity, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                                    } else
                                        Log.d("xxx", response.errorBody()!!.string())
                                }

                            })
                    editComment!!.text?.clear()
                    imgPreview!!.visibility = View.GONE
                } else {
                    ClueApi.service.createReportReplyNopic(RequestBody.create(MediaType.parse("text/plain"), reportId.toString())
                            , RequestBody.create(MediaType.parse("text/plain"), comment)
                            , RequestBody.create(MediaType.parse("text/plain"), token))
                            .enqueue(object : retrofit2.Callback<CreateReportReplyResponse> {
                                override fun onFailure(call: Call<CreateReportReplyResponse>, t: Throwable) {
                                    Log.d("onFailure", t.message)
                                }

                                override fun onResponse(call: Call<CreateReportReplyResponse>, response: Response<CreateReportReplyResponse>) {
                                    if (response.isSuccessful) {
                                        loadComment(commentNum)

                                        Toast.makeText(this@DetailReportClueActivity, "ส่งข้อความสำเร็จ", Toast.LENGTH_SHORT).show()
                                    } else
                                        Log.d("xxx", response.errorBody()!!.string())
                                }

                            })
                    editComment!!.text?.clear()
                    imgPreview!!.visibility = View.GONE
                }
            }
        }
    }

    fun loadComment(commentCount: Int) {
        Log.d("commentToNum", commentCount.toString())
        clueViewModel?.loadComment(CommentInfo(token!!, reportId, commentCount, limit))
        clueViewModel?.commentData?.observe(this, Observer { event ->
            event?.reportReplyWrapper?.reportReplyList?.reportReply?.let {
                adapterComment?.items = it
                adapterComment?.notifyDataSetChanged()
                val scrollToPosition = adapterComment?.items?.size ?: 0
                if (!recyclerViewComment.canScrollVertically(1)) {
                    recyclerViewComment.scrollToPosition(scrollToPosition)
                }
            }
        })
    }


    // preview image then choose image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val uri: Uri = data!!.data

//            var file:File = File(URI(URLEncoder.encode(uri.toString(), "utf-8")))
            val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? = this.contentResolver.query(uri, filePathColumn, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(filePathColumn[0])
                path = it.getString(index)
            }
            if (path == null) {
                Log.d("xx", "path is null")
                return
            }
            file = File(path)
            imgPreview!!.visibility = View.VISIBLE
            imgPreview!!.setImageURI(uri)


        }

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }
    fun FragmentImage(position: Int, images: ArrayList<String>) {
        val intent = Intent(this, ImageActivity::class.java)
        val bundle = Bundle()
        intent.putExtra("name", bundle)
        bundle.putStringArrayList("image", images)
        bundle.putInt("position", position)
        startActivity(intent)
    }


    override fun onClickViewPager(position: Int, images: ArrayList<String>) {
        FragmentImage(position, images)
    }
}
