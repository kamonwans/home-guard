package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.*
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

data class Operations(@SerializedName("ChatRoomID") val chatRoomID: Int,
                      @SerializedName("Detail") val detail: String,
                      @SerializedName("IsOperate") val isWorking: Int,
                      @SerializedName("ModifiedDate") val modifiedDate: String,
                      @SerializedName("OperationID") val operationID: Int,
                      @SerializedName("OperationName") val operationName: String,
                      @SerializedName("OperationStatusID") val operationStatusID: Int,
                      @SerializedName("OperationTypeID") val operationTypeID: Int,
                      @SerializedName("OrganizationName") val organizationName: String,
                      @SerializedName("OrganizationPic") val organizationPic: String,
                      @SerializedName("OrganizationID") val organizationID: Int,
                      @SerializedName("OperationMembers") var operationMembers: MutableList<OperationMembers>?,
                      @SerializedName("OperationPOIs") var operationPOIs: MutableList<OperationPOIs>?,
                      @SerializedName("Images") var images: MutableList<String>?)