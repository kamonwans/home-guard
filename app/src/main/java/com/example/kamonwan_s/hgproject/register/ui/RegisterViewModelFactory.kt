package com.example.kamonwan_s.hgproject.register.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.register.repository.RegisterRepository

class RegisterViewModelFactory(private val app:Application,private val registerRepository: RegisterRepository)
    :ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(app, registerRepository) as T
    }
}