package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class SyncPOITypeResponse(@SerializedName("SyncPOITypeResult") val syncPOITypeWrapper : SyncPOITypeWrapper)