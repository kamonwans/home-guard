package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class SyncOrganizationResponse(@SerializedName("SyncOrganizationResult")val syncOrganizationWrapper:MutableList<SyncOrganizationWrapper>)