package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportTypeWrapper(@SerializedName("ReportType") val reportTypeList : ArrayList<ReportType>)