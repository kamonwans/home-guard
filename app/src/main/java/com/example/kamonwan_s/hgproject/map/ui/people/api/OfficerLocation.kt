package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class OfficerLocation(@SerializedName("AccountFullName") val accountFullName :String,
                           @SerializedName("Lat") val lat : String,
                           @SerializedName("Lng") val lng : String,
                           @SerializedName("OperationID") val operationID : Int,
                           @SerializedName("OperationName")val operationName : String,
                           @SerializedName("OperationStatusID") val operationStatusID : Int,
                           @SerializedName("OperationTypeID") val operationTypeID : Int,
                           @SerializedName("OrganizationID") val organizationID : Int,
                           @SerializedName("AccountID") val accountID : Int,
                           @SerializedName("FirstName")val firstName:String,
                           @SerializedName("LastName")val lastName:String,
                           @SerializedName("OperationStatusOnlineColor") val operationStatusOnlineColor:String)