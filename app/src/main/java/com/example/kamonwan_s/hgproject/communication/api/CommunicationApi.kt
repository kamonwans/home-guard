package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface CommunicationApi {

    @POST("SyncAccountContactList")
    fun syncAccountContactList(@Body accountContactInfo: AccountContactInfo): Call<SyncAccountContactListResponse>

    @POST("SyncChatRoomList")
    fun syncChatRoomList(@Body chatRoomInfo: ChatRoomInfo): Call<SyncChatRoomListResponse>

    @POST("GetChatRoomPedingList")
    fun getChatRoomPendingList(@Body token: Token): Call<GetChatRoomPedingListResponse>

    @POST("UpdateChatRoomStatus")
    fun updateChatRoomStatus(@Body chatRoomStatusInfo: ChatRoomStatusInfo): Call<UpdateChatRoomStatusResponse>

    @POST("GetChatContentList")
    fun getChatContentList(@Body chatContentListInfo: ChatContentListInfo): Call<GetChatContentListResponse>

    @POST("GetChatRoomMember")
    fun getChatRoomMember(@Body chatRoomMemberOperationInfo: ChatRoomMemberOperationInfo) : Call<GetChatRoomMemberResponse>

    @POST("GetChatRoomP2P")
    fun getChatRoomP2P(@Body chatRoomPToPInfo: ChatRoomPToPInfo) : Call<GetChatRoomPToPResponse>

    @POST("GetChatContentReaded")
    fun getChatContentReaded(@Body charContentReadedInfo: ChatContentReadedInfo) : Call<GetChatContentReadedResponse>

    @Multipart
    @Streaming
    @POST("CreateChatRoom")
    fun createChatRoom(@Part("ChatRoomName") chatRoomName: RequestBody,
                       @Part("Token") token: RequestBody,
                       @Part("Member") member: RequestBody,
                       @Part file: MultipartBody.Part): Call<CreateChatRoomResponse>

    @Multipart
    @Streaming
    @POST("CreateChatRoom")
    fun createChatRoomNoPic(@Part("ChatRoomName") chatRoomName: RequestBody,
                            @Part("Token") token: RequestBody,
                            @Part("Member") member: RequestBody): Call<CreateChatRoomResponse>


    @Multipart
    @Streaming
    @POST("CreateChatContentP2P")
    fun createChatContentP2POnlyPic(@Part("MemberID") memberId: RequestBody,
                                    @Part("ChatContentTypeID") chatContentTypeID: RequestBody,
                                    @Part("Token") token: RequestBody,
                                    @Part files: MultipartBody.Part): Call<CreateChatContentPToPResponse>

    @Multipart
    @Streaming
    @POST("CreateChatContentP2P")
    fun createChatContentP2POnlyText(@Part("MemberID") memberId: RequestBody,
                                     @Part("ChatContentTypeID") chatContentTypeID: RequestBody,
                                     @Part("ChatContent") chatContent: RequestBody,
                                     @Part("Token") token: RequestBody): Call<CreateChatContentPToPResponse>


    @Multipart
    @Streaming
    @POST("CreateChatContentP2P")
    fun createChatContentP2POnlyLatLng(@Part("MemberID") memberId: RequestBody,
                                       @Part("ChatContentTypeID") chatContentTypeID: RequestBody,
                                       @Part("Lat") lat: RequestBody,
                                       @Part("Lng") lng: RequestBody,
                                       @Part("Token") token: RequestBody): Call<CreateChatContentPToPResponse>

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/CommunicationService.svc/"
        private const val TIME_OUT: Long = 20
        val service: CommunicationApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(CommunicationApi::class.java)
        }
    }
}