package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class DataAccount(@SerializedName("Account") val account : Account)