package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class CreateChatContentP2PWrapper(@SerializedName("Data") val chatContentDataList: ChatContentDataList,
                                       @SerializedName("Response") val responseList: ResponseList)