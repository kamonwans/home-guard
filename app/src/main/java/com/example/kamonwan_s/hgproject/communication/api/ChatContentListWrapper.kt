package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContentListWrapper(@SerializedName("Data")val chatContentListData :ChatContentListData,
                                  @SerializedName("Response")val responseList: ResponseList)