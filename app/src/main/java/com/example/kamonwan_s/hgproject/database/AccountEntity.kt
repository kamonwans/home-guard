package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "account")
data class AccountEntity(
        @ColumnInfo(name = "account_first_name") val firstName: String,
        @ColumnInfo(name = "account_last_name") val lastName: String,
        @ColumnInfo(name = "account_tel") val tel: String,
        @ColumnInfo(name = "account_email") val email: String,
        @ColumnInfo(name = "account_password") val password: String?,
        @ColumnInfo(name = "account_type_id") val accountTypeID: Int,
        @ColumnInfo(name = "account_profile_pic") val profilePic: String? = null,
        @PrimaryKey @ColumnInfo(name = "account_account_id") val accountId: Int? = null,
        @ColumnInfo(name = "account_organization_id") val OrganizationID: Int? = null

)