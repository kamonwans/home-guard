package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class OfficerLocationWrapper(@SerializedName("Data") val officerLocationData : OfficerLocationData,
                                  @SerializedName("Response") val responseList : ResponseList)