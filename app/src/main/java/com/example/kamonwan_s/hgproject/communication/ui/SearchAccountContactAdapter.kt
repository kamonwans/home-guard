package com.example.kamonwan_s.hgproject.communication.ui


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.AccountContactEntity
import kotlinx.android.synthetic.main.item_account_contact_search.view.*
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAccountContactAdapter(val onClickAccountItem: (AccountContactEntity) -> Unit) : RecyclerView.Adapter<SearchAccountContactAdapter.SearchViewHolder>() {
    private var accountContactList = ArrayList<AccountContactEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SearchAccountContactAdapter.SearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_account_contact_search, parent, false)
        return SearchAccountContactAdapter.SearchViewHolder(view)

    }

    override fun getItemCount(): Int {
        return accountContactList.size
    }

    override fun onBindViewHolder(holder: SearchAccountContactAdapter.SearchViewHolder, position: Int) {
        holder.bind(accountContactList.get(position).firstName)
        holder.bindLastName(accountContactList.get(position).lastName)
        holder.bindAccountId(accountContactList.get(position).accountID.toString())
        holder.itemView.setOnClickListener { onClickAccountItem(accountContactList.get(position)) }

    }

    fun updateAccountCOntactList(newList: ArrayList<AccountContactEntity>) {
        accountContactList.clear()
        accountContactList.addAll(newList)
        notifyDataSetChanged()
    }

    class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: String) = with(itemView) {
            tvFirstName.text = item
        }

        fun bindLastName(item: String) = with(itemView) {
            tvLastName.text = item
        }

        fun bindAccountId(item: String) = with(itemView) {
            tvAccountId.text = "[" + item + "]"
        }
    }
}