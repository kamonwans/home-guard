package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class AccountContactInfo(@SerializedName("Token") val token :String,
                              @SerializedName("OrganizationID") val organizationID:Int,
                              @SerializedName("DateTimeStr") val dateTimeStr:String)