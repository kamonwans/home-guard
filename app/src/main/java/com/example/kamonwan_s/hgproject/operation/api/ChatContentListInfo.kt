package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class ChatContentListInfo(@SerializedName("Token")val token :String,
                               @SerializedName("ChatRoomID")val chatRoomID:String,
                               @SerializedName("Offset")val offset:String,
                               @SerializedName("Limit")val limit:String,
                               @SerializedName("DateTimeStr")val dateTimeStr:String)