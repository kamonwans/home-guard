package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import kotlinx.android.synthetic.main.item_chat_room_member.view.*

class ChatRoomAdapter(val context: Context) : RecyclerView.Adapter<ChatRoomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.item_chat_room_member, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 9

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.imageUserProfileMemberChatRoom.setProfile("https://idc2.klickerlab.com/img/CHATROOM/12/ProfileChatRoom.png")
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }
    interface BtnClickListener {
        fun onBtnClick(accountId: Int, name: String,pic:ArrayList<String>)
    }
}