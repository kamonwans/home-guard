package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class SyncChatRoomListResponse(@SerializedName("SyncChatRoomListResult")val chatRoomListWrapper : ChatRoomListWrapper)