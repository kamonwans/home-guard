package com.example.kamonwan_s.hgproject.map.ui.people

import android.app.Application
import android.arch.lifecycle.*
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.map.ui.people.api.*
import com.example.kamonwan_s.hgproject.map.ui.people.repository.PeopleRepository
import com.example.kamonwan_s.hgproject.poi.api.Token
import com.google.android.gms.maps.model.BitmapDescriptor

class PeopleViewModel(private val app : Application,private val peopleRepository: PeopleRepository)
    :AndroidViewModel(app){

    val peopleTypeBitmapDescriptors: LinkedHashMap<Int, BitmapDescriptor> = linkedMapOf()
    val people: MutableLiveData<SingleEvent<OfficerLocation>> = MutableLiveData()

    var getOfficerDetail = MediatorLiveData<OfficerDetail>()
    var tokenInfo = MediatorLiveData<Token>()
    var peopleDetailInfo = MediatorLiveData<PeopleDetailInfo>()

    private val officerLocationList:LiveData<GetOfficerLocationResponse> = peopleRepository.loadOfficerLocation(Token(getToken()))
    val officerLocation:LiveData<OfficerLocationData?> = Transformations.map(officerLocationList){
        it.officerLocationWrapper.officerLocationData
    }

//    private val peopleListInfo : LiveData<SingleEvent<GetOfficerLocationResponse>> = Transformations.switchMap(tokenInfo){
//        peopleRepository.loadOfficerLocation(it)
//    }

//    val peoples : LiveData<SingleEvent<MutableList<OfficerLocation>>> = Transformations.map(peopleListInfo){
//        SingleEvent(it.peekContent().officerLocationWrapper.officerLocationData.officerLocation)
//    }
//    val peopleList : LiveData<SingleEvent<GetOfficerLocationResponse>> = Transformations.switchMap(tokenInfo){
//        peopleRepository.loadOfficerLocation(it)
//    }

    val peopleDetail : LiveData<SingleEvent<GetOfficerDetailReponse>> = Transformations.switchMap(peopleDetailInfo){
        peopleRepository.loadDetailPeople(it)
    }

    fun loadPeopleList(token: Token){
        this.tokenInfo.value = token
    }
    fun loadOfficerDetail(peopleDetailInfo: PeopleDetailInfo){
        this.peopleDetailInfo.value = peopleDetailInfo
    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }
    fun getToken(): String {
        return retrieveToken(app)
    }

    var showLayers: Boolean = true


}