package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class NewsListAfterWrapper(@SerializedName("Data") val newsAfterList:NewsAfterList,
                                @SerializedName("Response")val responseList: ResponseList)