package com.example.kamonwan_s.hgproject.map.ui.report

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.map.ui.people.PeopleViewModel
import com.example.kamonwan_s.hgproject.map.ui.people.api.PeopleDetailInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_people_detail_dialog.view.*

private const val AccountID = "accountID"


class ReportDetailDialogFragment : BottomSheetDialogFragment() {
    private var accountID: Int? = null
    private var peopleViewModel: PeopleViewModel? = null
    var token:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_people_detail_dialog, container, false)
        initInstance(view,savedInstanceState)
        return view
    }

    private fun initInstance(view: View?, savedInstanceState: Bundle?) {
        peopleViewModel = ViewModelProviders.of(parentFragment!!
                ,InjectorUtil.providePeopleViewModelFactory(activity!!.application))[PeopleViewModel::class.java]

        token = peopleViewModel?.getToken()

        peopleViewModel?.loadOfficerDetail(PeopleDetailInfo(token!!, accountID!!))
        peopleViewModel?.peopleDetail?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { people ->
                people.officerDetailWrapper.officerDetailData.let {
                    if (it != null){
                        view?.tvPeopleFirstName?.text = it.officerDetail.firstName
                        view?.tvPeopleLastName?.text = it.officerDetail.lastName
                        view?.tvPeopleEmail?.text = it.officerDetail.email
                        view?.tvPeopleTel?.text = it.officerDetail.tel.toString()
                    }
                }
            }
        })

        view?.imageClosed?.setOnClickListener {
            dismiss()
        }
    }


    companion object {
        val tag: String = ReportDetailDialogFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ReportDetailDialogFragment().apply {

                }
    }
}
