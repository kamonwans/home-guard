package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class SyncProvinceList(@SerializedName("ModifiedDate")val modifiedDate:String,
                            @SerializedName("ProvinceID")val provinceID:String,
                            @SerializedName("ProvinceName")val provinceName:String)