package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "province")
data class ProvinceEntity(@PrimaryKey @ColumnInfo(name = "province_id") val provinceId:Int,
                          @ColumnInfo(name = "province_name")val provinceName: String,
                          @ColumnInfo(name = "modified_date")val modifiedDate: Long)