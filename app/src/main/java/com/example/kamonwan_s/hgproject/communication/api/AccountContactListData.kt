package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class AccountContactListData(@SerializedName("AccountContacts") val accountContacts: MutableList<AccountContacts>)