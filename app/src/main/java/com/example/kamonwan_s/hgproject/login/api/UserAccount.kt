package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class UserAccount(@SerializedName("Email") val email:String
                       , @SerializedName("Password") val password:String) {
}