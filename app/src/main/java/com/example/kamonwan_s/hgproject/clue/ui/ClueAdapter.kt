package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ReportListData
import com.example.kamonwan_s.hgproject.clue.api.ReportStatusData
import com.example.kamonwan_s.hgproject.clue.api.ReportTypeWrapper
import kotlinx.android.synthetic.main.item_clue.view.*

class ClueAdapter(val context: Context, val listener: ClueAdapter.BtnClickListener,
                  var location: ClueAdapter.ClickLocation, var favoriteUpdate: ClueAdapter.ClickFavorite)
    : RecyclerView.Adapter<ClueAdapter.ViewHolder>() {

    var items: ReportListData? = null
    var itemStatus: ReportStatusData? = null
    var itemType: ReportTypeWrapper? = null

    interface BtnClickListener {
        //        fun onBottomReached()
        fun onBtnClick(position: Int, title: String, dateConvert: String, status: String,
                       picProfile: String, lat: String, lng: String, favorite: Int,
                       reportId: Int, commentNum: Int, name: String, reportTypeId: Int)
    }

    interface ClickLocation {
        fun onLocation(lat: String, lon: String, iconReport: String, title: String)
    }

    interface ClickFavorite {
        fun onFavorite(position: Int, reportId: Int, isFavorite: Int)
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var tvType: TextView
        var tvStatus: TextView
        var tvTitle: TextView
        var tvDate: TextView
        var imgPic: ImageView
        var tvContent: TextView
        var tvName: TextView
        var imgProfile: ImageView
        var imgLocationNonActive: ImageView
        var imgLocationActive: ImageView
        var imgFavNonActive: ImageView
        var imgFavActive: ImageView
        var lnComment: LinearLayout
        var tvNumComment: TextView

        init {
            tvType = row.findViewById(R.id.tvType) as TextView
            tvStatus = row.findViewById(R.id.tvStatus) as TextView
            tvTitle = row.findViewById(R.id.tvTitle) as TextView
            imgPic = row.findViewById(R.id.imgPic) as ImageView
            tvContent = row.findViewById(R.id.tvContent) as TextView
            tvDate = row.findViewById(R.id.tvDate) as TextView
            tvName = row.findViewById(R.id.tvName) as TextView
            imgProfile = row.findViewById(R.id.imgProfile) as ImageView
            imgLocationNonActive = row.findViewById(R.id.imgLocationNonActive) as ImageView
            imgLocationActive = row.findViewById(R.id.imgLocationActive) as ImageView
            imgFavNonActive = row.findViewById(R.id.imgFavNonActive) as ImageView
            imgFavActive = row.findViewById(R.id.imgFavActive) as ImageView
            lnComment = row.findViewById(R.id.lnComment) as LinearLayout
            tvNumComment = row.findViewById(R.id.tvNumComment) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ClueAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_clue, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items?.reportList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var reportOwn = items!!.reportList[position]
        var reportStatus = itemStatus?.reportStatuses
        var reportType = itemType?.reportTypeList
        Log.d("reportStatus", reportStatus.toString())

        val reportId = reportOwn.reportID
        val title = reportOwn.title
        val date = reportOwn.modifiedDate.toString()
        val pic = reportOwn.profilePic
        val lat = reportOwn.lat
        val long = reportOwn.lng
        val favorite = reportOwn.isFavorite
        var status = reportOwn.reportStatusID.toString()
        var comment = reportOwn.numComment
        var name = reportOwn.creatorName
        val iconReport = reportOwn.profilePic
        val reportTypeId = reportOwn.reportTypeID
//        val colorType = reportStatus?.get(position)?.ColorCode

        var dateConvert = convertDate(date, "dd/MM/yyyy hh:mm:ss")

        holder.itemView.tvContent.text = reportOwn.content
        holder.itemView.tvName.text = reportOwn.creatorName
        holder.itemView.tvDate.text = dateConvert
        holder.itemView.tvTitle.text = reportOwn.title
        holder.itemView.imgProfile.setProfile(reportOwn.profilePic)
        holder.itemView.tvNumComment.text = reportOwn.numComment.toString()

        reportStatus?.forEach {
            var fontColor = it.ColorCode
            var statusName = it.ReportStatusName
            var statusId = it.ReportStatusID
            if (status.equals("1")) {
                if (statusId.equals(status)) {
                    holder.itemView.tvStatus.text = statusName
                    holder.itemView.tvStatus.setTextColor(Color.parseColor("#" + fontColor))
                }

            } else if (status.equals("2")) {
                if (statusId.equals(status)) {
                    holder.itemView.tvStatus.text = statusName
                    holder.itemView.tvStatus.setTextColor(Color.parseColor("#" + fontColor))
                }
            }
        }


//        if (reportTypeId == 1) {
//            holder.itemView.tvType.text = itemType?.reportTypeList?.get(0)?.reportTypeName
//        } else if (reportTypeId == 2) {
//            holder.itemView.tvType.text = itemType?.reportTypeList?.get(1)?.reportTypeName
//        } else if (reportTypeId == 3) {
//            holder.itemView.tvType.text = itemType?.reportTypeList?.get(2)?.reportTypeName
//        }


        holder.itemView.imgFavNonActive.setOnClickListener {
            favoriteUpdate.onFavorite(position, reportId, 1)
            holder.itemView.imgFavActive.visibility = View.VISIBLE
            holder.itemView.imgFavNonActive.visibility = View.INVISIBLE
        }

        holder.itemView.imgFavActive.setOnClickListener {
            if (favorite != null)
                favoriteUpdate.onFavorite(position, reportId, 0)
            holder.itemView.imgFavActive.visibility = View.INVISIBLE
            holder.itemView.imgFavNonActive.visibility = View.VISIBLE

        }


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listener.onBtnClick(position, title, date, status, pic, lat, long, favorite, reportId, comment, name, reportTypeId)
            }
        })

        if (reportOwn.images.size == 0) {
            holder.itemView.imgPic.visibility = View.GONE
        } else {
            holder.itemView.imgPic.visibility = View.VISIBLE
            holder.itemView.imgPic.setImage(reportOwn.images)
        }

        if (lat.equals("") && long.equals("")) {
            holder.itemView.imgLocationNonActive.visibility = View.VISIBLE
            holder.itemView.imgLocationActive.visibility = View.INVISIBLE
            holder.itemView.imgLocationNonActive.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    Toast.makeText(context, "ไม่ทราบพิกัด", Toast.LENGTH_SHORT).show()
                }

            })
        } else {
            holder.itemView.imgLocationActive.visibility = View.VISIBLE
            holder.itemView.imgLocationNonActive.visibility = View.INVISIBLE
            holder.itemView.imgLocationActive.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    if (location != null)
                        location.onLocation(lat, long, iconReport, title)
                    Log.d("fff", "fff")

                }

            })
        }

        if (favorite == 0) {
            holder.itemView.imgFavNonActive.visibility = View.VISIBLE
            holder.itemView.imgFavActive.visibility = View.INVISIBLE
        } else {
            holder.itemView.imgFavActive.visibility = View.VISIBLE
            holder.itemView.imgFavNonActive.visibility = View.INVISIBLE
        }

        if (position == items?.reportList?.size) {
//            listener.onBottomReached()
        }

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    fun ImageView.setImage(url: MutableList<String>) {
        Glide.with(context).load(url[0]).apply(RequestOptions.placeholderOf(R.drawable.default_image).error(R.drawable.default_image)).into(this)
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }
}