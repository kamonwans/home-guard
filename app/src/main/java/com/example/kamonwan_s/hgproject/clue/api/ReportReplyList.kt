package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportReplyList(@SerializedName("ReportReply") val reportReply : ArrayList<ReportReply>)