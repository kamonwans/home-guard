package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.example.kamonwan_s.hgproject.poi.api.Token
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.concurrent.TimeUnit
interface PeopleApi {

    @POST("GetOfficerLocation")
    fun getOfficerLocation(@Body token: Token) : Call<GetOfficerLocationResponse>

    @POST("GetOfficerDetail")
    fun getOfficerDetail(@Body peopleDetailInfo: PeopleDetailInfo) :Call<GetOfficerDetailReponse>

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/OperationService.svc/"
        private const val TIME_OUT: Long = 20
        val service: PeopleApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(PeopleApi::class.java)
        }
    }
}