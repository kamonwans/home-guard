package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "poi_type")
data class PoiTypeEntity(@PrimaryKey  @ColumnInfo(name = "poi_type_id") val poiTypeId : String ,
                         @ColumnInfo(name = "poi_type_icon")val icon : String,
                         @ColumnInfo(name = "poi_type_modifiedDate")val modifiedDate : String,
                         @ColumnInfo(name = "poi_type_category_id") val poiCategoryId : String,
                         @ColumnInfo(name = "poi_type_short_name")val shortName : String)