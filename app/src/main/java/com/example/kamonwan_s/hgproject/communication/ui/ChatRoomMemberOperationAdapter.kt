package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomMemberData
import kotlinx.android.synthetic.main.item_member_operation.view.*

class ChatRoomMemberOperationAdapter(val context: Context) : RecyclerView.Adapter<ChatRoomMemberOperationAdapter.ViewHolder>(){

    var itemsChatRoomMembeer: ChatRoomMemberData? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_member_operation,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return itemsChatRoomMembeer?.chatRoomMembers?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvPeopleFirstName.text = itemsChatRoomMembeer?.chatRoomMembers?.get(position)?.firstName
        holder.itemView.tvPeopleLastName.text = itemsChatRoomMembeer?.chatRoomMembers?.get(position)?.lastName
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }
}