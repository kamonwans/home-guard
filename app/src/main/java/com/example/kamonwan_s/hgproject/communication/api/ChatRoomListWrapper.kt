package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomListWrapper(@SerializedName("Data")val chatRoomListData : ChatRoomListData,
                               @SerializedName("Response") val responseList: ResponseList)