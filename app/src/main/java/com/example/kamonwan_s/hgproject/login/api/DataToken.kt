package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class DataToken(@SerializedName("Token") private val token: String)