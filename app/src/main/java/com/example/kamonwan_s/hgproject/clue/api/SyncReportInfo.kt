package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class SyncReportInfo(@SerializedName("Token") val token:String,
                          @SerializedName("DateTimeStr") val dateTimeStr : String)