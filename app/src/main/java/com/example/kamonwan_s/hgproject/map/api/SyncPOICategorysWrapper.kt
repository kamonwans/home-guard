package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class SyncPOICategorysWrapper(@SerializedName("Data") val poiCategoryData: PoiCategoryData,
                                   @SerializedName("Response") val responseList: ResponseList)