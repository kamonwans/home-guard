package com.example.kamonwan_s.hgproject.clue.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ClueOwn
import com.example.kamonwan_s.hgproject.clue.api.SyncReportInfo
import com.example.kamonwan_s.hgproject.clue.api.TokenInfo
import com.example.kamonwan_s.hgproject.clue.api.UpdateFavoriteInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_favorite_clue.view.*
import kotlinx.android.synthetic.main.fragment_organization_clue.*

class FavoriteClueFragment : Fragment(), ClueAdapter.BtnClickListener, ClueAdapter.ClickLocation, ClueAdapter.ClickFavorite {


    var adapter: ClueAdapter? = null
    var layoutManager: LinearLayoutManager? = null
    private var clueViewModel: ClueViewModel? = null
    var token: String? = null
    var progressbarClueFavorite: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_favorite_clue, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)


        setHasOptionsMenu(true)
        clueViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideClueReportViewModelFactory(activity!!.application))[ClueViewModel::class.java]
        initInstance(view)
        return view
    }

    var offset: Int = 0
    var limit: Int = 4
    var favoriteItemSize = 0
    private fun initInstance(view: View) {
        token = clueViewModel?.getToken()

        adapter = ClueAdapter(context!!, this, this, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewReportFavorite.layoutManager = layoutManager
        view.recyclerViewReportFavorite.itemAnimator = DefaultItemAnimator()
        view.recyclerViewReportFavorite.adapter = adapter

        // load Favorite
        clueViewModel?.loadFavoriteReport(ClueOwn(token!!, offset, limit, "0"))
        clueViewModel?.reportFavorite?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportFavoriteWrapper ->
                reportFavoriteWrapper.reportListFavoriteWrapper.reportListData.let {
                    if (it != null){
                        it.reportList.let {
                            adapter?.items?.reportList?.addAll(it)
                        }
                    }

                    if (adapter?.items == null) {
                        adapter?.items = reportFavoriteWrapper.reportListFavoriteWrapper.reportListData
                    }
                    adapter?.notifyDataSetChanged()
                    hideProgressbarLoading()

                    if (offset <= favoriteItemSize) {
                        offset = favoriteItemSize
                        favoriteItemSize = offset + limit
                    }


                }
            }
        })

        clueViewModel?.loadStatusReport(SyncReportInfo(token!!,"1544893200000"))
        clueViewModel?.reportStatus?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportStatus ->
                reportStatus.reportStatusWrapper.reportStatusData.let { statusData ->
                    statusData.reportStatuses.let { status ->
                        adapter?.itemStatus?.reportStatuses?.addAll(status)
                    }
                    if (adapter?.itemStatus == null){
                        adapter?.itemStatus = reportStatus.reportStatusWrapper.reportStatusData
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })

        clueViewModel?.loadReportType(TokenInfo(token!!))
        clueViewModel?.reportClueType?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportType ->
                reportType.reportTypeData.reportTypeWrapper.reportTypeList.let { typeData ->
                    if (typeData != null){
                        adapter?.itemType?.reportTypeList?.addAll(typeData)
                    }

                    if (adapter?.itemType == null){
                        adapter?.itemType = reportType.reportTypeData.reportTypeWrapper
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })

        view.recyclerViewReportFavorite.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)){
                    // Toast.makeText(context, "Last", Toast.LENGTH_SHORT).show()
                    var currentSizeItem = adapter?.itemCount ?: 0

                    if (offset <= currentSizeItem){
                        displayProgressbarLoading()
                        clueViewModel?.loadFavoriteAfter(ClueOwn(token!!,offset,limit,"1549353087000"))
                        clueViewModel?.favoriteAfter?.observe(this@FavoriteClueFragment, Observer { event ->
                            event?.getContentIfNotHandled()?.let { favoriteAfterList ->
                                favoriteAfterList.reportListOwnFavoriteAfterWrapper.reportListData.let {
                                    if (it != null){
                                        it.reportList.let {
                                            adapter?.items?.reportList?.addAll(it)
                                        }
                                    }

                                    if (adapter?.items == null){
                                        adapter?.items = favoriteAfterList.reportListOwnFavoriteAfterWrapper.reportListData
                                    }

                                    adapter?.notifyDataSetChanged()
                                }
                            }
                        })

                    }
                }
            }
        })


    }

    fun displayProgressbarLoading() {
        view?.progressbarClueFavorite?.visibility = View.VISIBLE
    }

    fun hideProgressbarLoading() {
        view?.progressbarClueFavorite?.visibility = View.GONE
    }

    override fun onBtnClick(position: Int, title: String, date: String, status: String, picProfile: String, lat: String, lng: String, favorite: Int, reportId: Int, commentNum: Int, name: String,reportTypeId:Int) {
        FragmentDetailReport(position, title, date, status, picProfile, lat, lng, favorite, reportId, commentNum, name,reportTypeId)

    }

    override fun onLocation(lat: String, lon: String, iconReport: String, title: String) {
        Toast.makeText(context, "Map", Toast.LENGTH_SHORT).show()
    }

    override fun onFavorite(position: Int, reportId: Int, isFavorite: Int) {
        clueViewModel?.updateFavorite(UpdateFavoriteInfo(token!!, reportId, isFavorite))
        clueViewModel?.favorite?.observe(this, Observer { event ->
            event?.getContentIfNotHandled().let { updateReportFavoriteResponse ->
                updateReportFavoriteResponse?.reportList.let {

                }
            }
        })
    }

    fun FragmentDetailReport(position: Int, title: String, date: String, status: String?,
                             picProfile: String, lat: String, lng: String, favorite: Int,
                             reportId: Int, commentNum: Int, name: String,reportTypeId:Int) {

        val intent = Intent(context,DetailReportClueActivity::class.java)
        intent.putExtra("position", position)
        intent.putExtra("title", title)
        intent.putExtra("dateModified", date)
        intent.putExtra("status", status)
        intent.putExtra("picProfile", picProfile)
        intent.putExtra("lat", lat)
        intent.putExtra("long", lng)
        intent.putExtra("favorite", favorite)
        intent.putExtra("reportId", reportId)
        intent.putExtra("commentNum", commentNum)
        intent.putExtra("name", name)
        intent.putExtra("reportTypeId",reportTypeId)
        startActivity(intent)

    }
    companion object {

        @JvmStatic
        fun newInstance() =
                FavoriteClueFragment().apply {

                }
    }
}
