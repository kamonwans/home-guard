package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "sub_district")
data class SubDistrictEntity(@PrimaryKey @ColumnInfo(name = "sub_district_id") val subDistrictID: Int,
                             @ColumnInfo(name = "district_id")val districtID: Int,
                             @ColumnInfo(name = "sub_district_name")val subDistrictName: String,
                             @ColumnInfo(name = "modified_date")val modifiedDate: Long)