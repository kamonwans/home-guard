package com.example.kamonwan_s.hgproject.map.ui.people.api

import com.google.gson.annotations.SerializedName

data class PeopleDetailInfo(@SerializedName("Token") val token :String,
                            @SerializedName("AccountID") val accountID :Int)