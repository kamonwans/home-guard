package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomPToPAccountContact(@SerializedName("AccountContacts")val accountContacts:AccountContacts)