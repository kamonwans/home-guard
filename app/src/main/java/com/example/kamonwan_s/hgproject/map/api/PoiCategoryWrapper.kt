package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiCategoryWrapper(@SerializedName("Data") val poiCategoryData :PoiCategoryData,
                              @SerializedName("Response") val responseList: ResponseList)