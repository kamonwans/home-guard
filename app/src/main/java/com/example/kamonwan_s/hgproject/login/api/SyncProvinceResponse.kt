package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class SyncProvinceResponse(@SerializedName("SyncProvinceResult")val syncProvinceList: MutableList<SyncProvinceList>)