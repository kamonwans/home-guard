package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportOrganizationInfo(@SerializedName("Token") val token: String,
                                  @SerializedName("Offset") val offset: Int,
                                  @SerializedName("Limit") val limit: Int,
                                  @SerializedName("DateTimeStr") val dateTimeStr: String,
                                  @SerializedName("OrganizationID") val organizationID: Int)