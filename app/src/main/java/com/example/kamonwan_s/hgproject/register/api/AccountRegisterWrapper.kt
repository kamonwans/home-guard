package com.example.kamonwan_s.hgproject.register.api


import com.google.gson.annotations.SerializedName

data class AccountRegisterWrapper(@SerializedName("Account") val account: Account
                                  , @SerializedName("Address") val address: AddressAccount?
                                  , @SerializedName("Organization") val organization: OrganizationAccount) {
}