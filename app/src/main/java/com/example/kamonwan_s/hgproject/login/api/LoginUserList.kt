package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class LoginUserList(@SerializedName("Data") val dataLoginUser : DataLoginUser,
                         @SerializedName("Response") val responseLoginUser: ResponseData)