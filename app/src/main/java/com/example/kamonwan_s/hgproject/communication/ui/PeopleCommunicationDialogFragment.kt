package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.*
import com.example.kamonwan_s.hgproject.map.ui.people.PeopleViewModel
import com.example.kamonwan_s.hgproject.map.ui.people.api.PeopleDetailInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_people_communication_dialog.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PeopleCommunicationDialogFragment : DialogFragment() {

    private var accountID: Int? = null
    private var peopleViewModel: PeopleViewModel? = null
    private var token: String? = null
    private var phoneNumber: String? = null
    private var firstName:String? = null
    private var communicationViewModel: CommunicationViewModel? = null
    private var organizationId:String? = null
    private var chatRoomId:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            accountID = it.getInt("accountID")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_people_communication_dialog, container, false)
        initInstance(view, savedInstanceState)
        return  view
    }

    private fun initInstance(view: View?, savedInstanceState: Bundle?) {
        peopleViewModel = ViewModelProviders.of(parentFragment!!
                , InjectorUtil.providePeopleViewModelFactory(activity!!.application))[PeopleViewModel::class.java]

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]

        token = communicationViewModel?.getToken()
        organizationId = communicationViewModel?.getOrganizationId()

        if (accountID != 0){
            communicationViewModel?.syncAccountContact(AccountContactInfo(token!!, organizationId!!.toInt(), "0"))
            communicationViewModel?.accoutContactList?.observe(this, Observer {event ->
                event?.getContentIfNotHandled()?.let { accountContactWrapper ->
                    accountContactWrapper.accountContactListWrapper.accountContactListData.accountContacts.let {
                        if (it != null){
                            it.let {accountContact ->
                                accountContact.forEach {account ->
                                    if (account.accountID.toInt() == accountID){
                                        view?.tvPeopleFirstName?.text = account.firstName
                                        view?.tvPeopleLastName?.text = account.lastName
                                        view?.tvPeopleEmail?.text = account.email
                                        view?.tvPeopleTel?.text = account.tel
                                        phoneNumber = account.tel
                                        firstName = account.firstName
                                        chatRoomId = account.chatRoomID
                                    }

                                }

                            }
                            view?.llCallPhoneNumber?.setOnClickListener {
                                val phone = phoneNumber
                                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                                startActivity(intent)
                            }


                            view?.llChat?.setOnClickListener {

                                communicationViewModel?.getChatRoomPToP(ChatRoomPToPInfo(token!!, accountID.toString()))
                                communicationViewModel?.chatRoomPToP?.observe(this, Observer { event ->
                                    event?.getContentIfNotHandled()?.let { chatRoom ->
                                        chatRoom.chatRoomPToPData.chatRoomPToPAccountContact.let {
                                            if (it != null) {
                                                chatRoomId = it.accountContacts.chatRoomID
                                                firstName = it.accountContacts.firstName
                                                accountID = it.accountContacts.accountID.toInt()
                                            }

                                            view?.llChat?.setOnClickListener {
                                                val intent = Intent(context, ChatMessageActivity::class.java)
                                                intent.putExtra("nameMember", firstName)
                                                intent.putExtra("accountId", accountID.toString())
                                                intent.putExtra("chatRoomId", chatRoomId)
                                                startActivity(intent)
                                            }
                                        }
                                    }


                                })

//                                CommunicationApi.service.createChatContentP2POnlyText(
//                                        RequestBody.create(MediaType.parse("text/plain"), accountID.toString()),
//                                        RequestBody.create(MediaType.parse("text/plain"), "1"),
//                                        RequestBody.create(MediaType.parse("text/plain"), "test"),
//                                        RequestBody.create(MediaType.parse("text/plain"), token)
//                                ).enqueue(object : Callback<CreateChatContentPToPResponse> {
//                                    override fun onFailure(call: Call<CreateChatContentPToPResponse>, t: Throwable) {
//                                        Log.d("onFailure", t.message)
//                                    }
//
//                                    override fun onResponse(call: Call<CreateChatContentPToPResponse>, response: Response<CreateChatContentPToPResponse>) {
//                                        if (response.isSuccessful) {
//                                            var chatRoom : String? = null
//                                            chatRoom = response.body()?.createChatContentP2PWrapper?.chatContentDataList?.chatContent?.chatRoomID
//                                           Log.d("isSuccessful",response.body().toString())
//                                            Toast.makeText(context, "สร้างห้องสำเร็จ", Toast.LENGTH_SHORT).show()
//                                            val intent = Intent(context,ChatMessageActivity::class.java)
//                                            intent.putExtra("nameMember",firstName)
//                                            intent.putExtra("accountId",accountID.toString())
//                                            intent.putExtra("chatRoomId",chatRoom)
//                                            startActivity(intent)
//                                        } else
//                                            Log.d("xxx", response.errorBody()!!.string())
//                                    }
//
//                                })


                            }
                        }
                    }
                }
            })

            view?.imageClosed?.setOnClickListener {
                dismiss()
            }
        }




//        if (accountID != 0) {
//            peopleViewModel?.loadOfficerDetail(PeopleDetailInfo(token!!, accountID!!))
//            peopleViewModel?.peopleDetail?.observe(this, Observer { event ->
//                event?.getContentIfNotHandled()?.let { people ->
//                    people.officerDetailWrapper.officerDetailData.let {
//                        if (it != null) {
//                            view?.tvPeopleFirstName?.text = it.officerDetail.firstName
//                            view?.tvPeopleLastName?.text = it.officerDetail.lastName
//                            view?.tvPeopleEmail?.text = it.officerDetail.email
//                            view?.tvPeopleTel?.text = it.officerDetail.tel
//                            phoneNumber = it.officerDetail.tel
//                            firstName = it.officerDetail.firstName
//                            Log.d("phoneNumber", phoneNumber)
//
//                            view?.llCallPhoneNumber?.setOnClickListener {
//                                val phone = phoneNumber
//                                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
//                                startActivity(intent)
//                            }
//
//                        }
//                    }
//
//                }
//            })
//        } else {
//            Toast.makeText(context, "account is 0", Toast.LENGTH_SHORT).show()
//        }
//
//        view?.imageClosed?.setOnClickListener {
//            dismiss()
//        }


    }


    companion object {
        val tag: String = PeopleCommunicationDialogFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(accountId:Int) =
                PeopleCommunicationDialogFragment().apply {
                    arguments = Bundle().apply {
                        putInt("accountID", accountId)
                    }
                }
    }
}
