package com.example.kamonwan_s.hgproject.clue.api

import android.content.SyncInfo
import com.example.kamonwan_s.hgproject.login.api.AccountWrapper
import com.example.kamonwan_s.hgproject.login.api.Token
import com.example.kamonwan_s.hgproject.register.api.AccountRegisterWrapper
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ClueApi {

    @POST("GetReportListOwn")
    fun getReportListOwn(@Body clueOwn: ClueOwn): Call<GetReportListOwnResponse>

    @POST("GetReportListOwnFavorite")
    fun getReportListOwnFavorite(@Body clueOwn: ClueOwn): Call<GetReportListOwnFavoriteResponse>

    @POST("GetReportListOrganization")
    fun getReportListOrganization(@Body reportOrganizationInfo: ReportOrganizationInfo): Call<GetReportListOrganizationResponse>

    @POST("UpdateReportFavorite")
    fun updateReportFavorite(@Body updateFavoriteInfo: UpdateFavoriteInfo): Call<UpdateReportFavoriteResponse>

    @POST("GetReportListOwnAfter")
    fun getReportListOwnAfter(@Body clueOwn: ClueOwn) : Call<GetReportListOwnAfterResponse>

    @POST("GetReportListOwnFavoriteAfter")
    fun getReportListOwnFavoriteAfter(@Body clueOwn: ClueOwn) : Call<GetReportListOwnFavoriteAfterResponse>

    @POST("GetReportListOrganizationAfter")
    fun getReportListOrganizationAfter(@Body reportOrganizationInfo: ReportOrganizationInfo) : Call<GetReportListOrganizationAfterResponse>

    @Multipart
    @Streaming
    @POST("CreateReport")
    fun createReportClue(@Part("Title") title: RequestBody,
                         @Part("Content") content: RequestBody,
                         @Part("Remark") remark: RequestBody,
                         @Part("ReportTime") reportTime: RequestBody,
                         @Part("ReportTypeID") reportTypeID: RequestBody,
                         @Part("OrganizationID") organizationID: RequestBody,
                         @Part("Lat") lat: RequestBody,
                         @Part("Lng") lng: RequestBody,
                         @Part files: MutableList<MultipartBody.Part>,
                         @Part("Token") token: RequestBody) : Call<CreateReportResponse>

    @Multipart
    @Streaming
    @POST("CreateReport")
    fun createReportClueNoPic(@Part("Title") title: RequestBody,
                         @Part("Content") content: RequestBody,
                         @Part("Remark") remark: RequestBody,
                         @Part("ReportTime") reportTime: RequestBody,
                         @Part("ReportTypeID") reportTypeID: RequestBody,
                         @Part("OrganizationID") organizationID: RequestBody,
                         @Part("Lat") lat: RequestBody,
                         @Part("Lng") lng: RequestBody,
                         @Part("Token") token: RequestBody) : Call<CreateReportResponse>

    @POST("LoadReportType")
    fun loadReportType(@Body tokenInfo: TokenInfo):Call<LoadReportTypeResponse>

    @POST("GetAccount")
    fun loadAccount(@Body  tokenInfo: TokenInfo): Call<AccountWrapper>

    @POST("GetReport")
    fun getReportDetail(@Body reportDetail: ReportDetail) :Call<GetReportDetailResponse>

    @POST("SyncReportType")
    fun syncReportType(@Body syncReportInfo: SyncReportInfo) : Call<SyncReportTypeResponse>

    @POST("SyncReportStatus")
    fun syncReportStatus(@Body syncReportInfo: SyncReportInfo) : Call<SyncReportStatusResponse>

    @POST("LoadReportStatus")
    fun loadReportStatus(@Body syncReportInfo: SyncReportInfo) : Call<LoadReportStatusResponse>


    @Multipart
    @POST("CreateReportReply")
    fun createReportReply(@Part("ReportID")  reportID:RequestBody,
                          @Part("Content")  content:RequestBody,
                          @Part files: MultipartBody.Part,
                          @Part("Token") token :RequestBody) : Call<CreateReportReplyResponse>

    @Multipart
    @POST("CreateReportReply")
    fun createReportReplyNopic(@Part("ReportID")  reportID:RequestBody,
                          @Part("Content")  content:RequestBody,
                          @Part("Token") token :RequestBody) : Call<CreateReportReplyResponse>

    @POST("GetReportReply")
    fun loadReportReply(@Body commentInfo: CommentInfo):Call<GetReportReplyResponse>
    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/ReportService.svc/"
        private const val TIME_OUT: Long = 20
        val service: ClueApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(ClueApi::class.java)
        }
    }
}