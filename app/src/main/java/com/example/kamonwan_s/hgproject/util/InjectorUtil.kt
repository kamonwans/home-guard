package com.example.kamonwan_s.hgproject.util

import android.app.Application
import android.content.Context
import com.example.kamonwan_s.hgproject.clue.api.ClueApi
import com.example.kamonwan_s.hgproject.clue.ui.ClueRepository
import com.example.kamonwan_s.hgproject.clue.ui.ClueViewModelFactory
import com.example.kamonwan_s.hgproject.communication.api.CommunicationApi
import com.example.kamonwan_s.hgproject.communication.repository.CommunicationRepository
import com.example.kamonwan_s.hgproject.communication.ui.CommunicationViewModelFactory
import com.example.kamonwan_s.hgproject.database.AppDatabase
import com.example.kamonwan_s.hgproject.map.api.PoiApi
import com.example.kamonwan_s.hgproject.map.MapViewModelFactory
import com.example.kamonwan_s.hgproject.map.repository.MapRepository
import com.example.kamonwan_s.hgproject.news.api.NewsApi
import com.example.kamonwan_s.hgproject.news.repository.NewsRepository
import com.example.kamonwan_s.hgproject.news.ui.NewsViewModelFactory
import com.example.kamonwan_s.hgproject.operation.api.OperationApi
import com.example.kamonwan_s.hgproject.operation.repository.OperationRepository
import com.example.kamonwan_s.hgproject.operation.ui.OperationViewModelFactory
import com.example.kamonwan_s.hgproject.register.api.RegisterApi
import com.example.kamonwan_s.hgproject.register.ui.RegisterViewModelFactory
import com.example.kamonwan_s.hgproject.register.repository.RegisterRepository
import com.example.kamonwan_s.hgproject.map.ui.people.api.PeopleApi
import com.example.kamonwan_s.hgproject.map.ui.people.repository.PeopleRepository
import com.example.kamonwan_s.hgproject.map.ui.people.PeopleViewModelFactory
import com.example.kamonwan_s.hgproject.statution.api.StationApi
import com.example.kamonwan_s.hgproject.statution.repository.StationRepository
import com.example.kamonwan_s.hgproject.statution.ui.StationViewModelFactory


object InjectorUtil {

    private fun provideRegisterRepository(context:Context): RegisterRepository {
        val registerApi = RegisterApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return RegisterRepository.getInstance(executors,registerApi,database.mapDao())
    }

    private fun provideMapRepository(context:Context): MapRepository {
        val poiApi = PoiApi.service
        val database = AppDatabase.getAppDatabase(context)
        val executors = AppExecutors.getInstance()
        return MapRepository.getInstance(executors,poiApi,database.mapDao())
    }

    private fun providePeopleRepository(): PeopleRepository {
        val peopleApi = PeopleApi.service
        val executors = AppExecutors.getInstance()
        return PeopleRepository.getInstance(executors,peopleApi)
    }

    private fun provideClueReportRepository(context: Context):ClueRepository{
        val cluApi = ClueApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return ClueRepository.getInstance(executors,cluApi,database.mapDao())
    }

    private fun provideNewsRepository(context: Context):NewsRepository{
        val newsApi = NewsApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return NewsRepository.getInstance(executors,newsApi,database.mapDao())
    }

    private fun provideOperationRepository(context: Context): OperationRepository {
        val operationApi = OperationApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return OperationRepository.getInstance(executors,operationApi,database.mapDao())
    }

    private fun provideCommunicationRepository(context: Context): CommunicationRepository {
        val communicationApi = CommunicationApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return CommunicationRepository.getInstance(executors,communicationApi,database.mapDao())
    }

    private fun provideStationRepository(context: Context): StationRepository {
        val stationApi = StationApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context)
        return StationRepository.getInstance(executors,stationApi,database.mapDao())
    }

    fun provideRegisterViewModelFactory(app:Application) : RegisterViewModelFactory {
        return RegisterViewModelFactory(app, provideRegisterRepository(app.applicationContext))
    }

    fun provideMapViewModelFactory(app:Application): MapViewModelFactory {
        return MapViewModelFactory(app, provideMapRepository(app.applicationContext))
    }

    fun providePeopleViewModelFactory(app: Application): PeopleViewModelFactory {
        return PeopleViewModelFactory(app, providePeopleRepository())
    }

    fun provideClueReportViewModelFactory(app: Application):ClueViewModelFactory{
        return ClueViewModelFactory(app,provideClueReportRepository(app.applicationContext))
    }

    fun provideNewsViewModelFactory(app: Application):NewsViewModelFactory{
        return NewsViewModelFactory(app, provideNewsRepository(app.applicationContext))
    }

    fun provideOperationViewModelFactory(app: Application) : OperationViewModelFactory {
        return OperationViewModelFactory(app, provideOperationRepository(app.applicationContext))
    }

    fun provideCommunicationViewModelFactory(app: Application) : CommunicationViewModelFactory{
        return CommunicationViewModelFactory(app, provideCommunicationRepository(app.applicationContext))
    }

    fun provideStationViewModelFactory(app: Application) : StationViewModelFactory {
        return StationViewModelFactory(app, provideStationRepository(app.applicationContext))
    }

}