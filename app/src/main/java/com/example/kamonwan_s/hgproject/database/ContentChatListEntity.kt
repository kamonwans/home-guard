package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "chat_content")
data class ContentChatListEntity(@PrimaryKey @ColumnInfo(name = "chat_content_id")val chatContentID:String,
                                 @ColumnInfo(name = "chat_content_type_id")val chatContentTypeID:String,
                                 @ColumnInfo(name = "chat_room_id")val chatRoomID:String,
                                 @ColumnInfo(name = "content_description")val contentDescription:String,
                                 @ColumnInfo(name = "content")val content:String,
                                 @ColumnInfo(name = "first_name")val firstName:String,
                                 @ColumnInfo(name = "is_group")val isGroup:Int,
                                 @ColumnInfo(name = "last_name")val lastName:String,
                                 @ColumnInfo(name = "modified_date")val modifiedDate:String,
                                 @ColumnInfo(name = "profile_pic")val profilePic:String,
                                 @ColumnInfo(name = "sender_id")val senderID:String)