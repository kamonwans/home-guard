package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class UpdateOperationWorkingResponse(@SerializedName("UpdateIsOperateResult") val operationWorkingWrapper :OperationWorkingWrapper)