package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class GetNewsInfo(@SerializedName("Token") val token :String,
                       @SerializedName("NewsID") val newsID :String)