package com.example.kamonwan_s.hgproject.operation.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.operation.api.OperationList
import com.example.kamonwan_s.hgproject.operation.api.Operations
import kotlinx.android.synthetic.main.item_member.view.*

class MemberAdapter(val context: Context) : RecyclerView.Adapter<MemberAdapter.ViewHolder>() {

   // var items: OperationList? = null
    var itemMember: Operations? = null


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MemberAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_member, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (itemMember != null) {
            var organizationPic = itemMember?.organizationPic
            holder.itemView.tvFirstNameMember.text = itemMember?.operationMembers?.get(position)?.firstName
            holder.itemView.tvLastNameMember.text = itemMember?.operationMembers?.get(position)?.lastName
            holder.itemView.tvReportNameMember.text = itemMember?.operationName
            holder.itemView.tvActionDetail.text = itemMember?.operationMembers?.get(position)?.actionDetail
            holder.itemView.tvOperationRoleTypeName.text = itemMember?.operationMembers?.get(position)?.operationRoleTypeName
            holder.itemView.profileOrganization.setProfile(organizationPic!!)
        }


    }

    override fun getItemCount(): Int {
        return itemMember?.operationMembers?.size ?: 0
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    interface ClickLocation {
        fun onLocation(lat: String, lon: String, iconReport: String, name: String)
    }
}