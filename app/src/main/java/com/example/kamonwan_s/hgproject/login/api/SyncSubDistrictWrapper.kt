package com.example.kamonwan_s.hgproject.login.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity(tableName = "subDistrict")
data class SyncSubDistrictWrapper(@NonNull @ColumnInfo(name = "district_id") @SerializedName("DistrictID")val districtID:String,
                                  @NonNull @ColumnInfo(name = "modified_date")@SerializedName("ModifiedDate")val modifiedDate:String,
                                  @PrimaryKey @NonNull @ColumnInfo(name = "sub_district_id")@SerializedName("SubDistrictID")val subDistrictID:String,
                                  @NonNull @ColumnInfo(name = "sub_district_name") @SerializedName("SubDistrictName")val subDistrictName:String)