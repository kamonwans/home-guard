package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class GetPOI(@SerializedName("Token") val token : String,
                  @SerializedName("POIID") val poiID : Int)