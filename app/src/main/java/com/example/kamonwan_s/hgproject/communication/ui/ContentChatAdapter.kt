package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.MediaController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatContentListData
import com.example.kamonwan_s.hgproject.communication.api.ChatContentReaded
import com.example.kamonwan_s.hgproject.communication.api.ChatContentReadedInfo
import com.example.kamonwan_s.hgproject.communication.api.ChatContentReadedWrapper
import kotlinx.android.synthetic.main.item_chat.view.*
import kotlinx.android.synthetic.main.item_chat_content.view.*

class ContentChatAdapter(val context: Context,val listener: ContentChatAdapter.BtnClickListener) : RecyclerView.Adapter<ContentChatAdapter.ViewHolder>() {

    var items: ChatContentListData? = null
    var itemRead : ChatContentReaded? = null
    private var uri: Uri? = null
    private var mediaController: MediaController? = null
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ContentChatAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat, parent, false)
        return ContentChatAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.chatContents?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var senderId = items?.chatContents?.get(position)?.senderID
        var picProfile = items?.chatContents?.get(position)?.profilePic
        var content = items?.chatContents?.get(position)?.content
        var chatContentTypeId = items?.chatContents?.get(position)?.chatContentTypeID
        var date = items?.chatContents?.get(position)?.modifiedDate
        var chatContentId = items?.chatContents?.get(position)?.chatContentID
        var contentDescription = items?.chatContents?.get(position)?.contentDescription
        var isGroup = items?.chatContents?.get(position)?.isGroup




        var dateConvert = convertDate(date!!, "hh:mm")
        senderId.let {
            if (!it.equals(senderId)) {
                if (chatContentTypeId.equals("1")) {
                    holder.itemView.MessageOther.text = items?.chatContents?.get(position)?.content
                    holder.itemView.imageProfileOther.setProfile(picProfile!!)
                    holder.itemView.tvTimeMessage.text = dateConvert

                    holder.itemView.MessageOther.visibility = View.VISIBLE
                    holder.itemView.imageProfileOther.visibility = View.VISIBLE
                    holder.itemView.tvTimeMessage.visibility = View.VISIBLE



                } else if (chatContentTypeId.equals("2")) {
                    holder.itemView.imageImageOther.visibility = View.VISIBLE
                    holder.itemView.tvTimeImage.visibility = View.VISIBLE
                    holder.itemView.MessageImageOther.visibility = View.VISIBLE

                    holder.itemView.tvTimeImage.text = dateConvert
                    holder.itemView.MessageImageOther.setImage(content!!)

                } else if (chatContentTypeId.equals("3")) {
                    mediaController = MediaController(context)
                    uri = Uri.parse(content)
                    mediaPlayer = MediaPlayer()
                    mediaPlayer = MediaPlayer.create(context, uri)
                    holder.itemView.imageProfileAudio.setProfile(picProfile!!)
                    holder.itemView.tvTimeAudio.text = dateConvert

                    holder.itemView.tvTimeAudio.visibility =View.VISIBLE
                    holder.itemView.imageProfileAudio.visibility = View.VISIBLE
                    holder.itemView.MessageAudioPlayOther.visibility = View.VISIBLE

                    holder.itemView.MessageAudioPlayOther.setOnClickListener {
                        mediaPlayer?.start()
                        holder.itemView.MessageAudioPauseOther.visibility = View.VISIBLE
                        holder.itemView.MessageAudioPlayOther.visibility = View.INVISIBLE

                    }

                    holder.itemView.MessageAudioPauseOther.setOnClickListener {
                        mediaPlayer?.stop()
                        holder.itemView.MessageAudioPlayOther.visibility = View.VISIBLE
                        holder.itemView.MessageAudioPauseOther.visibility = View.INVISIBLE
                        mediaPlayer?.prepare()
                    }

                } else if (chatContentTypeId.equals("4")) {
                    holder.itemView.imageProfileVideo.visibility = View.VISIBLE
                    holder.itemView.tvTimeVideo.visibility = View.VISIBLE
                    holder.itemView.MessageVideoOther.visibility = View.VISIBLE
                    holder.itemView.MessageImageDefaultOther.visibility = View.VISIBLE

                    holder.itemView.imageProfileVideo.setProfile(picProfile!!)
                    holder.itemView.tvTimeVideo.text = date
                    if (!contentDescription.equals("")){
                        holder.itemView.MessageVideoOther.setImage(contentDescription!!)
                    }
                    holder.itemView.MessageVideoOther.setOnClickListener(object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            listener.onBtnClick(content!!)
                        }
                    })
                } else if (chatContentTypeId.equals("5")) {
                    holder.itemView.MessageLocationOther.text = items?.chatContents?.get(position)?.content
                    holder.itemView.imageProfileLocationOther.setProfile(picProfile!!)
                    holder.itemView.tvTimeLocation.text = dateConvert

                    holder.itemView.MessageLocationOther.visibility = View.VISIBLE
                    holder.itemView.imageProfileLocationOther.visibility = View.VISIBLE
                    holder.itemView.tvTimeLocation.visibility = View.VISIBLE

                }

            } else {
                if (chatContentTypeId.equals("1")) {
                    holder.itemView.MessageMe.visibility = View.VISIBLE
                    holder.itemView.tvTimeMessage.visibility = View.VISIBLE
                    if (isGroup == 1){
                        holder.itemView.tvReadMessage.visibility = View.VISIBLE
                    }

                    holder.itemView.MessageMe.text = items?.chatContents?.get(position)?.content
                    holder.itemView.tvTimeMessage.text = dateConvert

                } else if (chatContentTypeId.equals("2")) {
                    holder.itemView.MessageImageMe.setImage(content!!)
                    holder.itemView.tvTimeImage.text = dateConvert

                    holder.itemView.MessageImageMe.visibility = View.VISIBLE
                    holder.itemView.tvTimeImage.visibility = View.VISIBLE
                    if (isGroup == 1){
                        holder.itemView.tvReadImage.visibility = View.VISIBLE
                    }

                } else if (chatContentTypeId.equals("3")) {
                    mediaController = MediaController(context)
                    uri = Uri.parse(content)
                    mediaPlayer = MediaPlayer()
                    mediaPlayer = MediaPlayer.create(context, uri)
                    holder.itemView.tvTimeAudio.text = dateConvert

                    holder.itemView.tvTimeAudio.visibility = View.VISIBLE
                    holder.itemView.MessageAudioPlayMe.visibility = View.VISIBLE
                    if (isGroup == 1){
                        holder.itemView.tvReadAudio.visibility = View.VISIBLE
                    }

                    holder.itemView.MessageAudioPlayMe.setOnClickListener {
                        mediaPlayer?.start()
                        holder.itemView.MessageAudioPauseMe.visibility = View.VISIBLE
                        holder.itemView.MessageAudioPlayMe.visibility = View.INVISIBLE

                    }

                    holder.itemView.MessageAudioPauseMe.setOnClickListener {
                        mediaPlayer?.stop()
                        holder.itemView.MessageAudioPlayMe.visibility = View.VISIBLE
                        holder.itemView.MessageAudioPauseMe.visibility = View.INVISIBLE
                        mediaPlayer?.prepare()
                    }

                } else if (chatContentTypeId.equals("4")) {
                    holder.itemView.MessageVideoMe.visibility = View.VISIBLE
                    holder.itemView.MessageImageDefaultMe.visibility = View.VISIBLE
                    holder.itemView.tvTimeVideo.visibility = View.VISIBLE
                    if (isGroup == 1){
                        holder.itemView.tvReadVideo.visibility = View.VISIBLE
                    }

                    holder.itemView.tvTimeVideo.text = dateConvert

                    if (!contentDescription.equals("")){
                        holder.itemView.MessageVideoMe.setImage(contentDescription!!)
                    }

                    holder.itemView.MessageVideoMe.setOnClickListener(object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            listener.onBtnClick(content!!)
                        }
                    })

                } else if (chatContentTypeId.equals("5")) {
                    holder.itemView.MessageLocationMe.visibility = View.VISIBLE
                    holder.itemView.tvTimeLocation.visibility = View.VISIBLE
                    if (isGroup == 1){
                        holder.itemView.tvReadLocation.visibility = View.VISIBLE
                    }

                    holder.itemView.MessageLocationMe.text = content
                    holder.itemView.tvTimeLocation.text = dateConvert

                }

            }
        }
    }



    interface BtnClickListener {
        fun onBtnClick(content:String)
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    fun ImageView.setImage(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.default_image).error(R.drawable.default_image)).into(this)
    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }
}



