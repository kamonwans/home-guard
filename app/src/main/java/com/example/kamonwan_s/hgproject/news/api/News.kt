package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class News(@SerializedName("Content") val content : String,
                @SerializedName("Header") val header:String,
                @SerializedName("Images") val images : ArrayList<String>,
                @SerializedName("ModifiedDate") val modifiedDate : String,
                @SerializedName("NewsID") val newsID :Int,
                @SerializedName("NewsTypeID") val newsTypeID :Int)