package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactListData
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomListData
import kotlinx.android.synthetic.main.item_list_message_room.view.*

class MessageRoomAdapter(val context: Context, val listener: BtnClickListener) : RecyclerView.Adapter<MessageRoomAdapter.ViewHolder>() {

    var items: ChatRoomListData? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_message_room, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.chatRooms?.size ?: 0

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var name = items?.chatRooms?.get(position)?.chatRoomName
        var message = items?.chatRooms?.get(position)?.content
        var date = items?.chatRooms?.get(position)?.modifiedDate
        var picProfile = items?.chatRooms?.get(position)?.chatRoomPic

        var accountId = items?.chatRooms?.get(position)?.accountID
        var chatRoomId = items?.chatRooms?.get(position)?.chatRoomID


        var dateConvert = convertDate(date!!, "hh:mm")

        holder.itemView.tvName.text = name
        holder.itemView.tvLastMessage.text = message
        holder.itemView.tvTime.text = dateConvert
        holder.itemView.imageUserProfile.setProfile(picProfile!!)


        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listener.onBtnClick(name!!,accountId!!,chatRoomId!!)
            }
        })
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }
    interface BtnClickListener {
        fun onBtnClick(fistName:String,accountId:String,chatRoomId:String)
    }
}