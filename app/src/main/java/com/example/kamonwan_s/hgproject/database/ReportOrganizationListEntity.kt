package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
@Entity(tableName = "report_organization")
data class ReportOrganizationListEntity(@ColumnInfo(name = "account_id") val accountID : Int,
                                        @ColumnInfo(name = "report_content") val content : String,
                                        @ColumnInfo(name = "report_creator_name") val creatorName:String,
                                        @ColumnInfo(name = "report_is_favorite") val isFavorite : Int,
                                        @ColumnInfo(name = "report_lat")val lat:String,
                                        @ColumnInfo(name = "report_lng") val lng : String,
                                        @ColumnInfo(name = "report_modified_date") val modifiedDate :Long,
                                        @ColumnInfo(name = "report_num_comment")val numComment :Int,
                                        @ColumnInfo(name = "report_profile_pic") val profilePic : String,
                                        @PrimaryKey @ColumnInfo(name = "report_id") val reportID : Int,
                                        @ColumnInfo(name = "report_status_id") val reportStatusID : Int,
                                        @ColumnInfo(name = "report_type_id") val reportTypeID : Int,
                                        @ColumnInfo(name = "report_title") val title : String)