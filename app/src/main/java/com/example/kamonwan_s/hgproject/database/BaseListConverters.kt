package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.TypeConverter
import com.example.kamonwan_s.hgproject.R.drawable.operation
import com.example.kamonwan_s.hgproject.operation.api.OperationMembers
import com.example.kamonwan_s.hgproject.operation.api.OperationPOIs
import com.example.kamonwan_s.hgproject.operation.api.Operations
import com.google.gson.GsonBuilder

abstract class BaseListConverters<T> {

    @TypeConverter
    abstract fun fromString(value: String): ArrayList<T>?

    @TypeConverter
    fun operationArrayListToString(value: MutableList<T>): String {
        val json = GsonBuilder().create().toJson(value)
        return json ?: ""
    }



//    @TypeConverter
//    fun fromStringPoi(operation: String): MutableList<OperationPOIs>? {
//        return if (operation.equals("operationMembers")) {
//            Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                    1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf()).operationPOIs
//        } else {
//            Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                    1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf()).operationPOIs
//
//        }
//    }
//
//    @TypeConverter
//    fun operationListToStringPoi(operation: MutableList<Operations>): String {
//        return if (operation == Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                        1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf())
//                        .operationPOIs) {
//            ""
//        } else ""
//    }
//
//    @TypeConverter
//    fun fromStringImage(image:String) : MutableList<String>{
//        return if (image.equals("images")) {
//            Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                    1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf()).images
//        }else {
//            Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                    1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf()).images
//        }
//    }
//
//    @TypeConverter
//    fun operationListToStringImage(operation: MutableList<String>): String {
//        return if (operation == Operations(0, "qq", mutableListOf(), 1, "12", 1, "ss",
//                        1, 1, "ss", "sd", 1, mutableListOf(), mutableListOf())
//                        .images) {
//            ""
//        } else ""
//    }
}