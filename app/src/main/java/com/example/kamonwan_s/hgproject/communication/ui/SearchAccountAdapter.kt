package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.AccountContactEntity

class SearchAccountAdapter(context: Context, val layout: Int, val accountContract: MutableList<AccountContactEntity>)
    : ArrayAdapter<AccountContactEntity>(context, layout, accountContract) {
    override fun getCount(): Int {
        return accountContract.size
    }

    override fun getItem(position: Int): AccountContactEntity? {
        return accountContract.get(position)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var tvSearch: View
        var vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (convertView == null) {
            tvSearch = vi.inflate(layout, null)
        } else {
            tvSearch = convertView
        }

        var accountItem = getItem(position)
        val accountFirstName = tvSearch.findViewById(R.id.tvFistName) as TextView
        val accountLastName = tvSearch.findViewById(R.id.tvLastName) as TextView
        accountFirstName.text = accountItem?.firstName
        accountLastName.text = accountItem?.lastName
        return tvSearch
    }
}