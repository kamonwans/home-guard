package com.example.kamonwan_s.hgproject.map.ui.people.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.map.ui.people.api.*
import com.example.kamonwan_s.hgproject.poi.api.Token
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PeopleRepository private constructor(private val executors: AppExecutors,
                                           private val peopleApi : PeopleApi){
    fun loadOfficerLocation(token: Token) : LiveData<GetOfficerLocationResponse>{
        val officerLocationdata = MutableLiveData<GetOfficerLocationResponse>()
        peopleApi.getOfficerLocation(token).enqueue(object :Callback<GetOfficerLocationResponse?>{
            override fun onFailure(call: Call<GetOfficerLocationResponse?>, t: Throwable) {
                Log.d(tag, t.message)
            }

            override fun onResponse(call: Call<GetOfficerLocationResponse?>, response: Response<GetOfficerLocationResponse?>) {
                if (response.isSuccessful){
                    officerLocationdata.value = response.body()!!
                }else{
                    Log.d(tag, response.errorBody()!!.string())
                }
            }

        })
        return officerLocationdata
    }

    fun loadDetailPeople(peopleDetailInfo: PeopleDetailInfo):LiveData<SingleEvent<GetOfficerDetailReponse>>{
        val peopleData = MutableLiveData<SingleEvent<GetOfficerDetailReponse>>()
        peopleApi.getOfficerDetail(peopleDetailInfo).enqueue(object :Callback<GetOfficerDetailReponse>{
            override fun onFailure(call: Call<GetOfficerDetailReponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetOfficerDetailReponse>, response: Response<GetOfficerDetailReponse>) {
                 if (response.isSuccessful)
                     peopleData.value = SingleEvent(response.body()!!)
                else
                     Log.d(tag, response.errorBody()!!.string())
            }

        })
        return peopleData
    }
    companion object {
        @Volatile
        private var instance: PeopleRepository? = null
        private val tag = PeopleRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, peopleApi: PeopleApi) = instance
                ?: synchronized(this) {
                    instance
                            ?: PeopleRepository(executor, peopleApi).apply {
                                instance = this
                            }
                }
    }

}