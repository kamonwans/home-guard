package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class NewsTypeWrapper(@SerializedName("Data") val newsTypeData : NewsTypeData,
                           @SerializedName("Response") val responseList: ResponseList)