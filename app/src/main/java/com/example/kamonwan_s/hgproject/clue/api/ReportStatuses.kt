package com.example.kamonwan_s.hgproject.clue.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class ReportStatuses(@SerializedName("ColorCode") val ColorCode : String,
                          @SerializedName("ModifiedDate") val ModifiedDate:String,
                          @SerializedName("ReportStatusID") val ReportStatusID:String,
                          @SerializedName("ReportStatusName") val ReportStatusName:String)