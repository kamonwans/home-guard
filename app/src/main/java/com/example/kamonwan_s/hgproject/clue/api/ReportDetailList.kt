package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportDetailList(@SerializedName("Report") val report : Report)