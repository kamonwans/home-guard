package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomMembers(@SerializedName("AccountID")val accountID:String,
                           @SerializedName("ChatRoomStatusID")val chatRoomStatusID:String,
                           @SerializedName("FirstName")val firstName:String,
                           @SerializedName("LastName")val lastName:String,
                           @SerializedName("ModifiedDate")val modifiedDate:String,
                           @SerializedName("OrganizationID")val organizationID:String,
                           @SerializedName("ChatRoomID")val ChatRoomID:String)