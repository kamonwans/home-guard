package com.example.kamonwan_s.hgproject.clue.api

import com.example.kamonwan_s.hgproject.clue.api.ReportType
import com.google.gson.annotations.SerializedName

data class SyncReportTypeData(@SerializedName("ReportType") val syncReportType : MutableList<ReportType>)