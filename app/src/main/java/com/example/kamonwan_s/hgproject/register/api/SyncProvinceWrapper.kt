package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName


data class SyncProvinceWrapper(@SerializedName("SyncProvinceResult") val syncProvince: MutableList<Province>?)