package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class GetReportListOwnAfterResponse(@SerializedName("GetReportListOwnAfterResult") val reportListOwnAfterWrapper:ReportListOwnAfterWrapper)