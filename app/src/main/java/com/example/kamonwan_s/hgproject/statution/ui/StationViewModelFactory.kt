package com.example.kamonwan_s.hgproject.statution.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.statution.repository.StationRepository

class StationViewModelFactory(private val app : Application, private val stationRepository: StationRepository)
    : ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StationViewModel(app,stationRepository) as T
    }
}