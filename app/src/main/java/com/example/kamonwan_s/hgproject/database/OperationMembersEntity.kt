package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull
@Entity(tableName = "operation_member")
data class OperationMembersEntity(@ColumnInfo(name = "operation_member_account_id")val accountID:Int,
                                  @ColumnInfo(name = "operation_member_action_detail")val actionDetail:String,
                                  @PrimaryKey @ColumnInfo(name = "operation_member_operation_id")val operationID:Int,
                                  @ColumnInfo(name = "operation_member_operation_role_type_id")val operationRoleTypeID:Int,
                                  @ColumnInfo(name = "operation_member_role_type_name")val operationRoleTypeName:String,
                                  @ColumnInfo(name = "operation_member_fist_name")val firstName:String,
                                  @ColumnInfo(name = "operation_member_last_name")val lastName :String)