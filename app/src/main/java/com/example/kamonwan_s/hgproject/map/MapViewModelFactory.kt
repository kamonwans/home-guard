package com.example.kamonwan_s.hgproject.map

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.map.repository.MapRepository

class MapViewModelFactory(private val app :Application,private val mapRepository: MapRepository)
    : ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MapViewModel(app, mapRepository) as T
    }
}