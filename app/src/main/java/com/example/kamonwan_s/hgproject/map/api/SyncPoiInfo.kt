package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class SyncPoiInfo(@SerializedName("Token") val token : String,
                       @SerializedName("DateTimeStr") val dateTime : String)