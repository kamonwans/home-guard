package com.example.kamonwan_s.hgproject.map.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.kamonwan_s.hgproject.R
import kotlinx.android.synthetic.main.item_poi_image.view.*

class ImageAdapter(private val imageData:MutableList<String>, private val listener:ImageAdapter.OnItemClickListener)
    :RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageViewHolder(parent)
    }

    override fun getItemCount(): Int = imageData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ImageViewHolder).bindTo(imageData[position])
    }

    inner class ImageViewHolder(private val parent:ViewGroup)
        :RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_poi_image,parent,false)){
        fun bindTo(url:String){
            Glide.with(parent.context).load(url).into(itemView.ivPoi)
            itemView.ivPoi.setOnClickListener {
                listener.onImageClick(adapterPosition,imageData)
            }
        }
    }
    interface OnItemClickListener{
        fun onImageClick(position: Int, imageData: MutableList<String>)
    }
}