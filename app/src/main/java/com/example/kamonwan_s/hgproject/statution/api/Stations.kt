package com.example.kamonwan_s.hgproject.statution.api

import com.google.gson.annotations.SerializedName

data class Stations(@SerializedName("DistrictID") val districtID: String,
                    @SerializedName("Lat") val lat: String,
                    @SerializedName("Lng") val lng: String,
                    @SerializedName("ModifiedDate") val modifiedDate:String,
                    @SerializedName("OrganizationID") val organizationID:String,
                    @SerializedName("ProvinceID")val provinceID:String,
                    @SerializedName("StationID")val stationID:String,
                    @SerializedName("StationName")val stationName:String,
                    @SerializedName("SubDistrictID")val subDistrictID:String,
                    @SerializedName("Tel")val tel:String
                    )