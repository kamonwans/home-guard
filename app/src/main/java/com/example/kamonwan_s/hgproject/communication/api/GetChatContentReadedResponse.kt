package com.example.kamonwan_s.hgproject.communication.api

import com.example.kamonwan_s.hgproject.clue.api.ReportList
import com.google.gson.annotations.SerializedName

data class GetChatContentReadedResponse(@SerializedName("GetChatContentReadedResult") val chatContentReadedWrapper : ChatContentReadedWrapper,
                                        @SerializedName("Response")val reportList: ReportList)