package com.example.kamonwan_s.hgproject.login.ui

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.kamonwan_s.hgproject.login.repository.AccountRepository

class LoginViewModelFactory(private val app:Application, private val accountRepository: AccountRepository)
    :ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(app, accountRepository) as T
    }
}