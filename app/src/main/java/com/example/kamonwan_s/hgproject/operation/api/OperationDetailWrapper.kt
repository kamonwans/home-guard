package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationDetailWrapper(@SerializedName("Data") val operationDetailData:OperationDetailData,
                                  @SerializedName("Response") val responseList: ResponseList)