package com.example.kamonwan_s.hgproject.operation.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.operation.api.OperationInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_member.view.*

class MemberFragment : Fragment(){


    private var layoutManager: LinearLayoutManager? = null
    private var adapter: MemberAdapter? = null
    private var token: String? = null
    private var operationViewModel: OperationViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("สมาชิก")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_member, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)
        operationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideOperationViewModelFactory(activity!!.application))[OperationViewModel::class.java]

        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = operationViewModel?.getToken()
        adapter = MemberAdapter(context!!)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewMember.layoutManager = layoutManager
        view.recyclerViewMember.itemAnimator = DefaultItemAnimator()
        view.recyclerViewMember.adapter = adapter

        operationViewModel?.loadOperationOwn(OperationInfo(token!!, 1, 0))
        operationViewModel?.operationOwn?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { operationWrapper ->

                operationWrapper.operationListOwnWrapper.operationList.operations.forEach { operations ->
                    operations.operationMembers.let {
                        if (it != null) {
                            adapter?.itemMember?.operationMembers?.addAll(it)
                        }

                        if (adapter?.itemMember == null) {
                            adapter?.itemMember = operations
                        }
                        adapter?.notifyDataSetChanged()
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            activity?.onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }

    }



    companion object {
        val tag = MemberFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                MemberFragment().apply {

                }
    }
}
