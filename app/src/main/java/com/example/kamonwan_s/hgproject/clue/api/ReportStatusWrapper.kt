package com.example.kamonwan_s.hgproject.clue.api

import com.example.kamonwan_s.hgproject.poi.api.ResponseList
import com.google.gson.annotations.SerializedName

data class ReportStatusWrapper(@SerializedName("Data") val reportStatusData : ReportStatusData,
                               @SerializedName("Response") val responseList: ResponseList)