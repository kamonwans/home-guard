package com.example.kamonwan_s.hgproject.clue.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ClueApi
import com.example.kamonwan_s.hgproject.clue.api.CreateReportResponse
import com.example.kamonwan_s.hgproject.clue.api.ReportType
import com.example.kamonwan_s.hgproject.clue.api.TokenInfo
import com.example.kamonwan_s.hgproject.register.api.AccountRegisterWrapper
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.latlngmap.LocationMapsActivity
import kotlinx.android.synthetic.main.fragment_create_report_clue.*
import kotlinx.android.synthetic.main.fragment_create_report_clue.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CreateReportClueFragment : Fragment() {
    var dropdownTypeAdapter: DropDownTypeAdapter? = null
    var dropdownStationAdapter: DropDownStationAdapter? = null
    var dropDownProvinceAdapter: DropDownProvinceAdapter? = null
//    var dropDownDistrictAdapter : DropDownDistrictAdapter? = null
//    var dropDownSubDistrictAdapter : DropDownSubDistrictAdapter? = null

    private var clueViewModel: ClueViewModel? = null
    var ownOrganizationType: ArrayList<ReportType>? = null
    var ownOrganizationName: AccountRegisterWrapper? = null
    var token: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var profile: String? = null
    val multipartPhotoBodies: MutableList<MultipartBody.Part> = mutableListOf()
    var photoBody: RequestBody? = null
    var file: File? = null
    var reportTypeID: String? = null
    var time: String? = null
    var day: String? = null
    var dateTime: Long? = null
    var lattitude: String? = ""
    var longitude: String? = ""
    var latMap: Double? = null
    var lngMap: Double? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        latMap = arguments?.getDouble("latMap") ?: 0.0
        lngMap = arguments?.getDouble("lngMap") ?: 0.0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_report_clue, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)

        setHasOptionsMenu(true)
        initInstance(view)
        checkDB()

        if (latMap != 0.0 || lngMap != 0.0) {
            view?.editLat?.setText(latMap.toString())
            view?.editLong?.setText(lngMap.toString())
        }
        // load report type
        clueViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideClueReportViewModelFactory(activity!!.application))[ClueViewModel::class.java]
        token = clueViewModel?.getToken()
        firstName = clueViewModel?.retrieveFirstName()
        lastName = clueViewModel?.retrieveLastName()
        profile = clueViewModel?.retrieveProfilePic()

        view.tvNameCreateReport.text = firstName
        view.tvLastNameCreateReport.text = lastName
        view.imgProfile.setProfile(profile!!)

        clueViewModel?.loadReportType(TokenInfo(token!!))
        clueViewModel?.reportClueType?.observe(this, Observer { event ->
            event?.getContentIfNotHandled().let { reportTypeWrapper ->
                reportTypeWrapper?.reportTypeData?.reportTypeWrapper.let {
                    dropdownTypeAdapter?.reportType = reportTypeWrapper?.reportTypeData?.reportTypeWrapper?.reportTypeList
                    dropdownTypeAdapter?.notifyDataSetChanged()
                    this.ownOrganizationType = it?.reportTypeList
                }
            }
        })

        // load organization report
//        clueViewModel?.loadOrganizationOwn(TokenInfo(token!!))
//        clueViewModel?.organizationOwn?.observe(this, Observer { event ->
//            event?.getContentIfNotHandled().let { organizationWrapper ->
//                organizationWrapper?.AccountList?.dataAccount.let { accountList ->
//                    accountList?.account.let {
//                        dropdownStationAdapter?.ownOrganization = ownOrganizationName
//                        dropdownStationAdapter?.notifyDataSetChanged()
//                        this.ownOrganizationName = ownOrganizationName
//                    }
//                }
//            }
//        })

        clueViewModel?.loadProvinceList()
        clueViewModel?.provinceList?.observe(this, Observer { wrapper ->
            dropDownProvinceAdapter?.province = wrapper
            dropDownProvinceAdapter?.notifyDataSetChanged()

        })

        clueViewModel?.loadStation()
        clueViewModel?.stationList?.observe(this, Observer { wrapper ->
            dropdownStationAdapter?.station = wrapper
            dropdownStationAdapter?.notifyDataSetChanged()
        })

        return view
    }

    fun checkDB(): Boolean {
        var db: SQLiteDatabase? = null

        try {
            db = SQLiteDatabase.openDatabase(dbPath + dbName, null, SQLiteDatabase.OPEN_READONLY)
        } catch (e: SQLException) {
        }
        return db != null
    }

    private fun initInstance(view: View) {
        //  var dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)
        var timeFormat = SimpleDateFormat("hh:mm:ss", Locale.US)
        dropdownTypeAdapter = DropDownTypeAdapter(context!!)
        dropDownProvinceAdapter = DropDownProvinceAdapter(context!!)
        dropdownStationAdapter = DropDownStationAdapter(context!!)



        view.spinnerTypeReport?.adapter = dropdownTypeAdapter
        view.spinnerTypeReport?.onItemSelectedListener = object : AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                reportTypeID = spinnerTypeReport?.selectedItem.toString()
                if (reportTypeID.equals("ฉุกเฉิน")) {
                    reportTypeID = "1"
                } else if (reportTypeID.equals("เบาะแส")) {
                    reportTypeID = "2"
                } else if (reportTypeID.equals("ขอความช่วยเหลือ")) {
                    reportTypeID = "3"
                }
                Log.d("typereportId", reportTypeID)
            }

        }

        view.spinnerTypeProvince?.adapter = dropDownProvinceAdapter
        view.spinnerTypeProvince?.onItemSelectedListener = object : AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        }

        view.spinnerTypeStation?.adapter = dropdownStationAdapter
        view.spinnerTypeStation?.onItemSelectedListener = object  : AdapterView.OnItemClickListener,AdapterView.OnItemSelectedListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        }



        day = view.editTime.text.toString()
        time = view.editTime.text.toString()

        if (day.isNullOrEmpty() && time.isNullOrBlank()) {
            Toast.makeText(context, "กรุณาใส่วันที่และเวลาเกิดเหตุ", Toast.LENGTH_SHORT).show()
        } else {
            view.editDate.setOnClickListener {
                val now = Calendar.getInstance()
                val year = now.get(Calendar.YEAR)
                val month = now.get(Calendar.MONTH)
                val date = now.get(Calendar.DAY_OF_MONTH)

                val dialogPickerDate = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, date ->
                    editDate.setText(date.toString() + "/" + (month + 1).toString() + "/" + year.toString())
                }, year, month, date)

                dialogPickerDate.show()
            }

            view.editTime.setOnClickListener {
                val now = Calendar.getInstance()
                val timePicker = TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { view, hour, minute ->
                    val selectTime = Calendar.getInstance()
                    selectTime.set(Calendar.HOUR_OF_DAY, hour)
                    selectTime.set(Calendar.MINUTE, minute)
                    editTime.setText(timeFormat.format(selectTime.time))
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false)
                timePicker.show()
            }
        }

        view.lnChooseImage.setOnClickListener {
            val intent = Intent().apply {
                putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                action = Intent.ACTION_PICK
                type = "image/*"
            }
            startActivityForResult(intent, 1)
        }
        view.tvSummitReport.setOnClickListener {
            var reportTime: Long? = null
            token = clueViewModel?.getToken()
            var token = token

            var dateText = editDate.text.toString()
            var timeText = editTime.text.toString()
            var title = editTitleReport.text.toString()
            var contentText = editContentReport.text.toString()
            var organizationID = "1"
            var lat = editLat.text.toString()
            var lng = editLong.text.toString()

            // get dateTime to convert Long
            val selectDateTime = Calendar.getInstance()
            dateTime = selectDateTime.timeInMillis
            reportTime = dateTime

            // check edit text lat lng empty
            if (lat == "" && lng == "") {
                lat = "0"
                lng = "0"
            } else if (lat == "" && lng != "") {
                lat = "0"
            } else if (lng == "" && lat != "") {
                lng = "0"
            }


            if (title.isBlank() || contentText.isBlank() || dateText.isBlank() || timeText.isBlank()) {
                Toast.makeText(context, "กรุณากรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
            } else if (multipartPhotoBodies != null) {
                ClueApi.service.createReportClue(RequestBody.create(MediaType.parse("text/plain"), title)
                        , RequestBody.create(MediaType.parse("text/plain"), contentText)
                        , RequestBody.create(MediaType.parse("text/plain"), "remark")
                        , RequestBody.create(MediaType.parse("text/plain"), reportTime.toString())
                        , RequestBody.create(MediaType.parse("text/plain"), reportTypeID)
                        , RequestBody.create(MediaType.parse("text/plain"), organizationID)
                        , RequestBody.create(MediaType.parse("text/plain"), lat)
                        , RequestBody.create(MediaType.parse("text/plain"), lng)
                        , multipartPhotoBodies
                        , RequestBody.create(MediaType.parse("text/plain"), token))
                        .enqueue(object : retrofit2.Callback<CreateReportResponse?> {
                            override fun onFailure(call: Call<CreateReportResponse?>, t: Throwable) {
                                Log.d("createOnFailure", t.message.toString())

                            }

                            override fun onResponse(call: Call<CreateReportResponse?>, response: Response<CreateReportResponse?>) {
                                if (response.isSuccessful) {
                                    Toast.makeText(context, "สร้างรายงานสำเร็จ", Toast.LENGTH_SHORT).show()
                                    FragmentReport()
                                } else {
                                    Log.d("notSuccessful", response.errorBody()!!.string())
                                }
                            }

                        })

            } else if (multipartPhotoBodies == null) {
                ClueApi.service.createReportClueNoPic(RequestBody.create(MediaType.parse("text/plain"), title)
                        , RequestBody.create(MediaType.parse("text/plain"), contentText)
                        , RequestBody.create(MediaType.parse("text/plain"), "remark")
                        , RequestBody.create(MediaType.parse("text/plain"), reportTime.toString())
                        , RequestBody.create(MediaType.parse("text/plain"), reportTypeID)
                        , RequestBody.create(MediaType.parse("text/plain"), organizationID)
                        , RequestBody.create(MediaType.parse("text/plain"), lat)
                        , RequestBody.create(MediaType.parse("text/plain"), lng)
                        , RequestBody.create(MediaType.parse("text/plain"), token))
                        .enqueue(object : Callback<CreateReportResponse> {
                            override fun onFailure(call: Call<CreateReportResponse>, t: Throwable) {
                                Log.d("createOnFailure", t.message.toString())
                            }

                            override fun onResponse(call: Call<CreateReportResponse>, response: Response<CreateReportResponse>) {
                                if (response.isSuccessful) {
                                    Toast.makeText(context, "สร้างรายงานสำเร็จ", Toast.LENGTH_SHORT).show()
                                    FragmentReport()
                                } else {
                                    Log.d("notSuccessful", response.errorBody()!!.string())
                                }
                            }

                        })
            }

        }

        view.lnChooseMap.setOnClickListener {
            val intent = Intent(context, LocationMapsActivity::class.java)
            startActivityForResult(intent, 2)
        }

    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    private fun createMultiPhotoFile(uris: ArrayList<Uri>): ArrayList<File> {
        val arrayListFile = arrayListOf<File>()
        val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
        for (uri in uris) {
            val cursor: Cursor? = context!!.contentResolver.query(uri, filePathColumn, null, null, null)
            var path: String? = null
            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(filePathColumn[0])
                path = it.getString(index)
            }
            if (path == null) {
                Log.d("xx", "path is null")
                continue
            }
            val file = File(path)
            arrayListFile.add(file)
        }
        return arrayListFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data?.clipData != null) {
            val uris = ArrayList<Uri>()
            for (index in 0 until data.clipData.itemCount) {
                uris.add(data.clipData.getItemAt(index).uri)
            }
            val files = createMultiPhotoFile(uris)

            photoBody = RequestBody.create(null, "")
            if (file != null) {
                photoBody = RequestBody.create(MediaType.parse("image/*"), file?.name)
            }
            var num = 1
            for (file in files) {
                val photoBody = RequestBody.create(MediaType.parse("image/*"), file)
                val photoForm = MultipartBody.Part.createFormData("file$num", file.name, photoBody)
                num++
                multipartPhotoBodies.add(photoForm)

                pagerPhoto?.adapter = ImagePagerAdaper(context!!, uris)
                indicator?.setViewPager(pagerPhoto)
                layoutViewPager!!.visibility = View.VISIBLE
            }
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK && data?.data != null) {
            val uri: Uri = data.data
            val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? = context!!.contentResolver.query(uri, filePathColumn, null, null, null)
            var path: String? = null

            cursor?.use {
                it.moveToFirst()
                val index = it.getColumnIndex(filePathColumn[0])
                path = it.getString(index)
            }
            if (path == null) {
                Log.d("xx", "path is null")
                return
            }
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            lattitude = data?.getStringExtra("result_lat")
            longitude = data?.getStringExtra("result_lng")
            editLat.setText(lattitude)
            editLong.setText(longitude)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == android.R.id.home) {
            activity?.onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }

    }

    fun FragmentReport() {
        val fragment = ClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    companion object {
        val dbVersion = 1
        val dbName = "dbname"
        val dbPath = "/data/user/0/com.example.kamonwan_s.hgproject/databases"

        @JvmStatic
        fun newInstance() =
                CreateReportClueFragment().apply {
                    arguments = Bundle().apply {

                    }
                }
    }
}
