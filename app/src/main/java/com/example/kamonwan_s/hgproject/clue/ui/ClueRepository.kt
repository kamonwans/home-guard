package com.example.kamonwan_s.hgproject.clue.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.clue.api.*
import com.example.kamonwan_s.hgproject.database.*
import com.example.kamonwan_s.hgproject.login.api.AccountWrapper
import com.example.kamonwan_s.hgproject.register.api.ProvinceWrapper
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ClueRepository private constructor(private val executor: AppExecutors,
                                         private val clueApi: ClueApi,
                                         private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    fun loadReportOwn(clueOwn: ClueOwn): LiveData<SingleEvent<GetReportListOwnResponse>> {
        val ownLiveData = MutableLiveData<SingleEvent<GetReportListOwnResponse>>()
        clueApi.getReportListOwn(clueOwn).enqueue(object : Callback<GetReportListOwnResponse> {
            override fun onFailure(call: Call<GetReportListOwnResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOwnResponse>, response: Response<GetReportListOwnResponse>) {
                if (response.isSuccessful) {

                    ownLiveData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.reportListOwnWrapper
                    executor.diskIO.execute {
                        try {
                            var reportOwn = dbInfoList.reportListData.reportList
                            var reportOwnList = ArrayList<ReportOwnListEntity>()
                            var reportOwnImage = ArrayList<ReportImageEntity>()
                            reportOwn.forEach {
                                var reportOwnEntity = ReportOwnListEntity(
                                        it.accountID,
                                        it.content,
                                        it.creatorName,
                                        it.isFavorite,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.numComment,
                                        it.profilePic,
                                        it.reportID,
                                        it.reportStatusID,
                                        it.reportTypeID,
                                        it.title
                                )

                                var reportOwnImageEntity = ReportImageEntity(
                                        it.images.toString(),
                                        it.reportID
                                )
                                reportOwnImage.add(reportOwnImageEntity)
                                reportOwnList.add(reportOwnEntity)
                            }
                            mapDao.insertReportListOwn(reportOwnList)
                            mapDao.insertReportImage(reportOwnImage)
                            if (dbInfoList.reportListData.reportList.isNotEmpty()) {
                                mapDao.insertReportListOwn(reportOwnList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d("messageError", ownLiveData.toString())
            }

        })
        return ownLiveData
    }

    fun loadReportFavorite(clueOwn: ClueOwn): LiveData<SingleEvent<GetReportListOwnFavoriteResponse>> {
        val favoriteLiveData = MutableLiveData<SingleEvent<GetReportListOwnFavoriteResponse>>()
        clueApi.getReportListOwnFavorite(clueOwn).enqueue(object : Callback<GetReportListOwnFavoriteResponse> {
            override fun onFailure(call: Call<GetReportListOwnFavoriteResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOwnFavoriteResponse>, response: Response<GetReportListOwnFavoriteResponse>) {
                if (response.isSuccessful) {
                    favoriteLiveData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.reportListFavoriteWrapper
                    executor.diskIO.execute {
                        try {
                            var reportFavorite = dbInfoList.reportListData.reportList
                            var reportFavoriteList = ArrayList<ReportFavoriteListEntity>()
                            reportFavorite.forEach {
                                var reportFavoriteEntity = ReportFavoriteListEntity(
                                        it.accountID,
                                        it.content,
                                        it.creatorName,
                                        it.isFavorite,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.numComment,
                                        it.profilePic,
                                        it.reportID,
                                        it.reportStatusID,
                                        it.reportTypeID,
                                        it.title
                                )
                                reportFavoriteList.add(reportFavoriteEntity)
                            }

                            if (dbInfoList.reportListData.reportList.isNotEmpty()) {
                                mapDao.insertReportListFavorite(reportFavoriteList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d("messageError", favoriteLiveData.toString())

            }

        })
        return favoriteLiveData
    }

    fun loadOrganization(reportOrganizationInfo: ReportOrganizationInfo): LiveData<SingleEvent<GetReportListOrganizationResponse>> {
        val organizationLiveData = MutableLiveData<SingleEvent<GetReportListOrganizationResponse>>()
        clueApi.getReportListOrganization(reportOrganizationInfo).enqueue(object : Callback<GetReportListOrganizationResponse> {
            override fun onFailure(call: Call<GetReportListOrganizationResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOrganizationResponse>, response: Response<GetReportListOrganizationResponse>) {
                if (response.isSuccessful) {
                    organizationLiveData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.reportListOrganizationWrapper
                    executor.diskIO.execute {
                        try {
                            var reportOrganization = dbInfoList.reportListData.reportList
                            var reportOrganizationList = ArrayList<ReportOrganizationListEntity>()
                            reportOrganization.forEach {
                                var reportOrganizationEntity = ReportOrganizationListEntity(
                                        it.accountID,
                                        it.content,
                                        it.creatorName,
                                        it.isFavorite,
                                        it.lat,
                                        it.lng,
                                        it.modifiedDate,
                                        it.numComment,
                                        it.profilePic,
                                        it.reportID,
                                        it.reportStatusID,
                                        it.reportTypeID,
                                        it.title
                                )
                                reportOrganizationList.add(reportOrganizationEntity)
                            }

                            if (dbInfoList.reportListData.reportList.isNotEmpty()) {
                                mapDao.insertReportListOrganization(reportOrganizationList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else {
                    Log.d("messageError", organizationLiveData.toString())
                }
            }
        })
        return organizationLiveData
    }

    fun updateFavorite(updateFavoriteInfo: UpdateFavoriteInfo): LiveData<SingleEvent<UpdateReportFavoriteResponse>> {
        val updateFavoriteLiveData = MutableLiveData<SingleEvent<UpdateReportFavoriteResponse>>()
        clueApi.updateReportFavorite(updateFavoriteInfo).enqueue(object : Callback<UpdateReportFavoriteResponse> {
            override fun onFailure(call: Call<UpdateReportFavoriteResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<UpdateReportFavoriteResponse>, response: Response<UpdateReportFavoriteResponse>) {
                if (response.isSuccessful)
                    updateFavoriteLiveData.value = SingleEvent(response.body()!!)
                else
                    Log.d("messageError", updateFavoriteLiveData.toString())
            }
        })
        return updateFavoriteLiveData
    }

    fun loadReportType(tokenInfo: TokenInfo): LiveData<SingleEvent<LoadReportTypeResponse>> {
        val reportTypeData = MutableLiveData<SingleEvent<LoadReportTypeResponse>>()
        clueApi.loadReportType(tokenInfo).enqueue(object : Callback<LoadReportTypeResponse> {
            override fun onFailure(call: Call<LoadReportTypeResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<LoadReportTypeResponse>, response: Response<LoadReportTypeResponse>) {
                if (response.isSuccessful) {
                    reportTypeData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.reportTypeData.reportTypeWrapper
                    executor.diskIO.execute {
                        try {
                            var reportType = dbInfoList.reportTypeList
                            var reportTypeList = ArrayList<ReportTypeEntity>()
                            reportType.forEach {
                                var reportTypeEntity = ReportTypeEntity(
                                        it.icon,
                                        it.modifiedDate,
                                        it.reportTypeID,
                                        it.reportTypeName
                                )
                                reportTypeList.add(reportTypeEntity)
                            }
                            mapDao.insertReportType(reportTypeList)
                            if (dbInfoList.reportTypeList.isEmpty()) {
                                mapDao.insertReportType(reportTypeList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d("messageError", reportTypeData.toString())
            }

        })
        return reportTypeData
    }

    fun loadOrganizationReport(tokenInfo: TokenInfo): LiveData<SingleEvent<AccountWrapper>> {
        val organizationData = MutableLiveData<SingleEvent<AccountWrapper>>()
        clueApi.loadAccount(tokenInfo).enqueue(object : Callback<AccountWrapper> {
            override fun onFailure(call: Call<AccountWrapper?>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<AccountWrapper?>, response: Response<AccountWrapper?>) {
                if (response.isSuccessful) {
                    organizationData.value = SingleEvent(response.body()!!)
                } else
                    Log.d("messageError", organizationData.toString())
            }

        })
        return organizationData
    }

    fun loadDetailReport(reportDetail: ReportDetail): LiveData<SingleEvent<GetReportDetailResponse>> {
        val detailReportData = MutableLiveData<SingleEvent<GetReportDetailResponse>>()
        clueApi.getReportDetail(reportDetail).enqueue(object : Callback<GetReportDetailResponse> {
            override fun onFailure(call: Call<GetReportDetailResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportDetailResponse>, response: Response<GetReportDetailResponse>) {
                if (response.isSuccessful)
                    detailReportData.value = SingleEvent(response.body()!!)
                else
                    Log.d("messageError", detailReportData.toString())
            }

        })
        return detailReportData
    }

    fun loadSatusReport(syncReportInfo: SyncReportInfo): LiveData<SingleEvent<LoadReportStatusResponse>> {
        val statusData = MutableLiveData<SingleEvent<LoadReportStatusResponse>>()
        clueApi.loadReportStatus(syncReportInfo).enqueue(object : Callback<LoadReportStatusResponse> {
            override fun onFailure(call: Call<LoadReportStatusResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<LoadReportStatusResponse>, response: Response<LoadReportStatusResponse>) {
                if (response.isSuccessful) {
                    statusData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.reportStatusWrapper
                    executor.diskIO.execute {
                        try {
                            var reportStatus = dbInfoList.reportStatusData.reportStatuses
                            var reportStatusList = ArrayList<ReportStatusesEntity>()
                            reportStatus.forEach {
                                var reportStatusEntity = ReportStatusesEntity(
                                        it.ColorCode,
                                        it.ModifiedDate,
                                        it.ReportStatusID,
                                        it.ReportStatusName
                                )
                                reportStatusList.add(reportStatusEntity)
                            }
                            mapDao.insertReportStatus(reportStatusList)
                            if (dbInfoList.reportStatusData.reportStatuses.isEmpty()) {
                                mapDao.insertReportStatus(reportStatusList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }

                } else
                    Log.d("messageError", statusData.toString())
            }

        })

        return statusData
    }

    fun loadClueOwnAfter(clueOwn: ClueOwn): LiveData<SingleEvent<GetReportListOwnAfterResponse>> {
        val clueOwnData = MutableLiveData<SingleEvent<GetReportListOwnAfterResponse>>()
        clueApi.getReportListOwnAfter(clueOwn).enqueue(object : Callback<GetReportListOwnAfterResponse> {
            override fun onFailure(call: Call<GetReportListOwnAfterResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOwnAfterResponse>, response: Response<GetReportListOwnAfterResponse>) {
                if (response.isSuccessful)
                    clueOwnData.value = SingleEvent(response.body()!!)
                else
                    Log.d("messageError", clueOwnData.toString())
            }

        })

        return clueOwnData
    }


    fun loadClueOrganizationAfter(reportOrganizationInfo: ReportOrganizationInfo): LiveData<SingleEvent<GetReportListOrganizationAfterResponse>> {
        val clueOrganizationData = MutableLiveData<SingleEvent<GetReportListOrganizationAfterResponse>>()
        clueApi.getReportListOrganizationAfter(reportOrganizationInfo).enqueue(object : Callback<GetReportListOrganizationAfterResponse> {
            override fun onFailure(call: Call<GetReportListOrganizationAfterResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOrganizationAfterResponse>, response: Response<GetReportListOrganizationAfterResponse>) {
                if (response.isSuccessful)
                    clueOrganizationData.value = SingleEvent(response.body()!!)
                else
                    Log.d("messageError", clueOrganizationData.toString())
            }

        })

        return clueOrganizationData
    }

    fun loadClueFavoriteAfter(clueOwn: ClueOwn): LiveData<SingleEvent<GetReportListOwnFavoriteAfterResponse>> {
        val clueFavoriteData = MutableLiveData<SingleEvent<GetReportListOwnFavoriteAfterResponse>>()
        clueApi.getReportListOwnFavoriteAfter(clueOwn).enqueue(object : Callback<GetReportListOwnFavoriteAfterResponse> {
            override fun onFailure(call: Call<GetReportListOwnFavoriteAfterResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportListOwnFavoriteAfterResponse>, response: Response<GetReportListOwnFavoriteAfterResponse>) {
                if (response.isSuccessful)
                    clueFavoriteData.value = SingleEvent(response.body()!!)
                else
                    Log.d("messageError", clueFavoriteData.toString())
            }

        })

        return clueFavoriteData
    }

    fun loadReportReply(commentInfo: CommentInfo): LiveData<GetReportReplyResponse> {
        val reportReplyData = MutableLiveData<GetReportReplyResponse>()
        clueApi.loadReportReply(commentInfo).enqueue(object : Callback<GetReportReplyResponse> {
            override fun onFailure(call: Call<GetReportReplyResponse>, t: Throwable) {
                if (t.message == null)
                    Log.d("messageError", t.message)
                else
                    Log.d("messageError", t.message)
            }

            override fun onResponse(call: Call<GetReportReplyResponse>, response: Response<GetReportReplyResponse>) {
                if (response.isSuccessful)
                    reportReplyData.value = response.body()!!
                else
                    Log.d("messageError", reportReplyData.toString())
            }

        })
        return reportReplyData
    }

    fun insertReportType(reportType: List<ReportTypeEntity>) {
        executor.diskIO.execute {
            try {
                mapDao.insertReportType(reportType)
            } catch (e: SQLiteConstraintException) {
                syncDatabaseError.postValue(SingleEvent(e.toString()))
            } catch (e: Exception) {
                syncDatabaseError.postValue(SingleEvent(e.toString()))
                Log.d(tag, e.toString())
            }
        }
    }


    fun insertReportStatus(reportStatus: List<ReportStatusesEntity>) {
        executor.diskIO.execute {
            try {
                mapDao.insertReportStatus(reportStatus)
            } catch (e: SQLiteConstraintException) {
                syncDatabaseError.postValue(SingleEvent(e.toString()))
            } catch (e: Exception) {
                syncDatabaseError.postValue(SingleEvent(e.toString()))
                Log.d(tag, e.toString())
            }
        }
    }

//    fun loadProvinceG(): LiveData<ProvinceWrapper> {
//        val provinceData = MutableLiveData<ProvinceWrapper>()
//        executor.diskIO.execute {
//            val province = mapDao.loadAllProvinceLiveData()
//            provinceData.postValue(ProvinceWrapper(province))
//        }
//        return provinceData
//    }

    fun loadProvince(): LiveData<ArrayList<ProvinceEntity>> {
        val provinceData = MutableLiveData<ArrayList<ProvinceEntity>>()
        executor.diskIO.execute {
            val province = mapDao.loadAllProvinceLiveData()
            var provinceList = ArrayList<ProvinceEntity>()
            province.forEach {
                var provinceEntity =ProvinceEntity(
                        it.provinceId,
                        it.provinceName,
                        it.modifiedDate
                )
                provinceData.postValue(provinceList)
                provinceList.add(provinceEntity)
                Log.d("provinceData",provinceList.toString())
            }
        }
        mapDao.loadAllProvinceLiveData()
        return provinceData
    }

    fun loadStation():LiveData<ArrayList<StationsEntity>>{
        val stationData = MutableLiveData<ArrayList<StationsEntity>>()
        executor.diskIO.execute{
            val stations = mapDao.loadAllStationLiveData()
            var stationList = ArrayList<StationsEntity>()
            stations.forEach {
                var stationEntity = StationsEntity(
                        it.districtID,
                        it.lat,
                        it.lng,
                        it.modifiedDate,
                        it.organizationID,
                        it.provinceID,
                        it.stationID,
                        it.stationName,
                        it.subDistrictID,
                        it.tel
                )
                stationData.postValue(stationList)
                stationList.add(stationEntity)
            }
        }
        mapDao.loadAllStationLiveData()
        return stationData
    }


    companion object {
        @Volatile
        private var instance: ClueRepository? = null
        private val tag = ClueRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, clueApi: ClueApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: ClueRepository(executor, clueApi, mapDao).apply {
                                instance = this
                            }
                }
    }
}