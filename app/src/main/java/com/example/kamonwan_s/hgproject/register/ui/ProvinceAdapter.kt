package com.example.kamonwan_s.hgproject.register.ui

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.register.api.Province

class ProvinceAdapter(context: Context,@LayoutRes resource : Int, objects:MutableList<Province>)
    :ArrayAdapter<Province>(context,resource,objects){

//    var modifiedayeProvince : Long? = null
//

    init {
        if (objects.size > 0 && objects[0] != Province(PROVINCE_DEFAULT_ID, "กรุณาเลือกจังหวัด",0))
            objects.add(0, Province(PROVINCE_DEFAULT_ID, "กรุณาเลือกจังหวัด",0))
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return if (convertView == null){
            val newView: View = LayoutInflater.from(parent.context).inflate(R.layout.item_spinner_dropdown
                    , parent, false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.provinceName }
        }else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.provinceName }
        }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return if (convertView == null){
            val newView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_spinner_dropdown
                    , parent, false)
            newView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.provinceName }
        }else{
            convertView.apply { findViewById<TextView>(R.id.tvDropdown).text = getItem(position)?.provinceName }
        }
    }

    companion object {
        const val PROVINCE_DEFAULT_ID = -1
    }
}