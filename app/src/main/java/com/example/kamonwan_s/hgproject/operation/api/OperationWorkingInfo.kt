package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationWorkingInfo(@SerializedName("Token") val token :String,
                                @SerializedName("OperationID") val operationID:String,
                                @SerializedName("IsOperate") val isWorking:String)