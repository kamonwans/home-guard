package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomStatusInfo(@SerializedName("Token")val token:String,
                              @SerializedName("ChatRoomID")val chatRoomId :String,
                              @SerializedName("IsAccept")val isAccept:Int)