package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContentReaded(@SerializedName("ChatContentID")val chatContentID:String,
                             @SerializedName("ChatRoomID")val chatRoomID:String,
                             @SerializedName("ContentReaded")val contentReaded:String,
                             @SerializedName("ModifiedDate")val modifiedDate:String)