package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "station")
data class StationsEntity(@ColumnInfo(name = "district_id") val districtID: String,
                          @ColumnInfo(name = "lat") val lat: String,
                          @ColumnInfo(name = "lng") val lng: String,
                          @ColumnInfo(name = "modified_date") val modifiedDate:String,
                          @ColumnInfo(name = "organization_id") val organizationID:String,
                          @ColumnInfo(name = "province_id")val provinceID:String,
                          @PrimaryKey @ColumnInfo(name = "station_id")val stationID:String,
                          @ColumnInfo(name = "station_name")val stationName:String,
                          @ColumnInfo(name = "sub_district_id")val subDistrictID:String,
                          @ColumnInfo(name = "tel")val tel:String
                    )