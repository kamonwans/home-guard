package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class RegisterUserResult(@SerializedName("RegisterUserResult") val responseRegister:ResponseRegister)