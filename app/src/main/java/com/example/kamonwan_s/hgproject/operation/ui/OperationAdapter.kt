package com.example.kamonwan_s.hgproject.operation.ui

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.news.ui.NewsAdapter
import com.example.kamonwan_s.hgproject.operation.api.OperationList
import kotlinx.android.synthetic.main.item_operation.view.*

class OperationAdapter(val context: Context,
                       var listener: OperationAdapter.BtnClickListener,
                       var listenerMember : OperationAdapter.ImageMemberClickListener,
                       var updateWorking: OperationAdapter.ClickUpdate,
                       var location: OperationAdapter.ClickLocation)
    : RecyclerView.Adapter<OperationAdapter.ViewHolder>() {

    var items: OperationList? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): OperationAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_operation, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var operationOwn = items?.operations?.get(position)

        val chatRoomId = operationOwn?.chatRoomID
        val detail = operationOwn?.detail
        val image = operationOwn?.images
        val date = operationOwn?.modifiedDate
        val operationId = operationOwn?.operationID
        val operationName = operationOwn?.operationName
        val operationTypeId = operationOwn?.operationTypeID
        val organizationName = operationOwn?.organizationName
        val working = operationOwn?.isWorking
        val profile = operationOwn?.organizationPic
        val organizationId = operationOwn?.organizationID


        var dateConvert = convertDate(date!!, "dd/MM/yyyy hh:mm:ss")

        val lat = items?.pois?.get(position)?.lat?.toDouble()
        val lng = items?.pois?.get(position)?.lng?.toDouble()
        val poiTypeId = items?.pois?.get(position)?.poiTypeID
        val poiName = items?.pois?.get(position)?.poiName

        holder.itemView.tvOperationName.text = operationName
        holder.itemView.tvOrganizationName.text = organizationName
        holder.itemView.tvDate.text = dateConvert
        holder.itemView.tvContentOperation.text = detail
        holder.itemView.imageOperationUser.setProfile(profile!!)
        holder.itemView.imageOperation.setImage(items!!.operations[position].images!!)


        holder.itemView.tvIsWorkingNon.setOnClickListener {
            if (operationId != null) {
                updateWorking.onUpdateWorking(position, operationId.toString(), "1")
            }
            holder.itemView.tvIsWorkingActive.visibility = View.VISIBLE
            holder.itemView.tvIsWorkingNon.visibility = View.INVISIBLE
        }

        holder.itemView.tvIsWorkingActive.setOnClickListener {
            if (operationId != null) {
                updateWorking.onUpdateWorking(position, operationId.toString(), "0")
            }
            holder.itemView.tvIsWorkingActive.visibility = View.INVISIBLE
            holder.itemView.tvIsWorkingNon.visibility = View.VISIBLE
        }

        if (working == 0) {
            holder.itemView.tvIsWorkingNon.visibility = View.VISIBLE
            holder.itemView.tvIsWorkingActive.visibility = View.INVISIBLE
        } else {
            holder.itemView.tvIsWorkingActive.visibility = View.VISIBLE
            holder.itemView.tvIsWorkingNon.visibility = View.INVISIBLE
        }

        holder.itemView.setOnClickListener {
            listener.onBtnClick(position, date, operationTypeId!!, detail!!, operationId!!, operationName!!, working!!,organizationId!!)
        }

        holder.itemView.imgGroupOperation.setOnClickListener {
            listenerMember.onImageMemberClick()
        }

        holder.itemView.imgMapOperation.setOnClickListener {
            location.onLocation(lat!!,lng!!,poiTypeId!!.toString(), poiName!!)
        }

    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }

    interface ClickLocation {
        fun onLocation(lat: Double, lon: Double, poiTypeId: String, title: String)
    }
    override fun getItemCount(): Int {
        return items?.operations?.size ?: 0

    }

    fun ImageView.setImage(url: MutableList<String>) {
        if(url.size != 0)
        Glide.with(context).load(url[0]).apply(RequestOptions.placeholderOf(R.drawable.default_image).error(R.drawable.default_image)).into(this)
    }

    fun ImageView.setProfile(url: String) {
         Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    interface ClickUpdate {
        fun onUpdateWorking(position: Int, operationId: String, isWorking: String)
    }

    interface BtnClickListener {
        fun onBottomReached()
        fun onBtnClick(position: Int, date: String, typeId: Int, content: String, operationId: Int, operationName: String, working: Int,organizationId:Int)

    }

    interface ImageMemberClickListener {
        fun onImageMemberClick()

    }


}