package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class SyncOperationListResponse(@SerializedName("SyncOperationListResult") val operationListOwnWrapper : OperationListOwnWrapper)