package com.example.kamonwan_s.hgproject.login.ui

import android.app.Application
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.*
import com.example.kamonwan_s.hgproject.clue.ui.DropDownProvinceAdapter
import com.example.kamonwan_s.hgproject.database.AppDatabase
import com.example.kamonwan_s.hgproject.login.AccountApi
import com.example.kamonwan_s.hgproject.login.api.Account
import com.example.kamonwan_s.hgproject.login.api.Token
import com.example.kamonwan_s.hgproject.login.api.UserAccount
import com.example.kamonwan_s.hgproject.login.repository.AccountRepository
import com.example.kamonwan_s.hgproject.register.api.Province
import com.example.kamonwan_s.hgproject.register.ui.RegisterActivity
import com.example.kamonwan_s.hgproject.util.AppExecutors
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlin.math.log


class LoginFragment : Fragment() {

    private var loginViewModel: LoginViewModel? = null
    private var tokenLogin: String? = null
    private var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appDatabase = AppDatabase.getAppDatabase(context!!)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        initInStance(view, savedInstanceState)
        return view
    }


    private fun initInStance(view: View, savedInstanceState: Bundle?) {
        loginViewModel = ViewModelProviders.of(this,
                provideLoginViewModelFactory(activity!!.application)).get(LoginViewModel::class.java)
        if (savedInstanceState == null) {
            loginViewModel?.retrieveUser()

            lock(view)
        }

        tokenLogin = loginViewModel?.getToken()

        loginViewModel?.syncProvince("0")
        loginViewModel?.syncProvinceData?.observe(this, Observer { wrapper ->
            wrapper?.syncProvinceList?.let {

            }
        })

        loginViewModel?.syncDistrict("0")
        loginViewModel?.syncDistrictData?.observe(this, Observer { wrapper ->
            wrapper?.syncDistrictWrapper?.let {

            }
        })

        loginViewModel?.syncSubDistrict("0")
        loginViewModel?.syncSubDistrictData?.observe(this, Observer { wrapper ->
            wrapper?.syncSubDistrictWrapper?.let {

            }
        })

        loginViewModel?.syncOrganization("0")
        loginViewModel?.syncOrganizationData?.observe(this, Observer { wrapper ->
            wrapper?.syncOrganizationWrapper?.let {

            }
        })

        // login ครั้งแรก
        loginViewModel?.loginState?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let {
                when (it.loginUserList.responseLoginUser.responseCode) {
                    in OK..Int.MAX_VALUE -> {
                        loginViewModel?.saveUser(loginViewModel?.userInfo?.value!!)
                        loginViewModel?.saveToken(it.loginUserList.dataLoginUser.token)
                        tokenLogin = it.loginUserList.dataLoginUser.token
                        loginViewModel?.loadAccount(Token(tokenLogin!!))
                        Log.d("messageCode", tokenLogin)
                        startActivity(Intent(context, MainActivity::class.java))
                        activity!!.finish()
                    }
                    NOT_FOUND -> {
                        Snackbar.make(view, "อีเมลหรือรหัสผ่านไม่ถูกต้อง", Snackbar.LENGTH_LONG).show()
                        unlock(view)
                    }
                    SERVER_ERROR -> {
                        Snackbar.make(view, "เกิดข้อผิดพลาดจากเซิร์ฟเวอร์", Snackbar.LENGTH_LONG).show()
                        unlock(view)
                    }
                    else -> {
                        if (!it.loginUserList.responseLoginUser.responseMessage.isNullOrBlank()) {
                            Snackbar.make(view, it.loginUserList.responseLoginUser.responseMessage as String, Snackbar.LENGTH_LONG).show()
                            Log.d("Snackbar", it.loginUserList.responseLoginUser.responseMessage)
                        } else
                            Snackbar.make(view, "เกิดข้อผิดพลาดบางอย่าง", Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        })

        // check token หมดอายุ
        loginViewModel?.loadToken(Token(tokenLogin!!))
        loginViewModel?.checkToken?.observe(this, Observer { checkToken ->
            checkToken?.reponse?.let {
                when (it.responseData.responseCode) {
                    NOT_FOUND -> {
                        Snackbar.make(view, "กรุณาล็อกอิน", Snackbar.LENGTH_LONG).show()
                        unlock(view)
                    }
                    SERVER_ERROR -> {
                        Snackbar.make(view, "กรุณาล็อกอิน", Snackbar.LENGTH_LONG).show()
                        unlock(view)
                    }
                    NOT_FOUND_TOKEN -> {
                        Snackbar.make(view, "กรุณาล็อกอิน", Snackbar.LENGTH_LONG).show()
                        unlock(view)
                    }
                    else -> {
                        Snackbar.make(view, it.responseData.responseMessage, Snackbar.LENGTH_LONG).show()
                    }
                }
            }

        })

        // check token != null -> auto login
        loginViewModel?.loadToken(Token(tokenLogin!!))
        loginViewModel?.checkToken?.observe(this, Observer { event ->
            event?.let {
                if (it.reponse.responseData.responseMessage.equals("Success!!")) {
                    loginViewModel?.loadAccount(Token(tokenLogin!!))
                    startActivity(Intent(context, MainActivity::class.java))
                    activity!!.finish()
                } else {
                    Snackbar.make(view, "กรุณาล็อกอิน", Snackbar.LENGTH_LONG).show()
                }
            }
        })

        loginViewModel?.accountInfo?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let {
                loginViewModel?.saveAccount(it.AccountList.dataAccount.account)
                loginViewModel?.saveOrganizationId(it.AccountList.dataAccount.account)
            }
        })



        view.btSignIn.setOnClickListener {
            if (view.actEmail.text.isNotBlank() && view.actPassword.text.isNotBlank()) {
                val userAccount = UserAccount(view.actEmail.text.toString(), view.actPassword.text.toString())
                lock(view)

                loginViewModel?.loginUser(userAccount)
            } else {
                when {
                    view.actEmail.text.isBlank() -> view.actEmail.error = "กรุณาใส่อีเมล"
                    view.actPassword.text.isBlank() -> view.actPassword.error = "กรุณาใส่รหัสผ่าน"
                }
            }
        }
        view.tvSignUp?.setOnClickListener {
            startActivity(Intent(context, RegisterActivity::class.java))
        }

        loginViewModel?.userData?.observe(this, Observer {
            if (it != null) {
                it.getContentIfNotHandled()?.run {
                    loginViewModel?.loginUser(this)
                }
            } else {
                unlock(view)
            }
        })

//        loginViewModel?.accountInfo?.observe(this, Observer { event ->
//            event?.getContentIfNotHandled()?.let {
//                startActivity(Intent(context,MainActivity::class.java))
//                Log.d("loginViewModel",   it.AccountList.dataAccount.account.toString())
//                Toast.makeText(context, "login success", Toast.LENGTH_SHORT).show()
//                activity!!.finish()
//            }
//        })
    }


    private fun unlock(view: View) {
        view.pbLoading.visibility = View.GONE
        view.btSignIn.visibility = View.VISIBLE
        view.tvSignUp.isEnabled = true
        view.actPassword.isEnabled = true
        view.actEmail.isEnabled = true
    }

    private fun lock(view: View) {
        view.btSignIn.visibility = View.GONE
        view.tvSignUp.isEnabled = false
        view.pbLoading.visibility = View.VISIBLE
        view.actPassword.isEnabled = false
        view.actEmail.isEnabled = false
    }


    private fun provideAccountRepository(): AccountRepository {
        val accountApi = AccountApi.service
        val executors = AppExecutors.getInstance()
        val database = AppDatabase.getAppDatabase(context!!)
        return AccountRepository.getInstance(executors, accountApi, database.mapDao())
    }

    fun provideLoginViewModelFactory(app: Application): LoginViewModelFactory {
        return LoginViewModelFactory(app, provideAccountRepository())
    }

    companion object {
        val tag = LoginFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                LoginFragment().apply {

                }
    }
}
