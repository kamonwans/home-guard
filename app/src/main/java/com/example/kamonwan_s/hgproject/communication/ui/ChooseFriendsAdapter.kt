package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactListData
import kotlinx.android.synthetic.main.item_choose_friends.view.*

class ChooseFriendsAdapter(val context: Context, val listener: BtnClickListener) : RecyclerView.Adapter<ChooseFriendsAdapter.ViewHolder>() {

    var items: AccountContactListData? = null
    var chaeked : Int? = null


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ChooseFriendsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_choose_friends, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.accountContacts?.size ?: 0

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var firstName = items?.accountContacts?.get(position)?.firstName
        var lastName = items?.accountContacts?.get(position)?.lastName
        var pic = items?.accountContacts?.get(position)?.profilePic
        var accountId = items?.accountContacts?.get(position)?.accountID


        holder.itemView.tvFistName.text = firstName
        holder.itemView.tvLastName.text = lastName
        holder.itemView.imageUserProfile.setProfile(pic!!)

        if (holder.itemView.chChooseFriends.isChecked){
            holder.itemView.chChooseFriends.isChecked = true
            chaeked = 1
            Log.d("isChecked",holder.itemView.chChooseFriends.isChecked.toString())
        }else if (!holder.itemView.chChooseFriends.isChecked){
            holder.itemView.chChooseFriends.isChecked = false
            chaeked = 0
            Log.d("unisChecked",holder.itemView.chChooseFriends.isChecked.toString())
        }

        holder.itemView.chChooseFriends.setOnClickListener {
            listener.onBtnClick(accountId!!,firstName!!,chaeked!!)
        }

    }


    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

    }

    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    interface BtnClickListener {
        fun onBtnClick(accountId: String, name: String,checked:Int)
    }
}