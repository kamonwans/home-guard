package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

data class AccountContacts(@SerializedName("AccountID") val accountID:Int,
                           @SerializedName("Email") val email :String,
                           @SerializedName("AccountTypeID") val accountTypeID:Int,
                           @SerializedName("FirstName") val firstName:String,
                           @SerializedName("LastName") val lastName:String,
                           @SerializedName("ModifiedDate")val modifiedDate:String,
                           @SerializedName("OrganizationID") val organizationID:Int,
                           @SerializedName("ProfilePic") val profilePic:String)