package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class Token(@SerializedName("Token") val token:String)