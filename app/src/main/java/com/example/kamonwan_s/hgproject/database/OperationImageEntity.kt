package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "operation_image",
        foreignKeys = arrayOf(ForeignKey(entity = OperationsEntity::class,
        parentColumns = arrayOf("operations_id"),
        childColumns = arrayOf("operations_id"),
        onDelete = ForeignKey.CASCADE)))

data class OperationImageEntity(@PrimaryKey(autoGenerate = true) val id: Int = 0,
                                @ColumnInfo(name = "operations_id") val operationsID: Int,
                                @ColumnInfo(name = "url")val image:String)