package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class Token(@SerializedName("Token") val token:String)