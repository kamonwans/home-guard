package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class GetChatRoomMemberResponse(@SerializedName("GetChatRoomMemberResult") val chatRoomMemberWrapper:ChatRoomMemberWrapper)

