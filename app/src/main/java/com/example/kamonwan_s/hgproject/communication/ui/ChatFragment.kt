package com.example.kamonwan_s.hgproject.communication.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.AppDatabase
import kotlinx.android.synthetic.main.fragment_chat.view.*

class ChatFragment : Fragment() {

    private var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("แชท")
        appDatabase = AppDatabase.getAppDatabase(context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chat, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)

        setHasOptionsMenu(true)
        initInstance(view)

        return view
    }

    private fun initInstance(view: View) {
        FragmentMessgeRoom()

        view.lnMessage.setOnClickListener { FragmentMessgeRoom() }
        view.lnFriendName.setOnClickListener { FragmentFriends() }
        view.lnAccept.setOnClickListener { FragmentAccept() }

    }



    fun FragmentMessgeRoom() {
        val fragment = MessageRoomFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChatMessage, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentFriends() {
        val fragment = FriendsFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChatMessage, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentAccept() {
        val fragment = AcceptFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentChatMessage, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentCreateRoom() {
        val fragment = ChooseFriendsChatRoomFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }
    companion object {
        val tag: String = ChatFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ChatFragment().apply {

                }
    }
}
