package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "report_image")
data class ReportImageEntity(@ColumnInfo (name = "url")val image : String,
                             @PrimaryKey(autoGenerate = true) val id: Int)