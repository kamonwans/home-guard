package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContentListData(@SerializedName("ChatContents")val chatContents:MutableList<ChatContents>)