package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "operation_type")
data class OperationTypeEntity( @ColumnInfo(name = "operation_type_color")val color:String,
                                @ColumnInfo(name = "operation_type_modified")val modifiedDate:String,
                               @PrimaryKey @ColumnInfo(name = "operation_type_id") val operationTypeID:String,
                                @ColumnInfo(name = "operation_type_name")val operationTypeName:String)