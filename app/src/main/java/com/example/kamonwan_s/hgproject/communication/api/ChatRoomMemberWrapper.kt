package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomMemberWrapper(@SerializedName("Data")val chatRoomMemberData:ChatRoomMemberData,
                                 @SerializedName("Response")val responseList: ResponseList)