package com.example.kamonwan_s.hgproject.communication.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.MediaController
import com.example.kamonwan_s.hgproject.R
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {

    private var content: String? = null
    private var mediaController: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        content = intent.extras.getBundle("contentVideo").getString("contentVideo")
        Log.d("contentVideo",content)
        initInstance()
    }

    private fun initInstance() {
        videoViewPlay?.setMediaController(mediaController)
        videoViewPlay?.setVideoPath(content)
        videoViewPlay?.start()
    }
}
