package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomMemberOperationInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_chat_room_member.view.*


class ChatRoomMemberFragment : BottomSheetDialogFragment() {

    private var communicationViewModel: CommunicationViewModel? = null
    private var token: String? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapterRoomMember : ChatRoomMemberOperationAdapter? = null
    private var chatRoomId:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chatRoomId = arguments?.getString("chatRoomId")
        Log.d("chatRoomId",chatRoomId.toString())

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chat_room_member, container, false)
        initInstance(view)

        return view
    }

    private fun initInstance(view: View) {
        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]

        token = communicationViewModel?.getToken()
        adapterRoomMember = ChatRoomMemberOperationAdapter(context!!)
        layoutManager = LinearLayoutManager(view.context)

        view.recyclerViewChatRoomMemberOperation.layoutManager = layoutManager
        view.recyclerViewChatRoomMemberOperation.itemAnimator = DefaultItemAnimator()
        view.recyclerViewChatRoomMemberOperation.adapter = adapterRoomMember

        view.imageClosed?.setOnClickListener {
            dismiss()
        }

        loadChatRoomMemberOperation()
    }

    private fun loadChatRoomMemberOperation(){
        communicationViewModel?.getChatRoomMember(ChatRoomMemberOperationInfo(token!!,chatRoomId!!.toString()))
        communicationViewModel?.chatRoomMemberOperation?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatRoomMemberWrapper ->
                if (chatRoomMemberWrapper != null){
                    chatRoomMemberWrapper.chatRoomMemberWrapper.chatRoomMemberData.let {
                        if (it != null){
                            it.chatRoomMembers.let { chatRoomMember ->
                                adapterRoomMember?.itemsChatRoomMembeer?.chatRoomMembers?.addAll(chatRoomMember)
                            }
                        }

                        if (adapterRoomMember?.itemsChatRoomMembeer == null){
                            adapterRoomMember?.itemsChatRoomMembeer = chatRoomMemberWrapper.chatRoomMemberWrapper.chatRoomMemberData
                        }
                        adapterRoomMember?.notifyDataSetChanged()
                    }
                }
            }
        })
    }



    companion object {
     val tag :String = ChatRoomMemberFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ChatRoomMemberFragment().apply {

                }
    }
}
