package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "operation_status")
data class OperationStatusEntity(@ColumnInfo(name = "operation_status_color") val color: String,
                                 @ColumnInfo(name = "operation_status_modified")  val modifiedDate: String,
                                 @PrimaryKey @ColumnInfo(name = "operation_status_id")val operationStatusID: String,
                                 @ColumnInfo(name = "operation_status_name") val operationStatusName: String)