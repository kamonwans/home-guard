package com.example.kamonwan_s.hgproject.clue.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*

import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.ClueOwn
import kotlinx.android.synthetic.main.fragment_clue.*
import kotlinx.android.synthetic.main.fragment_clue.view.*
import kotlinx.android.synthetic.main.fragment_own_clue.view.*

class ClueFragment : Fragment() {

    var adapter: ClueAdapter? = null
    var layoutManager: LinearLayoutManager? = null
    private var clueViewModel: ClueViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("เบาะแส")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_clue, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_dot)


        setHasOptionsMenu(true)

        initInstance(view)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater!!.inflate(R.menu.menu_report, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.createReport -> {
                FragmentCreateReport()
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun initInstance(view: View) {
        FragmentOwn()

        view.lnOrganizationReport.setOnClickListener { FragmentOrganization() }
        view.lnMyReport.setOnClickListener { FragmentOwn() }
        view.lnFavorite.setOnClickListener { FragmentFavorite() }
    }

    fun FragmentOwn() {
        val fragment = OwnClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.content, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentOrganization() {
        val fragment = OrganizationClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.content, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentFavorite() {
        val fragment = FavoriteClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.content, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    fun FragmentCreateReport() {
        val fragmentMap = CreateReportClueFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragmentMap)
        transaction?.addToBackStack(null)
        var bundle = Bundle()
        fragmentMap.arguments = bundle
        transaction?.commit()
    }

    companion object {
        val tag: String = ClueFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                ClueFragment().apply {
                }
    }
}
