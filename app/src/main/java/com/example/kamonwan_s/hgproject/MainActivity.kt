package com.example.kamonwan_s.hgproject

import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.example.kamonwan_s.hgproject.clue.ui.ClueFragment
import com.example.kamonwan_s.hgproject.communication.ui.ChatFragment
import com.example.kamonwan_s.hgproject.map.ui.MapsFragment
import com.example.kamonwan_s.hgproject.news.ui.NewsFragment
import com.example.kamonwan_s.hgproject.operation.ui.OperationFragment
import com.example.kamonwan_s.hgproject.util.addFragmentToActivity
import com.example.kamonwan_s.hgproject.util.replaceFragmentToActivityWithBackStack
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_search_location.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var toggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            addFragmentToActivity(ClueFragment.newInstance(), R.id.contentContainer, ClueFragment.tag)
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)

        val navigationView = findViewById<View>(R.id.bnvMain) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(false)

        toggle.drawerArrowDrawable.color = resources.getColor(R.color.color_text_white)
        setSupportActionBar(tbMain)

        navigationView.setNavigationItemSelectedListener(this)

    }

    override fun onNavigationItemSelected(menu: MenuItem): Boolean {
        val id = menu.getItemId()
        if (id == R.id.action_clue) {
            val fragment = supportFragmentManager.findFragmentByTag(ClueFragment.tag)
            if (fragment != null && fragment.isVisible)
                return true
            else {
                replaceFragmentToActivityWithBackStack(ClueFragment.newInstance(),
                        R.id.contentContainer, ClueFragment.tag)
                true
            }
        } else if (id == R.id.action_operation) {
            val fragment = supportFragmentManager.findFragmentByTag(OperationFragment.tag)
            if (fragment != null && fragment.isVisible)
                return true
            else {
                replaceFragmentToActivityWithBackStack(OperationFragment.newInstance(), R.id.contentContainer, OperationFragment.tag)
                true
            }
        } else if (id == R.id.action_chat) {
            val fragment = supportFragmentManager.findFragmentByTag(ChatFragment.tag)
            if (fragment != null && fragment.isVisible)
                return true
            else {
                replaceFragmentToActivityWithBackStack(ChatFragment.newInstance(), R.id.contentContainer, ChatFragment.tag)
                true
            }
        } else if (id == R.id.action_news) {
            val fragment = supportFragmentManager.findFragmentByTag(NewsFragment.tag)
            if (fragment != null && fragment.isVisible)
                return true
            else {
                replaceFragmentToActivityWithBackStack(NewsFragment.newInstance(), R.id.contentContainer, NewsFragment.tag)
                true
            }
        } else if (id == R.id.action_maps) {
            val fragment = supportFragmentManager.findFragmentByTag(MapsFragment.tag)
            if (fragment != null && fragment.isVisible)
                return true
            else {
                replaceFragmentToActivityWithBackStack(MapsFragment.newInstance(), R.id.contentContainer, MapsFragment.tag)
                true
            }
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()

    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true

        return super.onOptionsItemSelected(item)
    }
}
