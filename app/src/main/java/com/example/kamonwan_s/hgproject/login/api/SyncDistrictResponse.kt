package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class SyncDistrictResponse(@SerializedName("SyncDistrictResult")val syncDistrictWrapper : MutableList<SyncDistrictWrapper>)