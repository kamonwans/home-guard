package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class CreateChatContentPToPResponse(@SerializedName("CreateChatContentP2PResult") val createChatContentP2PWrapper :CreateChatContentP2PWrapper )