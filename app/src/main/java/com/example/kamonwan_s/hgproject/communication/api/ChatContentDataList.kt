package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatContentDataList(@SerializedName("ChatContent")val chatContent : ChatContents)