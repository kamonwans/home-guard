package com.example.kamonwan_s.hgproject.register.api

import com.google.gson.annotations.SerializedName

data class AccountInfo(@SerializedName("FirstName")val firstName:String,
                       @SerializedName("LastName")val lastName:String,
                       @SerializedName("Tel")val tel :String,
                       @SerializedName("Email")val email:String,
                       @SerializedName("Password")val password:String)