package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "district")
data class DistrictEntity(@PrimaryKey @ColumnInfo(name = "district_id")val districtID: Int,
                          @ColumnInfo(name = "district_name")val districtName: String,
                          @ColumnInfo(name = "province_id")val provinceID: Int,
                          @ColumnInfo(name = "modified_date")val modifiedDate: Long)