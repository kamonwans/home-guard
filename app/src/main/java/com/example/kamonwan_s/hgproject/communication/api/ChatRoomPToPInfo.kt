package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomPToPInfo(@SerializedName("Token")val token : String,
                            @SerializedName("MemberID")val memberId:String)