package com.example.kamonwan_s.hgproject.operation.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull


data class OperationMembers(@SerializedName("AccountID") val accountID: Int,
                            @SerializedName("ActionDetail") val actionDetail: String,
                            @SerializedName("OperationID") val operationID: Int,
                            @SerializedName("OperationRoleTypeID") val operationRoleTypeID: Int,
                            @SerializedName("OperationRoleTypeName") val operationRoleTypeName: String,
                            @SerializedName("FirstName") val firstName: String,
                            @SerializedName("LastName") val lastName: String)