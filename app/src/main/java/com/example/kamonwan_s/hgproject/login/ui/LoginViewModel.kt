package com.example.kamonwan_s.hgproject.login.ui

import android.app.Application
import android.arch.lifecycle.*
import android.os.LocaleList
import android.preference.PreferenceManager
import android.util.Log
import android.view.animation.TranslateAnimation
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.DistrictEntity
import com.example.kamonwan_s.hgproject.database.ProvinceEntity
import com.example.kamonwan_s.hgproject.database.SubDistrictEntity
import com.example.kamonwan_s.hgproject.login.AccountApi
import com.example.kamonwan_s.hgproject.login.api.*
import com.example.kamonwan_s.hgproject.login.api.Account
import com.example.kamonwan_s.hgproject.login.repository.AccountRepository
import com.example.kamonwan_s.hgproject.register.api.*


class LoginViewModel(private val app: Application,
                     private val accountRepository: AccountRepository)
    : AndroidViewModel(app) {

    val userData = MediatorLiveData<SingleEvent<UserAccount>?>()
    val userInfo = MutableLiveData<UserAccount>()
    val accountInfo = MediatorLiveData<SingleEvent<AccountWrapper>>()
    private val districtID = MutableLiveData<String>()
    private val provinceID = MutableLiveData<String>()

    private val modifiedDate = MutableLiveData<String>()


    val syncProvinceData: LiveData<SyncProvinceResponse> = Transformations.switchMap(modifiedDate) {
        accountRepository.syncProvince(it)
    }

    val syncDistrictData : LiveData<SyncDistrictResponse> = Transformations.switchMap(modifiedDate){
        accountRepository.syncDistrict(it)
    }

    val syncSubDistrictData : LiveData<SyncSubDistrictResponse> = Transformations.switchMap(modifiedDate){
        accountRepository.syncSubDistrict(it)
    }

    val syncOrganizationData:LiveData<SyncOrganizationResponse> = Transformations.switchMap(modifiedDate){
        accountRepository.syncOrganization(it)
    }


    fun retrieveUser() {
        userData.addSource(accountRepository.retrieveUser(app), Observer {
            userData.value = it
        })
    }

    fun loginUser(userAccount: UserAccount) {
        userInfo.value = userAccount
    }

    // val accountData :MutableLiveData<AccountWrapper> = MediatorLiveData()
    fun loadAccount(token: Token) {
        accountInfo.addSource(accountRepository.loadAccount(token)) {
            accountInfo.value = it

        }
    }

    // private val checkTokenResult:LiveData<CheckTokenResult> = accountRepository.checkToken(Token("xyQ8JQf/LkWwZr5a3165vQ=="))
    var tokenData = MediatorLiveData<Token>()
    var checkToken: LiveData<CheckTokenResult>? = Transformations.switchMap(tokenData) {
        accountRepository.checkToken(it)
    }

    fun syncProvince(modifiedDate: String) {
        this.modifiedDate.value = modifiedDate
    }

    fun syncDistrict(modifiedDate: String){
        this.modifiedDate.value = modifiedDate
    }

    fun syncSubDistrict(modifiedDate: String){
        this.modifiedDate.value = modifiedDate
    }

    fun syncOrganization(modifiedDate: String){
        this.modifiedDate.value = modifiedDate
    }
    fun loadToken(token: Token) {
        this.tokenData.value = token
    }

    val loginState: LiveData<SingleEvent<LoginUserResult>> = Transformations.switchMap(userInfo) {
        accountRepository.loginUser(it)
    }

    fun saveAccount(accountList: Account) {
        accountRepository.saveAccount(app, accountList)
    }

    fun saveOrganizationId(accountList: Account){
        accountRepository.saveAccountOrganiation(app,accountList)
    }


    fun saveAccountID(id: Int) {
        accountRepository.saveAccountID(app, id)
    }

    fun insertProvince(province: List<ProvinceEntity>) {
        accountRepository.insertProvince(province)
    }

    fun insertDistrict(district: List<DistrictEntity>) {
        accountRepository.insertDistrict(district)
    }

    fun insertSubDistrict(subDistrict: List<SubDistrictEntity>) {
        accountRepository.insertSubDistrict(subDistrict)
    }

    fun updateSubDistrict(subDistrict: List<SubDistrict>) {
        accountRepository.updateSubDistrict(subDistrict)
    }

    fun updateDistrict(district: List<District>) {
        accountRepository.updateDistrict(district)
    }

    fun updateProvince(province: List<Province>) {
        accountRepository.updateProvince(province)
    }

    fun saveUser(userAccount: UserAccount) {
        accountRepository.saveUser(app, userAccount)
    }


    fun saveToken(token: String) {
        accountRepository.saveToken(app, token)
    }

    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun getToken(): String {
        return retrieveToken(app)
    }

}