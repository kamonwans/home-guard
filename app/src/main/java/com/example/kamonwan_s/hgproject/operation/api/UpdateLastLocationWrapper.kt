package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class UpdateLastLocationWrapper(@SerializedName("Response")val responseList: ResponseList)