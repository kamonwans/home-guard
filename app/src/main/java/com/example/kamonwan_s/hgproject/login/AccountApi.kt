package com.example.kamonwan_s.hgproject.login

import com.example.kamonwan_s.hgproject.login.api.*
import com.example.kamonwan_s.hgproject.register.api.DistrictWrapper
import com.example.kamonwan_s.hgproject.register.api.ProvinceWrapper
import com.example.kamonwan_s.hgproject.register.api.SubDistrictWrapper
import com.example.kamonwan_s.hgproject.synceDatabase.SyncDBInfoListResponse
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface AccountApi {

    @POST("LoginUser")
    fun loginUser(@Body userAccount: UserAccount): Call<LoginUserResult>

    @POST("GetAccount")
    fun loadAccount(@Body  token: Token): Call<AccountWrapper>

    @POST("CheckToken")
    fun checkToken(@Body token: Token) : Call<CheckTokenResult>

    @GET("SyncProvince/{modifiedDate}")
    fun syncProvince(@Path("modifiedDate") modifiedDate : String) : Call<SyncProvinceResponse>

    @GET("SyncDistrict/{modifiedDate}")
    fun syncDistrict(@Path("modifiedDate") modifiedDate : String) : Call<SyncDistrictResponse>

    @GET("SyncSubDistrict/{modifiedDate}")
    fun syncSubDistrict(@Path("modifiedDate") modifiedDate : String) : Call<SyncSubDistrictResponse>

    @GET("SyncOrganization/{modifiedDate}")
    fun syncOrganization(@Path("modifiedDate") modifiedDate : String) : Call<SyncOrganizationResponse>

    @GET("loadSubDistrict/{districtID}")
    fun loadSubDistrict(@Path("districtID") districtID: String): Call<SubDistrictWrapper>

    @GET("loadDistrict/{provinceID}")
    fun loadDistrict(@Path("provinceID") provinceID: String): Call<DistrictWrapper>

    @GET("loadProvince")
    fun loadProvince(): Call<ProvinceWrapper>


    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/CoreService.svc/"
        private const val TIME_OUT: Long = 20
        val service: AccountApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(AccountApi::class.java)
        }
    }
}