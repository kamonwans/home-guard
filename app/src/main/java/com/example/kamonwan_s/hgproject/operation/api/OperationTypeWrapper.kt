package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.annotations.SerializedName

data class OperationTypeWrapper(@SerializedName("Data")val operationTypeData : OperationTypeData,
                                @SerializedName("Response")val responseList: ResponseList)