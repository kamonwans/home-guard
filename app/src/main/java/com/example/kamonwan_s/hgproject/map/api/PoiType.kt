package com.example.kamonwan_s.hgproject.poi.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

data class PoiType(@SerializedName("Icon") val icon : String,
                   @SerializedName("ModifiedDate") val modifiedDate : String ,
                   @SerializedName("POICategoryID") val poiCategoryId : String,
                   @SerializedName("POITypeID") val poiTypeId : String ,
                   @SerializedName("ShortName") val shortName : String ) : Parcelable {


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(icon)
        dest?.writeString(modifiedDate)
        dest?.writeString(poiCategoryId)
        dest?.writeString(poiTypeId)
        dest?.writeString(shortName)
        dest?.writeByte(if (checkedItem) 1 else 0)
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var checkedItem: Boolean = false

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
        checkedItem = parcel.readByte() != 0.toByte()
    }

    companion object CREATOR : Parcelable.Creator<PoiType> {
        override fun createFromParcel(parcel: Parcel): PoiType {
            return PoiType(parcel)
        }

        override fun newArray(size: Int): Array<PoiType?> {
            return arrayOfNulls(size)
        }
    }
}