package com.example.kamonwan_s.hgproject.register.api

import com.example.kamonwan_s.hgproject.synceDatabase.SyncDBInfoListResponse
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface RegisterApi {

    @POST("RegisterUser")
    fun registerUser(@Body accountRegisterWrapper: AccountRegisterWrapper): Call<RegisterUserResult>

    @POST("RegisterUser")
    fun registerUserAccount(@Body registerUserInfo: RegisterUserInfo): Call<RegisterUserResult>


    @GET("loadSubDistrict/{districtID}")
    fun loadSubDistrict(@Path("districtID") districtID: String): Call<SubDistrictWrapper>

    @GET("loadDistrict/{provinceID}")
    fun loadDistrict(@Path("provinceID") provinceID: String): Call<DistrictWrapper>

    @GET("loadProvince")
    fun loadProvince(): Call<ProvinceWrapper>

    @GET("loadOrganization")
    fun loadOrganization(): Call<OrganizationWrapper>

    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/CoreService.svc/"
        private const val TIME_OUT: Long = 20
        val service: RegisterApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .setLenient()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(RegisterApi::class.java)
        }
    }
}