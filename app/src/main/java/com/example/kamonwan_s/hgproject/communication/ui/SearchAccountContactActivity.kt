package com.example.kamonwan_s.hgproject.communication.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.example.kamonwan_s.hgproject.OK
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.AccountContactEntity
import kotlinx.android.synthetic.main.activity_search_account_contact.*

class SearchAccountContactActivity : AppCompatActivity(), TextWatcher {

    private lateinit var searchAccountContactAdapter: SearchAccountContactAdapter
    private var searchAccountContactList = ArrayList<AccountContactEntity>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_account_contact)

        setUpView()
    }

    private fun setUpView() {
        edtSearch.addTextChangedListener(this)
        btnBack.setOnClickListener {
            finish()
        }

        searchAccountContactAdapter = SearchAccountContactAdapter(onClickAccountContactItem())

        recyclerViewAccountContact.apply {
            layoutManager = LinearLayoutManager(this@SearchAccountContactActivity)
            adapter = searchAccountContactAdapter
        }
        searchAccountContactAdapter.updateAccountCOntactList(accountContactList)

    }

    private fun onClickAccountContactItem(): (AccountContactEntity) -> Unit {
        return { accountContact ->
            val bundle = Bundle().apply {
                putParcelable(ACCOUNT_CONTACT_ITEM, accountContact)
            }

            setResult(OK, Intent().apply {
                putExtras(bundle)
            })

            finish()
        }
    }

    override fun afterTextChanged(s: Editable?) {
        searchAccountContactList = accountContactList.filter {
            it.firstName.startsWith(s.toString(), true)
        } as ArrayList<AccountContactEntity>

        searchAccountContactAdapter.updateAccountCOntactList(searchAccountContactList)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    private val accountContactList: ArrayList<AccountContactEntity> get() = intent.getParcelableArrayListExtra("accountContactList")

    companion object {
        const val requestCode = 4
        const val ACCOUNT_CONTACT_ITEM = "accountContact"
        const val ACCOUNT_CONTACT_LIST = "accountContactList"
    }


}
