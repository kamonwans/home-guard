package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportListData(@SerializedName("ReportList") val reportList : MutableList<ReportList>)