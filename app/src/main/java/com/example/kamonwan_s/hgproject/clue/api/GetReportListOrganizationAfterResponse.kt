package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class GetReportListOrganizationAfterResponse(@SerializedName("GetReportListOrganizationAfterResult") val reportListOrganizationAfterWrapper: ReportListOrganizationAfterWrapper)