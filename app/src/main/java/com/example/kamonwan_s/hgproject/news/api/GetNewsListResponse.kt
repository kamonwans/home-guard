package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class GetNewsListResponse(@SerializedName("GetNewsListResult") val newsListWrapper : NewsListWrapper)