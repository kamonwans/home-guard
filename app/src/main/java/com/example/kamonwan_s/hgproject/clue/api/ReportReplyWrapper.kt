package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportReplyWrapper(@SerializedName("Data") val reportReplyList : ReportReplyList,
                              @SerializedName("Response") val reportList: ReportList)