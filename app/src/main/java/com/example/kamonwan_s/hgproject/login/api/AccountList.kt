package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class AccountList(@SerializedName("Data") val dataAccount: DataAccount,
                       @SerializedName("Response") val responseAccount:ResponseAccount)