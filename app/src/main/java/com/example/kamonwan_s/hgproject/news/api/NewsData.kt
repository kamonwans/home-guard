package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class NewsData(@SerializedName("News") val news : News)