package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportReply(@SerializedName("AccountID") val accountID:Int,
                       @SerializedName("Content") val content :String,
                       @SerializedName("FirstName") val firstName:String,
                       @SerializedName("Image") val image:String,
                       @SerializedName("LastName") val lastName:String,
                       @SerializedName("ModifiedDate") val modifiedDate:String,
                       @SerializedName("ProfilePic") val profilePic:String,
                       @SerializedName("ReportID") val reportID:Int,
                       @SerializedName("ReportReplyID") val reportReplyID:Int)