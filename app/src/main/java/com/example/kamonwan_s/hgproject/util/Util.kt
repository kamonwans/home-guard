package com.example.kamonwan_s.hgproject.util

import android.content.Context
import android.util.Base64
import com.example.kamonwan_s.hgproject.R
import com.orhanobut.hawk.Hawk
import java.nio.ByteBuffer
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec

object Util {
    private const val KEY_SIZE = 16

    fun dpToPX(context: Context, dp: Int): Int = (dp * context.resources.displayMetrics.density).toInt()

    private fun decode(data: String): ByteArray = Base64.decode(data, Base64.DEFAULT)

    private fun encode(data: ByteArray): String {
        return Base64.encodeToString(data, Base64.DEFAULT)
    }

    fun decryptString(context: Context, data: String): String {
        val cipherMessage = decode(data)
//        Log.d("util", cipherMessage.contentToString())
        val key = Hawk.get<ByteArray>(context.getString(R.string.pref_key))
        val byteBuffer = ByteBuffer.wrap(cipherMessage)
        val ivSize = byteBuffer.int
        if (ivSize < 12 || ivSize >= 16) {
            throw IllegalArgumentException("invalid iv length")
        }
        val iv = ByteArray(ivSize)
        byteBuffer.get(iv)
        val cipherText = ByteArray(byteBuffer.remaining())
        byteBuffer.get(cipherText)
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(key, "AES")
                , GCMParameterSpec(128, iv))
        val plainText = cipher.doFinal(cipherText)
        return String(plainText, Charsets.UTF_8)
    }


    fun encryptString(context: Context, data: String): String {
        val secureRandom = SecureRandom()
        val key = ByteArray(KEY_SIZE)
        secureRandom.nextBytes(key)
        Hawk.put(context.getString(R.string.pref_key), key)
        val secretKey = SecretKeySpec(key, "AES")
        val iv = ByteArray(12)
        secureRandom.nextBytes(iv)
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        val parameterSpec = GCMParameterSpec(128, iv)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec)
        val cipherText = cipher.doFinal(data.toByteArray())
        val byteBuffer = ByteBuffer.allocate(4 + iv.size + cipherText.size)
        byteBuffer.putInt(iv.size)
        byteBuffer.put(iv)
        byteBuffer.put(cipherText)
        val cipherMessage = byteBuffer.array()
        return encode(cipherMessage)
    }

    private fun throwValidation() {
        throw IllegalStateException("Hawk is not built. " + "Please call build() and wait the initialisation finishes.")
    }
}