package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName


data class Account(
        @SerializedName("FirstName") val firstName: String,
        @SerializedName("LastName") val lastName: String,
        @SerializedName("Tel") val tel: String,
        @SerializedName("Email") val email: String,
        @SerializedName("Password") val password: String?,
        @SerializedName("AccountTypeID") val accountTypeID: Int = ACCOUNT_TYPE_ID,
        @SerializedName("ProfilePic") val profilePic: String? = null,
        @SerializedName("AccountID") val accountId: Int? = null,
        @SerializedName("OrganizationID") val OrganizationID: Int? = null

) {
    companion object {
        private const val ACCOUNT_TYPE_ID = 1
    }
}