package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class ReportListOrganizationAfterWrapper(@SerializedName("Data") val reportListData: ReportListData,
                                              @SerializedName("Response")val responseList: ResponseList)