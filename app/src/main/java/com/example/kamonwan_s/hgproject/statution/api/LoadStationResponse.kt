package com.example.kamonwan_s.hgproject.statution.api

import com.google.gson.annotations.SerializedName

data class LoadStationResponse(@SerializedName("LoadStationResult")val stationWrapper : StationWrapper)