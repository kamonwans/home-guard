package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactInfo
import com.example.kamonwan_s.hgproject.communication.api.ChatRoomInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_message.view.*
import kotlinx.android.synthetic.main.fragment_register.*

class MessageRoomFragment : Fragment(),MessageRoomAdapter.BtnClickListener{

    private var communicationViewModel: CommunicationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: MessageRoomAdapter? = null
    private var token: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("รายการสนทนา")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_message, container, false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_dot)

        setHasOptionsMenu(true)

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = communicationViewModel?.getToken()
        adapter = MessageRoomAdapter(context!!,this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewListRoomChat.layoutManager = layoutManager
        view.recyclerViewListRoomChat.itemAnimator = DefaultItemAnimator()
        view.recyclerViewListRoomChat.adapter = adapter

        communicationViewModel?.syncChatRoom(ChatRoomInfo(token!!,"0"))
        communicationViewModel?.chatRoomList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { chatRoomWrapper ->
                chatRoomWrapper.chatRoomListWrapper.chatRoomListData.let { chatRoomList ->
                   if(chatRoomList != null){
                       chatRoomList.chatRooms.let { chatRoom ->
                           adapter?.items?.chatRooms?.addAll(chatRoom)
                       }
                   }

                    if (adapter?.items == null){
                        adapter?.items = chatRoomWrapper.chatRoomListWrapper.chatRoomListData
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })

    }


    override fun onBtnClick(firstName :String,accountId:String,chatRoomId:String) {
        val intent = Intent(context,ChatMessageActivity::class.java)
        intent.putExtra("nameMember",firstName)
        intent.putExtra("accountId",accountId)
        intent.putExtra("chatRoomId",chatRoomId)
        startActivity(intent)
        Log.d("chatRoomIdRoom",chatRoomId)
        Log.d("accountIdRoom",accountId)
        //FragmentChatMessage(firstName,accountId,chatRoomId)

    }

    fun FragmentChatMessage(nameMember:String,accountId: String,chatRoomId: String) {
        val fragment = ChatMessageFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        transaction?.addToBackStack(null)
        val bundle = Bundle()
        bundle.putString("chatRoomId", chatRoomId)
        bundle.putString("accountId", accountId)
        bundle.putString("nameMember", nameMember)
        fragment.arguments = bundle
        transaction?.commit()
    }

    companion object {
      val tag = MessageRoomFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                MessageRoomFragment().apply {

                }
    }
}
