package com.example.kamonwan_s.hgproject.map.ui.poi

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.map.MapViewModel
import com.example.kamonwan_s.hgproject.map.ui.ImageAdapter
import com.example.kamonwan_s.hgproject.poi.api.GetPOI
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.image.ImageActivity
import kotlinx.android.synthetic.main.fragment_poi_detail_dialog.view.*
import java.util.*

class PoiDetailDialogFragment : DialogFragment(), ImageAdapter.OnItemClickListener {

    private var mapViewModel: MapViewModel? = null
    private var token: String? = null
    private var poiID: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            poiID = it.getInt("PoiID")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_poi_detail_dialog, container, false)
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        mapViewModel = ViewModelProviders.of(activity!!, InjectorUtil
                .provideMapViewModelFactory(activity!!.application))[MapViewModel::class.java]
        Log.d("poiIdDetail", poiID.toString())
        token = mapViewModel?.getToken()
        if (poiID != 0) {
            mapViewModel?.loadPoi(GetPOI(token!!, poiID!!))
            mapViewModel?.poi?.observe(this, Observer { event ->
                event?.poiWrapper?.poiData?.poi.let {
                    if (it != null) {
                        if (it.images.size >= 0) {
                            view.recyclerViewPoiImage.adapter = ImageAdapter(it.images, this)
                            view.imageDefault?.visibility = View.GONE
                            view.recyclerViewPoiImage?.visibility = View.VISIBLE
                        } else {
                            view.imageDefault?.visibility = View.VISIBLE
                            view.recyclerViewPoiImage?.visibility = View.GONE
                        }

                        view.tvPoiName.text = it.poiName
                        view.tvPoiContactPerson.text = it.contactPerson
                        view.tvPoiDescription?.text = it.description
                        view.tvPoiLat?.text = it.lat
                        view.tvPoiLng?.text = it.lng
                        view.tvPoiProvinceName?.text = it.provinceName
                        view.tvPoiDistrictName?.text = it.districtName
                        view.tvPoiSubdistrictName?.text = it.subdistrictName
                        view.tvPoiTel?.text = it.tel
                    }
                }
            })

        } else {
            Toast.makeText(context, "poiId is 0", Toast.LENGTH_SHORT).show()
        }


        view.imageClosed?.setOnClickListener {
            dismiss()
        }
    }

    override fun onImageClick(position: Int, imageData: MutableList<String>) {
        startActivity(Intent(context, ImageActivity::class.java).apply {
            putExtra(KEY_IMAGE_DATA, Bundle().apply {
                putStringArrayList(IMAGES, imageData as ArrayList<String>)
                putInt(POSITION_IMAGE, position)
            })
        })
    }

    companion object {
        val tag: String = PoiDetailDialogFragment::class.java.simpleName
        private const val POSITION_IMAGE = "position"
        private const val IMAGES = "image"
        private const val KEY_IMAGE_DATA = "name"
        @JvmStatic
        fun newInstance(poiID: Int) =
                PoiDetailDialogFragment().apply {
                    arguments = Bundle().apply {
                        putInt("PoiID", poiID)
                    }
                }
    }
}
