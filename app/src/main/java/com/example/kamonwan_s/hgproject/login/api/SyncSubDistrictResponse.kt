package com.example.kamonwan_s.hgproject.login.api

import com.google.gson.annotations.SerializedName

data class SyncSubDistrictResponse(@SerializedName("SyncSubDistrictResult")val syncSubDistrictWrapper:MutableList<SyncSubDistrictWrapper>)