package com.example.kamonwan_s.hgproject.communication.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.communication.api.*
import com.example.kamonwan_s.hgproject.communication.api.GetChatRoomPedingListResponse
import com.example.kamonwan_s.hgproject.communication.api.Token
import com.example.kamonwan_s.hgproject.database.AccountContactEntity
import com.example.kamonwan_s.hgproject.database.ChatContentReadedEntity
import com.example.kamonwan_s.hgproject.database.ContentChatListEntity
import com.example.kamonwan_s.hgproject.database.MapDao
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommunicationRepository private constructor(private val executor: AppExecutors,
                                                  private val communicationApi: CommunicationApi,
                                                  private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()
    fun syncAccountContactList(accountContactInfo: AccountContactInfo): LiveData<SingleEvent<SyncAccountContactListResponse>> {
        val accountContactListData = MutableLiveData<SingleEvent<SyncAccountContactListResponse>>()
        communicationApi.syncAccountContactList(accountContactInfo).enqueue(object : Callback<SyncAccountContactListResponse?> {
            override fun onFailure(call: Call<SyncAccountContactListResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncAccountContactListResponse?>, response: Response<SyncAccountContactListResponse?>) {
                if (response.isSuccessful) {
                    accountContactListData.value = SingleEvent(response.body()!!)
                    var dbInfoList = response.body()!!.accountContactListWrapper
                    executor.diskIO.execute {
                        try {
                            var accountContract = dbInfoList.accountContactListData.accountContacts
                            var accountContractList = ArrayList<AccountContactEntity>()
                            accountContract.forEach {
                                val accountContractEntity = AccountContactEntity(
                                        it.accountID.toInt(),
                                        it.email,
                                        it.firstName,
                                        it.lastName,
                                        it.modifiedDate,
                                        it.organizationID,
                                        it.profilePic
                                )


                                accountContractList.add(accountContractEntity)
                            }
                            //  mapDao.insertAccountContactList(accountContractList)
                            if (dbInfoList.accountContactListData.accountContacts.isNotEmpty()) {
                                mapDao.insertAccountContactList(accountContractList)
                            } else {
                                mapDao.loadAllAccountContactLiveData()
                            }

                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return accountContactListData
    }

    fun syncChatRoomList(chatRoomInfo: ChatRoomInfo): LiveData<SingleEvent<SyncChatRoomListResponse>> {
        val chatRoomData = MutableLiveData<SingleEvent<SyncChatRoomListResponse>>()
        communicationApi.syncChatRoomList(chatRoomInfo).enqueue(object : Callback<SyncChatRoomListResponse?> {
            override fun onFailure(call: Call<SyncChatRoomListResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncChatRoomListResponse?>, response: Response<SyncChatRoomListResponse?>) {
                if (response.isSuccessful)
                    chatRoomData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return chatRoomData
    }

    fun getChatRoomPendingList(token: Token): LiveData<SingleEvent<GetChatRoomPedingListResponse>> {
        val chatRoomPendingData = MutableLiveData<SingleEvent<GetChatRoomPedingListResponse>>()
        communicationApi.getChatRoomPendingList(token).enqueue(object : Callback<GetChatRoomPedingListResponse?> {
            override fun onFailure(call: Call<GetChatRoomPedingListResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetChatRoomPedingListResponse?>, response: Response<GetChatRoomPedingListResponse?>) {
                if (response.isSuccessful)
                    chatRoomPendingData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return chatRoomPendingData
    }

    fun updateChatRoomStatus(chatRoomStatusInfo: ChatRoomStatusInfo): LiveData<SingleEvent<UpdateChatRoomStatusResponse>> {
        val chatRoomStatusData = MutableLiveData<SingleEvent<UpdateChatRoomStatusResponse>>()
        communicationApi.updateChatRoomStatus(chatRoomStatusInfo).enqueue(object : Callback<UpdateChatRoomStatusResponse?> {
            override fun onFailure(call: Call<UpdateChatRoomStatusResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<UpdateChatRoomStatusResponse?>, response: Response<UpdateChatRoomStatusResponse?>) {
                if (response.isSuccessful)
                    chatRoomStatusData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return chatRoomStatusData
    }

    fun getChatContentList(chatContentListInfo: ChatContentListInfo): LiveData<SingleEvent<GetChatContentListResponse>> {
        val chatContentListData = MutableLiveData<SingleEvent<GetChatContentListResponse>>()
        communicationApi.getChatContentList(chatContentListInfo).enqueue(object : Callback<GetChatContentListResponse?> {
            override fun onFailure(call: Call<GetChatContentListResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetChatContentListResponse?>, response: Response<GetChatContentListResponse?>) {
                if (response.isSuccessful) {
                    chatContentListData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!.chatContentListWrapper
                    executor.diskIO.execute {
                        try {
                            var chatContent = dbInfoList.chatContentListData.chatContents
                            var chatContentList = ArrayList<ContentChatListEntity>()
                            chatContent.forEach {
                                var chatContentEntity = ContentChatListEntity(
                                        it.chatContentID,
                                        it.chatContentTypeID,
                                        it.chatRoomID,
                                        it.contentDescription,
                                        it.content,
                                        it.firstName,
                                        it.isGroup,
                                        it.lastName,
                                        it.modifiedDate,
                                        it.profilePic,
                                        it.senderID
                                )
                                chatContentList.add(chatContentEntity)
                            }

                            mapDao.insertChatContent(chatContentList)
                            if (dbInfoList.chatContentListData.chatContents.isNotEmpty()) {
                                mapDao.insertChatContent(chatContentList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return chatContentListData
    }

    fun getChatRoomMember(chatRoomMemberOperationInfo: ChatRoomMemberOperationInfo): LiveData<SingleEvent<GetChatRoomMemberResponse>> {
        val chatRoomMemberData = MutableLiveData<SingleEvent<GetChatRoomMemberResponse>>()
        communicationApi.getChatRoomMember(chatRoomMemberOperationInfo).enqueue(object : Callback<GetChatRoomMemberResponse?> {
            override fun onFailure(call: Call<GetChatRoomMemberResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetChatRoomMemberResponse?>, response: Response<GetChatRoomMemberResponse?>) {
                if (response.isSuccessful)
                    chatRoomMemberData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }
        })
        return chatRoomMemberData
    }

    fun getChatRoomP2P(chatRoomPToPInfo: ChatRoomPToPInfo): LiveData<SingleEvent<GetChatRoomPToPResponse>> {
        val chatRoomPToPData = MutableLiveData<SingleEvent<GetChatRoomPToPResponse>>()
        communicationApi.getChatRoomP2P(chatRoomPToPInfo).enqueue(object : Callback<GetChatRoomPToPResponse?> {
            override fun onFailure(call: Call<GetChatRoomPToPResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetChatRoomPToPResponse?>, response: Response<GetChatRoomPToPResponse?>) {
                if (response.isSuccessful)
                    chatRoomPToPData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }
        })
        return chatRoomPToPData
    }

    fun getChatContentRead(chatContentReadedInfo: ChatContentReadedInfo): LiveData<SingleEvent<GetChatContentReadedResponse>> {
        val chatContentReadData = MutableLiveData<SingleEvent<GetChatContentReadedResponse>>()
        communicationApi.getChatContentReaded(chatContentReadedInfo).enqueue(object : Callback<GetChatContentReadedResponse> {
            override fun onFailure(call: Call<GetChatContentReadedResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetChatContentReadedResponse>, response: Response<GetChatContentReadedResponse>) {
                if (response.isSuccessful) {
                    chatContentReadData.value = SingleEvent(response.body()!!)
                    val dbInfoList = response.body()!!
                    executor.diskIO.execute {
                        try {
                            val chatContentRead = dbInfoList.chatContentReadedWrapper.chatContentReaded
                            val chatContentEntity = ChatContentReadedEntity(
                                    chatContentRead.chatContentID,
                                    chatContentRead.chatRoomID,
                                    chatContentRead.contentReaded,
                                    chatContentRead.modifiedDate

                            )
                            mapDao.insertChatContentRead(chatContentEntity)

                            if (dbInfoList.equals("")) {
                                mapDao.deleteAllContentRead()
                                mapDao.insertChatContentRead(chatContentEntity)
                            }else{
                                mapDao.loadAllContentRead()
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }
        })

        return chatContentReadData
    }

    companion object {
        @Volatile
        private var instance: CommunicationRepository? = null
        private val tag = CommunicationRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, communicationApi: CommunicationApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: CommunicationRepository(executor, communicationApi, mapDao).apply {
                                instance = this
                            }
                }
    }
}