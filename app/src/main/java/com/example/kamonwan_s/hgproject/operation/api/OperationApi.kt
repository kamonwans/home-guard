package com.example.kamonwan_s.hgproject.operation.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface OperationApi {

    @POST("SyncOperationList")
    fun getOperationListOwn(@Body operationInfo: OperationInfo): Call<SyncOperationListResponse>

    @POST("UpdateIsOperate")
    fun updateOperationWorking(@Body operationWorkingInfo: OperationWorkingInfo) : Call<UpdateOperationWorkingResponse>

    @POST("GetOperationDetail")
    fun getOperationDetail(@Body operationDetailInfo: OperationDetailInfo) : Call<GetOperationDetailResponse>

    @POST("LoadOperationStatus")
    fun loadOperationStatus(@Body token : Token) : Call<LoadOperationStatusResponse>

    @POST("LoadOperationType")
    fun loadOperationType(@Body token: Token) : Call<LoadOperationTypeResponse>

    @POST("SyncOperationAccountContactList")
    fun syncOperationAccountContactList(@Body accountContactInfo: AccountContactInfo) : Call<SyncAccountContactListResponse>

    @POST("SyncOperationList")
    fun syncOperationList(@Body operationInfo: OperationInfo):Call<SyncOperationListResponse>

    @POST("SyncOperationStatus")
    fun syncOperationStatus(@Body syncOperationInfo: SyncOperationInfo) : Call<SyncOperationStatusResponse>

    @POST("SyncOperationType")
    fun syncOperationType(@Body syncOperationInfo: SyncOperationInfo) :Call<SyncOperationTypeResponse>

    @POST("UpdateLastLocation")
    fun updateLastLocation(@Body updateLocationInfo: UpdateLocationInfo) :Call<UpdateLastLocationResponse>


    companion object {
        private const val BASE_URL = "https://idc2.klickerlab.com/OperationService.svc/"
        private const val TIME_OUT: Long = 20
        val service: OperationApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                    .build()
            Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
                            .create()
                    ))
                    .client(okHttpClient)
                    .build()
                    .create(OperationApi::class.java)
        }
    }
}