package com.example.kamonwan_s.hgproject.clue.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class ReportType(@SerializedName("Icon") val icon:String,
                     @SerializedName("ModifiedDate") val modifiedDate :Long,
                      @SerializedName("ReportTypeID")val reportTypeID:Int,
                     @SerializedName("ReportTypeName") val reportTypeName:String)