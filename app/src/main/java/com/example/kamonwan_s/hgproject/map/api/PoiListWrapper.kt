package com.example.kamonwan_s.hgproject.poi.api

import com.google.gson.annotations.SerializedName

data class PoiListWrapper(@SerializedName("Data") val poiListData : PoiListData,
                          @SerializedName("Response") val responseList: ResponseList)