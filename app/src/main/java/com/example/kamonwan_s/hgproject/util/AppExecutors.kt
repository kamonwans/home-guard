package com.example.kamonwan_s.hgproject.util

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutors private constructor(val diskIO : Executor,val mainThread:Executor){
    companion object {
        @Volatile
        private var instance: AppExecutors? = null

        fun getInstance() = instance ?: synchronized(this){
            instance
                    ?: AppExecutors(Executors.newSingleThreadExecutor(), MainThreadExecutor())
                    .also { instance = it }
        }
    }

    private class MainThreadExecutor : Executor{
        private val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable?) {
            handler.post(command)
        }

    }
}