package com.example.kamonwan_s.hgproject.operation.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.database.*
import com.example.kamonwan_s.hgproject.operation.api.*
import com.example.kamonwan_s.hgproject.util.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OperationRepository private constructor(private val executor: AppExecutors,
                                              private val operationApi: OperationApi,
                                              private val mapDao: MapDao) {

    val syncDatabaseError: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    fun loadOperationOwn(operationInfo: OperationInfo): LiveData<SingleEvent<SyncOperationListResponse>> {
        val operationOwnData = MutableLiveData<SingleEvent<SyncOperationListResponse>>()
        operationApi.getOperationListOwn(operationInfo).enqueue(object : Callback<SyncOperationListResponse?> {
            override fun onFailure(call: Call<SyncOperationListResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncOperationListResponse?>, response: Response<SyncOperationListResponse?>) {
                if (response.isSuccessful) {
                    operationOwnData.value = SingleEvent(response.body()!!)
                    val dbList = response.body()!!.operationListOwnWrapper
                    executor.diskIO.execute {
                        try {
                            val operation = dbList.operationList.operations
                            var operationList = ArrayList<OperationsEntity>()
                            var operationImageList = ArrayList<OperationImageEntity>()

                            operation.forEach {
                                var operationEntity = OperationsEntity(it.chatRoomID,
                                        it.detail,
                                        it.isWorking,
                                        it.modifiedDate,
                                        it.operationID,
                                        it.operationName,
                                        it.operationStatusID,
                                        it.operationTypeID,
                                        it.organizationName,
                                        it.organizationPic,
                                        it.organizationID)

                                operationList.add(operationEntity)

                                it.images?.forEach { image ->
                                    val operationImageEntity = OperationImageEntity(
                                            operationsID = it.operationID,
                                            image = image
                                    )
                                    operationImageList.add(operationImageEntity)
                                }
                            }

                            if (dbList.operationList.accountContacts.isNotEmpty()) {
                                mapDao.insertOperation(operationList)
                                mapDao.insertOperationImage(operationImageList)
                            } else {
                                mapDao.loadAllOperations()
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                } else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return operationOwnData
    }

    fun updateOperationWorking(operationWorkingInfo: OperationWorkingInfo): LiveData<SingleEvent<UpdateOperationWorkingResponse>> {
        val operationUpdate = MutableLiveData<SingleEvent<UpdateOperationWorkingResponse>>()
        operationApi.updateOperationWorking(operationWorkingInfo).enqueue(object : Callback<UpdateOperationWorkingResponse?> {
            override fun onFailure(call: Call<UpdateOperationWorkingResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<UpdateOperationWorkingResponse?>, response: Response<UpdateOperationWorkingResponse?>) {
                if (response.isSuccessful)
                    operationUpdate.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return operationUpdate
    }

    fun updateOperationLastLocation(updateLocationInfo: UpdateLocationInfo): LiveData<SingleEvent<UpdateLastLocationResponse>> {
        val lastLocationUpdateData = MutableLiveData<SingleEvent<UpdateLastLocationResponse>>()
        operationApi.updateLastLocation(updateLocationInfo).enqueue(object : Callback<UpdateLastLocationResponse?> {
            override fun onFailure(call: Call<UpdateLastLocationResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<UpdateLastLocationResponse?>, response: Response<UpdateLastLocationResponse?>) {
                if (response.isSuccessful)
                    lastLocationUpdateData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })
        return lastLocationUpdateData
    }


    fun loadOperationDetail(operationDetailInfo: OperationDetailInfo): LiveData<SingleEvent<GetOperationDetailResponse>> {
        val operationDetailData = MutableLiveData<SingleEvent<GetOperationDetailResponse>>()
        operationApi.getOperationDetail(operationDetailInfo).enqueue(object : Callback<GetOperationDetailResponse?> {
            override fun onFailure(call: Call<GetOperationDetailResponse?>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<GetOperationDetailResponse?>, response: Response<GetOperationDetailResponse?>) {
                if (response.isSuccessful)
                    operationDetailData.value = SingleEvent(response.body()!!)
                else
                    Log.d(tag, response.errorBody()!!.string())
            }

        })

        return operationDetailData
    }

    fun syncOperationAccountContactList(accountContactInfo: AccountContactInfo): LiveData<SingleEvent<SyncAccountContactListResponse>> {
        val operationAccountContactData = MutableLiveData<SingleEvent<SyncAccountContactListResponse>>()
        operationApi.syncOperationAccountContactList(accountContactInfo).enqueue(object : Callback<SyncAccountContactListResponse> {
            override fun onFailure(call: Call<SyncAccountContactListResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<SyncAccountContactListResponse>, response: Response<SyncAccountContactListResponse>) {
                if (response.isSuccessful) {
                    operationAccountContactData.value = SingleEvent(response.body()!!)
                    val dbList = response.body()!!.accountContactListWrapper

                    executor.diskIO.execute {
                        try {

                            val operationAccount = dbList.accountContactListData.accountContacts
                            var operationAccountList = ArrayList<AccountContactEntity>()

                            operationAccount?.forEach {
                                var operationEntity = AccountContactEntity(it.accountID,
                                        it.email,
                                        it.firstName,
                                        it.lastName,
                                        it.modifiedDate,
                                        it.organizationID,
                                        it.profilePic)

                                operationAccountList.add(operationEntity)
                            }
                            if (dbList.accountContactListData.accountContacts.isNotEmpty()) {
                                mapDao.insertAccountContactList(operationAccountList)
                            } else {
                                mapDao.updateAccountContact(operationAccountList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }

            }

        })
        return operationAccountContactData
    }

    fun loadOperationStatus(token: Token): LiveData<SingleEvent<LoadOperationStatusResponse>> {
        val operationStatusData = MutableLiveData<SingleEvent<LoadOperationStatusResponse>>()
        operationApi.loadOperationStatus(token).enqueue(object : Callback<LoadOperationStatusResponse> {
            override fun onFailure(call: Call<LoadOperationStatusResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<LoadOperationStatusResponse>, response: Response<LoadOperationStatusResponse>) {
                if (response.isSuccessful) {
                    operationStatusData.value = SingleEvent(response.body()!!)
                    val dbList = response.body()!!.operationStatusWrapper

                    executor.diskIO.execute {
                        try {
                            var operationStatus = dbList.operationStatusData.operationStatus
                            var operationStatusList = ArrayList<OperationStatusEntity>()

                            operationStatus?.forEach {
                                val operationStatusEntity = OperationStatusEntity(it.color,
                                        it.modifiedDate,
                                        it.operationStatusID,
                                        it.operationStatusName)
                                operationStatusList.add(operationStatusEntity)
                            }

                            if (dbList.operationStatusData.operationStatus.isNotEmpty()) {
                                mapDao.insertOperationStatus(operationStatusList)
                            } else {
                                mapDao.updateOperationStatus(operationStatusList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }
            }

        })
        return operationStatusData
    }


    fun loadOperationType(token: Token): LiveData<SingleEvent<LoadOperationTypeResponse>> {
        val operationTypeData = MutableLiveData<SingleEvent<LoadOperationTypeResponse>>()
        operationApi.loadOperationType(token).enqueue(object : Callback<LoadOperationTypeResponse> {
            override fun onFailure(call: Call<LoadOperationTypeResponse>, t: Throwable) {
                Log.d(tag, t.message.toString())
            }

            override fun onResponse(call: Call<LoadOperationTypeResponse>, response: Response<LoadOperationTypeResponse>) {
                if (response.isSuccessful) {
                    operationTypeData.value = SingleEvent(response.body()!!)
                    val dbList = response.body()!!.operationTypeWrapper

                    executor.diskIO.execute {
                        try {

                            var opertaionType = dbList.operationTypeData.operationType
                            var operationTypeList = ArrayList<OperationTypeEntity>()

                            opertaionType?.forEach {
                                var operationTypeEntity = OperationTypeEntity(it.color,
                                        it.modifiedDate,
                                        it.operationTypeID,
                                        it.operationTypeName)
                                operationTypeList.add(operationTypeEntity)
                            }
                            if (dbList.operationTypeData.operationType.isNotEmpty()) {
                                mapDao.insertOperationType(operationTypeList)
                            } else {
                                mapDao.updateOperationType(operationTypeList)
                            }
                        } catch (e: SQLiteConstraintException) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                        } catch (e: Exception) {
                            syncDatabaseError.postValue(SingleEvent(e.toString()))
                            Log.d(tag, e.toString())
                        }
                    }
                }
            }

        })
        return operationTypeData
    }

    fun insertOperation(operation: List<Operations>) {
//        executor.diskIO.execute {
//            try {
//                mapDao.insertOperation(operation)
//            }catch (e: SQLiteConstraintException) {
//                syncDatabaseError.postValue(SingleEvent(e.toString()))
//            } catch (e: Exception) {
//                syncDatabaseError.postValue(SingleEvent(e.toString()))
//                Log.d(tag, e.toString())
//            }
//        }
    }


    companion object {
        @Volatile
        private var instance: OperationRepository? = null
        private val tag = OperationRepository::class.java.simpleName
        fun getInstance(executor: AppExecutors, operationApi: OperationApi, mapDao: MapDao) = instance
                ?: synchronized(this) {
                    instance
                            ?: OperationRepository(executor, operationApi, mapDao).apply {
                                instance = this
                            }
                }
    }
}