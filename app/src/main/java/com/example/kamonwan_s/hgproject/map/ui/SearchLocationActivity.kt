package com.example.kamonwan_s.hgproject.map.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import com.example.kamonwan_s.hgproject.OK
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.database.PoiEntity
import kotlinx.android.synthetic.main.activity_search_location.*

class SearchLocationActivity : AppCompatActivity(), TextWatcher {

    private lateinit var searchAdapter: SearchAdapter
    private var searchLocationList = ArrayList<PoiEntity>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)

        setUpView()
    }

    fun setUpView() {
        edtSearch.addTextChangedListener(this)
        backButton.setOnClickListener {
            finish()
        }

        searchAdapter = SearchAdapter(onClickPoiItem())

        recyclerview.apply {
            layoutManager = LinearLayoutManager(this@SearchLocationActivity)
            adapter = searchAdapter
        }

        searchAdapter.updatePoiList(poiList)
    }

    private fun onClickPoiItem(): (PoiEntity) -> Unit {
        return { poi ->
            val bundle = Bundle().apply {
                putParcelable(POI_ITEM, poi)
            }

            setResult(OK, Intent().apply {
                putExtras(bundle)
            })
            finish()
        }
    }

    override fun afterTextChanged(s: Editable?) {
        searchLocationList = poiList.filter {
            it.poiName.startsWith(s.toString(), true)
        } as ArrayList<PoiEntity>

        searchAdapter.updatePoiList(searchLocationList)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    private val poiList: ArrayList<PoiEntity> get() = intent.getParcelableArrayListExtra("poiList")

    companion object {
        const val requestCode = 3
        const val POI_ITEM = "poi"
        const val POI_LIST = "poiList"
    }
}
