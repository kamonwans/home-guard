package com.example.kamonwan_s.hgproject.communication.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import com.example.kamonwan_s.hgproject.OK
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.communication.api.AccountContactInfo
import com.example.kamonwan_s.hgproject.communication.ui.SearchAccountContactActivity.Companion.ACCOUNT_CONTACT_ITEM
import com.example.kamonwan_s.hgproject.communication.ui.SearchAccountContactActivity.Companion.ACCOUNT_CONTACT_LIST
import com.example.kamonwan_s.hgproject.communication.ui.SearchAccountContactActivity.Companion.requestCode
import com.example.kamonwan_s.hgproject.database.AccountContactEntity
import com.example.kamonwan_s.hgproject.database.AppDatabase
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_friends.*
import kotlinx.android.synthetic.main.fragment_friends.view.*

class FriendsFragment : Fragment(), FriendsAdapter.BtnClickListener {

    private var communicationViewModel: CommunicationViewModel? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: FriendsAdapter? = null
    private var token: String? = null
    private var organizationId: String? = null
    private var accountContactList: MutableList<AccountContactEntity>? = null
    private var appDatabase: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("รายชื่อเพื่อน")
        appDatabase = AppDatabase.getAppDatabase(context!!)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_friends, container, false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_dot)
        setHasOptionsMenu(true)

        communicationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideCommunicationViewModelFactory(activity!!.application))[CommunicationViewModel::class.java]
        initInstance(view)

        accountContactList = appDatabase!!.mapDao().loadAllAccountContactLiveData()

        return view
    }

    private fun initInstance(view: View) {
        token = communicationViewModel?.getToken()
        organizationId = communicationViewModel?.getOrganizationId()
        Log.d("organizationId", organizationId)

        adapter = FriendsAdapter(context!!, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewFriendsChat?.layoutManager = layoutManager
        view.recyclerViewFriendsChat?.itemAnimator = DefaultItemAnimator()
        view.recyclerViewFriendsChat?.adapter = adapter

        communicationViewModel?.syncAccountContact(AccountContactInfo(token!!, organizationId!!.toInt(), "0"))
        communicationViewModel?.accoutContactList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { accountContactWrapper ->
                accountContactWrapper.accountContactListWrapper.accountContactListData.let { acountContactList ->
                    acountContactList.accountContacts.let { accountContact ->
                        adapter?.items?.accountContacts?.addAll(accountContact)
                    }

                    if (adapter?.items == null) {
                        adapter?.items = accountContactWrapper.accountContactListWrapper.accountContactListData
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.searchAccountContact -> {
                val intent = Intent(context, SearchAccountContactActivity::class.java).apply {
                    putParcelableArrayListExtra(ACCOUNT_CONTACT_LIST, ArrayList(accountContactList))
                }
                startActivityForResult(intent, requestCode)
                true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.menu_search_account_contact, menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == OK) {
            if (requestCode == SearchAccountContactActivity.requestCode) {
                val accountContactItem = data?.extras?.getParcelable<AccountContactEntity>(ACCOUNT_CONTACT_ITEM)

                if (accountContactItem != null) {
                    val accountId = accountContactItem.accountID
                    PeopleCommunicationDialogFragment.newInstance(accountId).show(childFragmentManager, PeopleCommunicationDialogFragment.tag)
                }
            }
        }

    }

    override fun onBtnClick(accountId: String) {
        PeopleCommunicationDialogFragment.newInstance(accountId.toInt()).show(childFragmentManager, PeopleCommunicationDialogFragment.tag)

    }


    companion object {
        val tag = FriendsFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                FriendsFragment().apply {

                }
    }
}
