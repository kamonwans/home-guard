package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomListData(@SerializedName("ChatRooms") val chatRooms:MutableList<ChatRooms>)