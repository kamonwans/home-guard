package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomMemberData(@SerializedName("ChatRoomMembers")val chatRoomMembers:MutableList<ChatRoomMembers>)