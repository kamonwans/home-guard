package com.example.kamonwan_s.hgproject.news.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.SingleEvent
import com.example.kamonwan_s.hgproject.news.api.*
import com.example.kamonwan_s.hgproject.news.repository.NewsRepository

class NewsViewModel(private val app:Application,private val newsRepository: NewsRepository)
    :AndroidViewModel(app){

    var getNewsListInfo = MediatorLiveData<GetNewsListInfo>()
    var getNewsInfo = MediatorLiveData<GetNewsInfo>()
    var token = MediatorLiveData<Token>()

     val newsList : LiveData<SingleEvent<GetNewsListResponse>> = Transformations.switchMap(getNewsListInfo){
        newsRepository.loadNewsList(it)
    }

    val news : LiveData<SingleEvent<GetNewsResponse>> = Transformations.switchMap(getNewsInfo){
        newsRepository.loadNews(it)
    }

    val newsType : LiveData<SingleEvent<LoadNewsTypeResponse>> = Transformations.switchMap(token){
        newsRepository.loadNewsType(it)
    }

    val newsAfter : LiveData<SingleEvent<GetNewsListAfterResponse>> = Transformations.switchMap(getNewsListInfo){
        newsRepository.loadNewsAfter(it)
    }

    fun loadNewsList(getNewsListInfo: GetNewsListInfo){
        this.getNewsListInfo.value = getNewsListInfo
    }

    fun loadNews(getNewsInfo: GetNewsInfo){
        this.getNewsInfo.value = getNewsInfo
    }

    fun loadNewsType(token: Token){
        this.token.value = token
    }

    fun loadNewsAfter(getNewsListInfo: GetNewsListInfo){
        this.getNewsListInfo.value = getNewsListInfo
    }
    fun retrieveToken(app: Application): String {
        val tokenData = PreferenceManager.getDefaultSharedPreferences(app)
        return tokenData.getString(app.getString(R.string.pref_token), "")!!
    }

    fun insertNewsType(newsType: List<NewsType>){
        newsRepository.insertNewsType(newsType)
    }

    fun getToken(): String {
        return retrieveToken(app)
    }
}
