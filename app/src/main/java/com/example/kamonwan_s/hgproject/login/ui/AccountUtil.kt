package com.example.kamonwan_s.hgproject.login.ui

import android.app.Application
import android.preference.PreferenceManager
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.login.api.Account
import com.example.kamonwan_s.hgproject.login.api.AccountList
import com.example.kamonwan_s.hgproject.login.api.DataLoginUser

object AccountUtil {
    const val DEFAULT__TOKEN = ""
    fun retrieveAccountID(app: Application): String {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        val token: String = data.getString(app.getString(R.string.pref_token), "-1")!!
        return token
    }

    fun retrieveToken(app:Application):String{
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        val token = data.getString(app.getString(R.string.pref_token), "")!!
        return token
    }

    fun saveAccountID(app: Application, id: Int) {
        val userData = PreferenceManager.getDefaultSharedPreferences(app)
        val editor = userData.edit()
        editor.run {
            putString("account_id", id.toString())
        }
        editor.apply()
    }

    fun saveOrganizationID(app: Application, accountList: Account) {
        val userData = PreferenceManager.getDefaultSharedPreferences(app)
        val editor = userData.edit()
        editor.run {
            putString("organization_id", accountList.OrganizationID.toString())
        }
        editor.apply()
    }

    fun saveAccount(app: Application, accountList: Account) {
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        val editor = data.edit()
        editor.run {
            putString("first_name", accountList.firstName)
            putString("last_name", accountList.lastName)
            putString("profile_pic_link", accountList.profilePic)
        }
        editor.apply()
    }

    fun saveToken(app: Application,token: String){
        val data = PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
        val editor = data.edit()
        editor.run {
            putString("token",token)
        }
        editor.apply()
    }
}