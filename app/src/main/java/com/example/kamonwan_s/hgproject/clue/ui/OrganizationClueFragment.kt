package com.example.kamonwan_s.hgproject.clue.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.clue.api.*
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_organization_clue.*
import kotlinx.android.synthetic.main.fragment_organization_clue.view.*


class OrganizationClueFragment : Fragment(), ClueAdapter.BtnClickListener, ClueAdapter.ClickLocation, ClueAdapter.ClickFavorite {

    private var adapterClueOr: ClueAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var clueViewModel: ClueViewModel? = null
    private var token: String? = null
    private var offset: Int = 0
    private var limit: Int = 4
    private var organizationItemSize = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_organization_clue, container, false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        setHasOptionsMenu(true)
        clueViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideClueReportViewModelFactory(activity!!.application))[ClueViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = clueViewModel?.getToken()
        adapterClueOr = ClueAdapter(context!!, this, this, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewReportOrganization.layoutManager = layoutManager
        view.recyclerViewReportOrganization.itemAnimator = DefaultItemAnimator()
        view.recyclerViewReportOrganization.adapter = adapterClueOr

        //load organization
        clueViewModel?.loadOrganizationReport(ReportOrganizationInfo(token!!, offset, limit, "0", 1))
        clueViewModel?.reportOrganization?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { organizationWrapper ->
                organizationWrapper.reportListOrganizationWrapper.reportListData.let {
                    if (it != null) {
                        it.reportList.let {
                            adapterClueOr?.items?.reportList?.addAll(it)
                        }
                    }

                    if (adapterClueOr?.items == null) {
                        adapterClueOr?.items = organizationWrapper.reportListOrganizationWrapper.reportListData
                    }
                    adapterClueOr?.notifyDataSetChanged()

                    hideProgressbarLoading()

                    if (offset <= organizationItemSize) {
                        offset = organizationItemSize
                        organizationItemSize = offset + limit
                    }
                }
            }
        })

        clueViewModel?.loadStatusReport(SyncReportInfo(token!!, "1544893200000"))
        clueViewModel?.reportStatus?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportStatus ->
                reportStatus.reportStatusWrapper.reportStatusData.let { statusData ->
                    statusData.reportStatuses.let { status ->
                        adapterClueOr?.itemStatus?.reportStatuses?.addAll(status)
                    }
                    if (adapterClueOr?.itemStatus == null) {
                        adapterClueOr?.itemStatus = reportStatus.reportStatusWrapper.reportStatusData
                    }
                    adapterClueOr?.notifyDataSetChanged()
                }
            }
        })

        clueViewModel?.loadReportType(TokenInfo(token!!))
        clueViewModel?.reportClueType?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { reportType ->
                reportType.reportTypeData.reportTypeWrapper.reportTypeList.let { typeData ->
                    if (typeData != null) {
                        adapterClueOr?.itemType?.reportTypeList?.addAll(typeData)
                    }

                    if (adapterClueOr?.itemType == null) {
                        adapterClueOr?.itemType = reportType.reportTypeData.reportTypeWrapper
                    }
                    adapterClueOr?.notifyDataSetChanged()
                }
            }
        })

        view.recyclerViewReportOrganization.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    val currentSizeItem = adapterClueOr?.itemCount ?: 0
                    if (offset <= currentSizeItem) {
                        displayProgressbarLoading()

                        clueViewModel?.loadOrganizationAfter(ReportOrganizationInfo(token!!, offset, limit, "0", 1))
                        clueViewModel?.organizationAfter?.observe(this@OrganizationClueFragment, Observer { event ->
                            event?.getContentIfNotHandled()?.let { organizationAfterList ->
                                organizationAfterList.reportListOrganizationAfterWrapper.reportListData.let {
                                    if (it != null) {
                                        it.reportList.let {
                                            adapterClueOr?.items?.reportList?.addAll(it)
                                        }
                                    }

                                    if (adapterClueOr?.items == null) {
                                        adapterClueOr?.items = organizationAfterList.reportListOrganizationAfterWrapper.reportListData
                                    }
                                }
                            }
                        })
                    }
                }
            }
        })

        view.swipeRefreshLayoutOrganizationClue.setOnRefreshListener {
            organizationItemSize = adapterClueOr?.items?.reportList?.size ?: 0

            if (offset <= organizationItemSize) {
                offset = organizationItemSize
            }

            view.swipeRefreshLayoutOrganizationClue.isRefreshing = false
            clueViewModel?.loadOwnReport(ClueOwn(token!!, offset, limit, "0"))
        }
    }

    override fun onBtnClick(position: Int, title: String, date: String, status: String, picProfile: String, lat: String, lng: String, favorite: Int, reportId: Int,
                            commentNum: Int, name: String, reportTypeId: Int) {
        FragmentDetailReport(position, title, date, status, picProfile, lat, lng, favorite, reportId, commentNum, name, reportTypeId)

    }

    override fun onLocation(lat: String, lon: String, iconReport: String, title: String) {
        Toast.makeText(context, "Map", Toast.LENGTH_SHORT).show()
    }

    override fun onFavorite(position: Int, reportId: Int, isFavorite: Int) {
        clueViewModel?.updateFavorite(UpdateFavoriteInfo(token!!, reportId, isFavorite))
        clueViewModel?.favorite?.observe(this, Observer { event ->
            event?.getContentIfNotHandled().let { updateReportFavoriteResponse ->
                updateReportFavoriteResponse?.reportList.let {

                }
            }
        })
    }

    fun displayProgressbarLoading() {
        view?.progressbarOrganizationClue?.visibility = View.VISIBLE
    }

    fun hideProgressbarLoading() {
        view?.progressbarOrganizationClue?.visibility = View.GONE
    }


    fun FragmentDetailReport(position: Int, title: String, date: String, status: String?,
                             picProfile: String, lat: String, lng: String, favorite: Int,
                             reportId: Int, commentNum: Int, name: String, reportTypeId: Int) {

        val intent = Intent(context,DetailReportClueActivity::class.java)
        intent.putExtra("position", position)
        intent.putExtra("title", title)
        intent.putExtra("dateModified", date)
        intent.putExtra("status", status)
        intent.putExtra("picProfile", picProfile)
        intent.putExtra("lat", lat)
        intent.putExtra("long", lng)
        intent.putExtra("favorite", favorite)
        intent.putExtra("reportId", reportId)
        intent.putExtra("commentNum", commentNum)
        intent.putExtra("name", name)
        intent.putExtra("reportTypeId",reportTypeId)
        startActivity(intent)

    }

    companion object {

        @JvmStatic
        fun newInstance() =
                OrganizationClueFragment().apply {


                }
    }
}
