package com.example.kamonwan_s.hgproject.clue.api

import com.google.gson.annotations.SerializedName

data class CommentInfo(@SerializedName("Token") val token :String,
                       @SerializedName("ReportID") val reportID:Int,
                       @SerializedName("Offset") val offset:Int,
                       @SerializedName("Limit") val limit :Int)