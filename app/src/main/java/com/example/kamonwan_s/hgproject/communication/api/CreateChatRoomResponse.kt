package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class CreateChatRoomResponse(@SerializedName("CreateChatRoomResult") val createChatRoomWrapper: CreateChatRoomWrapper)