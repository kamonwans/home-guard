package com.example.kamonwan_s.hgproject.poi.api

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName


data class POICategory(@SerializedName("ModifiedDate") val modifiedDate: String,
                       @SerializedName("POICategoryID") val poiCategoryID: String,
                       @SerializedName("ShortName") val shortName: String) : Parcelable {


    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(modifiedDate)
        dest?.writeString(poiCategoryID)
        dest?.writeString(shortName)
        dest?.writeByte(if (checkedItem) 1 else 0)
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var checkedItem: Boolean = false

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
        checkedItem = parcel.readByte() != 0.toByte()
    }

    companion object CREATOR : Parcelable.Creator<POICategory> {
        override fun createFromParcel(parcel: Parcel): POICategory {
            return POICategory(parcel)
        }

        override fun newArray(size: Int): Array<POICategory?> {
            return arrayOfNulls(size)
        }
    }
}