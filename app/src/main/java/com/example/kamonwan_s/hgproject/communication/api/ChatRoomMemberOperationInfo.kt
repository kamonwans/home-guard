package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class ChatRoomMemberOperationInfo(@SerializedName("Token")val token : String,
                                       @SerializedName("ChatRoomID")val chatRoomID:String)