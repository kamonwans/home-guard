package com.example.kamonwan_s.hgproject.clue.ui

import android.content.Context
import android.net.Uri
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.example.kamonwan_s.hgproject.R
import kotlinx.android.synthetic.main.item_photo.view.*

class ImagePagerAdaper : PagerAdapter {

    var context: Context
    lateinit var infator: LayoutInflater
    var images: ArrayList<Uri>? = null
    var imgSlider: ImageView? = null

    constructor(context: Context,  images: ArrayList<Uri>) : super() {
        this.context = context
        this.images = images
    }


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        if (images != null) {
            return images!!.size
        }
        return 0
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        infator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = infator.inflate(R.layout.item_photo,container,false)
        Glide.with(context).load(images!!.get(position)).into(view.imgSlider)
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

}
