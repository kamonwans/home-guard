package com.example.kamonwan_s.hgproject.communication.api

import com.google.gson.annotations.SerializedName

data class AccountContactListWrapper(@SerializedName("Data") val accountContactListData : AccountContactListData,
                                     @SerializedName("Response")val responseList: ResponseList)