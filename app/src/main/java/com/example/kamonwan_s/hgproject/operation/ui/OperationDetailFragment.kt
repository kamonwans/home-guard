package com.example.kamonwan_s.hgproject.operation.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.format.DateFormat
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.operation.api.OperationDetailInfo
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import com.klickerlab.tcsd.ui.image.ImageActivity
import kotlinx.android.synthetic.main.fragment_operation_detail.*
import kotlinx.android.synthetic.main.fragment_operation_detail.view.*

class OperationDetailFragment : Fragment(), PhotoOperationAdapter.ViewPagerClickListener {
    private var operationViewModel: OperationViewModel? = null
    private var token: String? = null
    private var content: String? = null
    private var date: String? = null
    private var operationId: Int? = null
    private var operationName: String? = null
    private var position: Int? = null
    private var title: String? = null
    private var adapter: PhotoOperationAdapter? = null
    private var timeDate: String? = null
    private var working: Int? = null

    override fun onClickViewPager(position: Int, images: ArrayList<String>) {
        FragmentImageNews(position, images)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_operation_detail, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_left)

        setHasOptionsMenu(true)
        operationViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideOperationViewModelFactory(activity!!.application))[OperationViewModel::class.java]

        position = arguments?.getInt("position") ?: 0
        content = arguments?.getString("content") ?: ""
        title = arguments?.getString("header") ?: ""
        operationId = arguments?.getInt("operationId") ?: 0
        operationName = arguments?.getString("operationName") ?: ""
        date = arguments?.getString("date") ?: ""
        working = arguments?.getInt("working") ?: 0

        timeDate = convertDate(date!!, "dd/MM/yyyy hh:mm:ss")
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitleOperation.text = title
        tvDateModifiedOperation.text = timeDate
        tvTitleOperation.text = operationName

        token = operationViewModel?.getToken()

        operationViewModel?.loadOperationDetail(OperationDetailInfo(token!!, 1))
        operationViewModel?.operationDetailData?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { operationWrapper ->
                operationWrapper.operationDetailWrapper.operationDetailData.let {
                    if (it != null) {
                        adapter = it.let { it1 -> PhotoOperationAdapter(context!!, this, it1.operationDetail.images) }
                        view.pagerPhoto?.adapter = adapter
                        view.indicator.setViewPager(pagerPhoto)

                        if (it == null) {
                            Toast.makeText(context, "เกิดข้อผิดพลาดบางอย่าง", Toast.LENGTH_LONG).show()
                        } else {
                            tvContentOperation.text = it.operationDetail.detail
                            imageOperationDetailUser.setProfile(it.operationDetail.organizationPic)
                        }
                    }
                }

            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            activity?.onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }

    }

    fun convertDate(dateInMilliseconds: String, dateFormat: String): String {
        return DateFormat.format(dateFormat, java.lang.Long.parseLong(dateInMilliseconds)).toString()
    }


    fun ImageView.setProfile(url: String) {
        Glide.with(context).load(url).apply(RequestOptions.placeholderOf(R.drawable.icon_user_profile).error(R.drawable.icon_user_profile)).into(this)
    }

    fun FragmentImageNews(position: Int, images: ArrayList<String>) {
        val intent = Intent(context, ImageActivity::class.java)
        val bundle = Bundle()
        intent.putExtra("name", bundle)
        bundle.putStringArrayList("image", images)
        bundle.putInt("position", position)
        startActivity(intent)
    }


    companion object {
        val tag = OperationDetailFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                OperationDetailFragment().apply {

                }
    }
}
