package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class  NewsTypeData(@SerializedName("NewsType") val newsType : MutableList<NewsType>)