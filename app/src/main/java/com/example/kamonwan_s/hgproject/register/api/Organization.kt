package com.example.kamonwan_s.hgproject.register.api

import com.example.kamonwan_s.hgproject.Checker
import com.google.gson.annotations.SerializedName

data class Organization(@SerializedName("OrganizationID") val organizationID: Int
                        , @SerializedName("Name") val name: String
                        , @SerializedName("ModifiedDate") val modifiedDate: Long
                        , override var checked: Boolean = false) : Checker