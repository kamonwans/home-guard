package com.example.kamonwan_s.hgproject.clue.api

import com.example.kamonwan_s.hgproject.poi.api.ResponseList
import com.google.gson.annotations.SerializedName

data class SyncReportTypeWrapper(@SerializedName("Data") val syncReportTypeData: SyncReportTypeData,
                                 @SerializedName("Response") val responseList: ResponseList)