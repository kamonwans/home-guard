package com.example.kamonwan_s.hgproject.news.api

import com.google.gson.annotations.SerializedName

data class GetNewsListInfo(@SerializedName("Token") val token:String,
                           @SerializedName("Offset") val offset:String,
                           @SerializedName("Limit") val limit :String,
                           @SerializedName("DateTimeStr") val dateTimeStr :String)