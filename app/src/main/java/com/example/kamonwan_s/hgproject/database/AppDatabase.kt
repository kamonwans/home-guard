package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context


@Database(entities = [PoiTypeEntity::class,
    PoiEntity::class, PoiCategoryEntity::class,
    ProvinceEntity::class, DistrictEntity::class,
    SubDistrictEntity::class, OrganizationEntity::class,
    OperationTypeEntity::class, OperationStatusEntity::class,
    AccountEntity::class, AccountContactEntity::class,
    NewsTypeEntity::class, ReportTypeEntity::class,
    ReportStatusesEntity::class, OperationsEntity::class,
    ReportOwnListEntity::class,ReportFavoriteListEntity::class,
    ReportOrganizationListEntity::class, ReportImageEntity::class,
    OperationPOIsEntity::class,OperationMembersEntity::class,
    NewsListEntity::class,NewsImageEntity::class,
    OperationImageEntity::class,ContentChatListEntity::class,
    ChatContentReadedEntity::class,StationsEntity::class], version = 1, exportSchema = false)

@TypeConverters(value = [OperationPoisConverter::class, OperationMemberConverter::class, OperationImageConverter::class])

abstract class AppDatabase : RoomDatabase() {
    abstract fun mapDao(): MapDao

    companion object {
        private const val NAME = "hg_app.db"
        @Volatile
        private var instance: AppDatabase? = null

        fun getAppDatabase(context: Context) = instance ?: synchronized(this) {
            instance
                    ?: Room.databaseBuilder(context, AppDatabase::class.java, NAME)
                            .allowMainThreadQueries()
                            .build()
                            .also { instance = it }

        }

    }
}