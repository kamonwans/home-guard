package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.TypeConverter
import com.example.kamonwan_s.hgproject.operation.api.OperationPOIs
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType

class OperationPoisConverter : BaseListConverters<OperationPOIs>() {
    @TypeConverter
    override fun fromString(value: String): ArrayList<OperationPOIs>? {
        val typeToken = object : TypeToken<ArrayList<OperationPOIs>>() {}
        val type = typeToken.type as ParameterizedType
        val list = GsonBuilder().create().fromJson<ArrayList<OperationPOIs>>(value, type)
        return list ?: ArrayList()
    }


}