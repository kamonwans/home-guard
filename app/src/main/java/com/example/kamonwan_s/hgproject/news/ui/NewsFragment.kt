package com.example.kamonwan_s.hgproject.news.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.kamonwan_s.hgproject.R
import com.example.kamonwan_s.hgproject.news.api.*
import com.example.kamonwan_s.hgproject.util.InjectorUtil
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.fragment_news.view.*

class NewsFragment : Fragment(), NewsAdapter.BtnClickListener {

    private var newsViewModel: NewsViewModel? = null
    private var adapter: NewsAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var token: String? = null
    private var offset: Int = 0
    private var limit: Int = 4
    private var sizeItem = 0
    private var isLoading = false

    override fun onBtnClick(position: Int, header: String, date: String, typeId: Int, content: String, newsId: Int, newsTypeName: String) {
        FragmentDetailNews(position, header, date, typeId, content, newsId, newsTypeName)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getActivity()!!.setTitle("ข่าวสาร")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_news, container, false)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)

        setHasOptionsMenu(true)

        newsViewModel = ViewModelProviders.of(this,
                InjectorUtil.provideNewsViewModelFactory(activity!!.application))[NewsViewModel::class.java]
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        token = newsViewModel?.getToken()

        adapter = NewsAdapter(context!!, this)
        layoutManager = LinearLayoutManager(view.context)
        view.recyclerViewNews?.layoutManager = layoutManager
        view.recyclerViewNews?.itemAnimator = DefaultItemAnimator()
        view.recyclerViewNews?.adapter = adapter


        newsViewModel?.loadNewsList(GetNewsListInfo(token!!, offset.toString(), limit.toString(), "0"))
        newsViewModel?.newsList?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { newsWrapper ->
                newsWrapper.newsListWrapper.newsList.let { newsList ->
                    newsList.newss.let { news ->
                        adapter?.items?.newss?.addAll(news)
                    }
                    if (adapter?.items == null) {
                        adapter?.items = newsWrapper.newsListWrapper.newsList
                    }
                    adapter?.notifyDataSetChanged()
                    hideProgressbarLoading()
                    if (offset <= sizeItem) {
                        offset = sizeItem
                        sizeItem = offset + limit
                    }

                }
            }
        })

        newsViewModel?.loadNewsType(Token(token!!))
        newsViewModel?.newsType?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { newsTypeWrapper ->
                newsTypeWrapper.newsTypeWrapper.newsTypeData.let { newsTypeData ->
                    newsTypeData.newsType.let { newsType ->
                        adapter?.itemsType?.newsType?.addAll(newsType)
                        newsViewModel?.insertNewsType(newsType)
                    }
                    if (adapter?.itemsType == null) {
                        adapter?.itemsType = newsTypeWrapper.newsTypeWrapper.newsTypeData
                    }
                    adapter?.notifyDataSetChanged()
                }
            }
        })

        view.recyclerViewNews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    val currentSizeItem = adapter?.itemCount ?: 0
                    if (offset <= currentSizeItem) {
                        displayProgressbarLoading()
                        loadNewsAfterList()
                        //newsViewModel?.loadNewsAfter(GetNewsListInfo(token!!, offset.toString(), limit.toString(),"0"))
                        hideProgressbarLoading()
                    }else{
                        Toast.makeText(context,"ไม่มีข้อมูลแล้ว",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })

        view.swipeRefreshLayoutNews.setOnRefreshListener {
            sizeItem = adapter?.items?.newss?.size ?: 0
            view.swipeRefreshLayoutNews.isRefreshing = false
            newsViewModel?.loadNewsList(GetNewsListInfo(token!!, offset.toString(), limit.toString(), "0"))
        }
    }


    fun loadNewsAfterList() {
        newsViewModel?.loadNewsAfter(GetNewsListInfo(token!!, offset.toString(), limit.toString(), "0"))
        newsViewModel?.newsAfter?.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { newsAfterWrapper ->
                newsAfterWrapper.newsListAfterWrapper.newsAfterList.let { newsList ->
                    newsList.newss.let {
                        adapter?.itemAfter?.newss?.addAll(it)
                    }

                    if (adapter?.itemAfter == null) {
                        adapter?.itemAfter = newsAfterWrapper.newsListAfterWrapper.newsAfterList
                    }
                    adapter?.notifyDataSetChanged()
                }

            }
        })
    }

    fun displayProgressbarLoading() {
        view?.progressbarNews?.visibility = View.VISIBLE
    }

    fun hideProgressbarLoading() {
        view?.progressbarNews?.visibility = View.GONE
    }

    fun FragmentDetailNews(position: Int, header: String, date: String, typeId: Int, content: String, newsId: Int?, newsTypeName: String) {
        val fragment = NewsDetailFragment()
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.contentContainer, fragment)
        val bundle = Bundle()
        bundle.putInt("position", position)
        bundle.putString("header", header)
        bundle.putString("date", date)
        bundle.putInt("typeId", typeId)
        bundle.putString("content", content)
        bundle.putInt("newsId", newsId!!)
        bundle.putString("newsTypeName", newsTypeName)
        transaction?.addToBackStack(null)
        fragment.arguments = bundle
        transaction?.commit()
    }

    companion object {
        val tag: String = NewsFragment::class.java.simpleName
        @JvmStatic
        fun newInstance() =
                NewsFragment().apply {

                }
    }
}
