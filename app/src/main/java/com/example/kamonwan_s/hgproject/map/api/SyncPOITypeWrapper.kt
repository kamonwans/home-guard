package com.example.kamonwan_s.hgproject.poi.api

import com.example.kamonwan_s.hgproject.clue.api.ReportList
import com.google.gson.annotations.SerializedName

data class SyncPOITypeWrapper(@SerializedName("Data") val poiTypeData: PoiTypeData,
                              @SerializedName("Response") val responseList: ResponseList)