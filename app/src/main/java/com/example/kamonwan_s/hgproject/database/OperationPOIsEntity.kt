package com.example.kamonwan_s.hgproject.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

@Entity(tableName = "operation_pois")
data class OperationPOIsEntity(@ColumnInfo(name = "operation_pois_action_detail") val actionDetail: String,
                               @PrimaryKey @ColumnInfo(name = "operation_pois_poi_id") val poiID: Int,
                               @ColumnInfo(name = "operations_id") val operationID: Int)